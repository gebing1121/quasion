#if !defined(AFX_NVBACKUPOPTLIST_H__1E401A66_75FF_404F_8D83_EEE15CA974A3__INCLUDED_)
#define AFX_NVBACKUPOPTLIST_H__1E401A66_75FF_404F_8D83_EEE15CA974A3__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// NvBackupOptList.h : header file
//
#include "SuperGridCtrl.h"
#include "BMAGlobal.h"
/////////////////////////////////////////////////////////////////////////////
// CNvBackupOptList window

class CNvBackupOptList : public CSuperGridCtrl
{
// Construction
public:
	CNvBackupOptList();
	void Init();
	void InitNvBackupInfo(PNV_BACKUP_ITEM_T pNvBkpItmInfo, int nCount);

// Attributes
public:
	PNV_BACKUP_ITEM_T   m_pNvBkpItmArray;
	int                 m_nNvBkpItmCount;
	PNV_BACKUP_ITEM_T   m_pTempNvBkpItmArray;
	int                 m_nTempNvBkpItmCount;
	BOOL                m_bBackNV;
	BOOL                m_bTempBackNV;
	
	int                 m_nNvBkpItmChkCount;
	int                 m_nTempNvBkpItmChkCount;
	CString             m_strNotChkItemName;
	CString             m_strTmpNotChkItemName;

// Operations
public:
    void AddNvBackupItemInfo(PNV_BACKUP_ITEM_T pNvBkpItmInfo);
	void AddNvBackupItemInfo(PNV_BACKUP_ITEM_T pNvBkpItmInfo, int nCount);
	BOOL SaveSettings(LPCTSTR lpszIniFile);
	void Update();
	void UpdateTemp();
	void FillList();
	
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CNvBackupOptList)
	//}}AFX_VIRTUAL
	int GetIcon(const CTreeItem* pItem);
	COLORREF GetCellRGB(void);
	CImageList * CreateDragImageEx(int nItem);
    void OnControlLButtonDown(UINT nFlags, CPoint point, LVHITTESTINFO& ht);
	BOOL OnItemExpanding(CTreeItem *pItem, int iItem);
	BOOL OnItemExpanded(CTreeItem* pItem, int iItem);
	BOOL OnCollapsing(CTreeItem *pItem);
	BOOL OnItemCollapsed(CTreeItem *pItem);
	BOOL OnItemLButtonDown(LVHITTESTINFO& ht);
	BOOL OnVkReturn(void);
	BOOL OnDeleteItem(CTreeItem* pItem, int nIndex);
// Implementation
public:
	virtual ~CNvBackupOptList();
    
	// Generated message map functions
protected:
	//{{AFX_MSG(CNvBackupOptList)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	//}}AFX_MSG
    afx_msg void OnItemCheck(LPNMITEMACTIVATE lpnmitem);
	DECLARE_MESSAGE_MAP()

private:
	CImageList *m_pImageList;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_NVBACKUPOPTLIST_H__1E401A66_75FF_404F_8D83_EEE15CA974A3__INCLUDED_)
