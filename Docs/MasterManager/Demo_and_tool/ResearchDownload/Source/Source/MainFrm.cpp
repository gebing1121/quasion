// MainFrm.cpp : implementation of the CMainFrame class
//
#include "stdafx.h"
#include <initguid.h>
#include "DLoader.h"
#include "XAboutDlg.h"

#include "MainFrm.h"

#include "Calibration.h"
#include "Calibration_Struct.h"

#include "DLoaderView.h"

#include "BMFile.h"

#include "MasterImgGen.h"


#include <dbt.h>
#include <algorithm>

#include "BinPack.h"
#include "BarcodeDlg.h"

#pragma warning(push,3)
#include <vector>
#pragma warning(pop)

#include "ICommChannel.h"

#include "./phasecheck/PhaseCheckBuild.h"

#include "DlgPassword.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


#define NV_LENGTH      0x10000

#define WM_WARN_MESSAGEBOX (WM_USER+1000)
#define WM_INIT_PACKET     (WM_USER+1001)


#define OPR_SUCCESS     (0)
#define OPR_FAIL        (1)
//////////////////////////////////////////////////////////////////////////
static HWND g_hLayer1TesterHwnd = NULL;
static BOOL CALLBACK EnumWindowsProc(HWND hParent, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
	if (NULL == hParent)
    {
		return FALSE;
    }
	else
	{
		_TCHAR szTitle[_MAX_PATH] = {0};
		::GetWindowText(hParent, szTitle, _MAX_PATH);
     	if ( NULL != _tcsstr(szTitle, _T("Layer1Tester")) )
		{
			g_hLayer1TesterHwnd = hParent;
			return FALSE;
		}		
	}    
	return TRUE;
}

static BOOL GetAbsolutePath(CString &strAbsoluteFilePath,LPCTSTR lpszFilePath )
{
    TCHAR szFileName[_MAX_FNAME];    
    TCHAR szDir[_MAX_DIR];
    TCHAR szDirve[_MAX_DRIVE];	
    
    _tsplitpath( lpszFilePath,szDirve,NULL,NULL,NULL);	
    if( szDirve[0] == _T('\0') )
	{//do it if strHelpTopic is ralatively 
        GetModuleFileName( AfxGetApp()->m_hInstance, szFileName,_MAX_FNAME);
        _tsplitpath( szFileName , szDirve , szDir , NULL , NULL );	
        strAbsoluteFilePath = szDirve;
        strAbsoluteFilePath += szDir;
        if( lpszFilePath[0] == _T('\\') || lpszFilePath[0] == _T('/') )
            lpszFilePath++;
        
        strAbsoluteFilePath += lpszFilePath;		
    }
	else
	{
        strAbsoluteFilePath = lpszFilePath;
    }
    return true;
}
/////////////////////////////////////////////////////////////////////////////
// CMainFrame

IMPLEMENT_DYNCREATE(CMainFrame, CFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
//{{AFX_MSG_MAP(CMainFrame)
ON_WM_CREATE()
ON_COMMAND(ID_SETTINGS, OnSettings)
ON_COMMAND(ID_START, OnStart)
ON_COMMAND(ID_STOP, OnStop)
ON_UPDATE_COMMAND_UI(ID_START, OnUpdateStart)
ON_UPDATE_COMMAND_UI(ID_STOP, OnUpdateStop)
ON_WM_CLOSE()
ON_UPDATE_COMMAND_UI(ID_SETTINGS, OnUpdateSettings)
ON_WM_HELPINFO()
ON_COMMAND(ID_LOAD_PACKET, OnLoadPacket)
ON_UPDATE_COMMAND_UI(ID_LOAD_PACKET, OnUpdateLoadPacket)
//}}AFX_MSG_MAP
// Global help commands
//    ON_COMMAND(ID_HELP_FINDER, CFrameWnd::OnHelpFinder)
//    ON_COMMAND(ID_HELP, CFrameWnd::OnHelp)
//    ON_COMMAND(ID_CONTEXT_HELP, CFrameWnd::OnContextHelp)
//    ON_COMMAND(ID_DEFAULT_HELP, CFrameWnd::OnHelpFinder)
ON_MESSAGE(WM_WARN_MESSAGEBOX,OnWarnMessageBox)
ON_MESSAGE(WM_INIT_PACKET,OnInitalPacket)
ON_MESSAGE(WM_STOP_ONE_PORT,OnStopOnePort)
ON_MESSAGE(WM_POWER_MANAGE,OnPowerManage)
ON_MESSAGE(WM_DWONLOAD_START,OnStartDownload)
ON_MESSAGE(WM_DEV_HOUND,OnDevHound)
END_MESSAGE_MAP()

static UINT indicators[] =
{
	ID_SEPARATOR,           // status line indicator
		ID_INDICATOR_CAPS,
		ID_INDICATOR_NUM,
		ID_INDICATOR_SCRL,
};

/////////////////////////////////////////////////////////////////////////////
// CMainFrame construction/destruction

CMainFrame::CMainFrame():m_sheetSettings(IDS_DL_SETTINGS)
{
    m_bStarted = FALSE;
	m_lStartNVRef = 0;
    ZeroMemory( m_lpReceiveBuffer , MAX_RECEIVE_SIZE );
    m_dwReceiveSize = 0;
	
    m_bAutoStart = FALSE;
    m_nFileCount = 0;
	m_pBMAF  = g_theApp.m_pBMAFramework;
	
    m_pMasterImg = NULL;
	
	m_bPacketOpen = FALSE;
	m_strSpecConfig = _T("");
	m_strPacketPath = _T("");
	m_aPacReleaseDir.RemoveAll();
	
	m_mapBMObj.RemoveAll();
	m_pNVFileBuf = NULL;
	m_dwReceiveSize = 0;
	m_pReportFile = NULL;
	
	m_dwMaxNVLength = NV_LENGTH;

	m_strOprErrorConfigFile = _T("BMError.ini");

	m_bCheckCali = FALSE;

	m_strCodeChipID = _T("");

	m_bShowFailedMsgbox = FALSE;
	m_bFailedMsgboxShowed = FALSE;

	m_bPowerManage = FALSE;
	m_bPMInDLProcess = TRUE;
	m_dwPowerMgrInter = 10;
	
	m_bNeedPhaseCheck = TRUE;

	m_bDoReport = FALSE;

	m_strLoadSettingMsg = _T("");
	
	InitializeCriticalSection( &m_csReportFile );

	InitializeCriticalSection( &m_csPowerManage );
	
//#if defined(_DOWNLOAD_FOR_PRODUCTION) || defined(_SPUPGRADE)
	InitializeCriticalSection( &m_cs );
//#endif
}//lint !e1401

CMainFrame::~CMainFrame()
{
/*lint -save -e1551 */
	m_pBMAF = NULL;
	if(m_pMasterImg)
    {
		delete m_pMasterImg;
		m_pMasterImg = NULL;
    }
	
	SAFE_DELETE_ARRAY(m_pNVFileBuf);
	
	DeleteCriticalSection( &m_csReportFile );
	DeleteCriticalSection( &m_csPowerManage );
	
	if(NULL != m_pReportFile)
	{
		fclose(m_pReportFile);
		m_pReportFile = NULL;
	}
	
	DWORD dwPort = 0;	
    _BMOBJ  *pStruct = NULL;
	POSITION pos = m_mapBMObj.GetStartPosition();
    while( pos )
    {
        m_mapBMObj.GetNextAssoc( pos, dwPort, pStruct );	
		SAFE_DELETE_ARRAY(pStruct->pBuf);
    }
	m_mapBMObj.RemoveAll();
	
//#ifdef _DOWNLOAD_FOR_PRODUCTION
	DeleteCriticalSection( &m_cs );
	
    pos = m_mapPortData.GetStartPosition();
    while( NULL != pos )
    {
        DWORD dwKey;
        //HANDLE hEvent;
		PORT_DATA* pPortData;
        m_mapPortData.GetNextAssoc( pos, dwKey, pPortData );
        CloseHandle( pPortData->hSNEvent );
		SAFE_DELETE(pPortData->lpPhaseCheck);
		SAFE_DELETE(pPortData);
    }
	m_mapPortData.RemoveAll();
//#endif

	ClearExtImgMap(m_mapUDiskIMg);
	ClearExtImgMap(m_mapChipDsp);
	ClearMultiFileBuf();
/*lint -restore */
}

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	if (!m_wndToolBar.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP
		| CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) ||
		!m_wndToolBar.LoadToolBar(IDR_MAINFRAME))
	{
		TRACE0("Failed to create toolbar\n");
		return -1;      // fail to create
	}
	
	
	if (!m_wndStatusBar.Create(this) ||
		!m_wndStatusBar.SetIndicators(indicators,
		sizeof(indicators)/sizeof(UINT)))
	{
		TRACE0("Failed to create status bar\n");
		return -1;      // fail to create
	}
	
    SetMenu(NULL);
	
    
    //@ Liu Kai 2004-3-25 CR8123
    // Read .ini file
#ifndef _SPUPGRADE
#ifndef _DOWNLOAD_FOR_PRODUCTION	
    if(!LoadSettings())
    {  
		if(!m_strLoadSettingMsg.IsEmpty())
		{
			CString strError;
			strError.LoadString(IDS_LOAD_SETTINGS_FAIL);
			strError += m_strLoadSettingMsg;
			AfxMessageBox(strError,MB_ICONSTOP);
		}
        return -1;
    }
#endif
#endif
	
	// TODO: Delete these three lines if you don't want the toolbar to
	//  be dockable
	//	m_wndToolBar.EnableDocking(CBRS_ALIGN_ANY);
	//	EnableDocking(CBRS_ALIGN_ANY);
	//	DockControlBar(&m_wndToolBar);
	
	UINT uStyle = m_wndToolBar.GetButtonStyle(1);
	uStyle |=  BS_FLAT;
	m_wndToolBar.SetButtonStyle(1,uStyle);
//  just for pc-lint
	UNUSED_ALWAYS(IDB_BMP_TB_FACTORY);
	UNUSED_ALWAYS(IDB_BMP_TB_FACTORY_DIS);
	UNUSED_ALWAYS(IDB_BMP_TB_FACTORY_HOT);
	UNUSED_ALWAYS(IDB_BMP_TB_UPGRADE);
	UNUSED_ALWAYS(IDB_BMP_TB_UPGRADE_DIS);
	UNUSED_ALWAYS(IDB_BMP_TB_UPGRADE_HOT);
	UNUSED_ALWAYS(IDB_BMP_TOOLBAR);
	UNUSED_ALWAYS(IDB_BMP_TOOLBAR_DIS);
	UNUSED_ALWAYS(IDB_BMP_TOOLBAR_HOT);
#ifdef _SPUPGRADE
	m_wndToolBar.SetBitmaps(IDB_BMP_TB_FACTORY,IDB_BMP_TB_FACTORY_DIS,IDB_BMP_TB_FACTORY_HOT);	
#elif defined _DOWNLOAD_FOR_PRODUCTION
	m_wndToolBar.SetBitmaps(IDB_BMP_TB_UPGRADE,IDB_BMP_TB_UPGRADE_DIS,IDB_BMP_TB_UPGRADE_HOT);
#else	
	m_wndToolBar.SetBitmaps(IDB_BMP_TOOLBAR,IDB_BMP_TOOLBAR_DIS,IDB_BMP_TOOLBAR_HOT);	
#endif

	int nIndex =0;
	CRect rcStatic;
	CRect rcWnd;
	GetClientRect(rcWnd);
	int nRight;
	nRight= rcWnd.right;
	nIndex= m_wndToolBar.CommandToIndex(ID_SPD_VERSION);
    m_wndToolBar.SetButtonInfo(nIndex,ID_SPD_VERSION,TBBS_SEPARATOR,800);
	m_wndToolBar.GetItemRect(nIndex,&rcStatic);
    rcStatic.top+=2;
	rcStatic.bottom-=2;
	//	rcStatic.right -= 400;
	m_wndToolBar.m_stcWnd.Create(_T(""),WS_CHILD|WS_VISIBLE |SS_LEFTNOWORDWRAP,rcStatic,&m_wndToolBar,ID_SPD_VERSION);
	m_wndToolBar.m_stcWnd.SetText(_T(""));
    m_wndToolBar.m_stcWnd.ShowWindow(SW_SHOWNA);
    m_wndToolBar.GetToolBarCtrl().EnableButton(ID_SPD_VERSION,TRUE);

	SetDLTitle();

	m_usbMoniter.SetReceiver((DWORD)this->GetSafeHwnd(),WM_DEV_HOUND,FALSE);
	m_usbMoniter.Start();
	
	PostMessage(WM_INIT_PACKET);

	if(g_theApp.m_bCMDPackage)
	{
		AfxGetApp()->m_nCmdShow = SW_HIDE;
	}
	
	return 0;
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CFrameWnd::PreCreateWindow(cs) )
		return FALSE;
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs
    cs.style &= ~FWS_ADDTOTITLE;	
	
	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CMainFrame diagnostics

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CFrameWnd::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMainFrame message handlers


void CMainFrame::OnSettings() 
{
	if(g_theApp.m_bNeedPassword)
	{
		CDlgPassword dlgPW;
		if(dlgPW.DoModal() == IDCANCEL)
		{
			return;
		}
	}
    m_sheetSettings.DoModal();
}

void CMainFrame::OnStart()
{
	m_bStarted = TRUE;	

	m_wndToolBar.GetToolBarCtrl().EnableButton(ID_START,FALSE);
	m_wndToolBar.GetToolBarCtrl().EnableButton(ID_SETTINGS,FALSE);
	m_wndToolBar.GetToolBarCtrl().EnableButton(ID_LOAD_PACKET,FALSE);
	
	MSG msg;
	while(PeekMessage(&msg,NULL,0,0,PM_REMOVE))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	PostMessage(WM_DWONLOAD_START);
}

void CMainFrame::StartWork()
{
	CWaitCursor wait;

	///////////////////////////////////////////////////////////////////////////
	DWORD dwWaitTime = g_theApp.m_dwWaitTimeForNextChip;

	_TCHAR szConfigPath[_MAX_PATH]={0};
	DWORD dwRetSize = ::GetModuleFileName(g_theApp.m_hInstance,szConfigPath,_MAX_PATH);
	if(dwRetSize != 0)
	{
		 LPTSTR pResult = _tcsrchr(szConfigPath,_T('.'));
		 *pResult = 0;
		_tcscat(szConfigPath,_T(".ini"));
		CString strProduct = m_sheetSettings.GetCurProduct();
		if(strProduct.Find(_T("PAC_")) == 0)
		{
			strProduct = strProduct.Right(strProduct.GetLength()-4);
		}
		dwWaitTime = GetPrivateProfileInt(_T("Settings"),strProduct,dwWaitTime,szConfigPath);
	}	

	if(g_theApp.m_bManual)
	{// manual to control download start. so it will close the port, when download finished.
		dwWaitTime = 0;		
	}	
	m_pBMAF->BMAF_SetProperty(BMAF_TIME_WAIT_FOR_NEXT_CHIP,dwWaitTime);	
	m_pBMAF->BMAF_SetProperty(BMAF_NAND_REPARTION_FLAG, m_sheetSettings.GetRepartitionFlag() );	

			
	m_pBMAF->BMAF_SetProperty(BMAF_SPECIAL_CONFIG_FILE, (DWORD)(LPCTSTR)m_strSpecConfig );
	
// 	CString strNULL = _T("");
//     CString strNVFileName = _T("");
// 	if(m_sheetSettings.IsReadFlashInFDL2() || m_sheetSettings.IsHasLang() )
// 	{
// 		CString strNVFileType;
// 		PFILE_INFO_T pNVFile = NULL;
// 		int nIdx = m_sheetSettings.GetFileInfo(_T("NV"),(LPDWORD)&pNVFile);
// 		if(nIdx != -1 && pNVFile != NULL)
// 		{
// 			strNVFileType = pNVFile->szType;
// 			m_pBMAF->BMAF_SetProperty(BMAF_SPEC_FILE_TYPE,(DWORD)(LPCTSTR)strNVFileType);			
// 			strNVFileName = m_arrFile.GetAt(nIdx);
// 		}		
// 	}
// 	else
// 	{
// 		m_pBMAF->BMAF_SetProperty(BMAF_SPEC_FILE_TYPE,(DWORD)(LPCTSTR)strNULL);
// 	}	
// 	m_pBMAF->BMAF_SetProperty(BMAF_READ_FLASH_BEFORE_REPARTITION, (DWORD)m_sheetSettings.IsReadFlashInFDL2());
	//////////////////////////////////////////////////////////////////////////

	m_bAutoStart = TRUE;
/*
    // Search valible serial ports and open them
    HKEY  hOpenKey;
    const int cLen = 255;
    DWORD dwValueLen;
    DWORD dwDataLen;
    _TCHAR szValueName[cLen];
    _TCHAR szData[cLen];
    DWORD dwType;
    DWORD dwRet;
    int nSelPort = m_sheetSettings.GetComPort();
    CString strErrMsg;
  
    if((dwRet = RegOpenKeyEx(HKEY_LOCAL_MACHINE, 
        _T("HARDWARE\\DEVICEMAP\\SERIALCOMM"),
        NULL,
        KEY_QUERY_VALUE | KEY_READ,
        &hOpenKey)) != ERROR_SUCCESS)
    {
		//m_bStarted = TRUE;
        return;
    }

    std::vector<int> lstPort;

	CMap<DWORD,DWORD,DWORD,DWORD> mapPort;
	DWORD dwKey;
	DWORD dwValue;
	int i = 0;
	
    for(;;)
    {
        dwValueLen = cLen;
        dwDataLen = cLen;
        szValueName[0] = 0;
        szData[0] = 0;
        
        dwRet = ::RegEnumValue(hOpenKey,
            i++,
            szValueName,
            &dwValueLen,
            NULL,
            &dwType,
            (BYTE*)szData,
            &dwDataLen
            );
        
        if(dwRet != ERROR_SUCCESS )
            break;
        
        int port;
        int nRet = _stscanf( szData,_T("COM%d"),&port );
        
        if( nRet == 0 )
            continue;
        
        if( nSelPort != 0 && nSelPort != port )
            continue;

		if(g_theApp.m_bFilterPort)
		{
			DWORD dwValue = 0;
			if(g_theApp.m_mapFilterPort.Lookup((DWORD)port,dwValue))
			{
				continue;
			}
		}

		dwKey = port;
		if(!mapPort.Lookup(dwKey,dwValue))
		{
			lstPort.push_back(port); 
			mapPort.SetAt(dwKey,dwKey);
		}

		if(nSelPort != 0 && nSelPort == port)
			break;
    }
	
    RegCloseKey(hOpenKey);	
*/	
	std::vector<DEV_INFO> lstPort;
	m_usbMoniter.ScanPort(lstPort);
	int i = 0;
	if(lstPort.size() != 0)
	{
		//std::sort(lstPort.begin(),lstPort.end());
		for(i = 0; i< (int)lstPort.size(); i++)
		{
			DWORD dwPort = lstPort[i].nPortNum;
			CString strPortName = lstPort[i].szFriendlyName;
			if(g_theApp.m_bFilterPort)
			{
				DWORD dwValue = 0;
				if(g_theApp.m_mapFilterPort.Lookup((DWORD)dwPort,dwValue))
				{
					continue;
				}
			}

			if(m_sheetSettings.IsNeedRebootByAT())
			{
				// Is AT port?			
				if(IsATPort(strPortName))
				{
					SendAT2Reboot(dwPort);
					continue;
				}
				
				// If not DL port, continue
				if(!IsDLPort(strPortName))
				{
					continue;
				}
			}

			if(g_theApp.m_bManual)
			{				
				((CDLoaderView*)GetActiveView())->AddProg( dwPort, TRUE ); 
				CreatePortData( dwPort );
			}
		    else
			{
				// The port enumed is regarded as UART
				if( !StartOnePortWork(dwPort,TRUE) )
				{
					OnStop();
					return;
				}
				wait.Restore();
			}
		}
	}
    
    //m_bStarted = TRUE;

}

BOOL CMainFrame::StartOnePortWork( DWORD dwPort, BOOL bUart )
{

	CWaitCursor wait;
    CString strErrMsg = _T("");
    BYTE btOpenArgument[ 8 ];
    *(DWORD *)&btOpenArgument[ 0 ] = dwPort; 
    *(DWORD *)&btOpenArgument[ 4 ] = m_sheetSettings.GetBaudRate();   
    
	
    HRESULT hr = 0;
    CComPtr<IBMOprObserver> pObserver;
    hr = GetControllingUnknown()->QueryInterface( IID_IBMOprObserver,(LPVOID*)&pObserver );
    if( SUCCEEDED( hr ))
    {
		m_pBMAF->BMAF_SubscribeObserver( dwPort,pObserver, NULL );
    } 
	else
	{
		return FALSE;
	}

	LPCTSTR *ppFileList = new LPCTSTR[m_nFileCount];
	if(ppFileList == NULL)
	{
		return FALSE;
	}
	for(int i = 0; i<m_nFileCount; i++)
	{
		ppFileList[i] = (LPCTSTR)m_arrFile.GetAt(i);
		//TRACE(_T("FILE[%d]:%s\n"),i,ppFileList[i]);
	}	
	_BMOBJ     * pStruct = NULL;
	if( m_mapBMObj.Lookup(dwPort, pStruct) )
    {
/*		if(pStruct->pBuf)
		{
			delete [] pStruct->pBuf;
			pStruct->pBuf = NULL;
			pStruct->dwBufSize = 0;
		}

		pStruct->dwCookie = dwPort;
		pStruct->dwIsUart = bUart;
		
		memset(pStruct->szErrorMsg,0,sizeof(pStruct->szErrorMsg));
		memset(pStruct->szSN,0,sizeof(pStruct->szSN));
*/
		pStruct->Clear();
    }
	else
	{
		pStruct = new _BMOBJ;
		if( pStruct == NULL ) //lint !e774
        {
			SAFE_DELETE_ARRAY(ppFileList);
            strErrMsg.LoadString( IDS_NEWOPR_FAIL );
            AfxMessageBox( strErrMsg );
            return FALSE;                        
        }

		pStruct->dwCookie = dwPort;
		pStruct->dwIsUart = bUart;

		m_mapBMObj.SetAt(dwPort,pStruct);
	}

    m_mapBMObj.Lookup(dwPort, pStruct);

	if(m_sheetSettings.IsReadFlashInFDL2() || m_sheetSettings.IsHasLang() )
	{
		CString strNVFileName = _T("");
		PFILE_INFO_T pNVFile = NULL;
		int nIdx = m_sheetSettings.GetFileInfo(_T("NV"),(LPDWORD)&pNVFile);
		if(nIdx != -1 && pNVFile != NULL)
		{				
			strNVFileName = m_arrFile.GetAt(nIdx);
		}

		_ASSERTE(!strNVFileName.IsEmpty() && strNVFileName.CompareNoCase(FILE_OMIT) != 0);
		
		if(m_dwNvFileSize == 0 || m_pNVFileBuf == NULL)
		{
			SAFE_DELETE_ARRAY(ppFileList);
			AfxMessageBox( _T("NV file is empty!") );			
			return FALSE;
		}
        pStruct->dwBufSize = m_dwNvFileSize;
		
		pStruct->pBuf = new BYTE[pStruct->dwBufSize];
		
		if(pStruct->pBuf != NULL)
		{
			memcpy(pStruct->pBuf, m_pNVFileBuf,m_dwNvFileSize);
		}
		else
		{
			SAFE_DELETE_ARRAY(ppFileList);
			AfxMessageBox( _T("Memory full!") );			
			return FALSE;
		}
	}
	
	
    hr = m_pBMAF->BMAF_StartOneWork( m_sheetSettings.GetCurProduct(),
		ppFileList, 
		m_nFileCount,
		(DWORD)btOpenArgument, 
		TRUE, 
		dwPort, 
		FALSE, 
		(DWORD)(GetActiveView()->GetSafeHwnd()),
		L"UartManager.UartObj" );

	SAFE_DELETE_ARRAY(ppFileList);
	
    BOOL bReturn = TRUE;
	BOOL bFirst = TRUE;
	PBOOTMODEOBJ_T pBMO = NULL;
	m_pBMAF->BMAF_GetBootModeObjInfo(dwPort,(LPDWORD)&pBMO);
	bFirst = pBMO->bFirstStart;
	
    switch( (DWORD)hr )
    {
    case BM_E_CREATETHREAD_FAILED:
        strErrMsg.LoadString( IDS_CREATETHREAD_FAIL );
        bReturn =  FALSE;   
        break;
    case BM_E_FILEINFO_ERROR:
        strErrMsg.LoadString( IDS_FILEINFO_ERROR );
        bReturn =  FALSE;   
        break;
    case BM_E_OPR_NOTREG:
        strErrMsg.LoadString( IDS_BMOPR_NOTREG );
        bReturn =  FALSE;   
        break;
    case BM_E_FAILED:
        strErrMsg.LoadString( IDS_START_FAIL );
        bReturn =  FALSE;   
        break;
	case BMAF_E_INIT_BMFILES_FAIL:		
		strErrMsg = pStruct->szErrorMsg; // prepare file fail, mainly for making master image
		if(strErrMsg.IsEmpty())
		{
			strErrMsg.Format(_T("Init BMFiles failed!"));
		}
        bReturn =  FALSE;   
        break;
    default:
		if(!g_theApp.m_bManual)
		{
			((CDLoaderView*)GetActiveView())->AddProg( dwPort, SUCCEEDED(hr) ); //lint !e730
		}

        break;        
    }
	
    if( FALSE == bReturn )
    {
		if(!strErrMsg.IsEmpty())
		{
			AfxMessageBox( strErrMsg );
		}
		m_pBMAF->BMAF_StopOneWork( dwPort);
		m_pBMAF->BMAF_UnsubscribeObserver( dwPort );
        return FALSE;        
    }     
    RecalcLayout(); 
    return TRUE;
}

BOOL CMainFrame::StopOnePortWork(DWORD dwPort,BOOL bRemoved /*= FALSE*/)
{
	UNREFERENCED_PARAMETER(bRemoved);
	HRESULT hr;
    PBOOTMODEOBJ_T pStruct = NULL;	
	hr = m_pBMAF->BMAF_GetBootModeObjInfo(dwPort,(LPDWORD)&pStruct);
	BOOL bSuc = TRUE;
	if( SUCCEEDED(hr) && pStruct != NULL )
	{
		if(!pStruct->bStop)
		{
#ifdef _DOWNLOAD_FOR_PRODUCTION
			DL_STAGE stage;
			((CDLoaderView*)GetActiveView())->GetStatus((int)dwPort, stage);
			if ( stage != DL_FINISH_STAGE )
#endif
			{
				((CDLoaderView*)GetActiveView())->SetResult( dwPort,FALSE,NULL,bRemoved? 2: 0 );
			}	

			bSuc = FALSE;
		}
		else
		{
			// for release port
			pStruct->bStop = FALSE;
		}	

		((CDLoaderView *)GetActiveView())->ClearPortInfo(dwPort);
		PORT_DATA *pPortData = NULL;
		if( m_mapPortData.Lookup(dwPort, pPortData) )
		{
			if(pPortData != NULL)
			{
				SetEvent(pPortData->hSNEvent);
				CloseHandle(pPortData->hSNEvent);
				SAFE_DELETE(pPortData->lpPhaseCheck);
				SAFE_DELETE(pPortData);
			}

			m_mapPortData.RemoveKey(dwPort);
		}		


		//m_pBMAF->BMAF_StopOneWork(dwPort);
		DWORD dwParam[3] = {0};
		dwParam[0] = (DWORD)this;
		dwParam[1] = dwPort;
		dwParam[2] = bSuc;
		
		DWORD dwThreadID = 0;
		HANDLE hThread = CreateThread(NULL,
            0,
            (LPTHREAD_START_ROUTINE)GetThreadFunc,
            (LPVOID)dwParam,
            NULL,
            &dwThreadID);

		MSG msg;
		while(PeekMessage(&msg,NULL,0,0,PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}

        if( hThread == NULL)
        {
            m_pBMAF->BMAF_StopOneWork(dwPort);	
        }
		else
		{
			WaitForSingleObject( hThread, 30000);	
			CloseHandle( hThread);
			hThread = NULL;
		}
					
	}
	if(!bRemoved && g_theApp.m_bManual)
	{
		((CDLoaderView *)GetActiveView())->AddProg(dwPort,TRUE);
		CreatePortData(dwPort);	
	}


	return TRUE;
}

void CMainFrame::OnStop()
{
    CWaitCursor wait;	

	POSITION pos = m_mapPortData.GetStartPosition();
    DWORD dwCookie = 0;
    
    PORT_DATA* pPortData = NULL;
    while( pos )
    {
        m_mapPortData.GetNextAssoc( pos, dwCookie, pPortData );
		SetEvent(pPortData->hSNEvent);        
	}

    
    m_pBMAF->BMAF_StopAllWork();
	m_arrFile.RemoveAll();
	m_nFileCount = 0;
	
	
    ((CDLoaderView*)GetActiveView())->RemoveAll();
    m_bStarted = FALSE;
    m_bAutoStart = FALSE;
	m_lStartNVRef = 0;
    if( m_pMasterImg )
    {
        delete m_pMasterImg;
        m_pMasterImg = NULL;
    }
	
	if(NULL != m_pReportFile)
	{
		fclose(m_pReportFile);
		m_pReportFile = NULL;
	}

	((CDLoaderView *)(this->GetActiveView()))->StopDLTimer();
	
    //CoUninitialize(); 
}

void CMainFrame::OnUpdateStart(CCmdUI* pCmdUI) 
{
#if defined( _DOWNLOAD_FOR_PRODUCTION ) || defined(_SPUPGRADE)
	BOOL bOK = m_bPacketOpen &&!m_bStarted && m_sheetSettings.IsMainPageInit();
#else // _DLOADERR
    BOOL bOK = !m_bStarted && m_sheetSettings.IsMainPageInit();
#endif
	
    pCmdUI->Enable(bOK);
}

void CMainFrame::OnUpdateStop(CCmdUI* pCmdUI) 
{
#if defined(_SPUPGRADE)
	//not allowed to click stop button when dealing with NV
	pCmdUI->Enable( m_bStarted && m_lStartNVRef == 0 );
#elif defined _DOWNLOAD_FOR_PRODUCTION
	pCmdUI->Enable( m_bStarted && m_lStartNVRef == 0 ); //lint !e730
#else
	pCmdUI->Enable(m_bStarted);	
#endif
}

void CMainFrame::DeletePacTmpDir()
{
	CWaitCursor wait;

	CString strPath = _T("");
	CBinPack bp;
	int n = m_aPacReleaseDir.GetSize();
	for(int i = 0; i< n; i++)
	{
		strPath = m_aPacReleaseDir.GetAt(i);
		if(!strPath.IsEmpty())
		{				
			bp.RemoveReleaseDir(strPath);
		}
	}
	m_aPacReleaseDir.RemoveAll();
}

void CMainFrame::OnClose() 
{
	if(m_bStarted)
    {
        CString str;
        str.LoadString(IDS_QUIT_TEST);
        AfxMessageBox(str);
    }
    else
    {
        ShowWindow(SW_HIDE);        
		if(g_theApp.m_pBMAFramework != NULL)
			g_theApp.m_pBMAFramework->BMAF_Release();   
		
		DeletePacTmpDir();
   
		SAFE_DELETE_ARRAY(m_pNVFileBuf);
		m_dwNvFileSize = 0;
		m_usbMoniter.Stop();
        
		CFrameWnd::OnClose();
    }
}


void CMainFrame::OnUpdateSettings(CCmdUI* pCmdUI) 
{
	// Disable Settings button while downloading
#if defined(_SPUPGRADE) || defined(_DOWNLOAD_FOR_PRODUCTION)
	pCmdUI->Enable(m_bPacketOpen && !m_bStarted); //lint !e730
#else
    pCmdUI->Enable(!m_bStarted);
#endif	
}

//@ Liu Kai 2004-3-25 CR8123
BOOL CMainFrame::LoadSettings()
{
	m_strLoadSettingMsg = _T("");

    _TCHAR szFilePath[_MAX_PATH]={0};
    if(!GetIniFilePath(szFilePath))
    {
        return FALSE;
    }
	_TCHAR szAppPath[_MAX_PATH]={0};
	GetModuleFilePath(g_theApp.m_hInstance,szAppPath);
	CFileFind finder;
	
	CString strBMFileType;
	strBMFileType.Format(_T("%s\\BMFileType.ini"),szAppPath);	
	if(finder.FindFile(strBMFileType))
	{
		m_dwMaxNVLength = GetPrivateProfileInt(_T("DownloadNV"),_T("MaxReadLength"),NV_LENGTH,strBMFileType);
	}

	_TCHAR szBuf[_MAX_PATH]={0};
	GetPrivateProfileString(_T("AT_REBOOT_SETTING"),_T("ATPort"),_T(""),szBuf,_MAX_PATH,strBMFileType);
	SplitStr(szBuf,m_agATPort,',');
	

	memset(szBuf,0,sizeof(szBuf));
	GetPrivateProfileString(_T("AT_REBOOT_SETTING"),_T("DLPort"),_T(""),szBuf,_MAX_PATH,strBMFileType);
	SplitStr(szBuf,m_agDLPort,',');

    m_strOprErrorConfigFile.Format(_T("%s\\BMError.ini"),szAppPath); 

	m_bCheckCali =  GetPrivateProfileInt(_T("Settings"),_T("CheckCali"),FALSE,szFilePath);
	//@ hongliang.xin 2010-6-17
	m_bShowFailedMsgbox = GetPrivateProfileInt(_T("GUI"),_T("ShowFailedMsgBox"),FALSE,szFilePath);

	m_bPowerManage = GetPrivateProfileInt(_T("GUI"),_T("PowerManageFlag"),FALSE,szFilePath);
	m_dwPowerMgrInter = GetPrivateProfileInt(_T("GUI"),_T("PowerManageInterval"),10,szFilePath);
	m_bPMInDLProcess = GetPrivateProfileInt(_T("GUI"),_T("InDLProcess"),1,szFilePath);

	m_bNeedPhaseCheck = GetPrivateProfileInt(_T("SN"),_T("NeedPhaseCheck"),1,szFilePath);

	m_bDoReport  = GetPrivateProfileInt(_T("Report"),_T("enable"),0,szFilePath);
	
    return m_sheetSettings.LoadSettings(szFilePath,m_strLoadSettingMsg);    
}

BOOL CMainFrame::GetIniFilePath(LPTSTR pName)
{
    if(pName == NULL)
        return FALSE;
	
    DWORD dwRet = ::GetModuleFileName(AfxGetApp()->m_hInstance,pName,_MAX_PATH);
    if(dwRet == 0)
    {
        pName[0] = 0;
        return FALSE;
    }
	
    LPTSTR pResult = _tcsrchr(pName,_T('.'));
    if(pResult == NULL)
    {
        pName[0] = 0;
        return FALSE;
    }
	
    *pResult = 0;
    _tcscat(pName,_T(".ini"));
	
    return TRUE;
}


BEGIN_INTERFACE_MAP(CMainFrame, CFrameWnd)
INTERFACE_PART( CMainFrame, IID_IBMOprObserver, BMOprObserver)
END_INTERFACE_MAP()

STDMETHODIMP_(ULONG) CMainFrame::XBMOprObserver::AddRef()
{
    METHOD_PROLOGUE(CMainFrame, BMOprObserver)
		return (ULONG)pThis->ExternalAddRef();
}

STDMETHODIMP_(ULONG) CMainFrame::XBMOprObserver::Release()
{
    METHOD_PROLOGUE(CMainFrame, BMOprObserver)
		return (ULONG)pThis->ExternalRelease();
}

STDMETHODIMP CMainFrame::XBMOprObserver::QueryInterface( REFIID iid, LPVOID* ppvObj)
{
    METHOD_PROLOGUE(CMainFrame, BMOprObserver)
		return (HRESULT)pThis->ExternalQueryInterface(&iid, ppvObj);
}

STDMETHODIMP CMainFrame::XBMOprObserver::OnOperationStart( THIS_ DWORD dwOprCookie, 
														  const BSTR cbstrFileID,
                                                          const BSTR cbstrFileType, 
                                                          const BSTR cbstrOperationType,
                                                          DWORD pBMFileInterface )
{
    METHOD_PROLOGUE(CMainFrame, BMOprObserver)
	UNUSED_ALWAYS( dwOprCookie );
	UNUSED_ALWAYS( cbstrFileID );
    UNUSED_ALWAYS( pBMFileInterface );
    
	//	CString strFileType(OLE2T( cbstrFileType));
	//	CString strOprType(OLE2T( cbstrOperationType));	
	
	CString strErrMsg = _T("");
	_BMOBJ * pbj = NULL;
	if( !pThis->m_mapBMObj.Lookup(dwOprCookie,pbj) ||  NULL == pbj )	
	{
		return E_FAIL;
	}

	CString strFileID = OLE2T( cbstrFileID );	
	if( _tcsnicmp( strFileID, _T("_BKF_"), 5) == 0 &&
		_tcsicmp( OLE2T( cbstrFileType ), _T("ReadFlash")) == 0 &&
		_tcsicmp( OLE2T( cbstrOperationType ),_T("ReadFlash")) == 0 )
	{
		CString strID = OLE2T( cbstrFileID );
		strID = strID.Right(strID.GetLength() - 5); // "_BKF_"
		PFILE_INFO_T pFileInfo = NULL;
		if(pThis->m_sheetSettings.GetFileInfo(strID,(LPDWORD)&pFileInfo) == -1)
		{
			strErrMsg.Format( _T("Can not find %s!"), strID);
			_tcscpy(pbj->szErrorMsg,strErrMsg);
			return E_FAIL;
		}
		
		BOOTMODEOBJ_T *pBMO = NULL;
		pThis->m_pBMAF->BMAF_GetBootModeObjInfo(dwOprCookie, (LPDWORD)&pBMO);
		IBMSettings *pSetting;
		pBMO->pSnapin->QueryInterface(IID_IBMSettings,(LPVOID *)&pSetting);
		DWORD dwPacketLen = pSetting->GetPacketLength(pFileInfo->szType);
		if(dwPacketLen == 0)
		{
			dwPacketLen = 0x1000;
		}

		IBMFile* pBMFile = (IBMFile*)pBMFileInterface;
		pBMFile->SetCurMaxLength(dwPacketLen);
	}

/*	
	if( _tcsicmp( OLE2T( cbstrFileType ), _T("NAND_FDL_OPT")) == 0
		&& _tcsicmp( OLE2T( cbstrOperationType ),_T("ReadFlash")) == 0 
		&& pThis->m_sheetSettings.IsReadFlashInFDL2())
	{
	
		IBMFile* pBMFile = (IBMFile*)pBMFileInterface;
		
		PFILE_INFO_T pNVFileInfo = NULL;
		if(pThis->m_sheetSettings.GetFileInfo(_T("NV"),(LPDWORD)&pNVFileInfo) == -1)
		{
			strErrMsg.Format( _T("Can not find NV!"));
			_tcscpy(pbj->szErrorMsg,strErrMsg);
			return E_FAIL;
		}
		
		if(pNVFileInfo != NULL)
		{
			pBMFile->SetCurCodeBase(pNVFileInfo->arrBlock[0].dwBase);
			pBMFile->SetCurOprSize(pThis->m_dwMaxNVLength);
			//DWORD dwNVPacketLength;
			//pThis->m_pBMAF->BMAF_GetProperty(BMAF_SPEC_PACKET_LENGTH,&dwNVPacketLength);
			//pBMFile->SetCurMaxLength(dwNVPacketLength);
			BOOTMODEOBJ_T *pBMO = NULL;
			pThis->m_pBMAF->BMAF_GetBootModeObjInfo(dwOprCookie, (LPDWORD)&pBMO);
			IBMSettings *pSetting;
			pBMO->pSnapin->QueryInterface(IID_IBMSettings,(LPVOID *)&pSetting);
			DWORD dwPacketLen = pSetting->GetPacketLength(pNVFileInfo->szType);
			if(dwPacketLen == 0)
			{
				dwPacketLen = 0x1000;
			}
			pBMFile->SetCurMaxLength(dwPacketLen);
			
		}		
	}
*/

	if(	_tcsicmp( strFileID, _T("PhaseCheck")) == 0 && 
		_tcsicmp( OLE2T( cbstrFileType ), _T("CODE")) == 0 &&
		_tcsicmp( OLE2T( cbstrOperationType ),_T("Download")) == 0)
	{
		int nID = pThis->m_sheetSettings.IsBackupFile(_T("PhaseCheck"));
		if(nID != -1)
		{
			IBMFile* pBMFile = (IBMFile*)pBMFileInterface;
			pBMFile->SetCurCode(pbj->tFileBackup[nID].pBuf, pbj->tFileBackup[nID].dwSize);
		}
		else
		{
#ifndef _SPUPGRADE
			PORT_DATA* pPortData = NULL;
			BOOL bFound = pThis->m_mapPortData.Lookup(dwOprCookie, pPortData);
			if( bFound )
			{
				IBMFile* pBMFile = (IBMFile*)pBMFileInterface;
				DWORD dwPhaseCheckSize  = pBMFile->GetCurCodeSize();
				if(dwPhaseCheckSize == 0  || dwPhaseCheckSize > PRODUCTION_INFO_SIZE )
				{
					dwPhaseCheckSize = PRODUCTION_INFO_SIZE;
				}
				pBMFile->SetCurCode(pPortData->lpPhaseCheck, dwPhaseCheckSize);
			}	
			else
			{
				//TRACE(_T("Cannot find information about Port:%d"), dwOprCookie);
				//_ASSERT(0);//lint !e774 !e506
				strErrMsg.Format( _T("Cannot find phasecheck info!"));
				_tcscpy(pbj->szErrorMsg,strErrMsg);
				return E_FAIL;
			}	
#endif
		}
	}

	if(_tcsicmp( strFileID, _T("PhaseCheck")) != 0 )
	{
		int nID = pThis->m_sheetSettings.IsBackupFile(strFileID);
		if(nID != -1)
		{
			IBMFile* pBMFile = (IBMFile*)pBMFileInterface;
			pBMFile->SetCurCode(pbj->tFileBackup[nID].pBuf, pbj->tFileBackup[nID].dwSize);
		}
	}

	if( pThis->m_bPowerManage && pThis->m_bPMInDLProcess && 
		pThis->m_sheetSettings.IsBackupNV() && 
		(_tcsicmp( OLE2T( cbstrFileID ), _T("NV")) == 0 || 
		_tcsicmp( OLE2T( cbstrFileID ), _T("_CHECK_NV_")) == 0) && 
		_tcsicmp( OLE2T( cbstrOperationType ),_T("Download")) == 0)
	{
		::PostMessage(pThis->GetSafeHwnd(),WM_POWER_MANAGE,dwOprCookie,0);
		Sleep( pThis->m_dwPowerMgrInter);
	}	

    return S_OK;
}

STDMETHODIMP CMainFrame::XBMOprObserver::OnOperationEnd( THIS_ DWORD dwOprCookie, 
														const BSTR cbstrFileID,
														const BSTR cbstrFileType, 
														const BSTR cbstrOperationType,
														DWORD dwResult,
														DWORD pBMFileInterface)
{
    UNUSED_ALWAYS( cbstrFileID );
    METHOD_PROLOGUE(CMainFrame, BMOprObserver)
	USES_CONVERSION;
	
	HRESULT hr = NULL;
	PBOOTMODEOBJ_T pStruct = NULL;

	CString strErrMsg = _T("");
	_BMOBJ * pbj = NULL;
	if( !pThis->m_mapBMObj.Lookup(dwOprCookie,pbj) ||  NULL == pbj )	
	{
		return E_FAIL;
	}

	CString strFileID = OLE2T( cbstrFileID);


	if(OPR_SUCCESS != dwResult)
	{
		if( OPR_FAIL != dwResult)
		{
			pThis->GetOprErrorCodeDescription(dwResult,pbj->szErrorMsg,_MAX_PATH);
			if(  dwResult == 0xA3 && !pThis->m_strCodeChipID.IsEmpty() )
			{
				CString strChipID;
				strChipID.Format(_T(" [CodeVer:%s]"),pThis->m_strCodeChipID.operator LPCTSTR());
				_tcscat(pbj->szErrorMsg,strChipID);	
				
				hr = pThis->m_pBMAF->BMAF_GetBootModeObjInfo(dwOprCookie,(LPDWORD)&pStruct);
		
				if( SUCCEEDED(hr) )
				{
					LPBYTE lpReadBuffer = NULL; 
					DWORD dwReadSize = 0;
					IBMOprBuffer* pBuffer;
					hr = pStruct->pSnapin->QueryInterface( IID_IBMOprBuffer, (LPVOID*)&pBuffer);
					if( SUCCEEDED(hr) )
					{
						lpReadBuffer = pBuffer->GetReadBuffer();
						dwReadSize = pBuffer->GetReadBufferSize();
					}
					if( lpReadBuffer != NULL && dwReadSize >= sizeof( DWORD) )
					{
						DWORD dwSoruceValue, dwDestValue;            
						dwSoruceValue =  *(DWORD *)&lpReadBuffer[ 0 ];    
						dwDestValue   = 0;
						CONVERT_INT( dwSoruceValue, dwDestValue); 
						pbj->dwChipID = dwDestValue;
						CString strMduChipID;
						strMduChipID.Format(_T(" [ModuleVer:%08X]"),dwDestValue);
						_tcscat(pbj->szErrorMsg,strMduChipID);
					}
				}
			}
		}
		return E_FAIL;
	}
  
    
    if(  (_tcsicmp( OLE2T( cbstrFileType), _T("CODE")) == 0 || 
		  _tcsicmp( OLE2T( cbstrFileType), _T("READ_CHIPID")) == 0 ) &&
          _tcsicmp( OLE2T( cbstrOperationType),_T("ReadChipType")) == 0 ) 
    {
        hr = pThis->m_pBMAF->BMAF_GetBootModeObjInfo(dwOprCookie,(LPDWORD)&pStruct);
		
        if( SUCCEEDED(hr) )
        {
            LPBYTE lpReadBuffer = NULL; 
            DWORD dwReadSize = 0;
            IBMOprBuffer* pBuffer;
            hr = pStruct->pSnapin->QueryInterface( IID_IBMOprBuffer, (LPVOID*)&pBuffer);
            if( SUCCEEDED(hr) )
            {
                lpReadBuffer = pBuffer->GetReadBuffer();
                dwReadSize = pBuffer->GetReadBufferSize();
            }
            if( lpReadBuffer == NULL || dwReadSize < sizeof( DWORD) )
            {
                return E_FAIL;
            }
			
            DWORD dwSoruceValue, dwDestValue;            
            dwSoruceValue =  *(DWORD *)&lpReadBuffer[ 0 ];    
            dwDestValue   = 0;
            CONVERT_INT( dwSoruceValue, dwDestValue); 
			pbj->dwChipID = dwDestValue;
        }
        return S_OK;
    }
	
    if( (_tcsicmp( OLE2T( cbstrFileType), _T("CHECK_MCPTYPE")) == 0  ||
		 _tcsicmp( OLE2T( cbstrFileType), _T("READFLASHTYPE")) == 0 )&&
        _tcsicmp( OLE2T(cbstrOperationType),_T("ReadFlashType")) == 0 )
    {
		hr = pThis->m_pBMAF->BMAF_GetBootModeObjInfo(dwOprCookie,(LPDWORD)&pStruct);
		
		if( SUCCEEDED(hr) )
        {
            LPBYTE lpReadBuffer = NULL; 
            DWORD dwReadSize = 0;
            IBMOprBuffer * pBuffer;
            hr = pStruct->pSnapin->QueryInterface( IID_IBMOprBuffer, (LPVOID*)&pBuffer);
            if( SUCCEEDED(hr) )
            {
                lpReadBuffer = pBuffer->GetReadBuffer();
                dwReadSize = pBuffer->GetReadBufferSize();
            }
            if( lpReadBuffer == NULL || dwReadSize < sizeof( DWORD)*4 )
            {
				strErrMsg.Format(_T("Read flash type failed."));
				_tcscpy(pbj->szErrorMsg,strErrMsg);
                return E_FAIL;
            }
            LPDWORD pDw = (LPDWORD)lpReadBuffer;
			CONVERT_INT((*pDw),    pbj->aFlashType[0]);  // MID
			CONVERT_INT((*(pDw+1)),pbj->aFlashType[1]);  // DID
			CONVERT_INT((*(pDw+2)),pbj->aFlashType[2]);  // EID  
			CONVERT_INT((*(pDw+3)),pbj->aFlashType[3]);  // SUPPORT, not used now.

			//if(_tcsicmp( OLE2T( cbstrFileType), _T("CHECK_MCPTYPE")) == 0 )
			{
				//Check MCP Type;
				CString strMCPType;
				if(pbj->aFlashType[2] != MAXDWORD)
				{
					strMCPType.Format(_T("%X-%X-%X"),pbj->aFlashType[0],pbj->aFlashType[1],pbj->aFlashType[2]);
				}
				else
				{
					strMCPType.Format(_T("%X-%X"),pbj->aFlashType[0],pbj->aFlashType[1]);
				}
				BOOL bMatch = FALSE;
				CString strDesc = pThis->m_sheetSettings.GetMCPTypeDesc(strMCPType.operator LPCTSTR(),
																		pbj->dwPage,pbj->dwOob,bMatch);
				
				CString strIdDesc;
				strIdDesc.Format(_T("ID: %s, DESC: %s"),
					strMCPType.operator LPCTSTR(),strDesc.operator LPCTSTR());			
				((CDLoaderView*)pThis->GetActiveView())->SetMcpType((int)dwOprCookie,strIdDesc);
				
				strDesc.MakeLower();
				_tcscpy(pbj->szMcpInfo,strDesc);
				if(_tcsicmp( OLE2T( cbstrFileType), _T("CHECK_MCPTYPE")) == 0 )
				{					
					CString strTmp = strDesc;
					int nFind = strTmp.ReverseFind('-');
					if(nFind != -1) strTmp = strTmp.Left(nFind);
					DWORD dwCount = 0;
					if( !bMatch || (pThis->m_sheetSettings.IsEnablePageOobFile() &&
						((!pThis->m_mapPageOobInfo.Lookup(strDesc,dwCount)&&
						 !pThis->m_mapPageInfo.Lookup(strTmp,dwCount)) ||
						 pbj->dwPage == 0 || pbj->dwOob == 0)))
					{
						if(strDesc.IsEmpty())
						{
							strErrMsg.Format(_T("Invalid MCP Type, [ID: %s]."),
								strMCPType.operator LPCTSTR());
						}
						else
						{
							strErrMsg.Format(_T("Invalid MCP Type, [ID: %s, DESC: %s]."),
								strMCPType.operator LPCTSTR(),strDesc.operator LPCTSTR());
						}
						_tcscpy(pbj->szErrorMsg,strErrMsg);
						return E_FAIL;
					}
				}				
			}
        }
        return S_OK;
    }
/*	
	//read sn from flash
	if( _tcsicmp( OLE2T( cbstrFileType), _T("ReadSN")) == 0 &&
        _tcsicmp( OLE2T(cbstrOperationType),_T("ReadFlash")) == 0 )
    {
		hr = pThis->m_pBMAF->BMAF_GetBootModeObjInfo(dwOprCookie,(LPDWORD)&pStruct);
		
		if( SUCCEEDED(hr) )
        {
            LPBYTE lpReadBuffer = NULL; 
            DWORD dwReadSize = 0;
            IBMOprBuffer * pBuffer;
            hr = pStruct->pSnapin->QueryInterface( IID_IBMOprBuffer, (LPVOID*)&pBuffer);
            if( SUCCEEDED(hr) )
            {
                lpReadBuffer = pBuffer->GetReadBuffer();
                dwReadSize = pBuffer->GetReadBufferSize();
            }

			IBMFile* pBMFile = (IBMFile*)pBMFileInterface;
			DWORD dwPhaseCheckSize  = pBMFile->GetCurOprSize();
			if(dwPhaseCheckSize == 0  || dwPhaseCheckSize > PRODUCTION_INFO_SIZE )
			{
				dwPhaseCheckSize = PRODUCTION_INFO_SIZE;
			}

			if(lpReadBuffer != NULL && dwReadSize >= dwPhaseCheckSize)
			{	
				CPhaseCheckBuild pcb;
				if(dwPhaseCheckSize < PRODUCTION_INFO_SIZE )
				{
					LPBYTE pBuf = new BYTE[PRODUCTION_INFO_SIZE];
					if(pBuf != NULL)
					{
						memset(pBuf,0xFF,PRODUCTION_INFO_SIZE);
						memcpy(pBuf,lpReadBuffer,dwReadSize);
						pcb.FindSnFrom8K(pBuf,PRODUCTION_INFO_SIZE,(BYTE*)pbj->szSN,X_SN_LEN);
						delete []  pBuf;
						pBuf = NULL;
					}			
				}
				else
				{
					pcb.FindSnFrom8K(lpReadBuffer,dwReadSize,(BYTE*)pbj->szSN,X_SN_LEN);	
				}						
			}

        }
        return S_OK;
    } 
*/	
	/************************************************************************/
	/*  backup NV                                                           */
	/************************************************************************/
    if( (_tcsicmp( strFileID, _T("NV")) == 0 &&
		 pThis->m_sheetSettings.IsBackupNV() &&
		_tcsnicmp( OLE2T( cbstrFileType), _T("NV"), 2 ) == 0 &&
		_tcsicmp( OLE2T(cbstrOperationType),_T("ReadFlash")) == 0 ) ||		  
		/*( pThis->m_sheetSettings.IsReadFlashInFDL2() &&
        _tcsicmp( OLE2T( cbstrFileType), _T("NAND_FDL_OPT")) == 0 &&
        _tcsicmp( OLE2T(cbstrOperationType),_T("ReadFlash")) == 0 )*/
		(_tcsicmp( strFileID, _T("_BKF_NV")) == 0 &&
		_tcsicmp( OLE2T( cbstrFileType), _T("ReadFlash")) == 0 &&
        _tcsicmp( OLE2T(cbstrOperationType),_T("ReadFlash")) == 0))
    {
		IBMFile* pBMFile = (IBMFile*)pBMFileInterface;
		
		hr = pThis->m_pBMAF->BMAF_GetBootModeObjInfo(dwOprCookie,(LPDWORD)&pStruct);
		
		if( SUCCEEDED(hr) )
        {
            LPBYTE lpReadBuffer = NULL; 
            DWORD dwReadSize = 0;
            DWORD dwCodeSize = 0;
			LPVOID pDestCode = NULL;
            IBMOprBuffer * pBuffer;
            hr = pStruct->pSnapin->QueryInterface( IID_IBMOprBuffer, (LPVOID*)&pBuffer);
            if( SUCCEEDED(hr) )
            {
                lpReadBuffer = pBuffer->GetReadBuffer();
                dwReadSize = pBuffer->GetReadBufferSize();
            }
			else
			{
				strErrMsg.Format(_T("Read Flash (NV) failed."));
				_tcscpy(pbj->szErrorMsg,strErrMsg);
				return E_FAIL;
			}
            if( lpReadBuffer == NULL || dwReadSize == 0)
            {
				strErrMsg.Format(_T("Read Flash (NV) failed."));
				_tcscpy(pbj->szErrorMsg,strErrMsg);
                return E_FAIL;
            }           
            
			if( _tcsnicmp( OLE2T( cbstrFileType), _T("NV"), 2 ) == 0 )
			{				
				const LPVOID pSrcCode = pBMFile->GetCurCode();
				dwCodeSize = pBMFile->GetCurCodeSize();
				pDestCode = pSrcCode;
			}
			else
			{
				dwCodeSize = pbj->dwBufSize;
				pDestCode = (LPVOID)(pbj->pBuf);
			}
//////////////////////////////////////////////////////////////////////////
// Check NV struct, lpReadBuffer will be changed if it is the driver level endian
			BOOL _bBigEndian = TRUE;
			if(!XCheckNVStructEx(lpReadBuffer,dwReadSize,_bBigEndian,TRUE))
			{
				CString strErr=_T("NV data read in phone is crashed.");
				_tcscpy(pbj->szErrorMsg,strErr);
				return E_FAIL;
			}

//////////////////////////////////////////////////////////////////////////
// check calibration reserved 7 and struct itself only for GSM			
			if(pThis->m_bCheckCali)
			{	
				LPBYTE lpPhoBuf = lpReadBuffer;
				DWORD  dwPhoSize = dwReadSize;

				CString strErr=_T("");

				if(!XCheckCalibration(lpPhoBuf,dwPhoSize,strErr,TRUE))
				{
					_tcscpy(pbj->szErrorMsg,strErr);
					return E_FAIL;
				}
			}
//////////////////////////////////////////////////////////////////////////
			PNV_BACKUP_ITEM_T pNvBkpItem = NULL;
			int nCount = pThis->m_sheetSettings.GetNvBkpItemCount();	
			BOOL bReplace = FALSE;
			BOOL bContinue = FALSE;
			for(int k=0;k<nCount;k++)
			{
				bReplace = FALSE;
				bContinue = FALSE;
				
				pNvBkpItem = pThis->m_sheetSettings.GetNvBkpItemInfo(k);
				
				int nNvBkpFlagCount = pNvBkpItem->dwFlagCount;
				if(nNvBkpFlagCount > MAX_NV_BACKUP_FALG_NUM)
				{
					nNvBkpFlagCount = MAX_NV_BACKUP_FALG_NUM;
				}
				for(int m=0;m<nNvBkpFlagCount;m++)
				{
					if(_tcscmp(pNvBkpItem->nbftArray[m].szFlagName,_T("Replace"))==0)
					{
						if(pNvBkpItem->nbftArray[m].dwCheck == 1)
							bReplace = TRUE;
					}
					else if(_tcscmp(pNvBkpItem->nbftArray[m].szFlagName,_T("Continue"))==0)
					{
						if(pNvBkpItem->nbftArray[m].dwCheck == 1)
							bContinue = TRUE;
					}
				}
				
				if(_tcscmp(pNvBkpItem->szItemName,_T("Calibration"))==0)
				{
#ifndef _SPUPGRADE
					if(pNvBkpItem->wIsBackup == 1)
#endif
					{
						DWORD dwErrorRCID = GSMCaliPreserve( (LPBYTE)pDestCode, dwCodeSize, lpReadBuffer, dwReadSize, 
															  bReplace, bContinue);
						if(  dwErrorRCID !=0  )
						{
							strErrMsg = GetErrorDesc( dwErrorRCID );
							strErrMsg.Format( _T("Preserve calibration fail [%s]."), strErrMsg.operator LPCTSTR() );
							_tcscpy(pbj->szErrorMsg,strErrMsg);
							return E_FAIL;
						}						
					}
					continue;					
				}
				else if(_tcscmp(pNvBkpItem->szItemName,_T("TD_Calibration"))==0)
				{					
#ifndef _SPUPGRADE
					if(pNvBkpItem->wIsBackup == 1)
#endif
					{
						DWORD dwErrorRCID = XTDCaliPreserve( (LPBYTE)pDestCode, dwCodeSize, lpReadBuffer, dwReadSize, 
							                                 bReplace,bContinue);
						if(  dwErrorRCID !=0  )
						{
							strErrMsg = GetErrorDesc( dwErrorRCID );
							strErrMsg.Format( _T("Preserve TD calibration fail [%s]."), strErrMsg.operator LPCTSTR() );
							_tcscpy(pbj->szErrorMsg,strErrMsg);
							return E_FAIL;
						}
					}
				}
				else if(_tcscmp(pNvBkpItem->szItemName,_T("LTE_Calibration"))==0)
				{					
#ifndef _SPUPGRADE
					if(pNvBkpItem->wIsBackup == 1)
#endif
					{
						//结构还不稳定，暂时采用简单的全备份方式
						WORD wNVItemID = (WORD)(pNvBkpItem->dwID&0xFFFF);
						if(wNVItemID == 0xFFFF)
						{
							wNVItemID = LTE_CALI_ITEM_ID;
						}

						DWORD dwErrorRCID = XPreserveNVItem( wNVItemID,(LPBYTE)pDestCode, dwCodeSize, 
							lpReadBuffer, dwReadSize, bReplace, bContinue);
						if(  dwErrorRCID !=0  )
						{
							strErrMsg = GetErrorDesc( dwErrorRCID );
							strErrMsg.Format( _T("Preserve %s fail [%s]."), pNvBkpItem->szItemName,strErrMsg.operator LPCTSTR() );
							_tcscpy(pbj->szErrorMsg,strErrMsg);
							return E_FAIL;
						}

						/*
						DWORD dwErrorRCID = LTECaliPreserve( (LPBYTE)pDestCode, dwCodeSize, lpReadBuffer, dwReadSize, 
							bReplace,bContinue);
						if(  dwErrorRCID !=0  )
						{
							strErrMsg = GetErrorDesc( dwErrorRCID );
							strErrMsg.Format( _T("Preserve LTE calibration fail [%s]."), strErrMsg.operator LPCTSTR() );
							_tcscpy(pbj->szErrorMsg,strErrMsg);
							return E_FAIL;
						}
						*/
					}
				}
				else if(_tcscmp(pNvBkpItem->szItemName,_T("IMEI"))==0)
				{
#ifndef _SPUPGRADE
					if(pNvBkpItem->wIsBackup == 1)
#endif
					{
						int nIMEIIdx = 0;
						DWORD dwErrorRCID = XPreserveIMEIs(  &pThis->m_aIMEIID, (LPBYTE)pDestCode,dwCodeSize,
															  lpReadBuffer, dwReadSize, nIMEIIdx, bReplace, bContinue);
						if(  dwErrorRCID !=0  )
						{
							strErrMsg = GetErrorDesc( dwErrorRCID );
							strErrMsg.Format( _T("Preserve IMEI%d fail [%s]."), nIMEIIdx+1,strErrMsg.operator LPCTSTR() );
							_tcscpy(pbj->szErrorMsg,strErrMsg);
							return E_FAIL;
						}	
					}
					continue;					
				}
				else
				{
#ifdef _SPUPGRADE
					if(_tcscmp(pNvBkpItem->szItemName,_T("MMITest"))==0 || _tcscmp(pNvBkpItem->szItemName,_T("MMITest Result"))==0)
					{
						bContinue = TRUE;
					}
#endif
					if(pNvBkpItem->wIsBackup == 1 && pNvBkpItem->dwID != 0xFFFFFFFF)
					{						
						DWORD dwErrorRCID = XPreserveNVItem( (WORD)(pNvBkpItem->dwID),(LPBYTE)pDestCode, dwCodeSize, 
															 lpReadBuffer, dwReadSize, bReplace, bContinue);
						if(  dwErrorRCID !=0  )
						{
							strErrMsg = GetErrorDesc( dwErrorRCID );
							strErrMsg.Format( _T("Preserve %s fail [%s]."), pNvBkpItem->szItemName,strErrMsg.operator LPCTSTR() );
							_tcscpy(pbj->szErrorMsg,strErrMsg);
							return E_FAIL;
						}	
					}
					continue;	
				}
			}
			/************************************************************************/
			/* backup multi-language item, uint8 type and need not consider endian  */
			/************************************************************************/
			WORD wLangID = pThis->m_sheetSettings.GetLangNVItemID();
			if(pThis->m_sheetSettings.IsBackupLang() && wLangID != 0xFFFF)
			{					
				DWORD dwErrorRCID = XPreserveNVItem( wLangID,(LPBYTE)pDestCode, dwCodeSize, 
													 lpReadBuffer, dwReadSize);
				if(  dwErrorRCID !=0  )
				{
					strErrMsg = GetErrorDesc( dwErrorRCID );
					strErrMsg.Format( _T("Preserve multi-language fail [%s]."), strErrMsg.operator LPCTSTR() );
					_tcscpy(pbj->szErrorMsg,strErrMsg);
					return E_FAIL;
				}
			}
			
			if( _tcsnicmp( OLE2T( cbstrFileType), _T("NV"), 2 ) == 0 )
			{
				if(pThis->m_sheetSettings.IsNvBaseChange())
				{
					PFILE_INFO_T pNvFileInfo = NULL;
					pThis->m_sheetSettings.GetFileInfo(OLE2T(cbstrFileID),(LPDWORD)&pNvFileInfo);
					if(pNvFileInfo!=NULL)
					{
						int nPos = pThis->m_sheetSettings.GetNvNewBasePosition();
						if(nPos<0 || nPos>=MAX_BLOCK_NUM)
						{
							strErrMsg.Format(_T("The position index [%d] of the new Nv address is incorrect."),nPos);
							_tcscpy(pbj->szErrorMsg,strErrMsg);
							return E_FAIL;
						}
						
						DWORD dwNewBase = pNvFileInfo->arrBlock[nPos].dwBase;
						pBMFile = (IBMFile*)pBMFileInterface;
						pBMFile->SetCurCodeBase(dwNewBase);
					}			
				}
				
				// backup to memory				
				if(pbj->pBuf == NULL || pbj->dwBufSize == 0)
				{
					pbj->pBuf = new BYTE[dwCodeSize];
					pbj->dwBufSize = dwCodeSize;
				}
				memcpy(pbj->pBuf,pDestCode,dwCodeSize);				
			}

			/* record IMEI */
			{
				DWORD dwIMEIOffset=0;
				DWORD dwIMEILen =0;						
				_TCHAR szWIMEI[100]={0};
				char szIMEI[100]={0};
				BOOL bBigEndian = TRUE;
				if (XFindNVOffsetEx(GSM_IMEI_ITEM_ID,(LPBYTE)pDestCode,dwCodeSize,dwIMEIOffset,dwIMEILen,bBigEndian,FALSE))
				{		
					BYTE bIMEI[100] ={0};
					memcpy(bIMEI,((LPBYTE)pDestCode) + dwIMEIOffset,dwIMEILen);							
					pThis->BCDToWString(bIMEI,dwIMEILen,szWIMEI,100);
#if defined(_UNICODE) || defined(UNICODE)
					WideCharToMultiByte(CP_ACP,0,szWIMEI,100,szIMEI,100,NULL,NULL);
#else
					strcpy(szIMEI,szWIMEI);
#endif
					if(strlen(szIMEI) <= X_SN_LEN)
					{
						strcpy(pbj->szIMEI,szIMEI);
					}
					else
					{
						memcpy(pbj->szIMEI,szIMEI,X_SN_LEN);
					}
				}
			}
			
			if(pThis->m_sheetSettings.IsNVSaveToLocal())
			{
				if(!pThis->SaveNVToLocal(pbj,(BYTE*)pDestCode,dwCodeSize))
					return E_FAIL;
			} 
			
        }
		else
		{	
			return E_FAIL;		
		}
    }
	
	/************************************************************************/
	/* read nv 3 times if nv readed from phone is not correct               */
	/* check it by the nv memory in _BMOBJ.pBuf,if they are same            */
	/* it is validate                                                       */
	/************************************************************************/	
	if( _tcsicmp(OLE2T(cbstrFileID),_T("_CHECK_NV_")) == 0
		&& _tcsnicmp( OLE2T( cbstrFileType), _T("CHECK_NV"), 2 ) == 0 &&
		_tcsicmp( OLE2T(cbstrOperationType),_T("ReadFlash")) == 0 )
	{
		BOOL bValidate = TRUE;
		if(pbj->pBuf == NULL || pbj->dwBufSize == 0)
		{
			return E_FAIL;
		}

		hr = pThis->m_pBMAF->BMAF_GetBootModeObjInfo(dwOprCookie,(LPDWORD)&pStruct);		
		if( SUCCEEDED(hr) )
        {
            LPBYTE lpReadBuffer = NULL; 
            DWORD dwReadSize = 0;
            //DWORD dwCodeSize = 0;
            IBMOprBuffer *pBuffer;
            hr = pStruct->pSnapin->QueryInterface( IID_IBMOprBuffer, (LPVOID*)&pBuffer);
            if( SUCCEEDED(hr) )
            {
                lpReadBuffer = pBuffer->GetReadBuffer();
                dwReadSize = pBuffer->GetReadBufferSize();
            }
			else
			{
				return E_FAIL;
			}

            if( lpReadBuffer == NULL )
            {
                return E_FAIL;
            }       

			BOOL _bBigEndian = TRUE;
			XCheckNVStructEx(lpReadBuffer,dwReadSize,_bBigEndian,TRUE);
			            
			// omit the first 2 crc bytes and 2 timestamp bytes.	
			if(memcmp(pbj->pBuf+4,lpReadBuffer+4,pbj->dwBufSize-4) != 0)
			{
				bValidate = FALSE;
			}

// #if 0
// 			FILE *pFile = fopen("D:\\nv1.bin","wb");
// 			fwrite(pbj->pBuf,1,pbj->dwBufSize,pFile);
// 			fclose(pFile);
// 
// 			pFile = fopen("D:\\nv2.bin","wb");
// 			fwrite(lpReadBuffer,1,pbj->dwBufSize,pFile);
// 			fclose(pFile);
// 
// #endif

			IBMFile* pBMFile = (IBMFile*)pBMFileInterface;
			pBMFile->SetCurCode((const LPVOID)(pbj->pBuf),pbj->dwBufSize);
			
			if(pThis->m_sheetSettings.IsNvBaseChange())
			{
				PFILE_INFO_T pNvFileInfo = NULL;
				pThis->m_sheetSettings.GetFileInfo(OLE2T(cbstrFileID),(LPDWORD)&pNvFileInfo);
				if(pNvFileInfo != NULL)
				{
					int nPos = pThis->m_sheetSettings.GetNvNewBasePosition();
					if(nPos<0 || nPos>=MAX_BLOCK_NUM)
					{
						strErrMsg.Format(_T("The position index [%d] of the new Nv address is incorrect."),nPos);
						_tcscpy(pbj->szErrorMsg,strErrMsg);
						return E_FAIL;
					}
					
					DWORD dwNewBase = pNvFileInfo->arrBlock[nPos].dwBase;
					pBMFile->SetCurCodeBase(dwNewBase);
				}		
			}

// #ifdef _DEBUG
//			static int ii= 0;
//			if(ii==0)
//			{
//				ii++;
// 				return E_FAIL;
//			}
// #endif

			if(bValidate)
			{
				return S_OK;
			}			
		}		
		return E_FAIL;
	}

	if( _tcsicmp( OLE2T( cbstrFileType), _T("UDISK_IMG")) == 0 &&
        _tcsicmp( OLE2T(cbstrOperationType),_T("ReadSectorSize")) == 0 )
    {
        hr = pThis->m_pBMAF->BMAF_GetBootModeObjInfo(dwOprCookie,(LPDWORD)&pStruct);
		
        if( SUCCEEDED(hr) )
        {
            LPBYTE lpReadBuffer = NULL; 
            DWORD dwReadSize = 0;
            IBMOprBuffer *pBuffer;
            hr = pStruct->pSnapin->QueryInterface( IID_IBMOprBuffer, (LPVOID*)&pBuffer);
            if( SUCCEEDED(hr) )
            {
                lpReadBuffer = pBuffer->GetReadBuffer();
                dwReadSize = pBuffer->GetReadBufferSize();
            }
            if( lpReadBuffer == NULL || dwReadSize < sizeof( DWORD) )
            {
                return E_FAIL;
            }
			
            DWORD dwSoruceValue, dwSectorSize;            
            dwSoruceValue =  *(DWORD *)&lpReadBuffer[ 0 ];    
            dwSectorSize   = 0;
            CONVERT_INT( dwSoruceValue, dwSectorSize);  
	
			EXT_IMG_INFO_PTR pDiskImg = NULL;
			if(pThis->m_mapUDiskIMg.Lookup(dwSectorSize,pDiskImg))
			{
				IBMFile* pBMFile = (IBMFile*)pBMFileInterface;
				if(!pBMFile->SetCurCode(pDiskImg->pBuf,pDiskImg->dwSize))
				{
					strErrMsg.Format(_T("Set udisk_img buffer to bootmodeplatform failed.[SetctorSize:%d]"),dwSectorSize);
					_tcscpy(pbj->szErrorMsg,strErrMsg);
					return E_FAIL;
				}
			}
			else
			{
				strErrMsg.Format(_T("Can not find udisk_img matching the sector size [%d]."),dwSectorSize);
				_tcscpy(pbj->szErrorMsg,strErrMsg);
				return E_FAIL;
			}
        }
        return S_OK;
    }


	if( _tcsicmp( OLE2T( cbstrFileType), _T("CHIP_DSP")) == 0 &&
        _tcsicmp( OLE2T(cbstrOperationType),_T("ReadChipType")) == 0 )
    {
        hr = pThis->m_pBMAF->BMAF_GetBootModeObjInfo(dwOprCookie,(LPDWORD)&pStruct);
		
        if( SUCCEEDED(hr) )
        {
            LPBYTE lpReadBuffer = NULL; 
            DWORD dwReadSize = 0;
            IBMOprBuffer *pBuffer;
            hr = pStruct->pSnapin->QueryInterface( IID_IBMOprBuffer, (LPVOID*)&pBuffer);
            if( SUCCEEDED(hr) )
            {
                lpReadBuffer = pBuffer->GetReadBuffer();
                dwReadSize = pBuffer->GetReadBufferSize();
            }
            if( lpReadBuffer == NULL || dwReadSize < sizeof( DWORD) )
            {
                return E_FAIL;
            }
			
            DWORD dwSoruceValue=0;
			DWORD dwChipID =0;            
            dwSoruceValue =  *(DWORD *)&lpReadBuffer[ 0 ];       
            CONVERT_INT( dwSoruceValue, dwChipID);  
	
			EXT_IMG_INFO_PTR pChipDsp = NULL;
			if(pThis->m_mapChipDsp.Lookup(dwChipID,pChipDsp))
			{
				IBMFile* pBMFile = (IBMFile*)pBMFileInterface;
				if(!pBMFile->SetCurCode(pChipDsp->pBuf,pChipDsp->dwSize))
				{
					strErrMsg.Format(_T("Set dsp buffer to bootmodeplatform failed.[ChipID:0x%08X]."),dwChipID);
					_tcscpy(pbj->szErrorMsg,strErrMsg);
					return E_FAIL;
				}
			}
			else
			{
				strErrMsg.Format(_T("Can not find DSP file matching the chip ID[0x%08X]."),dwChipID);
				_tcscpy(pbj->szErrorMsg,strErrMsg);
				return E_FAIL;
			}
        }
        return S_OK;
    }

	
	if( _tcsnicmp( strFileID, _T("_BKF_"), 5) == 0 &&
		_tcsicmp( strFileID, _T("_BKF_NV")) != 0 &&
		_tcsicmp( OLE2T( cbstrFileType ), _T("ReadFlash")) == 0 &&
		_tcsicmp( OLE2T( cbstrOperationType ),_T("ReadFlash")) == 0 )
    {
		CString strID = strFileID.Right(strFileID.GetLength()-5);
		int nID = pThis->m_sheetSettings.IsBackupFile(strID);
		if(nID != -1)
		{
			hr = pThis->m_pBMAF->BMAF_GetBootModeObjInfo(dwOprCookie,(LPDWORD)&pStruct);
			
			if( SUCCEEDED(hr) )
			{
				LPBYTE lpReadBuffer = NULL; 
				DWORD dwReadSize = 0;
				IBMOprBuffer *pBuffer;
				hr = pStruct->pSnapin->QueryInterface( IID_IBMOprBuffer, (LPVOID*)&pBuffer);
				if( SUCCEEDED(hr) )
				{
					lpReadBuffer = pBuffer->GetReadBuffer();
					dwReadSize = pBuffer->GetReadBufferSize();
				}
				if( lpReadBuffer == NULL )
				{
					return E_FAIL;
				}
				pbj->tFileBackup[nID].dwSize = dwReadSize;
				pbj->tFileBackup[nID].pBuf = new BYTE[dwReadSize];
				memcpy(pbj->tFileBackup[nID].pBuf,lpReadBuffer,dwReadSize);

			}
			return S_OK;
		}
    }
	
	
    return S_OK;
}

STDMETHODIMP CMainFrame::XBMOprObserver::OnFileOprStart( THIS_ DWORD dwOprCookie,
														const BSTR cbstrFileID,
														const BSTR cbstrFileType, 
														DWORD pBMFileInterface )
{
    UNUSED_ALWAYS( cbstrFileType );
    METHOD_PROLOGUE(CMainFrame, BMOprObserver)
		USES_CONVERSION;
	
    if( _tcsicmp( OLE2T( cbstrFileType), _T("NV")) == 0 )
    {
        InterlockedIncrement( &(pThis->m_lStartNVRef) );
    }
	
    ::PostMessage( ((CDLoaderView*)pThis->GetActiveView())->GetSafeHwnd(), 
		BM_FILE_BEGIN, dwOprCookie, 0 );
	
	//HRESULT hr = NULL;
	//PBOOTMODEOBJ_T pStruct = NULL;
	PFILE_INFO_T pFileInfo = NULL;
	IBMFile* pBMFile = (IBMFile*)pBMFileInterface;

	CString strErrMsg = _T("");
	_BMOBJ * pbj = NULL;
	if( !pThis->m_mapBMObj.Lookup(dwOprCookie,pbj) ||  NULL == pbj )	
	{
		return E_FAIL;
	}
	
	CString strFileID = OLE2T(cbstrFileID);

	if( pThis->m_sheetSettings.IsEnableMultiFileBuf() && pBMFile->GetCurIsChangeCode() )
	{
		if(strFileID.CompareNoCase(_T("FDL")) != 0  && strFileID.CompareNoCase(_T("FDL2")) != 0)
		{
			pFileInfo = NULL;
			pThis->m_sheetSettings.GetFileInfo(strFileID,(LPDWORD)&pFileInfo);
			if(pFileInfo != NULL && pFileInfo->dwFlag != 0)
			{
				EXT_IMG_INFO_PTR pImg = NULL;
				pImg = pThis->m_mapMultiFileBuf[std::make_pair(pbj->dwChipID,strFileID)];
				if(pImg != NULL)
				{
					pBMFile->SetCurCode(pImg->pBuf,pImg->dwSize);
				}

				if(_tcsicmp(OLE2T(cbstrFileID),_T("NV")) == 0 && pThis->m_sheetSettings.IsNvBaseChange())
				{
					PFILE_INFO_T pNvFileInfo = NULL;
					pThis->m_sheetSettings.GetFileInfo(OLE2T(cbstrFileID),(LPDWORD)&pNvFileInfo);
					if(pNvFileInfo != NULL)
					{
						int nPos = pThis->m_sheetSettings.GetNvNewBasePosition();
						if(nPos<0 || nPos>=MAX_BLOCK_NUM)
						{
							strErrMsg.Format(_T("The position index [%d] of the new Nv address is incorrect."),nPos);
							_tcscpy(pbj->szErrorMsg,strErrMsg);
							return E_FAIL;
						}
						
						DWORD dwNewBase = pNvFileInfo->arrBlock[nPos].dwBase;
						//pBMFile = (IBMFile*)pBMFileInterface;
						pBMFile->SetCurCodeBase(dwNewBase);
					}				
				}
			}			
		}
		
	}

	if( pThis->m_sheetSettings.IsEnablePageOobFile())
	{
		pFileInfo = NULL;
		pThis->m_sheetSettings.GetFileInfo(strFileID,(LPDWORD)&pFileInfo);

		if( pFileInfo != NULL &&
			(_tcsicmp(pFileInfo->szType,_T("PAGE"))==0 ||
			_tcsicmp(pFileInfo->szType,_T("PAGE_OOB"))==0) &&
			pFileInfo->dwFlag != 0)		
		{			
			CString strKey = pbj->szMcpInfo;

			if(strKey.IsEmpty())
			{
				strErrMsg.Format(_T("Not read MCP type yet!"));
				_tcscpy(pbj->szErrorMsg,strErrMsg);
				return E_FAIL;
			}

			DWORD dwMaxLen = pbj->dwPage;
			if(_tcsicmp(pFileInfo->szType,_T("PAGE"))==0)
			{
				int nFind = strKey.ReverseFind('-');
				if(nFind == -1)
				{
					strErrMsg.Format(_T("[%s] is not correct!"),strKey);
					_tcscpy(pbj->szErrorMsg,strErrMsg);
					return E_FAIL;
				}
				strKey = strKey.Left(nFind);
			}
			else
			{
				dwMaxLen += pbj->dwOob;
			}
			EXT_IMG_INFO_PTR pImg = NULL;
			pImg = pThis->m_mapPageOobFile[std::make_pair(strKey,strFileID)];
			if(pImg != NULL)
			{
				pBMFile->SetCurCode(pImg->pBuf,pImg->dwSize);
				pBMFile->SetCurMaxLength(dwMaxLen);
			}	
			else
			{
				strErrMsg.Format(_T("Not find page/oob [%s] matched file!"),strKey);
				_tcscpy(pbj->szErrorMsg,strErrMsg);
				return E_FAIL;
			}
		}		
	}
	
	if( _tcsicmp(OLE2T(cbstrFileID),_T("NV")) == 0 && !pThis->m_sheetSettings.IsEnableMultiFileBuf() &&
		((pThis->m_sheetSettings.IsReadFlashInFDL2() && _tcsicmp(OLE2T(cbstrFileType),_T("CODE")) == 0) 
		|| pThis->m_sheetSettings.IsHasLang()))
	{
		if(pbj->pBuf == NULL || pbj->dwBufSize == 0)
		{
			return E_FAIL;
		}

		pBMFile->SetCurCode((const LPVOID)(pbj->pBuf),pbj->dwBufSize);		
		
		if(pThis->m_sheetSettings.IsNvBaseChange())
		{
			PFILE_INFO_T pNvFileInfo = NULL;
			pThis->m_sheetSettings.GetFileInfo(OLE2T(cbstrFileID),(LPDWORD)&pNvFileInfo);
			if(pNvFileInfo != NULL)
			{
				int nPos = pThis->m_sheetSettings.GetNvNewBasePosition();
				if(nPos<0 || nPos>=MAX_BLOCK_NUM)
				{
					strErrMsg.Format(_T("The position index [%d] of the new Nv address is incorrect."),nPos);
					_tcscpy(pbj->szErrorMsg,strErrMsg);
					return E_FAIL;
				}
				
				DWORD dwNewBase = pNvFileInfo->arrBlock[nPos].dwBase;
				//pBMFile = (IBMFile*)pBMFileInterface;
				pBMFile->SetCurCodeBase(dwNewBase);
			}
	
		}
	}
	
	int nID = pThis->m_sheetSettings.IsBackupFile(strFileID);
	if(nID != -1)
	{
		pBMFile->SetCurCode((const LPVOID)(pbj->tFileBackup[nID].pBuf),pbj->tFileBackup[nID].dwSize);
	}
	
	if( _tcsicmp(OLE2T(cbstrFileID),_T("_CHECK_NV_")) == 0 )
	{
		if(pbj->pBuf == NULL || pbj->dwBufSize == 0)
		{
			return E_FAIL;
		}
		
		pBMFile->SetCurCode((const LPVOID)(pbj->pBuf),pbj->dwBufSize);
	}	

    return S_OK;
}
														
STDMETHODIMP CMainFrame::XBMOprObserver::OnFileOprEnd(    THIS_ DWORD dwOprCookie,
													  const BSTR cbstrFileID,
													  const BSTR cbstrFileType, 
													  DWORD dwResult )
{
    UNUSED_ALWAYS( dwOprCookie );
	UNUSED_ALWAYS( cbstrFileID );
    UNUSED_ALWAYS( cbstrFileType );
    UNUSED_ALWAYS( dwResult );
    METHOD_PROLOGUE(CMainFrame, BMOprObserver)
		USES_CONVERSION;
    if( _tcsicmp( OLE2T( cbstrFileType), _T("NV")) == 0 )
    {
        InterlockedDecrement( &( pThis->m_lStartNVRef ) );
    }
    return S_OK;
}

STDMETHODIMP CMainFrame::XBMOprObserver::OnEnd(  THIS_ DWORD dwOprCookie, 
											   DWORD dwResult )
{
    UNUSED_ALWAYS( dwOprCookie );
    UNUSED_ALWAYS( dwResult );
    METHOD_PROLOGUE(CMainFrame, BMOprObserver)

	_BMOBJ * pbj = NULL;
	if( !pThis->m_mapBMObj.Lookup(dwOprCookie,pbj) ||  NULL == pbj )	
	{
		return E_FAIL;
	}

	BOOL bSucceed = TRUE;

	LPARAM lParam = NULL;
    if( dwResult != OPR_SUCCESS )
    {
		bSucceed = FALSE;
        lParam = (LPARAM)(LPCTSTR)pbj->szErrorMsg;
		if(pThis->m_bShowFailedMsgbox)
		{
			::PostMessage(pThis->GetSafeHwnd(),WM_WARN_MESSAGEBOX,dwOprCookie,0);
		}
    }

    ::PostMessage( ((CDLoaderView*)pThis->GetActiveView())->GetSafeHwnd(), 
		BM_END, dwOprCookie, lParam );


	pThis->DoReport(dwOprCookie,bSucceed);    
	
    HRESULT hr;
    PBOOTMODEOBJ_T pStruct = NULL;	
	hr = pThis->m_pBMAF->BMAF_GetBootModeObjInfo(dwOprCookie,(LPDWORD)&pStruct);
	//If the port is "USB" regarded (may be not), or not wait for next chip
	//need stop the port to close the channel, prevent for "Blue screen"
	if(g_theApp.m_dwWaitTimeForNextChip == 0 ) 
	{
		pStruct->bStop = TRUE;	
	}

	if( (g_theApp.m_bClosePortFlag && pbj->dwIsUart != 1) || g_theApp.m_bManual )
	{
		pStruct->bStop = TRUE;	
		::PostMessage( pThis->GetSafeHwnd(), WM_STOP_ONE_PORT, dwOprCookie, 0 );
	}   


	if(g_theApp.m_bScriptCtrl)
	{
		::PostMessage( pThis->GetSafeHwnd(), WM_COMMAND, ID_STOP, 0 );
		::PostMessage( pThis->GetSafeHwnd(), WM_CLOSE, 0, 0 );
	}


    return S_OK;
}

STDMETHODIMP CMainFrame::XBMOprObserver::OnStart(  THIS_ DWORD dwOprCookie, 
												 DWORD dwResult )
{
    UNUSED_ALWAYS( dwOprCookie );
    UNUSED_ALWAYS( dwResult );
    METHOD_PROLOGUE(CMainFrame, BMOprObserver)

	::PostMessage( ((CDLoaderView*)pThis->GetActiveView())->GetSafeHwnd(), 
		BM_BEGIN, dwOprCookie, 0 );
		
	//HRESULT hr = NULL;
	//PBOOTMODEOBJ_T pStruct = NULL;

#ifndef _SPUPGRADE
	if(!g_theApp.m_bManual)
	{
		if (OPR_SUCCESS != dwResult)
		{
			return S_OK;
		}
		
		PORT_DATA *pPortData = NULL;
		HANDLE hEvent = NULL;
		BOOL bFound = pThis->m_mapPortData.Lookup(dwOprCookie, pPortData);
		if( !bFound )
		{
			// There is no event associate with the port
			pPortData = pThis->CreatePortData( dwOprCookie );		
		}	
		if(pThis->m_bNeedPhaseCheck)
		{
			hEvent = pPortData->hSNEvent;		
			ResetEvent( hEvent );
			::PostMessage( pThis->GetActiveView()->GetSafeHwnd(), WM_REQUIRE_SN, dwOprCookie, (LPARAM)pPortData);
			WaitForSingleObject(hEvent, INFINITE);
		}
	}
#endif
    
    return S_OK;
}

STDMETHODIMP CMainFrame::XBMOprObserver::OnFilePrepare(THIS_ DWORD dwOprCookie,
													   const BSTR bstrProduct,
													   const BSTR bstrFileName,
													   DWORD lpFileInfo,
													   /*[out]*/ DWORD      pBMFileInfoArr,
													   /*[out]*/ LPDWORD    lpBMFileInfoCount,
													   /*[out]*/ LPDWORD    lpdwFlag)
{
	UNUSED_ALWAYS(bstrProduct);
	UNUSED_ALWAYS(dwOprCookie);
	METHOD_PROLOGUE(CMainFrame, BMOprObserver)
	USES_CONVERSION;

	CString strErrMsg = _T("");
	_BMOBJ * pbj = NULL;
	if( !pThis->m_mapBMObj.Lookup(dwOprCookie,pbj) ||  NULL == pbj )	
	{
		return E_FAIL;
	}
	
	*lpdwFlag = 0; //不做处理
	PBMFileInfo pBMFileInfo = (PBMFileInfo)pBMFileInfoArr;
	if(lpFileInfo == NULL)
	{
		CUIntArray agFlashOprFiles;
		int nFlashOprCount = pThis->m_sheetSettings.GetFlashOprFileInfo(agFlashOprFiles);
		if(nFlashOprCount > MAX_RET_FILE_NUM )
		{
			strErrMsg.Format(_T("Too much flash oprations!"));
			_tcscpy(pbj->szErrorMsg,strErrMsg);
			return E_FAIL;
		}
		for(int i= 0; i< nFlashOprCount; i++)
		{
			PFILE_INFO_T pFileInfo = (PFILE_INFO_T)agFlashOprFiles[i];
			PBMFileInfo pBMFileInfoCur = pBMFileInfo+i;

			//hognliang.xin 2010-9-30
			//BMPlatform to deal with file name appending with the COM Number.			

			_tcscpy(pBMFileInfoCur->szFileID,pFileInfo->szID);
			_tcscpy(pBMFileInfoCur->szFileType, pFileInfo->szType);
			_tcscpy(pBMFileInfoCur->szFileName,pFileInfo->szFilePath);
			pBMFileInfoCur->dwBase = pFileInfo->arrBlock[0].dwBase;
			pBMFileInfoCur->dwOprSize = pFileInfo->arrBlock[0].dwSize;
			pBMFileInfoCur->bChangeCode = FALSE;
			if(_tcsicmp(pFileInfo->szType,_T("CODE")) == 0)
			{
				pBMFileInfoCur->bLoadCodeFromFile = TRUE;
			}
			else
			{
				pBMFileInfoCur->bLoadCodeFromFile = FALSE;
			}
			

			CString strID = pFileInfo->szID;
			((CDLoaderView *)(pThis->GetActiveView()))->AddStepDescription(strID);
			
		}

		if((!pThis->m_sheetSettings.IsNandFlash()) &&
			pThis->m_sheetSettings.IsBackupNV() &&
			pThis->m_sheetSettings.IsNeedCheckNV())
		{
			if((nFlashOprCount+1) > MAX_RET_FILE_NUM)
			{
				strErrMsg.Format(_T("Too much flash oprations!"));
				_tcscpy(pbj->szErrorMsg,strErrMsg);
				return E_FAIL;
			}			
			
			PFILE_INFO_T pNVFileInfo = NULL;
			if( pThis->m_sheetSettings.GetFileInfo(_T("NV"),(LPDWORD)&pNVFileInfo) != -1 &&
				pNVFileInfo != NULL)
			{
				PBMFileInfo pBMFileInfoCur = pBMFileInfo+nFlashOprCount;
				_tcscpy(pBMFileInfoCur->szFileID,_T("_CHECK_NV_"));
				_tcscpy(pBMFileInfoCur->szFileType, _T("CHECK_NV"));	
				pBMFileInfoCur->dwBase = pNVFileInfo->arrBlock[0].dwBase;
				pBMFileInfoCur->dwOprSize = pThis->m_dwMaxNVLength;
				pBMFileInfoCur->bChangeCode = TRUE;
				pBMFileInfoCur->bLoadCodeFromFile = FALSE;
				
				CString strID = pBMFileInfoCur->szFileID;
				((CDLoaderView *)(pThis->GetActiveView()))->AddStepDescription(strID);
				
				nFlashOprCount++;
			}			
		}

		if( pThis->m_sheetSettings.IsEraseAll())
		{
			if((nFlashOprCount+1) > MAX_RET_FILE_NUM)
			{
				strErrMsg.Format(_T("Too much flash oprations!"));
				_tcscpy(pbj->szErrorMsg,strErrMsg);
				return E_FAIL;
			}			

			PBMFileInfo pBMFileInfoCur = pBMFileInfo+nFlashOprCount;
			_tcscpy(pBMFileInfoCur->szFileID,_T("ERASE_ALL"));
			_tcscpy(pBMFileInfoCur->szFileType, _T("EraseFlash"));	
			pBMFileInfoCur->dwBase = 0x0;
			pBMFileInfoCur->dwOprSize = 0xFFFFFFFF;
			pBMFileInfoCur->bChangeCode = FALSE;
			pBMFileInfoCur->bLoadCodeFromFile = FALSE;

			CString strID = pBMFileInfoCur->szFileID;
			((CDLoaderView *)(pThis->GetActiveView()))->AddStepDescription(strID);

			nFlashOprCount++;
		}
		
		if( pThis->m_sheetSettings.IsReadMcpType())
		{
			if((nFlashOprCount+1) > MAX_RET_FILE_NUM)
			{
				strErrMsg.Format(_T("Too much flash oprations!"));
				_tcscpy(pbj->szErrorMsg,strErrMsg);
				return E_FAIL;
			}			
			
			PBMFileInfo pBMFileInfoCur = pBMFileInfo+nFlashOprCount;
			_tcscpy(pBMFileInfoCur->szFileID,_T("READ_MCPTYPE"));
			_tcscpy(pBMFileInfoCur->szFileType, _T("READFLASHTYPE"));	
			pBMFileInfoCur->dwBase = 0x0;
			pBMFileInfoCur->dwOprSize = 0x0;
			pBMFileInfoCur->bChangeCode = FALSE;
			pBMFileInfoCur->bLoadCodeFromFile = FALSE;
			
			CString strID = pBMFileInfoCur->szFileID;
			((CDLoaderView *)(pThis->GetActiveView()))->AddStepDescription(strID);
			
			nFlashOprCount++;
		}

		if( pThis->m_sheetSettings.IsReset())
		{
			if((nFlashOprCount+1) > MAX_RET_FILE_NUM)
			{
				strErrMsg.Format(_T("Too much flash oprations!"));
				_tcscpy(pbj->szErrorMsg,strErrMsg);
				return E_FAIL;
			}			
			
			PBMFileInfo pBMFileInfoCur = pBMFileInfo+nFlashOprCount;
			_tcscpy(pBMFileInfoCur->szFileID,_T("_RESET_"));
			_tcscpy(pBMFileInfoCur->szFileType, _T("Reset"));	
			pBMFileInfoCur->dwBase = 0x0;
			pBMFileInfoCur->dwOprSize = 0x0;
			pBMFileInfoCur->bChangeCode = FALSE;
			pBMFileInfoCur->bLoadCodeFromFile = FALSE;
			
			CString strID = pBMFileInfoCur->szFileID;
			((CDLoaderView *)(pThis->GetActiveView()))->AddStepDescription(strID);
			
			nFlashOprCount++;
		}

		*lpdwFlag = 1;
		*lpBMFileInfoCount= nFlashOprCount;
		
		return S_OK;
	}

	PFILE_INFO_T pFileInfo = (PFILE_INFO_T)lpFileInfo;	
	CString strID = pFileInfo->szID;
	CString strFileType = pFileInfo->szType;
	
	if( strFileType.CompareNoCase(_T("MasterImage"))==0 )
	{
		_tcscpy(pBMFileInfo->szFileID,pFileInfo->szID);
        _tcscpy(pBMFileInfo->szFileType, _T("CODE"));
		_tcscpy(pBMFileInfo->szFileName,OLE2T(bstrFileName));
		pBMFileInfo->dwBase = pFileInfo->arrBlock[0].dwBase;
		IMAGE_PARAM imageparam;
		pBMFileInfo->bLoadCodeFromFile = FALSE;
		memset( &imageparam,0,sizeof( IMAGE_PARAM ) );
		_tcscpy( imageparam.szPath,OLE2T(bstrFileName) );
		CMasterImgGen mig;
		pBMFileInfo->lpCode = mig.MakeMasterImageSingle( &pBMFileInfo->dwCodeSize,1,&imageparam,pThis->m_sheetSettings.GetFlashPageType() );
		
		if(pBMFileInfo->lpCode == NULL)
		{
			strErrMsg.Format(_T("Make master image faile! [%s]"),pFileInfo->szID);
			_tcscpy(pbj->szErrorMsg,strErrMsg);
			return E_FAIL;
		}
		pThis->m_pMasterImg = pBMFileInfo->lpCode;

		if(pThis->m_sheetSettings.IsEnableMultiFileBuf())
		{
			pBMFileInfo->bChangeCode = TRUE;
		}
		
		*lpdwFlag = 1;
		*lpBMFileInfoCount =1;	
		strID = pFileInfo->szID;
		((CDLoaderView *)(pThis->GetActiveView()))->AddStepDescription(strID);
	}	
	
    if( strID.CompareNoCase(_T("NV"))==0 )
	{
		_tcscpy(pBMFileInfo->szFileName,OLE2T(bstrFileName));
		_tcscpy(pBMFileInfo->szFileID,_T("NV"));

		// need to calculate the CRC16 and add to the head of NV
		// so the bChangeCode must be TRUE;
		pBMFileInfo->bChangeCode = TRUE;

		if(pThis->m_sheetSettings.IsReadFlashInFDL2())
		{
			//pBMFileInfo->bChangeCode = FALSE;
			_tcscpy(pBMFileInfo->szFileType, _T("CODE"));
		}
		else
		{
			//pBMFileInfo->bChangeCode = pThis->m_sheetSettings.IsBackupNV();
			if(pThis->m_sheetSettings.IsBackupNV()) // backup NV 4
				_tcscpy(pBMFileInfo->szFileType, pFileInfo->szType); // type is "NV" or "NV_NAND"
			else
				_tcscpy(pBMFileInfo->szFileType, _T("CODE"));
		}		
		pBMFileInfo->bLoadCodeFromFile = TRUE;
		pBMFileInfo->dwBase = pFileInfo->arrBlock[0].dwBase;
		pBMFileInfo->dwOprSize = pThis->m_dwMaxNVLength;		
		
		strID = pFileInfo->szID;
		((CDLoaderView *)(pThis->GetActiveView()))->AddStepDescription(strID);

		int nInfoCount = 1;
		
		if(pThis->m_sheetSettings.IsNVOrgDownload())
		{
			int nPos = pThis->m_sheetSettings.GetNVOrgBasePosition();
			if(nPos<=0 || nPos >= 5)
			{
				strErrMsg.Format(_T("NV orignal base position [%d] is not correct,must be [1-4]!\nPlease check the production configure file!"),nPos);
				_tcscpy(pbj->szErrorMsg,strErrMsg);
				return E_FAIL;
			}
			PBMFileInfo pBMFileInfo2 = pBMFileInfo + nInfoCount;
			_tcscpy(pBMFileInfo2->szFileName,OLE2T(bstrFileName));
			pBMFileInfo2->bLoadCodeFromFile = TRUE;
			pBMFileInfo2->bChangeCode = FALSE;
			_tcscpy(pBMFileInfo2->szFileType, _T("CODE")); 
			_tcscpy(pBMFileInfo2->szFileID,_T("NVOriginal"));
			pBMFileInfo2->dwBase = pFileInfo->arrBlock[nPos].dwBase;
			CString strDes = _T("NVOriginal");
			((CDLoaderView *)(pThis->GetActiveView()))->AddStepDescription(strDes);
			nInfoCount++;
		}

		*lpdwFlag = 1;
		*lpBMFileInfoCount =nInfoCount;
	}
	
	if(strID.CompareNoCase(_T("FDL")) == 0 && !pThis->m_sheetSettings.IsNandFlash())
	{		
		// For FDL
		_tcscpy(pBMFileInfo->szFileName,OLE2T(bstrFileName));
		_tcscpy(pBMFileInfo->szFileID,pFileInfo->szID);
		pBMFileInfo->bChangeCode = FALSE;
		pBMFileInfo->bLoadCodeFromFile = TRUE;
		pBMFileInfo->dwBase = pFileInfo->arrBlock[0].dwBase;	
		_tcscpy(pBMFileInfo->szFileType, pFileInfo->szType); 
		((CDLoaderView *)(pThis->GetActiveView()))->AddStepDescription(strID);
		
		int nInfoCount = 1;

		//////////////////////////////////////////////////////////////////////////
		//  read chip ID
		if( pThis->m_sheetSettings.IsEnableMultiFileBuf())
		{
			// add ReadChipType function
			PBMFileInfo pBMFileInfo2 = pBMFileInfo + nInfoCount;
			pBMFileInfo2->bLoadCodeFromFile = FALSE;
			_tcscpy(pBMFileInfo2->szFileType, _T("READ_CHIPID")); 
			_tcscpy(pBMFileInfo2->szFileID,_T("ReadChipID"));
			CString strDes = _T("ReadChipID");
			((CDLoaderView *)(pThis->GetActiveView()))->AddStepDescription(strDes);
			nInfoCount++;			
		}
	
		if( pThis->m_sheetSettings.IsCheckMCPType())
		{
			// add ReadFlashType function
			PBMFileInfo pBMFileInfo2 = pBMFileInfo + nInfoCount;
			pBMFileInfo2->bLoadCodeFromFile = FALSE;
			_tcscpy(pBMFileInfo2->szFileType, _T("CHECK_MCPTYPE")); 
			_tcscpy(pBMFileInfo2->szFileID,_T("CheckMcpType"));
			CString strDes = _T("CheckMcpType");
			((CDLoaderView *)(pThis->GetActiveView()))->AddStepDescription(strDes);
			nInfoCount++;			
		}
/*
		if(pThis->m_sheetSettings.IsNVSaveToLocal() || pThis->m_bDoReport)
		{
			PFILE_INFO_T pPhaseCheckFileInfo = NULL;
			if(pThis->m_sheetSettings.GetFileInfo(_T("PhaseCheck"),(LPDWORD)&pPhaseCheckFileInfo) != -1
				&& pPhaseCheckFileInfo != NULL)
			{
				// add Read SN function for saving NV to local as the name of NV file
				PBMFileInfo pBMFileInfo2 = pBMFileInfo + nInfoCount;
				pBMFileInfo2->bLoadCodeFromFile = FALSE;
				_tcscpy(pBMFileInfo2->szFileType, _T("ReadSN")); 
				_tcscpy(pBMFileInfo2->szFileID,_T("ReadSN"));
				CString strDes = _T("ReadSN");
				((CDLoaderView *)(pThis->GetActiveView()))->AddStepDescription(strDes);				
				

				DWORD dwPhaseCheckSize = pPhaseCheckFileInfo->arrBlock[0].dwSize;
				if( pPhaseCheckFileInfo->arrBlock[0].dwSize == 0 ||
					pPhaseCheckFileInfo->arrBlock[0].dwSize > PRODUCTION_INFO_SIZE)
				{
					dwPhaseCheckSize = PRODUCTION_INFO_SIZE;
				}

				pBMFileInfo2->dwBase = pPhaseCheckFileInfo->arrBlock[0].dwBase;
				pBMFileInfo2->dwOprSize = dwPhaseCheckSize;
				
				nInfoCount++;
			}			
		}		
*/		
		*lpdwFlag = 1;
		*lpBMFileInfoCount =nInfoCount;	
	}	
	
	
	if(strID.CompareNoCase(_T("Fdl2")) == 0 && pThis->m_sheetSettings.IsNandFlash())
	{		
		// For Fdl2
		_tcscpy(pBMFileInfo->szFileName,OLE2T(bstrFileName));
		_tcscpy(pBMFileInfo->szFileID,pFileInfo->szID);
		pBMFileInfo->bChangeCode = FALSE;
		pBMFileInfo->bLoadCodeFromFile = TRUE;
		pBMFileInfo->dwBase = pFileInfo->arrBlock[0].dwBase;
		_tcscpy(pBMFileInfo->szFileType, pFileInfo->szType); 
// 		if(!pThis->m_sheetSettings.IsReadFlashInFDL2())
// 		{
// 			_tcscpy(pBMFileInfo->szFileType, pFileInfo->szType); 
// 		}
// 		else
// 		{
// 			_tcscpy(pBMFileInfo->szFileType, _T("NAND_FDL_OPT")); 
// 		}
		_tcscpy(pBMFileInfo->szFileType, pFileInfo->szType); 

		((CDLoaderView *)(pThis->GetActiveView()))->AddStepDescription(strID);

		int nInfoCount = 1;

		if(pThis->m_sheetSettings.IsReadFlashInFDL2())
		{
			CString strFileID = _T("NV");
			PFILE_INFO_T pBackupFileInfo = NULL;
			if(pThis->m_sheetSettings.GetFileInfo(strFileID,(LPDWORD)&pBackupFileInfo) != -1
				&& pBackupFileInfo != NULL)
			{				
				PBMFileInfo pBMFileInfo2 = pBMFileInfo + nInfoCount;
				pBMFileInfo2->bLoadCodeFromFile = FALSE;
				_tcscpy(pBMFileInfo2->szFileType, _T("ReadFlash"));
				_stprintf(pBMFileInfo2->szFileID,_T("_BKF_%s"),strFileID);
				pBMFileInfo2->dwBase = pBackupFileInfo->arrBlock[0].dwBase;
				pBMFileInfo2->dwOprSize = pThis->m_dwMaxNVLength;

				CString strDes = pBMFileInfo2->szFileID;
				((CDLoaderView *)(pThis->GetActiveView()))->AddStepDescription(strDes);				
				nInfoCount++;
			}
		}

		CStringArray agBackupFileID;
		pThis->m_sheetSettings.GetBackupFiles(agBackupFileID);
		for(int i = 0; i< agBackupFileID.GetSize(); i++)
		{
			CString strFileID = agBackupFileID[i];
			PFILE_INFO_T pBackupFileInfo = NULL;
			if(pThis->m_sheetSettings.GetFileInfo(strFileID,(LPDWORD)&pBackupFileInfo) != -1
				&& pBackupFileInfo != NULL)
			{

				PBMFileInfo pBMFileInfo2 = pBMFileInfo + nInfoCount;
				pBMFileInfo2->bLoadCodeFromFile = FALSE;
				_tcscpy(pBMFileInfo2->szFileType, _T("ReadFlash"));
				_stprintf(pBMFileInfo2->szFileID,_T("_BKF_%s"),strFileID);
				pBMFileInfo2->dwBase = pBackupFileInfo->arrBlock[0].dwBase;
				pBMFileInfo2->dwOprSize = pBackupFileInfo->arrBlock[0].dwSize;
				if( pBMFileInfo2->dwOprSize == 0 )
				{
					if(strFileID.CompareNoCase(_T("PhaseCheck")) == 0)
					{
						pBMFileInfo2->dwOprSize = PRODUCTION_INFO_SIZE;
					}
					else
					{
						strErrMsg.Format(_T("[%s] Backup size is zero!"),pFileInfo->szID);
						_tcscpy(pbj->szErrorMsg,strErrMsg);
						return E_FAIL;
					}					
				}
				CString strDes = pBMFileInfo2->szFileID;
				((CDLoaderView *)(pThis->GetActiveView()))->AddStepDescription(strDes);				
				nInfoCount++;
			}
		}


		if( pThis->m_sheetSettings.IsCheckMCPType() ||
			pThis->m_sheetSettings.IsEnablePageOobFile())
		{
			// add ReadFlashType function
			PBMFileInfo pBMFileInfo2 = pBMFileInfo + nInfoCount;
			pBMFileInfo2->bLoadCodeFromFile = FALSE;
			_tcscpy(pBMFileInfo2->szFileType, _T("CHECK_MCPTYPE"));
			_tcscpy(pBMFileInfo2->szFileID,_T("CheckMcpType"));
			CString strDes = _T("CheckMcpType");
			((CDLoaderView *)(pThis->GetActiveView()))->AddStepDescription(strDes);
					
			nInfoCount++;	
		}		
/*
		if(pThis->m_sheetSettings.IsNVSaveToLocal() || pThis->m_bDoReport)
		{
			PFILE_INFO_T pPhaseCheckFileInfo = NULL;
			if(pThis->m_sheetSettings.GetFileInfo(_T("PhaseCheck"),(LPDWORD)&pPhaseCheckFileInfo) != -1
				&& pPhaseCheckFileInfo != NULL)
			{
				// add Read SN function for saving NV to local as the name of NV file
				PBMFileInfo pBMFileInfo2 = pBMFileInfo + nInfoCount;
				pBMFileInfo2->bLoadCodeFromFile = FALSE;
				_tcscpy(pBMFileInfo2->szFileType, _T("ReadSN")); 
				_tcscpy(pBMFileInfo2->szFileID,_T("ReadSN"));
				CString strDes = _T("ReadSN");
				((CDLoaderView *)(pThis->GetActiveView()))->AddStepDescription(strDes);				
				
				DWORD dwPhaseCheckSize = pPhaseCheckFileInfo->arrBlock[0].dwSize;
				if( pPhaseCheckFileInfo->arrBlock[0].dwSize == 0 ||
					pPhaseCheckFileInfo->arrBlock[0].dwSize > PRODUCTION_INFO_SIZE)
				{
					dwPhaseCheckSize = PRODUCTION_INFO_SIZE;
				}

				pBMFileInfo2->dwBase = pPhaseCheckFileInfo->arrBlock[0].dwBase;
				pBMFileInfo2->dwOprSize = dwPhaseCheckSize;
				
				nInfoCount++;
			}			
		}
*/

		if( pThis->m_sheetSettings.GetRepartitionFlag() == REPAR_STRATEGY_ALWAYS)
		{
			PBMFileInfo pBMFileInfo2 = pBMFileInfo + nInfoCount;
			pBMFileInfo2->bLoadCodeFromFile = FALSE;
			_tcscpy(pBMFileInfo2->szFileType, _T("REPARTITION")); 
			_tcscpy(pBMFileInfo2->szFileID,_T("_REPARTITION_"));
			CString strDes = _T("_REPARTITION_");
			((CDLoaderView *)(pThis->GetActiveView()))->AddStepDescription(strDes);
			nInfoCount++;
		}

		*lpdwFlag = 1;
		*lpBMFileInfoCount =nInfoCount;	
		
	}	

	BOOL bUpgrade = FALSE;

	
#ifdef _SPUPGRADE	
	bUpgrade = TRUE;
#endif 

	if( strID.CompareNoCase(_T("PhaseCheck"))==0 )
	{
		if((pThis->m_bNeedPhaseCheck && !bUpgrade) || pThis->m_sheetSettings.IsBackupFile(_T("PhaseCheck")) != -1)
		{
			_tcscpy(pBMFileInfo->szFileName,OLE2T(bstrFileName));
			_tcscpy(pBMFileInfo->szFileID,_T("PhaseCheck"));
			
			pBMFileInfo->bChangeCode = FALSE;	
			_tcscpy(pBMFileInfo->szFileType, pFileInfo->szType); 
			
			DWORD dwPhaseCheckSize = pFileInfo->arrBlock[0].dwSize;
			if( pFileInfo->arrBlock[0].dwSize == 0 ||
				pFileInfo->arrBlock[0].dwSize > PRODUCTION_INFO_SIZE)
			{
				dwPhaseCheckSize = PRODUCTION_INFO_SIZE;
			}
			
			pBMFileInfo->bLoadCodeFromFile = FALSE;
			pBMFileInfo->dwBase = pFileInfo->arrBlock[0].dwBase;
			pBMFileInfo->dwCodeSize = dwPhaseCheckSize;	
			pBMFileInfo->lpCode = NULL;
			
			*lpdwFlag = 1;
			*lpBMFileInfoCount =1;
			strID = pFileInfo->szID;
			((CDLoaderView *)(pThis->GetActiveView()))->AddStepDescription(strID);
		}
		else
		{
			*lpdwFlag = 1;
			*lpBMFileInfoCount =0;	
		}
	}	
	
	if( strFileType.CompareNoCase(_T("UDISK_IMG"))==0 )
	{
		_tcscpy(pBMFileInfo->szFileName,OLE2T(bstrFileName));
		_tcscpy(pBMFileInfo->szFileID,strID);
		_tcscpy(pBMFileInfo->szFileType,strFileType);	
		pBMFileInfo->bChangeCode = TRUE;		
		pBMFileInfo->bLoadCodeFromFile = TRUE;
		pBMFileInfo->dwBase = pFileInfo->arrBlock[0].dwBase;		
		
		*lpdwFlag = 1;
		*lpBMFileInfoCount =1;
		strID = pFileInfo->szID;
		((CDLoaderView *)(pThis->GetActiveView()))->AddStepDescription(strID);
	}

	if( strFileType.CompareNoCase(_T("PAGE"))==0 ||
		strFileType.CompareNoCase(_T("PAGE_OOB"))==0)
	{
		_tcscpy(pBMFileInfo->szFileName,OLE2T(bstrFileName));
		_tcscpy(pBMFileInfo->szFileID,strID);
		_tcscpy(pBMFileInfo->szFileType,strFileType);	
		pBMFileInfo->bChangeCode = FALSE;		
		pBMFileInfo->bLoadCodeFromFile = FALSE;
		pBMFileInfo->dwBase = pFileInfo->arrBlock[0].dwBase;		
		
		*lpdwFlag = 1;
		*lpBMFileInfoCount =1;
		strID = pFileInfo->szID;
		((CDLoaderView *)(pThis->GetActiveView()))->AddStepDescription(strID);
	}

	if( strID.CompareNoCase(_T("DSPCode")) == 0 )
	{
		_tcscpy(pBMFileInfo->szFileName,OLE2T(bstrFileName));
		_tcscpy(pBMFileInfo->szFileID,strID);
		_tcscpy(pBMFileInfo->szFileType,strFileType);	
		pBMFileInfo->bChangeCode = FALSE;		
		pBMFileInfo->bLoadCodeFromFile = TRUE;
		pBMFileInfo->dwBase = pFileInfo->arrBlock[0].dwBase;	
		
		if(pThis->m_sheetSettings.IsEnableChipDspMap())
		{
			_tcscpy(pBMFileInfo->szFileType,_T("CHIP_DSP"));
			pBMFileInfo->bChangeCode = TRUE;
		}
		if(pThis->m_sheetSettings.IsEnableMultiFileBuf())
		{
			pBMFileInfo->bChangeCode = TRUE;
		}
		
		*lpdwFlag = 1;
		*lpBMFileInfoCount =1;
		strID = pFileInfo->szID;
		((CDLoaderView *)(pThis->GetActiveView()))->AddStepDescription(strID);
	}

    // use for GUI display
	if(*lpdwFlag == 0 )
	{
		strID = pFileInfo->szID;
		((CDLoaderView *)(pThis->GetActiveView()))->AddStepDescription(strID);
	}
	pFileInfo = NULL;
	pBMFileInfo = NULL;
	
	return S_OK;
}

LRESULT CMainFrame::WindowProc(UINT message, WPARAM wParam, LPARAM lParam) 
{
	return CFrameWnd::WindowProc(message, wParam, lParam);

	// TODO: Add your specialized code here and/or call the base class
	if( m_bAutoStart && (message == WM_DEVICECHANGE) )
    {
        PDEV_BROADCAST_HDR pdbh = (PDEV_BROADCAST_HDR)lParam;
		int nSelPort = m_sheetSettings.GetComPort();
        switch(wParam)
        {
        case DBT_DEVICEARRIVAL:
            if( pdbh != NULL && pdbh->dbch_devicetype == DBT_DEVTYP_PORT )
            {
                PDEV_BROADCAST_PORT pdbp = (PDEV_BROADCAST_PORT)lParam;
                _ASSERTE( _tcsstr(pdbp->dbcp_name, _T("COM")) != NULL );
				
				// skip COM
                int nPort = (int)_ttol( pdbp->dbcp_name + 3 ); //lint !e416
                //_ASSERTE( nPort > 0 && nPort < 256 );

				
                Sleep(200);
				if(nSelPort != 0 && nPort != nSelPort)
				{
					break;
				}
				if(g_theApp.m_bFilterPort)
				{
					DWORD dwValue = 0;
					if(g_theApp.m_mapFilterPort.Lookup((DWORD)nPort,dwValue))
					{
						break;
					}
				}
				
                _ASSERTE( m_nFileCount > 0 && m_nFileCount < MAX_FILE_COUNT );
				
#ifndef _DOWNLOAD_FOR_PRODUCTION
                DL_STAGE stage;
                if( ((CDLoaderView*)GetActiveView())->GetStatus( nPort,stage ) )
                {
                    if( DL_FINISH_STAGE != stage && DL_NONE_STAGE != stage && DL_UNPLUGGED_STAGE != stage)
                    {
                        // Last download process is not finished.
                        break;
                    }
                }
#endif
				// The port plugged is regarded as USB
//#if defined(_DOWNLOAD_FOR_PRODUCTION) || defined(_SPUPGRADE)
				if(g_theApp.m_bManual)
				{
					((CDLoaderView*)GetActiveView())->AddProg( nPort, TRUE ); 
					CreatePortData( nPort );
				}
				else
				{
					StartOnePortWork(nPort,FALSE);	
				}
//#else
//                StartOnePortWork(nPort,FALSE);
//#endif
            }
            break;
        case DBT_DEVICEREMOVECOMPLETE:
            if( pdbh != NULL && pdbh->dbch_devicetype == DBT_DEVTYP_PORT )
            {
				PDEV_BROADCAST_PORT pdbp = (PDEV_BROADCAST_PORT)lParam;
                _ASSERTE( _tcsstr(pdbp->dbcp_name, _T("COM")) != NULL );
				
                int nPort = (int)_ttol( pdbp->dbcp_name + 3 );//lint !e416
                _ASSERTE( nPort > 0 && nPort < 256 );

				if(nSelPort != 0 && nPort != nSelPort)
				{
					break;
				}

				if(g_theApp.m_bFilterPort)
				{
					DWORD dwValue = 0;
					if(g_theApp.m_mapFilterPort.Lookup((DWORD)nPort,dwValue))
					{
						break;
					}
				}
				
#ifndef _DOWNLOAD_FOR_PRODUCTION
				DL_STAGE _stage;
                if( ((CDLoaderView*)GetActiveView())->GetStatus( nPort,_stage ) )
                {
                    if( DL_NONE_STAGE == _stage)
                    {
                        // Last download process is not finished.
                        break;
                    }
                }
#endif
				StopOnePortWork((DWORD)nPort,TRUE);
				// CDLoaderView displays [Removed].
				((CDLoaderView*)GetActiveView())->SetResult( nPort,0,NULL,2);

            }
            break;
        default:
            break;
        }
    }
	
	return CFrameWnd::WindowProc(message, wParam, lParam);
}

/* 为了防止线程调用回调函数时使用AfxMessageBox发生阻塞
* 该函数主要在回调函数中使用PostMessage调用
* lpParam为对话框显示的文本字符串指针
*/
void CMainFrame::OnWarnMessageBox(WPARAM wParam,LPARAM lpParam)
{
	//UNUSED_ALWAYS(wParam);
	//ASSERT(lpParam != NULL);
	if(!m_bFailedMsgboxShowed)
	{
		DWORD dwPort = wParam;
		CString strWarnMsg;
		if(lpParam!= NULL)
		{
			strWarnMsg = (LPCTSTR)lpParam;
		}
		else
		{
			strWarnMsg.Format(_T("Port[%d] is failed, please check!!!"),dwPort);
		}
		m_bFailedMsgboxShowed = TRUE;
		//AfxMessageBox(strWarnMsg);
		MessageBox(strWarnMsg,_T("Warning"),MB_OK | MB_ICONWARNING);
		m_bFailedMsgboxShowed = FALSE;
		
	}
}

BOOL CMainFrame::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	UNUSED_ALWAYS(pHelpInfo);
	// TODO: Add your message handler code here and/or call default
	return TRUE;
	//return CFrameWnd::OnHelpInfo(pHelpInfo);
}

void CMainFrame::OnLoadPacket() 
{
	// TODO: Add your command handler code here	
	if(g_theApp.m_bNeedPassword)
	{
		CDlgPassword dlgPW;
		if(dlgPW.DoModal() == IDCANCEL)
		{
			return;
		}
	}

	static _TCHAR BASED_CODE szFilter[] = _T("Packet Files (*.pac)|*.pac|All Files(*.*)|*.*||");
    CString strFile = _T("");
    CFileDialog dlg(TRUE, _T(".pac"), strFile,OFN_NOCHANGEDIR |OFN_HIDEREADONLY | OFN_FILEMUSTEXIST, szFilter, NULL);
	
	if(dlg.DoModal() == IDCANCEL)
		return; 
	
	m_strPacketPath = dlg.GetPathName();

	if(!LoadPac())
	{
		DeletePacTmpDir();
		return;
	}
	
	_TCHAR szFilePath[_MAX_PATH]={0};
    if(GetIniFilePath(szFilePath))
    {
		DWORD dwAttr = GetFileAttributes(szFilePath);
		if(MAXDWORD != dwAttr)
		{
			dwAttr &= ~FILE_ATTRIBUTE_READONLY;
			::SetFileAttributes(szFilePath,dwAttr);
		}
		WritePrivateProfileString(_T("Download_Packet"),_T("packet"),m_strPacketPath,szFilePath);		
		WritePrivateProfileString(_T("GUI"),_T("PacketMode"),_T("1"),szFilePath);
	}
}

void CMainFrame::OnUpdateLoadPacket(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
#if defined( _DOWNLOAD_FOR_PRODUCTION ) || defined(_SPUPGRADE)
	pCmdUI->Enable(!m_bPacketOpen || !m_bStarted); //lint !e730
#else // _DLOADERR
    pCmdUI->Enable(!m_bStarted); //lint !e730
#endif
}
CString CMainFrame::GetSpecConfigFile()
{
	return m_strSpecConfig;
}
void CMainFrame::SetPrdVersion(LPCTSTR lpszSpdVer)
{
	if(lpszSpdVer!= NULL)
	{
		m_wndToolBar.m_stcWnd.SetText(lpszSpdVer);
		m_wndToolBar.m_stcWnd.Invalidate();
	}
}


DWORD CMainFrame::BCDToWString(LPBYTE pBcd,DWORD dwSize,LPTSTR szStr,DWORD dwStrLen)
{
	if(dwStrLen < dwSize * 2 )
		return 0;
	
	_TCHAR szValue[ 4 ] = { 0 };
	
	for(UINT i=0;i<dwSize;i++)
	{
		_stprintf(szValue, _T("%02x"), *pBcd);
		if(i==0)
		{
			szStr[i]=szValue[0];
		}
		else
		{
			szStr[2*i-1]=szValue[1];
			szStr[2*i]=szValue[0];
		}
		
		pBcd++;
	}
	
	
	
	return dwSize*2-1;
}


void CMainFrame::OnInitalPacket(WPARAM wParam,LPARAM lpParam)
{
	UNUSED_ALWAYS(wParam);
	UNUSED_ALWAYS(lpParam);
	
	_TCHAR szIniFilePath[_MAX_PATH]={0};
	
	if(!GetIniFilePath(szIniFilePath))
	{
		return;
	}

	BOOL bPacketMode = GetPrivateProfileInt(_T("GUI"),_T("PacketMode"),0,szIniFilePath);

#if defined(_SPUPGRADE) || defined(_DOWNLOAD_FOR_PRODUCTION)
	bPacketMode = TRUE;
#endif
	if(!bPacketMode) //lint !e774
	{
		return;
	}	
	
	_TCHAR szPacPath[_MAX_PATH]={0};
	DWORD dwRet =  GetPrivateProfileString(_T("Download_Packet"),_T("packet"),_T(""),szPacPath,_MAX_PATH,szIniFilePath);
	if(dwRet == 0 )
		return;	
	
	if(_tcslen(szPacPath) == 0)
		return;	
	
	CFileFind finder;
	if(!finder.FindFile(szPacPath))
		return;	
	
	m_strPacketPath = szPacPath;

	if(!LoadPac())
	{
		DeletePacTmpDir();
	}
		
}

void CMainFrame::DoReport(DWORD dwOprCookie,BOOL bSuccess /*= TRUE*/)
{
	if(NULL == m_pReportFile && !g_theApp.m_bScriptCtrl)
		return;
	
	EnterCriticalSection( &m_csReportFile);
	// do success report
	_BMOBJ * pbj = NULL;
	char szSN[X_SN_LEN+1] = {0};
	char szIMEI[X_SN_LEN+1] = {0};
	if(m_mapBMObj.Lookup(dwOprCookie,pbj) && pbj != NULL )
	{
		 strcpy(szSN,pbj->szSN);	
		 strcpy(szIMEI,pbj->szIMEI);
	}
	if(m_pReportFile != NULL && bSuccess)
	{
		char szReport[MAX_PATH] = {0};
		CTime timeCur = CTime::GetCurrentTime();
		if(strlen(szSN)!=0)
		{
			sprintf(szReport,"%4d-%02d-%02d %02d:%02d:%02d\t[COM%d] [SN:%s] download success.\r\n",
				timeCur.GetYear(),
				timeCur.GetMonth(),
				timeCur.GetDay(),
				timeCur.GetHour(),
				timeCur.GetMinute(),
				timeCur.GetSecond(),
				dwOprCookie,
				szSN);
		}
		else if(strlen(szIMEI)!=0)
		{
			sprintf(szReport,"%4d-%02d-%02d %02d:%02d:%02d\t[COM%d] [IMEI:%s] download success.\r\n",
				timeCur.GetYear(),
				timeCur.GetMonth(),
				timeCur.GetDay(),
				timeCur.GetHour(),
				timeCur.GetMinute(),
				timeCur.GetSecond(),
				dwOprCookie,
				szIMEI);
		}
		else
		{
			sprintf(szReport,"%4d-%02d-%02d %02d:%02d:%02d\t[COM%d] [SN/IMEI:] download success.\r\n",
				timeCur.GetYear(),
				timeCur.GetMonth(),
				timeCur.GetDay(),
				timeCur.GetHour(),
				timeCur.GetMinute(),
				timeCur.GetSecond(),
				dwOprCookie);
		}

		fwrite(szReport,strlen(szReport),1,m_pReportFile);
		fflush(m_pReportFile);
	}
	//[[ for auto download
	if(g_theApp.m_bScriptCtrl)
	{
		CString strResultPath = g_theApp.m_strResultPath;
		if(g_theApp.m_bResultPathWithPort)
		{
			CString strCom;
			strCom.Format(_T("_com%d.txt"),dwOprCookie);
			strResultPath.Replace(_T(".txt"),strCom);
		}
		FILE *pFile = _tfopen(strResultPath,_T("wt"));
		
		CString strResult = _T("success");
		if(!bSuccess)
		{
			if(pbj != NULL && _tcslen(pbj->szErrorMsg)!=0 )
			{
				strResult = pbj->szErrorMsg;		
			}
			else
			{
				strResult = _T("unknown errors occur in downloading");
			}
		}

		_ftprintf(pFile,strResult);
		fclose(pFile);
	}
	//]]


	LeaveCriticalSection( &m_csReportFile);
}



//#if defined(_DOWNLOAD_FOR_PRODUCTION) || defined(_SPUPGRADE)
PORT_DATA* CMainFrame::CreatePortData( DWORD dwPort )
{
	PORT_DATA * pPortData = NULL;
	if(m_mapPortData.Lookup(dwPort,pPortData))
	{
		ResetEvent(pPortData->hSNEvent);
		return pPortData;
	}

    HANDLE hEvent = CreateEvent( NULL,TRUE,FALSE,NULL );
	
    EnterCriticalSection( &m_cs );
	pPortData = new PORT_DATA;
	pPortData->dwPort = dwPort;
	pPortData->hSNEvent = hEvent;
	pPortData->lpPhaseCheck = NULL;
    m_mapPortData.SetAt(dwPort, pPortData);
    LeaveCriticalSection( &m_cs );
    
    return pPortData;
}

PORT_DATA * CMainFrame::GetPortDataByPort(int nPort)
{
	PORT_DATA * pPortData = NULL;
	m_mapPortData.Lookup(nPort, pPortData);
	return pPortData;
}
//#endif


void CMainFrame::OnStopOnePort(WPARAM wParam,LPARAM lpParam)
{
	UNUSED_ALWAYS(lpParam);

	DWORD dwPort = wParam;
	StopOnePortWork(dwPort);
}


DWORD WINAPI CMainFrame::GetThreadFunc(LPVOID lpParam)
{
	DWORD *pStruct = (DWORD *)lpParam;
	CMainFrame * This = (CMainFrame * )(pStruct[0]);

	DWORD dwPort = pStruct[1];
	return This->ClosePortFunc(dwPort,pStruct[2]);
}

DWORD CMainFrame::ClosePortFunc(DWORD dwPort,BOOL bSuccess)
{
	m_pBMAF->BMAF_StopOneWork(dwPort);
	if(m_bShowFailedMsgbox && !bSuccess)
	{
		::PostMessage(this->GetSafeHwnd(),WM_WARN_MESSAGEBOX,dwPort,0);
	}

	return 0L;
}
BOOL CMainFrame::CheckDLFiles()
{
	CWaitCursor wait;
	CFileFind finder;	
	CString strTmp = _T("");
	for(int i=0; i< m_nFileCount; i++)
	{
		CString strFile = m_arrFile.GetAt(i);
		if(!strFile.IsEmpty() && strFile.CompareNoCase(FILE_OMIT)!=0)
		{
			if(!finder.FindFile(strFile))
			{
				strTmp += strFile;
				strTmp += _T("\n");
			}
			else
			{
			   finder.FindNextFile();
			   if(finder.GetLength() == 0)
			   {
					strTmp += strFile;
					strTmp += _T("\n");
			   }
			   finder.Close();
			}
		}
	}

	if(!strTmp.IsEmpty())
	{
		CString strError;
		strError.Format(IDS_ERR_FILES_NOT_EXIST,strTmp.operator LPCTSTR());
		AfxMessageBox(strError);
		return FALSE;
	}

	return TRUE;
}
BOOL CMainFrame::InitUDiskBufMap()
{
	CWaitCursor wait;
	/*clear UDISK_IMG map*/
	ClearExtImgMap(m_mapUDiskIMg);

	PFILE_INFO_T pFileInfo = NULL;
    int nFileNameLen = UDSIK_IMG_NAME_LEN; // udisk_img_000
	CString strUDiskImgFile;
	int nIdx = m_sheetSettings.GetFileInfo(_T("UDISK_IMG"),(LPDWORD)&pFileInfo);
	if(nIdx != -1 && pFileInfo != NULL)
	{
		strUDiskImgFile = m_arrFile.GetAt(nIdx);
		if(strUDiskImgFile.IsEmpty() || strUDiskImgFile.CompareNoCase(FILE_OMIT)==0)
		{
			// not select UDISK_IMG
			return TRUE;
		}
		CString strDir = strUDiskImgFile;		
		int nFind = strDir.ReverseFind(_T('\\'));
		strDir = strDir.Left(nFind);
		CString strFind = strDir + _T("\\udisk_img_*.bin");	

		WIN32_FIND_DATA wfd = { 0 };
		HANDLE hFind = ::FindFirstFile( strFind, &wfd );
		BOOL bFind = TRUE;
		while( INVALID_HANDLE_VALUE != hFind  && bFind)
		{
			CString strFileName =((CString) (wfd.cFileName)).Left(((CString) (wfd.cFileName)).GetLength() - 4);
			if( strFileName.GetLength() == nFileNameLen)
			{
				int nSectorSize = 0;
				CString strSectorSize = strFileName.Right(3);
				_stscanf(strSectorSize,_T("%03d"),&nSectorSize);
				nSectorSize *= 1024;
				CString strFile;
				strFile.Format(_T("%s\\%s"),strDir.operator LPCTSTR(),wfd.cFileName);
				EXT_IMG_INFO_PTR pUDisk = new EXT_IMG_INFO;
				_tcscpy(pUDisk->szFilePath,strFile);
				
				CFile file(strFile,CFile::modeRead | CFile::shareDenyWrite | CFile::typeBinary);
				pUDisk->dwSize = file.GetLength();
				if(pUDisk->dwSize  != 0 )
				{	
					pUDisk->pBuf = new BYTE[pUDisk->dwSize];
					file.Read(pUDisk->pBuf,pUDisk->dwSize);
				}				
				file.Close();

				m_mapUDiskIMg.SetAt(nSectorSize,pUDisk); //lint !e729
				/*lint -save -e429*/
			}
            /*lint -restore */
			bFind = ::FindNextFile(hFind, &wfd);
			
		}
		if(INVALID_HANDLE_VALUE != hFind)
		{
			::FindClose(hFind);	
		}
	}

	return TRUE;
}

BOOL CMainFrame::InitNVBuffer()
{
	CWaitCursor wait;
	CString strNVFileType;
	CString strNVFileName;
	PFILE_INFO_T pNVFile = NULL;
	int nIdx = m_sheetSettings.GetFileInfo(_T("NV"),(LPDWORD)&pNVFile);
	if(nIdx != -1 && pNVFile != NULL)
	{
		strNVFileName = m_arrFile.GetAt(nIdx);
	}
	
	if(m_sheetSettings.IsReadFlashInFDL2() || m_sheetSettings.IsHasLang() )
	{
		_ASSERTE(!strNVFileName.IsEmpty() && strNVFileName.CompareNoCase(FILE_OMIT) != 0);
		
		if(strNVFileName.IsEmpty() || strNVFileName.CompareNoCase(FILE_OMIT) == 0)
		{
			return FALSE;
		}
		
		CFile file(strNVFileName,CFile::modeRead | CFile::shareDenyWrite);
		m_dwNvFileSize = file.GetLength();
		if(m_dwNvFileSize != 0 )
		{	
			m_pNVFileBuf = new BYTE[m_dwNvFileSize];
			file.Read(m_pNVFileBuf,m_dwNvFileSize);
		}
		
		file.Close();
		if(m_sheetSettings.IsHasLang() && m_dwNvFileSize != 0 && m_pNVFileBuf != NULL )
		{
			DWORD dwOffset = 0;
			DWORD dwLength = 0;
			BOOL  bBigEndian = TRUE;
			if(XFindNVOffsetEx(NV_MULTI_LANG_ID,m_pNVFileBuf,m_dwNvFileSize,dwOffset,dwLength,bBigEndian,FALSE) &&
				dwLength == sizeof(NV_MULTI_LANGUE_CFG))
			{
				// "m_pNVFileBuf+dwOffset" not overflow is guaranteed by XFindNVOffsetEx
				NV_MULTI_LANGUE_CFG *pObj = (NV_MULTI_LANGUE_CFG *)(m_pNVFileBuf+dwOffset); //lint !e613
				for(int k = 0; k<NV_MAX_LANG_NUM;k++)
				{
					BYTE flag = (BYTE)m_sheetSettings.m_pageMultiLang.m_agLangFlag.GetAt(k);
					pObj->flag[k] = flag;
				}
			}
			else
			{
				AfxMessageBox(_T("Not find multi-lang item in NV file!"));
				return FALSE;
			}

			if(m_sheetSettings.IsEnableMultiFileBuf())
			{
				CStringArray agChipName;
				CUIntArray   agChipID;
				int nCount = m_sheetSettings.GetAllChipName(agChipName,agChipID);
				CString strID = _T("NV");
				for(int i = 0; i< nCount; i++)
				{
					DWORD dwChipID = agChipID[i];
					EXT_IMG_INFO_PTR pImg = NULL;
					pImg = m_mapMultiFileBuf[std::make_pair(dwChipID,strID)];
					if(pImg != NULL && pImg->pBuf != NULL && pImg->dwSize != 0)
					{
						if(XFindNVOffsetEx(NV_MULTI_LANG_ID,pImg->pBuf,pImg->dwSize,dwOffset,dwLength,bBigEndian,FALSE) &&
							dwLength == sizeof(NV_MULTI_LANGUE_CFG))
						{
							// "pImg->pBuf+dwOffset" not overflow is guaranteed by XFindNVOffsetEx
							NV_MULTI_LANGUE_CFG *pObj = (NV_MULTI_LANGUE_CFG *)(pImg->pBuf+dwOffset); //lint !e613
							for(int k = 0; k<NV_MAX_LANG_NUM;k++)
							{
								BYTE flag = (BYTE)m_sheetSettings.m_pageMultiLang.m_agLangFlag.GetAt(k);
								pObj->flag[k] = flag;
							}
						}
					}

				}
			}
		}
	}

	return TRUE;
}

BOOL CMainFrame::InitReportInfo()
{
	CWaitCursor wait;
	CFileFind finder;
	CString strErrMsg;
	_TCHAR szConfigPath[_MAX_PATH]={0};
	_TCHAR szDefaultPath[_MAX_PATH]={0};
//	_TCHAR szCustPath[_MAX_PATH]={0};
	::GetModuleFileName(g_theApp.m_hInstance,szConfigPath,_MAX_PATH);
	_tcscpy(szDefaultPath,szConfigPath);
    LPTSTR pResult = _tcsrchr(szConfigPath,_T('.'));
    *pResult = 0;
    _tcscat(szConfigPath,_T(".ini"));
	
	pResult = _tcsrchr(szDefaultPath,_T('\\'));
    *pResult = 0;
    _tcscat(szDefaultPath,_T("\\dl_report_result.txt"));	
	
	// BOOL bEnableReport = GetPrivateProfileInt(_T("Report"),_T("enable"),0,szConfigPath);
	if(m_bDoReport)
	{
		//GetPrivateProfileString(_T("Report"),_T("path"),szDefaultPath,szCustPath,_MAX_PATH,szConfigPath);
		if(finder.FindFile(szDefaultPath))
		{
			DWORD dwAtt = 0;
			dwAtt = ::GetFileAttributes(szDefaultPath);
			dwAtt &= ~FILE_ATTRIBUTE_READONLY;
			::SetFileAttributes(szDefaultPath,dwAtt);
		}
		
		m_pReportFile = _tfopen(szDefaultPath,_T("a+"));
		
		if(m_pReportFile == NULL)
		{
			strErrMsg.Format(_T("Open Success-Report file [%s] fail!"),szDefaultPath);
			AfxMessageBox( strErrMsg);
			return FALSE;
		}
	}

	return TRUE;
}

void CMainFrame::ClearExtImgMap(MAP_EXTIMG &mapExtImg)
{
	POSITION pos = mapExtImg.GetStartPosition();
	while(NULL != pos)
	{
		DWORD dwKey= 0;
        EXT_IMG_INFO_PTR pExtImgInfo = NULL;
      
        mapExtImg.GetNextAssoc( pos, dwKey, pExtImgInfo );

		if(pExtImgInfo != NULL)
		{
			if(pExtImgInfo->pBuf != NULL)
			{
				delete [] pExtImgInfo->pBuf;
				pExtImgInfo->pBuf= NULL;
			}
			delete pExtImgInfo;
		}
	}
	mapExtImg.RemoveAll();
}

BOOL CMainFrame::InitChipDspBufmap()
{
	CWaitCursor wait;
	/*clear UDISK_IMG map*/
	ClearExtImgMap(m_mapChipDsp);

	if(!m_sheetSettings.IsEnableChipDspMap())
	{
		// need not chip-dsp map
		return TRUE;
	}

	CString strErrMsg =_T("");
	PFILE_INFO_T pFileInfo = NULL;
	CString strChipDspFile;
	int nIdx = m_sheetSettings.GetFileInfo(_T("DSPCode"),(LPDWORD)&pFileInfo);
	if(nIdx != -1 && pFileInfo != NULL)
	{
		strChipDspFile = m_arrFile.GetAt(nIdx);
		if(strChipDspFile.IsEmpty() || strChipDspFile.CompareNoCase(FILE_OMIT)==0)
		{
			// not select "DSPCode"
			return TRUE;
		}

		CFileFind finder;
		
		_TCHAR szConfigPath[_MAX_PATH]={0};
		::GetModuleFileName(g_theApp.m_hInstance,szConfigPath,_MAX_PATH);
		LPTSTR pResult = _tcsrchr(szConfigPath,_T('\\'));
		*pResult = 0;
		_tcscat(szConfigPath,_T("\\BMFileType.ini"));
		
		_TCHAR szKeyValue[ MAX_BUF_SIZE ]={0}; 
		DWORD dwSize = GetPrivateProfileSection( _T("ChipDSPMap"), szKeyValue, MAX_BUF_SIZE, szConfigPath );
		if(dwSize == 0)
		{
			AfxMessageBox(_T("Not found ChipID-DSPName map setting in BMFileType.ini!"));
			return FALSE;
		}
		CStringArray arrKeyData;    
		UINT nFileCount = (UINT)EnumKeys(szKeyValue,&arrKeyData);
		if(nFileCount == 0)
		{
			AfxMessageBox(_T("Not found ChipID-DSPName map setting in BMFileType.ini!"));
			return FALSE;
		}
		
		CString strDir = strChipDspFile;
		
		int nFind = strDir.ReverseFind(_T('\\'));
		strDir = strDir.Left(nFind);

		for(UINT i= 0; i< nFileCount; i++)
		{		
			DWORD dwChipID = 0;
			CString strChipID = arrKeyData[2*i];
			strChipID.MakeUpper();
			_stscanf(strChipID,_T("0X%08x"),&dwChipID);		
			CString strFile;
			strFile.Format(_T("%s\\%s"),strDir.operator LPCTSTR(),arrKeyData[2*i+1].operator LPCTSTR());
			if(!finder.FindFile(strFile))
			{
				strErrMsg += strFile;
				strErrMsg += _T("\r\n");
				continue;
			}
			EXT_IMG_INFO_PTR pChipDsp = new EXT_IMG_INFO;
			_tcscpy(pChipDsp->szFilePath,strFile);			
			CFile file(strFile,CFile::modeRead | CFile::shareDenyWrite | CFile::typeBinary);
			pChipDsp->dwSize = file.GetLength();
			if(pChipDsp->dwSize  != 0 )
			{	
				pChipDsp->pBuf = new BYTE[pChipDsp->dwSize];
				file.Read(pChipDsp->pBuf,pChipDsp->dwSize);
			}				
			file.Close();			
			m_mapChipDsp.SetAt(dwChipID,pChipDsp); //lint !e429	
			/*lint -save -e429*/
		}
		/*lint -restore */
	}

	if(!strErrMsg.IsEmpty())
	{
		strErrMsg.Insert(0,_T("Not found bellow DSP file:\r\n"));
		strErrMsg += _T("\r\nPlease check BMFileType.ini, [ChipDspMap]!");
		AfxMessageBox(strErrMsg);
		return FALSE;
	}

	return TRUE;
}

void CMainFrame::GetOprErrorCodeDescription(
                                           DWORD dwErrorCode,
                                           LPTSTR lpszErrorDescription, 
                                           int nSize )
{
    if( lpszErrorDescription == NULL || nSize == 0 )
        return;
    
    _TCHAR szKey[ 10 ];
    _itot( dwErrorCode, szKey, 10 );
    
    GetPrivateProfileString(  _T("ErrorDescription"), szKey,
        _T("Unknown Error"), lpszErrorDescription, 
        nSize, m_strOprErrorConfigFile );  
    
    return;
}


void CMainFrame::InitCodeChipID()
{
	USES_CONVERSION;
	CWaitCursor wait;
	int nIdx = m_sheetSettings.GetFileInfo(_T("FDL2"),NULL);
	if(nIdx == -1)
	{
		return;
	}

	CString strFDL2File = m_arrFile[nIdx];

	CFileFind finder;
	if(!finder.FindFile(strFDL2File))
	{
		return;
	}

	CString strError;

	CFile file;
    CFileException fe;
    BOOL bRet = file.Open( strFDL2File,CFile::modeRead|CFile::shareDenyWrite,&fe );

    if( !bRet )
    {
        // Can not open file
        LPTSTR p = strError.GetBuffer( _MAX_PATH );
        fe.GetErrorMessage( p,_MAX_PATH );
        strError.ReleaseBuffer();
		CString strTmp;
		strTmp.Format(_T("Can not open file [%s]!\n(%s)"),strFDL2File.operator LPCTSTR(),strError.operator LPCTSTR());
		AfxMessageBox(strTmp);
        return;
    }

    LPBYTE lpContent = NULL;
    int nLen = 0;

    try
    {
        nLen = file.GetLength();
        lpContent = new BYTE[nLen];
        file.Read( lpContent,nLen );
    }
    catch( CFileException &ex )
    {
        LPTSTR p = strError.GetBuffer( _MAX_PATH );
        ex.GetErrorMessage( p,_MAX_PATH );
        strError.ReleaseBuffer();
		CString strTmp;
		strTmp.Format(_T("Can not open file [%s]!\n(%s)"),strFDL2File.operator LPCTSTR(),strError.operator LPCTSTR());
		AfxMessageBox(strTmp);
		if(lpContent != NULL)
		{
			 delete []lpContent;
		}
        return;
    }
   
    const char szChipIDPrefix[] = "#*CHIP_VER_";

#define MAX_CHIP_VER_LEN (100)

    BYTE* lpPos = NULL;
	lpPos = std::search( lpContent,lpContent + nLen,szChipIDPrefix,szChipIDPrefix + strlen( szChipIDPrefix ) - 1 );
    if( lpPos == lpContent + nLen )
    {
        // not find the version string
        delete []lpContent;
        return;
    }
	else
	{
		char szChipVer[MAX_CHIP_VER_LEN+1] = {0};
		lpPos += strlen( szChipIDPrefix );
		int nMin = MAX_CHIP_VER_LEN < (nLen-(lpPos-lpContent)) ? MAX_CHIP_VER_LEN : (nLen-(lpPos-lpContent));

		for(int i=0;i<(nMin-1); i++)
		{
			if(*(lpPos+i) != '*' && *(lpPos+i+1) != '#' )
			{
				szChipVer[i] = *(lpPos+i);
			}
			else
			{
				break;
			}
		}
#if defined(UNICODE) || defined(_UNICODE)
		m_strCodeChipID = A2W(szChipVer);  //lint !e733
#else
		m_strCodeChipID = szChipVer;
#endif
		delete []lpContent;    
		return;
	}   
}

void CMainFrame::OnPowerManage(WPARAM wParam,LPARAM lpParam)
{
	UNUSED_ALWAYS(wParam);
	UNUSED_ALWAYS(lpParam);
	static _TCHAR szData[] = _T("DOWNLOAD:POWER-MANAGE");
	 COPYDATASTRUCT  CopyDataStruct;
    CopyDataStruct.dwData = 0;
    CopyDataStruct.cbData = sizeof(szData);
    CopyDataStruct.lpData = szData;

	EnterCriticalSection( &m_csPowerManage);
    // Find the window
    g_hLayer1TesterHwnd = NULL;
    ::EnumWindows(EnumWindowsProc, 0);
    if (NULL != g_hLayer1TesterHwnd)
    {
		::SendMessage(g_hLayer1TesterHwnd, WM_COPYDATA, (WPARAM)GetSafeHwnd(), (long)&CopyDataStruct);
    }
	LeaveCriticalSection( &m_csPowerManage);
}

void CMainFrame::SetDLTitle()
{
	_TCHAR szTitle[_MAX_PATH]={0};
	_TCHAR szFilePath[_MAX_PATH]={0};
	BOOL bShowVer = TRUE;
    if(GetIniFilePath(szFilePath))
    {
        GetPrivateProfileString(_T("GUI"),_T("Title"),_T(""),szTitle,_MAX_PATH,szFilePath);
		bShowVer = GetPrivateProfileInt(_T("GUI"),_T("ShowVer"),1,szFilePath);
    }

	CString strVersion;

	if(bShowVer)
	{
		CXVerInfo ver;
		strVersion = ver.GetSPRDVersionString();
	}

	CString strTile;
#ifdef _SPUPGRADE
	if(_tcslen(szTitle) != 0)
	{
		strTile.Format(_T("UpgradeDownload [%s]"),szTitle);
	}
	else
	{
		strTile = _T("UpgradeDownload");
	}	
#elif defined _DOWNLOAD_FOR_PRODUCTION
	if(_tcslen(szTitle) != 0)
	{
		strTile.Format(_T("FactoryDownload [%s]"),szTitle);
	}
	else
	{
		strTile = _T("FactoryDownload");
	}
#else
	if(_tcslen(szTitle) != 0)
	{
		strTile.Format(_T("ResearchDownload [%s]"),szTitle);
	}
	else
	{
		strTile = _T("ResearchDownload");
	}
#endif

	if(bShowVer)
	{
#ifdef _DEBUG
		strTile += _T(" - D");
#else 
		strTile += _T(" - R");
#endif
		strTile += strVersion;	
	}
	SetWindowText(strTile); 	
}

static BOOL CheckLCDConfig(VEC_LCD_CFIG &vLcdCfig1,VEC_LCD_CFIG &vLcdCfig2)
{
	if(vLcdCfig1.size() != vLcdCfig2.size())
		return FALSE;

	int nSize = vLcdCfig1.size();
	for(int i = 0; i< nSize; i++)
	{
		if(vLcdCfig1[i].dwFlagOffset != vLcdCfig2[i].dwFlagOffset)
		{
			return FALSE;
		}
	}
	return TRUE;
}

BOOL  CMainFrame::InitPSFile()
{
	CString strPSFileName;

	int nIdx = m_sheetSettings.GetFileInfo(_T("PS"),NULL);
	if(nIdx == -1)
	{
		nIdx = m_sheetSettings.GetFileInfo(_T("UserImg"),NULL);		
	}
	if(nIdx == -1)
	{
		return TRUE;
	}

	strPSFileName = m_arrFile.GetAt(nIdx);
	
	if(m_sheetSettings.IsHasLCD() && !strPSFileName.IsEmpty() && strPSFileName.CompareNoCase(FILE_OMIT) != 0)
	{	
		//////////////////////////////////////////////////////////////////////////
		// check if select LCD config
		int nVSize = m_sheetSettings.m_pageLCDCfig.m_vLCDCfig.size();
		if(nVSize == 0)
		{
			return TRUE;
		}
		else
		{
			BOOL bSelLCD = FALSE;
			for(int i = 0 ;i< nVSize; i++)
			{
				if( m_sheetSettings.m_pageLCDCfig.m_vLCDCfig[i].dwFlag != 0)
				{
					bSelLCD = TRUE;
					break;
				}
			}
			if(!bSelLCD)
			{
				AfxMessageBox(_T("Must select one of LCD configure at least!"));
				return FALSE;
			}
		}

		CFileFind finder;
		if(finder.FindFile(strPSFileName))
		{
			DWORD dwAtt = GetFileAttributes(strPSFileName);
			dwAtt &= ~FILE_ATTRIBUTE_READONLY;
			SetFileAttributes(strPSFileName,dwAtt);
		}
		else
		{
			return TRUE;
		}

		HANDLE hFile = INVALID_HANDLE_VALUE;
		hFile = ::CreateFile(strPSFileName,
							 GENERIC_READ,
							 FILE_SHARE_READ,
							 NULL,
							 OPEN_EXISTING,
							 FILE_ATTRIBUTE_NORMAL,
							 NULL);

		if(hFile == INVALID_HANDLE_VALUE)
		{
			CString strFormatted;
			strFormatted.Format(_T("Can not open file [%s]."),strPSFileName.operator LPCTSTR());
			AfxMessageBox(strFormatted);
			return FALSE;
		}

		DWORD dwSize = GetFileSize(hFile,NULL);
		BYTE *pBuf = NULL;
		if(dwSize != 0 )
		{	
			pBuf = new BYTE[dwSize];
			DWORD dwRealRead =  0;
			ReadFile(hFile,pBuf,dwSize,&dwRealRead,NULL);
		}			
		CloseHandle(hFile);
		hFile = INVALID_HANDLE_VALUE;

		if(dwSize != 0)
		{
			VEC_LCD_CFIG vLcdCfig;
			if(m_sheetSettings.FindLCDItem(strPSFileName,vLcdCfig))
			{
				if(CheckLCDConfig(vLcdCfig,m_sheetSettings.m_pageLCDCfig.m_vLCDCfig))
				{
					int nSize = vLcdCfig.size();
					for(int i = 0; i<nSize; i++)
					{
						*(pBuf+vLcdCfig[i].dwFlagOffset) = (BYTE)m_sheetSettings.m_pageLCDCfig.m_vLCDCfig[i].dwFlag; //lint !e613
					}

					if(nSize > 0)
					{
						hFile = ::CreateFile(strPSFileName,
								 GENERIC_WRITE,
								 FILE_SHARE_READ,
								 NULL,
								 CREATE_ALWAYS,
								 FILE_ATTRIBUTE_NORMAL,
								 NULL);

						if(hFile == INVALID_HANDLE_VALUE)
						{
							CString strFormatted;
							strFormatted.Format(_T("Can not create file [%s]."),strPSFileName.operator LPCTSTR());
							AfxMessageBox(strFormatted);
							SAFE_DELETE_ARRAY(pBuf);
							return FALSE;
						}
						else
						{
							DWORD dwWrite= 0;
							WriteFile(hFile,pBuf,dwSize,&dwWrite,NULL);
							CloseHandle(hFile);
							hFile = INVALID_HANDLE_VALUE;
							SAFE_DELETE_ARRAY(pBuf);
						}
					}
				}
				else
				{
					CString strFormatted;
					strFormatted.Format(_T("PS/USER_IMG file [%s] is changed,please set LCD configure again."),strPSFileName.operator LPCTSTR());
					AfxMessageBox(strFormatted);
					SAFE_DELETE_ARRAY(pBuf);
					return FALSE;
				}
			}					
		}	

		SAFE_DELETE_ARRAY(pBuf);
	}

	return TRUE;
}

BOOL CMainFrame::InitIMEIID()
{
	CWaitCursor wait;

	m_aIMEIID.RemoveAll();
	m_aIMEIID.Add(GSM_IMEI_ITEM_ID);

	_TCHAR szAppPath[_MAX_PATH]={0};
	GetModuleFilePath(g_theApp.m_hInstance,szAppPath);
	CFileFind finder;
	
	CString strBMFileType;
	strBMFileType.Format(_T("%s\\BMFileType.ini"),szAppPath);	
	if(!finder.FindFile(strBMFileType))
	{
		AfxMessageBox(_T("Not found BMFileType.ini!"));
		return FALSE;		
	}

	_TCHAR szKeyValue[ MAX_BUF_SIZE ]={0}; 
	DWORD dwSize = GetPrivateProfileSection( _T("IMEI_ID"), szKeyValue, MAX_BUF_SIZE, strBMFileType );
	if(dwSize == 0)
	{
		return TRUE;
	}
	CStringArray arrKeyData;    
	UINT nIDCount = (UINT)EnumKeys(szKeyValue,&arrKeyData);
	if(nIDCount == 0)
	{		
		return TRUE;
	}	

	for(UINT i= 0;i< nIDCount; i++)
	{
		CString strTmp = arrKeyData[i*2+1];
		UINT uID = 0;
		strTmp.MakeLower();
		if(strTmp.Find(_T("0x")) == 0)
		{
			_stscanf(strTmp.operator LPCTSTR(),_T("0x%X"),&uID);
		}
		else
		{
			uID = _ttoi(strTmp.operator LPCTSTR());
		}
		if(uID > 0 && uID<0xFFFF)
		{
			m_aIMEIID.Add(uID);
		}
	}
	
	return TRUE;

}

void CMainFrame::ClearMultiFileBuf()
{
	MAP_FILEBUF::iterator it;
	for(it = m_mapMultiFileBuf.begin(); it != m_mapMultiFileBuf.end(); it++)
	{
		EXT_IMG_INFO_PTR pImg = it->second;
		if(pImg != NULL)
		{
			pImg->clear();
			delete pImg;
			it->second = NULL;
		}
	}

	m_mapMultiFileBuf.clear();
}

BOOL CMainFrame::InitMultiFileBuf()
{
	CWaitCursor wait;
	/*clear m_mapMultiFileBuf map*/
	ClearMultiFileBuf();

	if(!m_sheetSettings.IsEnableMultiFileBuf())
	{
		return TRUE;
	}

	int nStartFile = 1;

	if(m_sheetSettings.IsNandFlash())
	{
		nStartFile = 2;
	}

	CStringArray agChipName;
	CUIntArray   agChipID;
	m_sheetSettings.GetAllChipName(agChipName,agChipID);

	CStringArray agDLFiles;
	m_sheetSettings.GetDownloadFile(agDLFiles);
	int k=0;
	// check chip prefix
	CString strChipPre;
	BOOL bOK = TRUE;
	CString strErrChipPre = _T("");
 	for(k = nStartFile; k< agDLFiles.GetSize(); k++)
	{	
		CString strFilePath = agDLFiles.GetAt(k);
		if(strFilePath.IsEmpty() || strFilePath.CompareNoCase(FILE_OMIT) == 0 )
		{
			continue;
		}

		int nIndx = strFilePath.ReverseFind('\\');
		CString strFileName = strFilePath;
		if(nIndx != -1)
		{
			strFileName = strFilePath.Right(strFilePath.GetLength()-nIndx -1);
			strFilePath = strFilePath.Left(nIndx);
		}
		else
		{
			strFilePath.Empty();
		}
	
		if(strChipPre.IsEmpty())
		{
			for(int i = 0; i< agChipName.GetSize(); i++)
			{
				CString strChipName = agChipName.GetAt(i);
				strChipName += _T("_");
				if(strFileName.Left(strChipName.GetLength()).CompareNoCase(strChipName) == 0)
				{					
					strChipPre = strChipName;
					strChipPre.MakeUpper();
					break;
				}
			}
			if(strChipPre.IsEmpty())
			{
				CString strTmp = _T("");
				for(int j = 0; j< agChipName.GetSize(); j++)
				{
					strTmp += agChipName.GetAt(j);
					strTmp += _T("\n");
				}

				CString strErr;
				strErr.Format(_T("There are no download files matching with chip name prefix:\n%s."),strTmp);
				AfxMessageBox(strErr);
				return FALSE;	
			}
		}
		else
		{
			if(strFileName.Left(strChipPre.GetLength()).CompareNoCase(strChipPre) != 0)
			{					
				bOK = FALSE;
				strErrChipPre += agDLFiles.GetAt(k);
				strErrChipPre += _T("\n");
			}
		}
	}

	if(!bOK)
	{
		CString strErr;
		strErr.Format(_T("Following files not match with chip name prefix \"%s\":\n%s."),strChipPre,strErrChipPre);
		AfxMessageBox(strErr);
		return FALSE;
	}


	CStringArray agDLFileID;
	m_sheetSettings.GetAllFileID(agDLFileID);

	int i = 0;
	int j = 0;

	CString strErrNoFile = _T("");
	CString strErrMkMaster = _T("");

	bOK = TRUE;

	for(j = nStartFile; j< agDLFiles.GetSize(); j++)
	{		
		CString strFilePath = agDLFiles.GetAt(j);
		if(strFilePath.IsEmpty() || strFilePath.CompareNoCase(FILE_OMIT) == 0 )
		{
			continue;
		}

		CString strFileID = agDLFileID.GetAt(j);

		int nIndx = strFilePath.ReverseFind('\\');
		CString strFileName = strFilePath;
		if(nIndx != -1)
		{
			strFileName = strFilePath.Right(strFilePath.GetLength()-nIndx -1);
			strFilePath = strFilePath.Left(nIndx);
		}
		else
		{
			strFilePath.Empty();
		}

		CString strFileTitle;

// 		for(i = 0;  i< agChipName.GetSize(); i++)
// 		{
// 			CString strChipName = agChipName.GetAt(i);
// 			strChipName += _T("_");
// 			if(strFileName.Left(strChipName.GetLength()).CompareNoCase(strChipName) == 0)
// 			{
// 				strFileTitle = strFileName.Right(strFileName.GetLength() -strChipName.GetLength());
// 				break;
// 			}		
// 		}	

		strFileTitle = strFileName.Right(strFileName.GetLength() - strChipPre.GetLength());

// 		if(strFileTitle.IsEmpty())
// 		{
// 			//need not deal with error, becaude checked file size before.
// 		}

		for(i = 0;  i< agChipName.GetSize(); i++)
		{
			DWORD   dwChipID = agChipID.GetAt(i);
			CString strChipName = agChipName.GetAt(i);
			CString strCurFilePath;
			strCurFilePath.Format(_T("%s\\%s_%s"),strFilePath,strChipName,strFileTitle);

			CFileFind finder;
			if(!finder.FindFile(strCurFilePath))		
			{
				bOK = FALSE;
				strErrNoFile += strCurFilePath;
				strErrNoFile += _T("\n");
			}
			else
			{
				EXT_IMG_INFO_PTR pImg = new EXT_IMG_INFO;
				_tcscpy(pImg->szFilePath,strCurFilePath.operator LPCTSTR());
				CFile file(strCurFilePath,CFile::modeRead | CFile::shareDenyWrite | CFile::typeBinary);
				pImg->dwSize = file.GetLength();			
				if(pImg->dwSize  != 0 )
				{	
					pImg->pBuf = new BYTE[pImg->dwSize];
					file.Read(pImg->pBuf ,pImg->dwSize);
				}				
				file.Close();

				if(pImg->pBuf != NULL)
				{
					PFILE_INFO_T pfi = NULL;
					
					m_sheetSettings.GetFileInfo(strFileID,(LPDWORD)&pfi);
					if(pfi != NULL)
					{
						CString strFileType = pfi->szType;
						
						if( strFileType.CompareNoCase(_T("MasterImage"))==0 )
						{		
							DWORD dwSize = 0;
							LPBYTE pBuf = NULL;
							IMAGE_PARAM imageparam;						
							memset( &imageparam,0,sizeof( IMAGE_PARAM ) );
							_tcscpy( imageparam.szPath,pImg->szFilePath );
							CMasterImgGen mig;
							pBuf = (LPBYTE)mig.MakeMasterImageSingle( &dwSize,1,&imageparam,m_sheetSettings.GetFlashPageType() );
							
							if(pBuf == NULL)
							{
								//error;
								strErrMkMaster += strCurFilePath;
								strErrMkMaster += _T("\n");
								bOK = FALSE;
							}	
							else
							{
								delete [] pImg->pBuf;
								pImg->pBuf  = pBuf;
								pImg->dwSize = dwSize;
							}							
						}
					}
				}

				m_mapMultiFileBuf[std::make_pair(dwChipID,strFileID)] = pImg;

				// NV may have two files
				if(strFileID.CompareNoCase(_T("NV")) == 0 && m_sheetSettings.IsNVOrgDownload())
				{
					CString strExtID = _T("NVOriginal");
					EXT_IMG_INFO_PTR pImg2 = new EXT_IMG_INFO;
					_tcscpy(pImg2->szFilePath,pImg->szFilePath);
					pImg2->dwSize = pImg->dwSize;
					if(pImg2->dwSize != 0)
					{
						pImg2->pBuf = new BYTE[pImg2->dwSize];
						memcpy(pImg2->pBuf,pImg->pBuf,pImg2->dwSize);
					}
					m_mapMultiFileBuf[std::make_pair(dwChipID,strExtID)] = pImg;
					
				}
			}//if(!finder.FindFile(strCurFilePath)) else		
		}
	}

	if(bOK)
	{
		return TRUE;
	}
	else
	{
		ClearMultiFileBuf();

		CString strTotalErr1;
		if(strErrNoFile.GetLength()!=0)
		{
			strTotalErr1.Format(_T("Not found following files:\n%s"),strErrNoFile);
		}
		CString strTotalErr2;
		if(strErrMkMaster.GetLength()!=0)
		{
			strTotalErr2.Format(_T("Make master image for following files failed:\n%s"),strErrMkMaster);
		}

		CString strTotalErr;
		strTotalErr += strTotalErr1;
		strTotalErr += strTotalErr2;
		AfxMessageBox(strTotalErr);		
		return FALSE;
	}	
}

BOOL CMainFrame::LoadPac()
{
	CWaitCursor wait;
	DeletePacTmpDir();
	m_bPacketOpen = FALSE;	
	
	CFileFind finder;
	if(finder.FindFile(m_strPacketPath))
	{
		finder.FindNextFile();
		finder.GetLastWriteTime(m_tmPacModifyTime);		
	}
	finder.Close();	
	
	if(!m_sheetSettings.LoadPacket(m_strPacketPath))
	{
		DeletePacTmpDir();
		AfxMessageBox(IDS_ERR_LOAD_PACKET_FAIL);
		return FALSE;	
	}
	
	m_bPacketOpen = TRUE;
	
	if(!LoadSettings())
    {
		DeletePacTmpDir();
		m_bPacketOpen = FALSE;
		if(!m_strLoadSettingMsg.IsEmpty())
		{
			CString strError;
			strError.LoadString(IDS_LOAD_SETTINGS_FAIL);
			strError += m_strLoadSettingMsg;
			AfxMessageBox(strError,MB_ICONSTOP);
		}
        return FALSE;
    }	
	
#ifdef _DOWNLOAD_FOR_PRODUCTION
	m_sheetSettings.m_pageCalibration.m_lstBackup.m_bBackNV = FALSE;
	m_sheetSettings.m_pageMultiLang.m_bBackupLang = FALSE;
#endif	

	return TRUE;
}

BOOL CMainFrame::InitPageOobFile()
{
	CWaitCursor wait;

	/*clear m_mapPageOobFile map*/
	ClearPageOobFile();

	if(!m_sheetSettings.IsEnablePageOobFile())
	{
		return TRUE;
	}

	PFILE_INFO_T pFileInfo = NULL;
	int nCount = m_sheetSettings.GetAllFileInfo((LPDWORD)&pFileInfo);

	int i=0;
	CString strErr = _T("");
	int nPageOobFileCount = 0;
	int nPageFileCount = 0;
 	for(i = 0; i< nCount; i++)
	{	
		CString strFileType = pFileInfo[i].szType;
		CString strFileID = pFileInfo[i].szID;
		CString strFilePath = m_sheetSettings.GetDownloadFilePath(strFileID);
		if( strFilePath.IsEmpty() || 
			strFilePath.CompareNoCase(FILE_OMIT) == 0 ||
			(strFileType.CompareNoCase(_T("PAGE"))!=0 && 
			strFileType.CompareNoCase(_T("PAGE_OOB"))!=0))
		{
			continue;
		}

		CStringArray agFilePathInfo;
		GetFilePathInfo(strFilePath,agFilePathInfo);
		CString strFileName = agFilePathInfo[1];
		int  nIndex = strFileName.Find('-');
		if(nIndex == -1)
		{
			strErr.Format(_T("The name of file [%s] is invalid!\nFile name must be \"xxx-Nkpage[-Moob].yyy\", N and M is a number."),strFilePath);
			AfxMessageBox(strErr);
			ClearPageOobFile();
			return FALSE;
		}

		if(strFileType.CompareNoCase(_T("PAGE_OOB"))==0)
			nPageOobFileCount++;
		else
			nPageFileCount++;

		CString strExt = agFilePathInfo[2];
		CString strFilePre = strFileName.Left(nIndex);
		CString strFind;
		strFind.Format(_T("%s\\%s*%s"),agFilePathInfo[0],strFilePre,strExt);

		WIN32_FIND_DATA wfd = { 0 };
		HANDLE hFind = ::FindFirstFile( strFind, &wfd );
		BOOL bFind = TRUE;
		while( INVALID_HANDLE_VALUE != hFind  && bFind)
		{
			CString strPageOobInfo =((CString) (wfd.cFileName)).Left(((CString) (wfd.cFileName)).GetLength() - strExt.GetLength());
			strPageOobInfo = strPageOobInfo.Right(strPageOobInfo.GetLength()-strFilePre.GetLength());
			
			CString strFile;
			strFile.Format(_T("%s\\%s"),agFilePathInfo[0].operator LPCTSTR(),wfd.cFileName);
			EXT_IMG_INFO_PTR pImg = LoadPageOobFile(strFile);

			if(pImg == NULL)
			{
				strErr.Format(_T("Load file [%s] failed!"),strFile);
				AfxMessageBox(strErr);
				::FindClose(hFind);	
				ClearPageOobFile();	
				return FALSE;
			}

			strPageOobInfo.MakeLower();

			m_mapPageOobFile[std::make_pair(strPageOobInfo,strFileID)] = pImg;

			bFind = ::FindNextFile(hFind, &wfd);
			
			DWORD dwPageOobCount = 0;
			
			if(strFileType.CompareNoCase(_T("PAGE_OOB"))==0)
			{
				if(m_mapPageOobInfo.Lookup(strPageOobInfo,dwPageOobCount))
				{
					dwPageOobCount += 1;				
				}
				else
				{
					dwPageOobCount = 1;				
				}
				m_mapPageOobInfo.SetAt(strPageOobInfo,dwPageOobCount);
			}
			else
			{
				if(m_mapPageInfo.Lookup(strPageOobInfo,dwPageOobCount))
				{
					dwPageOobCount += 1;				
				}
				else
				{
					dwPageOobCount = 1;				
				}
				m_mapPageInfo.SetAt(strPageOobInfo,dwPageOobCount);
			}

		}

		if(INVALID_HANDLE_VALUE != hFind)
		{
			::FindClose(hFind);	
		}			
	}

	POSITION pos = m_mapPageOobInfo.GetStartPosition();
	while(NULL != pos)
	{
		CString strKey;
		DWORD dwPageOobCount = 0;
      
        m_mapPageOobInfo.GetNextAssoc( pos, strKey, dwPageOobCount );

		if(dwPageOobCount != (DWORD)nPageOobFileCount)
		{
			strErr.Format(_T("Page-OOB [%s] only have %d image files.\nIt is less than %d."),
				          strKey,dwPageOobCount,nPageOobFileCount);
			AfxMessageBox(strErr);
			ClearPageOobFile();
			return FALSE;
		}
	}

	pos = m_mapPageInfo.GetStartPosition();
	while(NULL != pos)
	{
		CString strKey;
		DWORD dwPageCount = 0;
		
        m_mapPageOobInfo.GetNextAssoc( pos, strKey, dwPageCount );
		
		if(dwPageCount != (DWORD)nPageOobFileCount)
		{
			strErr.Format(_T("Page [%s] only have %d image files.\nIt is less than %d."),
				strKey,dwPageCount,nPageOobFileCount);
			AfxMessageBox(strErr);
			ClearPageOobFile();
			return FALSE;
		}
	}

	return TRUE;
}

void CMainFrame::ClearPageOobFile()
{
	MAP_PAGEOOB::iterator it;
	for(it = m_mapPageOobFile.begin(); it != m_mapPageOobFile.end(); it++)
	{
		EXT_IMG_INFO_PTR pImg = it->second;
		if(pImg != NULL)
		{
			pImg->clear();
			delete pImg;
			it->second = NULL;
		}
	}
	
	m_mapPageOobFile.clear();
	m_mapPageOobInfo.RemoveAll();
	m_mapPageInfo.RemoveAll();
}

EXT_IMG_INFO_PTR CMainFrame::LoadPageOobFile(LPCTSTR lpszFile)
{
    if( lpszFile == NULL )
        return NULL;
    
    CString strFile;
    GetAbsolutePath( strFile,lpszFile );
    if( strFile.IsEmpty() )
    {
        return NULL;
    }
    CString strMapName = strFile ;
    strMapName.Replace('\\','.');
    strMapName = "Local\\" + strMapName;
    
    HANDLE hCode = INVALID_HANDLE_VALUE;
    HANDLE hCodeMapView = NULL;
    
    hCode = CreateFile(strFile.operator LPCTSTR (),
        GENERIC_READ,
        FILE_SHARE_READ,                         //Exclusive Open
        NULL,                      //Can't Be inherited
        OPEN_EXISTING,             //If not existing then failed
        FILE_ATTRIBUTE_READONLY,   //Read Only
        NULL);
    if( hCode == INVALID_HANDLE_VALUE)
    {   
        return FALSE;
    }
    
    DWORD dwCodeSize = GetFileSize( hCode, NULL);
    if( dwCodeSize == INVALID_FILE_SIZE)
    {
        CloseHandle( hCode);
        return NULL;
    }
    
    hCodeMapView = CreateFileMapping( hCode,   //The Handle of Opened File
        NULL,          //Security
        PAGE_READONLY, //Read Only Access
        0,             //Max Size
        0,             //Min Size
        strMapName);   //Object Name
    
    if( hCodeMapView == NULL)
    {
        CloseHandle( hCode);
        return NULL;
    }
    
    void* lpCode = ::MapViewOfFile( hCodeMapView, FILE_MAP_READ,  0, 0, 0);    
    if( lpCode == NULL)
    {
        CloseHandle( hCode );
        CloseHandle( hCodeMapView );
        return NULL;
    }      
    

	EXT_IMG_INFO_PTR pImg = new EXT_IMG_INFO;
	if(pImg == NULL)
	{
		UnmapViewOfFile(lpCode);
		CloseHandle( hCode );
        CloseHandle( hCodeMapView );
	}

	pImg->bIsFileMap = TRUE;
	pImg->hFile = hCode;
	pImg->hFileMap = hCodeMapView;
	pImg->pBuf = (LPBYTE)lpCode;
	pImg->dwSize = dwCodeSize;
	_tcscpy(pImg->szFilePath,strFile);
    
    return pImg; 
}

void CMainFrame::OnStartDownload(WPARAM wParam,LPARAM lpParam)
{
	UNUSED_ALWAYS(wParam);
	UNUSED_ALWAYS(lpParam);

	CWaitCursor wait;

	CString strErrMsg;
    if( FAILED( CoInitialize(NULL) ) )
    {
        strErrMsg.LoadString( IDS_ATL_INIT_FAIL );
        AfxMessageBox( strErrMsg );
		m_bStarted = FALSE;
        return;
    }    

	if(m_sheetSettings.IsEraseAll())
	{
		strErrMsg = _T("You have selected [Erase All Flash], this will clear all data\nstored in the flash including calibration.\nIf continue?");
		if(AfxMessageBox( strErrMsg,MB_YESNO )==IDNO)
		{
			m_bStarted = FALSE;
			return;
		}
	}
	
#if defined( _DOWNLOAD_FOR_PRODUCTION ) || defined(_SPUPGRADE)
#else
	if(m_bPacketOpen && !m_strPacketPath.IsEmpty())
	{
		CFileFind finder;
		if(finder.FindFile(m_strPacketPath))
		{
			CTime tm;
			finder.FindNextFile();
			finder.GetLastWriteTime(tm);
			if(tm != m_tmPacModifyTime)
			{
				if(AfxMessageBox( _T("Pac file is updated, do want load the new pac file?"),MB_YESNO )==IDYES)
				{
					 if(!LoadPac())
					 {
						 DeletePacTmpDir();
						 m_bStarted = FALSE;
						 return;
					 }
				}
			}
		}
	}
#endif

	wait.Restore();
    m_arrFile.RemoveAll();
	m_sheetSettings.GetDownloadFile(m_arrFile);
	m_nFileCount = m_arrFile.GetSize();

	SAFE_DELETE_ARRAY(m_pNVFileBuf);

	m_strCodeChipID = _T("");
	//////////////////////////////////////////////////////////////////////////
	//  check files
	if(!CheckDLFiles())
	{
		m_bStarted = FALSE;
		return;
	}	

	//////////////////////////////////////////////////////////////////////////	
	// get multi-files for different chip name.
	if(!InitMultiFileBuf())
	{
		m_bStarted = FALSE;
		return;
	}
	
	//////////////////////////////////////////////////////////////////////////
	//  initialize report success infomation
	if(!InitReportInfo())
	{
		m_bStarted = FALSE;
		return;
	}
	
	//////////////////////////////////////////////////////////////////////////	
	// init NV buffer
	if(!InitNVBuffer())
	{
		m_bStarted = FALSE;
		return;
	}

	//////////////////////////////////////////////////////////////////////////	
	// init UDiskIMG buffers
	if(!InitUDiskBufMap())
	{
		m_bStarted = FALSE;
		return;
	}
	
	//////////////////////////////////////////////////////////////////////////	
	// init DSP files buffers
	if(!InitChipDspBufmap())
	{
		m_bStarted = FALSE;
		return;
	}

	//////////////////////////////////////////////////////////////////////////	
	// get chip ID string from FDL2
	InitCodeChipID();
	
	//////////////////////////////////////////////////////////////////////////	
	// get IMEI ID from BMFileType.ini
	if(!InitIMEIID())
	{
		m_bStarted = FALSE;
		return;
	}

	//////////////////////////////////////////////////////////////////////////	
	// init page-oob file buffer
	if(!InitPageOobFile())
	{
		m_bStarted = FALSE;
		return;
	}    

	((CDLoaderView *)(this->GetActiveView()))->StartDLTimer();
		
    StartWork();
}

BOOL CMainFrame::SaveNVToLocal(_BMOBJ *pbj, BYTE *pBuf, DWORD dwSize)
{
	USES_CONVERSION;
	CString strErrMsg;
	CString strLocalPath = m_sheetSettings.GetNVSavePath();
	
	if(strLocalPath.IsEmpty())
	{
		strErrMsg.Format(_T("NV save to local: the path is empty."));
		_tcscpy(pbj->szErrorMsg,strErrMsg);
		return E_FAIL;
	}//end if(strLocalPath.IsEmpty())			
	
	CString strFullPath;
	HANDLE hFile = INVALID_HANDLE_VALUE;
/*
	BOOL bSNEmpty = FALSE;
	if(strlen(pbj->szSN)==0)
	{
		bSNEmpty = TRUE;
	}//end if(strlen(pbj->szSN)==0)
	

	if(!bSNEmpty)
	{
		
#if defined(_UNICODE) || defined(UNICODE)
		_TCHAR szWSN[100]={0};
		MultiByteToWideChar(CP_ACP,0,pbj->szSN,strlen(pbj->szSN),szWSN,100);
		strFullPath.Format(_T("%s\\%s.bin"),strLocalPath.operator LPCTSTR(),szWSN);
#else
		strFullPath.Format(_T("%s\\%s.bin"),strLocalPath.operator LPCTSTR(),pbj->szSN);
#endif
		DWORD dwAtt = 0;
		CFileFind finder;
		if(finder.FindFile(strFullPath))
		{
			dwAtt	= ::GetFileAttributes(strFullPath);
			dwAtt &= ~FILE_ATTRIBUTE_READONLY;
			::SetFileAttributes(strFullPath,dwAtt);
		}//end if(finder.FindFile(strFullPath))
		finder.Close();				
		
		hFile = CreateFile(strFullPath,GENERIC_WRITE,FILE_SHARE_READ,NULL,CREATE_ALWAYS,FILE_ATTRIBUTE_NORMAL,NULL);
		
	}
	
	if(!bSNEmpty && hFile != INVALID_HANDLE_VALUE)						
	{
		DWORD dwWritten=0;
		WriteFile(hFile,pDestCode,dwCodeSize,&dwWritten,NULL);
	}//end if(!bSNEmpty && hFile != INVALID_HANDLE_VALUE)	
	else // maybe there is no SN in phone, so there maybe be invalidate char in SN buffer*/
	{    // use IMEI replace of SN as file name.
				
		SYSTEMTIME timeCur;
		::GetLocalTime(&timeCur);		
		strFullPath.Format(_T("%s\\NV_IMEI%s_%4d-%02d-%02d_%02d-%02d-%02d-%03d"),strLocalPath.operator LPCTSTR(),A2T(pbj->szIMEI),
			timeCur.wYear,
			timeCur.wMonth,
			timeCur.wDay,
			timeCur.wHour,
			timeCur.wMinute,
			timeCur.wSecond,
			timeCur.wMilliseconds);
		CFileFind finder;
		int ext = 1;
		while(finder.FindFile(strFullPath+_T(".bin")))
		{
			CString strExt;
			strExt.Format(_T("(%d)"),ext++);
			strFullPath += strExt;
		}
		strFullPath += _T(".bin");
		finder.Close();
		hFile = CreateFile(strFullPath,GENERIC_WRITE,FILE_SHARE_READ,NULL,CREATE_ALWAYS,FILE_ATTRIBUTE_NORMAL,NULL);
		if(hFile != INVALID_HANDLE_VALUE)						
		{
			DWORD dwWritten=0;
			WriteFile(hFile,pBuf,dwSize,&dwWritten,NULL);
			CloseHandle(hFile);	
		}
		else
		{
			strErrMsg.Format(_T("[save nv to local] Create file [%s] fail."),strFullPath.operator LPCTSTR());
			_tcscpy(pbj->szErrorMsg,strErrMsg);
			return FALSE;
		}
	}//end if(!bSNEmpty && hFile != INVALID_HANDLE_VALUE)	
	 //end else		
	
	return TRUE;
}

LRESULT CMainFrame::OnDevHound(WPARAM wParam,LPARAM lpParam)
{
	if( m_bAutoStart)
    {
		unsigned int nPort = (unsigned int)wParam;
		_TCHAR szName[DEV_NAME_MAX_LEN] = {0};
		m_usbMoniter.GetPortName(nPort,szName);

		CString strPortName = szName;
		int nSelPort = m_sheetSettings.GetComPort();
        switch(lpParam)
        {
        case 1: //DBT_DEVICEARRIVAL              
			if(nSelPort != 0 && nPort != nSelPort)
			{
				break;
			}
			if(g_theApp.m_bFilterPort)
			{
				DWORD dwValue = 0;
				if(g_theApp.m_mapFilterPort.Lookup((DWORD)nPort,dwValue))
				{
					break;
				}
			}  	
			
			if(m_sheetSettings.IsNeedRebootByAT())
			{
				// Is AT port?
				if(IsATPort(strPortName))
				{
					SendAT2Reboot(nPort);
					break;
				}
				
				// If not DL port, break
				if(!IsDLPort(strPortName))
				{
					break;
				}
			}
#ifndef _DOWNLOAD_FOR_PRODUCTION
            DL_STAGE stage;
            if( ((CDLoaderView*)GetActiveView())->GetStatus( nPort,stage ) )
            {
                if( DL_FINISH_STAGE != stage && DL_NONE_STAGE != stage && DL_UNPLUGGED_STAGE != stage)
                {
                    // Last download process is not finished.
                    break;
                }
            }
#endif
			// The port plugged is regarded as USB
			if(g_theApp.m_bManual)
			{
				((CDLoaderView*)GetActiveView())->AddProg( nPort, TRUE ); 
				CreatePortData( nPort );
			}
			else
			{
				StartOnePortWork(nPort,FALSE);	
			}
    
            break;
        case 0: //DBT_DEVICEREMOVECOMPLETE
            
			if(nSelPort != 0 && nPort != nSelPort)
			{
				break;
			}

			if(g_theApp.m_bFilterPort)
			{
				DWORD dwValue = 0;
				if(g_theApp.m_mapFilterPort.Lookup((DWORD)nPort,dwValue))
				{
					break;
				}
			}				
#ifndef _DOWNLOAD_FOR_PRODUCTION
			DL_STAGE _stage;
            if( ((CDLoaderView*)GetActiveView())->GetStatus( nPort,_stage ) )
            {
                if( DL_NONE_STAGE == _stage)
                {
                    // Last download process is not finished.
                    break;
                }
            }
#endif
			StopOnePortWork((DWORD)nPort,TRUE);
			// CDLoaderView displays [Removed].
			((CDLoaderView*)GetActiveView())->SetResult( nPort,0,NULL,2);         
            break;
        default:
            break;
        }
    }	
	return 0L;
}

BOOL CMainFrame::SendAT2Reboot(UINT nPort)
{
	ICommChannel *pChannel;
	CreateChannel(&pChannel,CHANNEL_TYPE_COM);

	char szCmd[] = "AT+SPREF=\"AUTODLOADER\"\r\n";

	CHANNEL_ATTRIBUTE ca;
	ca.ChannelType = CHANNEL_TYPE_COM;
	ca.Com.dwPortNum = nPort;
	ca.Com.dwBaudRate = 115200;

	if(pChannel->Open(&ca))
	{
		pChannel->Write(szCmd,strlen(szCmd));
		pChannel->Write(szCmd,strlen(szCmd));
		pChannel->Write(szCmd,strlen(szCmd));
	}

	pChannel->Close();
	ReleaseChannel(pChannel);

	return TRUE;
}

BOOL CMainFrame::IsATPort(LPCTSTR lpszPort)
{
	if(m_agATPort.GetSize()==0)
		return FALSE;

	CString strPortName = lpszPort;

	for(int i = 0; i< m_agATPort.GetSize(); i++)
	{
		CString strATPort = m_agATPort[i];
		if( strPortName.Find(strATPort) != -1)
		{
			return TRUE;
		}
	}
	
	return FALSE;
}

BOOL CMainFrame::IsDLPort(LPCTSTR lpszPort)
{
	if(m_agDLPort.GetSize()==0)
		return TRUE;
	
	CString strPortName = lpszPort;
	
	for(int i = 0; i< m_agDLPort.GetSize(); i++)
	{
		CString strDLPort = m_agDLPort[i];
		if( strPortName.Find(strDLPort) != -1)
		{
			return TRUE;
		}
	}
	
	return FALSE;
}