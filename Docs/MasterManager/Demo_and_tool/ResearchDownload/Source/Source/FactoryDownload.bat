echo on

set COMPCP=..\..\..\common\bin\compcp.bat
set SRC_DIR=..\..\..\common\bin
set DST_DIR=..\..\..\Release\FactoryDownload\Bin
set FILES=Channel.dll DiagChan.dll BMPlatform.dll FactoryDownload.ini FactoryDownload.exe BMFileType.ini BMError.ini BMTimeout.ini BMAConfig.xml BMAFrame.dll fdl_bkmark.bmp MCPType.ini

for %%f in (%FILES%) do (
@call "%COMPCP%" "%SRC_DIR%\%%f" "%DST_DIR%\%%f"
)

@call "%COMPCP%" "..\..\..\NPI\common\bin\PhaseCheck.ini" "%DST_DIR%\PhaseCheck.ini"
