echo off

set COMPCP=..\..\..\common\bin\compcp.bat
set SRC_DIR=..\..\..\common\bin
set DST_DIR=..\..\..\Release\ResearchDownload\Bin
set FILES=Channel.dll DiagChan.dll BMPlatform.dll ResearchDownload.ini ResearchDownload.exe BMFileType.ini BMError.ini BMTimeout.ini BMAConfig.xml BMAFrame.dll rdl_bkmark.bmp MCPType.ini PhaseCheck.ini 

set _COMP=ture

for %%f in (%FILES%) do (
@call "%COMPCP%" "%SRC_DIR%\%%f" "%DST_DIR%\%%f" "%_COMP%"
)

