// PageOptions.cpp : implementation file
//

#include "stdafx.h"
#include "dloader.h"
#include "PageOptions.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPageOptions property page

IMPLEMENT_DYNCREATE(CPageOptions, CPropertyPage)

CPageOptions::CPageOptions() : CPropertyPage(CPageOptions::IDD)
{
	//{{AFX_DATA_INIT(CPageOptions)
	m_bTmpRepart = FALSE;
	m_bTmpReset = FALSE;
	m_bTmpReadMcpType = FALSE;
	//}}AFX_DATA_INIT

	m_bReadMcpType = m_bTmpReadMcpType;
	m_bRepart = m_bTmpRepart;
	m_bReset = m_bTmpReset;

	m_nFlashPageType = 0;

}

CPageOptions::~CPageOptions()
{
}

void CPageOptions::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CPageOptions)
	DDX_Check(pDX, IDC_DOP_REPART, m_bTmpRepart);
	DDX_Check(pDX, IDC_DOP_RESET, m_bTmpReset);
	DDX_Check(pDX, IDC_FOD_CHK_READ_MCPTYPE, m_bTmpReadMcpType);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CPageOptions, CPropertyPage)
	//{{AFX_MSG_MAP(CPageOptions)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPageOptions message handlers

BOOL CPageOptions::OnInitDialog() 
{
	CPropertyPage::OnInitDialog();
	
	// TODO: Add extra initialization here
	m_bTmpRepart = m_bRepart;
	m_bTmpReset  = m_bReset;

	m_bTmpReadMcpType = m_bReadMcpType;


	UpdateData(FALSE);

#if defined(_SPUPGRADE) || defined(_DOWNLOAD_FOR_PRODUCTION)
	GetDlgItem(IDC_DOP_REPART)->EnableWindow(FALSE);
#endif 
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CPageOptions::OnOK() 
{
	// TODO: Add your specialized code here and/or call the base class
	UpdateData();

	m_bReadMcpType = m_bTmpReadMcpType;
	m_bRepart = m_bTmpRepart;
	m_bReset = m_bTmpReset;

	if(!m_strIniFile.IsEmpty())
	{
		DWORD dwAttr = GetFileAttributes(m_strIniFile);
		if(MAXDWORD != dwAttr)
		{
			dwAttr &= ~FILE_ATTRIBUTE_READONLY;
			::SetFileAttributes(m_strIniFile,dwAttr);
		}
		SaveSettings(m_strIniFile);
	}

	CPropertyPage::OnOK();
}


BOOL CPageOptions::LoadSettings(LPCTSTR pFileName,CString &strErrorMsg)
{
	//	UNUSED_ALWAYS(strErrorMsg);
    ASSERT(NULL != pFileName);
    if(NULL == pFileName)
    {
		strErrorMsg += _T("Configure file is empty!\n");
        return FALSE;
    }
    m_strIniFile = pFileName;	

	m_bRepart = GetPrivateProfileInt( _T("Options"),_T("Repartition"),0,pFileName );
	m_bReset = GetPrivateProfileInt( _T("Options"),_T("Reset"),0,pFileName );
	m_nFlashPageType = GetPrivateProfileInt( _T("Options"),_T("FlashPageType"),0,pFileName );
	m_bReadMcpType = GetPrivateProfileInt( _T("Options"),_T("ReadMCPType"),0,pFileName );
	m_bTmpRepart = m_bRepart;
	m_bTmpReset = m_bReset;
	m_bTmpReadMcpType = m_bReadMcpType;

	
    return TRUE;
}

BOOL CPageOptions::SaveSettings(LPCTSTR pFileName)
{
    ASSERT(NULL != pFileName);
    if(NULL == pFileName)
    {
        return FALSE;
    }
	
    CString strTemp;
    strTemp.Format( _T("%d"),m_bRepart );
    WritePrivateProfileString(  _T("Options"),_T("Repartition"),strTemp,pFileName );
	
    strTemp.Format( _T("%d"),m_bReset );
    WritePrivateProfileString( _T("Options"),_T("Reset"),strTemp,pFileName );
    
    return TRUE;
}

int CPageOptions::GetGUINandRepartStrategy()
{
	if(::IsWindow(this->GetSafeHwnd()))
	{
		UpdateData(FALSE);
	}
	
	return (m_bTmpRepart ? REPAR_STRATEGY_ALWAYS : REPAR_STRATEGY_STOP);
}

int CPageOptions::GetNandRepartStrategy()
{
	return (m_bRepart ? REPAR_STRATEGY_ALWAYS : REPAR_STRATEGY_STOP);
}