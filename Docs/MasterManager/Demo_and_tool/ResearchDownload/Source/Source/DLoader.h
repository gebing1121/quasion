// DLoader.h : main header file for the DLOADER application
//

#if !defined(AFX_DLOADER_H__09DF2514_AE78_4B20_8103_993EB56996BA__INCLUDED_)
#define AFX_DLOADER_H__09DF2514_AE78_4B20_8103_993EB56996BA__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#define UDSIK_IMG_NAME_LEN		(13)

#include "resource.h"       // main symbols
#include "IBMAFramework.h"
#pragma warning(push,3)
#include <afxtempl.h>
#pragma warning(pop)
/////////////////////////////////////////////////////////////////////////////
// CDLoaderApp:
// See DLoader.cpp for the implementation of this class
//

class CDLoaderApp : public CWinApp
{
public:
	CDLoaderApp();

	IBMAFramework * m_pBMAFramework;
    //@ Liu Kai 2004-08-23
    CString m_strVersion;
    CString m_strBuild;
	
	BOOL    m_bShowOtherPage;
	
	//@ Hongliang Xin 2009-6-3
	BOOL    m_bScriptCtrl;
	CString m_strResultPath;		// result.txt
	BOOL    m_bResultPathWithPort; // result_com5.txt
	
	//@ Hongliang Xin 2009-7-7
	BOOL    m_bColorFlag;   // use color to mark the last failed.
	BOOL    m_bClosePortFlag;   // close port for USB,when finish the download
	BOOL    m_bResultHolding;   // when start, the waiting state holding the previous result

	BOOL    m_bManual;
	
	BOOL    m_bCMDPackage;
	CString m_strPacPath;
	CString m_strPrdVersion;
	
	BOOL    m_bKeepPacNVState;
	
	BOOL    m_bShowMcpTypePage;
	
	BOOL    m_bNeedPassword;

	DWORD   m_dwWaitTimeForNextChip;
	
	BOOL    m_bFilterPort;
	CMap<DWORD,DWORD,DWORD,DWORD> m_mapFilterPort;
	
	CString m_strFileFilter;


	/************************************************************************/
	/*if 0, not backup below data for GSM calibration Ver FF0A

		    RF_ramp_delta_timing for GSM calibration

	  if 1, not backup below data for GSM calibration Ver FF0A

	        temperature_and_voltage_composate_structure (reserved)
	        RF_ramp_table
	        RF_ramp_PA_power_on_duration
	        RF_ramp_delta_timing

	  if 2, backup below data for GSM calibration Ver FF0A	

			rf_common_param_dsp_use		
			
			rf_gsm_param_dsp_use->agc_ctrl_word
			rf_gsm_param_dsp_use->rx_compensate_value
			rf_gsm_param_dsp_use->max_rf_gain_index	
			
			rf_gsm_param_dsp_use->rf_ramppwr_step_factor
			rf_gsm_param_dsp_use->rf_edge_tx_gain_table
			rf_gsm_param_dsp_use->rf_8psk_tx_compensation
            rf_gsm_param_dsp_use->rf_ramp_param_constant_value  
	
	        adc                                                             */
	/************************************************************************/
	int     m_nGSMCaliVaPolicy;

protected:
	void LoadFilterPortSetting(LPCTSTR pszConfigFile);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDLoaderApp)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	//}}AFX_VIRTUAL

// Implementation
	//{{AFX_MSG(CDLoaderApp)
	afx_msg void OnAppAbout();
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

// declare the global App instance
extern CDLoaderApp g_theApp;
/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLOADER_H__09DF2514_AE78_4B20_8103_993EB56996BA__INCLUDED_)
