// MainFrm.h : interface of the CMainFrame class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_MAINFRM_H__1DDDF3B9_ED10_47BF_8EF9_3AD8B814C217__INCLUDED_)
#define AFX_MAINFRM_H__1DDDF3B9_ED10_47BF_8EF9_3AD8B814C217__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#pragma warning(disable : 4786)
#pragma warning(push,3)
#include <map>
#pragma warning(pop)

#include <afxtempl.h>
#include "BMAGlobal.h"

#include "IBMAFramework.h"

#include "SettingsSheet.h"

#include "DevHound.h"

#include <atlbase.h>
#ifndef _lint // bad lint
extern CComModule _Module;
#endif
#include <atlcom.h>
#include "BootModeitf.h"
#include "BootModeguid.h"

#include "XRandom.h"

#include "CoolToolBar.h"
//#include "FlashInfo.h"
#include "./phasecheck/PhaseCheckBuild.h"

#ifndef _SPUPGRADE
#include "BarcodeDlg.h"
#define WM_REQUIRE_SN			(WM_USER + 0x1213)
#endif

#define PRODUCTION_INFO_SIZE    (MAX_PRODUCTIONINFO_SIZE)         //8K
#define X_SN_LEN                (MAX_SN_LEN)

struct PORT_DATA
{
    DWORD  dwPort;
    LPBYTE lpPhaseCheck;
	HANDLE hSNEvent;
};

#define SAFE_DELETE(p)  \
do \
{\
    if (p != NULL) \
    {\
       delete p; \
	   p=NULL;\
	}\
} while(0) \

#define SAFE_DELETE_ARRAY(p)  \
do \
{\
    if (p != NULL) {\
       delete []p; \
	   p=NULL;\
	}\
} while(0) \

#define  MAX_RECEIVE_SIZE   0x0800

#define WM_STOP_ONE_PORT		(WM_USER + 0x1214)
#define WM_POWER_MANAGE         (WM_USER + 1002)
#define WM_DWONLOAD_START       (WM_USER + 1012)
#define WM_DEV_HOUND			(WM_USER + 1022)


typedef struct _BACKUP_INFO
{
	BYTE* pBuf;
	DWORD dwSize;
}BACKUP_INFO;

#define MAX_BACKUP_FILE_NUM 10

struct _BMOBJ
{
	_BMOBJ()
	{
		memset(this,0, sizeof(_BMOBJ));
	}

	void Clear()
	{
		SAFE_DELETE_ARRAY(pBuf);
		for(int i =0; i< MAX_BACKUP_FILE_NUM;i++)
		{
			SAFE_DELETE_ARRAY(tFileBackup[i].pBuf);
		}

		memset(this,0, sizeof(_BMOBJ));
	}

    DWORD dwCookie;
	DWORD dwIsUart;
	DWORD dwChipID;
	DWORD aFlashType[4];
	DWORD dwBufSize;
	DWORD dwPage;
	DWORD dwOob;
	BYTE* pBuf;
	char  szSN[X_SN_LEN+1];
	char  szIMEI[X_SN_LEN+1];
	TCHAR szErrorMsg[_MAX_PATH*2];
	TCHAR szMcpInfo[_MAX_PATH];
	BACKUP_INFO tFileBackup[MAX_BACKUP_FILE_NUM];
};

typedef struct _EXT_IMG_INFO
{
	_EXT_IMG_INFO()
	{
		memset(this,0, sizeof(_EXT_IMG_INFO));
	}
	DWORD dwSize;
	BYTE *pBuf;
	TCHAR szFilePath[MAX_PATH];
	BOOL   bIsFileMap;
	HANDLE hFile;
	HANDLE hFileMap;

	void clear()
	{
		if(!bIsFileMap)
		{
			if(pBuf != NULL)
			{
				delete [] pBuf;
			}
		}
		else
		{
			if( pBuf != NULL )
			{
				::UnmapViewOfFile(pBuf  );	
			}
			if( hFile != NULL )
			{
				::CloseHandle( hFile );	
			}
			if( hFileMap != NULL )
			{
				::CloseHandle( hFileMap );	
			}
		}

		memset(this,0, sizeof(_EXT_IMG_INFO));
	}
}EXT_IMG_INFO,*EXT_IMG_INFO_PTR;

typedef CMap<DWORD,DWORD,EXT_IMG_INFO_PTR,EXT_IMG_INFO_PTR> MAP_EXTIMG;

typedef std::map<std::pair<DWORD,CString>,EXT_IMG_INFO_PTR> MAP_FILEBUF;

typedef std::map<std::pair<CString,CString>,EXT_IMG_INFO_PTR> MAP_PAGEOOB;

struct BMFileInfo_TAG;

class CMainFrame : public CFrameWnd
{
	
protected: // create from serialization only
	CMainFrame();
	DECLARE_DYNCREATE(CMainFrame)

// Attributes
protected:
    BOOL m_bStarted;

    BYTE m_lpReceiveBuffer[MAX_RECEIVE_SIZE];
    DWORD m_dwReceiveSize;
	long  m_lStartNVRef;

    int       m_nFileCount;
    CStringArray m_arrFile;

	IBMAFramework *m_pBMAF; 
	
	CString m_strPacketPath;
	CTime   m_tmPacModifyTime;

	CMap<DWORD, DWORD, _BMOBJ*, _BMOBJ* > m_mapBMObj;

public:
	CStringArray m_aPacReleaseDir;
	CString m_strSpecConfig;
	LPBYTE m_pNVFileBuf;
	DWORD  m_dwNvFileSize;

	CSettingsSheet m_sheetSettings;
	BOOL m_bPacketOpen;
	
	BOOL    m_bCheckCali;
	
	CString m_strCodeChipID;
	
	BOOL    m_bNeedPhaseCheck;	

	CXRandom m_rand;

	BOOL    m_bDoReport;
	
	CUIntArray m_aIMEIID;	
 
// Operations
public:
	BOOL GetIniFilePath(LPTSTR pName);
	CString GetSpecConfigFile();
    void SetPrdVersion(LPCTSTR lpszSpdVer);	

	PORT_DATA * GetPortDataByPort(int nPort);

	static DWORD WINAPI GetThreadFunc(LPVOID lpParam);
	DWORD ClosePortFunc(DWORD dwPort,BOOL bSuccess);
	BOOL StartOnePortWork(DWORD dwPort, BOOL bUart);

	void SetStatusBarText(LPCTSTR pStr){
		if(pStr != NULL)
		{
			m_wndStatusBar.SetPaneText(0,pStr,TRUE);
		}
	}

	void DeletePacTmpDir();

protected:
    //@ Liu Kai 2004-3-25 CR8123
    BOOL LoadSettings();
    
    void SetDLTitle();
    
	BOOL StopOnePortWork(DWORD dwPort,BOOL bRemoved = FALSE);

    void StartWork();
	
	DWORD BCDToWString(LPBYTE pBcd,DWORD dwSize,LPTSTR szStr,DWORD dwStrLen);

	void DoReport(DWORD dwOprCookie,BOOL bSuccess = TRUE);
	
	BOOL CheckDLFiles();
	BOOL InitReportInfo();
	BOOL InitNVBuffer();
	BOOL InitUDiskBufMap();	
	BOOL InitChipDspBufmap();
	void InitCodeChipID();
	BOOL InitPSFile();

	BOOL InitIMEIID();

	BOOL InitMultiFileBuf();
	void ClearMultiFileBuf();

	BOOL InitPageOobFile();
	void ClearPageOobFile();


	void ClearExtImgMap(MAP_EXTIMG &mapExtImg);
	
	void GetOprErrorCodeDescription(DWORD dwErrorCode,
									LPTSTR lpszErrorDescription, 
									int nSize );
	
	EXT_IMG_INFO_PTR LoadPageOobFile(LPCTSTR lpszFile);

	BOOL SaveNVToLocal(_BMOBJ *pbj, BYTE *pBuf, DWORD dwSize);

	BOOL SendAT2Reboot(UINT nPort);

	BOOL IsATPort(LPCTSTR lpszPort);
	BOOL IsDLPort(LPCTSTR lpszPort);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMainFrame)
	public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CMainFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:  // control bar embedded members
	CStatusBar		m_wndStatusBar;
	CCoolToolBar    m_wndToolBar;	

    void* m_pMasterImg;

	CRITICAL_SECTION m_csReportFile;
	FILE * m_pReportFile;

	DWORD  m_dwMaxNVLength;

	PORT_DATA* CreatePortData( DWORD nPort );
	CMap<DWORD,DWORD,PORT_DATA*,PORT_DATA*> m_mapPortData;
	CRITICAL_SECTION m_cs;


	MAP_EXTIMG m_mapUDiskIMg;
	MAP_EXTIMG m_mapChipDsp;
	MAP_FILEBUF m_mapMultiFileBuf;

	MAP_PAGEOOB m_mapPageOobFile;  // PageSize and OOB map
	CMap<CString, LPCTSTR, DWORD, DWORD> m_mapPageOobInfo;  // PageSize and OOB 
	CMap<CString, LPCTSTR, DWORD, DWORD> m_mapPageInfo;  // PageSize and OOB 

	CString    m_strOprErrorConfigFile;
	
	BOOL       m_bShowFailedMsgbox;
	BOOL       m_bFailedMsgboxShowed;

	CRITICAL_SECTION m_csPowerManage;	
public:	
	BOOL       m_bPowerManage;
	BOOL       m_bPMInDLProcess;
	DWORD      m_dwPowerMgrInter;
	CString    m_strLoadSettingMsg;
private:
	BOOL       LoadPac();

	CDevHound  m_usbMoniter;
	CStringArray m_agATPort;
	CStringArray m_agDLPort;
	
// Generated message map functions
protected:
	BOOL m_bAutoStart;
	//{{AFX_MSG(CMainFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSettings();
	afx_msg void OnStart();
	afx_msg void OnStop();
	afx_msg void OnUpdateStart(CCmdUI* pCmdUI);
	afx_msg void OnUpdateStop(CCmdUI* pCmdUI);
	afx_msg void OnClose();
	afx_msg void OnUpdateSettings(CCmdUI* pCmdUI);
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	afx_msg void OnLoadPacket();
	afx_msg void OnUpdateLoadPacket(CCmdUI* pCmdUI);
	//}}AFX_MSG
	afx_msg void OnWarnMessageBox(WPARAM wParam,LPARAM lpParam);
	afx_msg void OnInitalPacket(WPARAM wParam,LPARAM lpParam);
	afx_msg void OnStopOnePort(WPARAM wParam,LPARAM lpParam);
	afx_msg void OnPowerManage(WPARAM wParam,LPARAM lpParam);
    afx_msg void OnStartDownload(WPARAM wParam,LPARAM lpParam);
	afx_msg LRESULT OnDevHound(WPARAM wParam,LPARAM lpParam);
	DECLARE_MESSAGE_MAP()

    BEGIN_INTERFACE_PART( BMOprObserver, IBMOprObserver )
    
	STDMETHOD(OnStart)( THIS_ DWORD dwOprCookie , 
						DWORD dwResult );       
    
	STDMETHOD(OnEnd)(  THIS_ DWORD dwOprCookie, 
		               DWORD dwResult ); 
        
	STDMETHOD(OnOperationStart)(  THIS_ DWORD dwOprCookie, 
		                            const BSTR cbstrFileID,
									const BSTR cbstrFileType,		
									const BSTR cbstrOperationType,
									DWORD pBMFileInterface );     
    
	STDMETHOD(OnOperationEnd)(  THIS_ DWORD dwOprCookie, 
								const BSTR cbstrFileID,
								const BSTR cbstrFileType, 
								const BSTR cbstrOperationType, 
								DWORD dwResult,
								DWORD pBMFileInterface );  
    
	STDMETHOD(OnFileOprStart)( THIS_ DWORD dwOprCookie,
								const BSTR cbstrFileID, 
								const BSTR cbstrFileType , 
								DWORD pBMFileInterface );
    
	STDMETHOD(OnFileOprEnd)(    THIS_ DWORD dwOprCookie, 
								const BSTR cbstrFileID,
								const BSTR cbstrFileType, 
								DWORD dwResult );
	
	STDMETHOD(OnFilePrepare)(THIS_ DWORD dwOprCookie,
							 const BSTR bstrProduct,
							 const BSTR bstrFileName,
							 DWORD lpFileInfo,
							 /*[out]*/ DWORD      pBMFileInfoArr,
							 /*[out]*/ LPDWORD    lpBMFileInfoCount,
							 /*[out]*/ LPDWORD    lpdwFlag);
        
    END_INTERFACE_PART( BMOprObserver )
        
    DECLARE_INTERFACE_MAP()
        
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MAINFRM_H__1DDDF3B9_ED10_47BF_8EF9_3AD8B814C217__INCLUDED_)
