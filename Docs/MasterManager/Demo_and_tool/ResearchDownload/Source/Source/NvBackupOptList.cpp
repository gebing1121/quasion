// NvBackupOptList.cpp : implementation file
//

#include "stdafx.h"
#include "dloader.h"
#include "NvBackupOptList.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CNvBackupOptList

CNvBackupOptList::CNvBackupOptList()
{
	m_pImageList = NULL;
	m_pNvBkpItmArray = NULL;
	m_nNvBkpItmCount = 0;
	m_pTempNvBkpItmArray = NULL;
	m_nTempNvBkpItmCount = 0;
	m_bBackNV = TRUE;
	m_bTempBackNV = TRUE;
	
	m_nNvBkpItmChkCount = 0;
	m_nTempNvBkpItmChkCount=0;
	m_strNotChkItemName = _T("");
	m_strTmpNotChkItemName = _T("");
}


CNvBackupOptList::~CNvBackupOptList()
{
/*lint -save -e1551 */
	if(NULL != m_pImageList)
	{
		m_pImageList->DeleteImageList();
		delete m_pImageList;
		m_pImageList = NULL;
	}
	
	if( NULL != m_pNvBkpItmArray)
	{
		delete []m_pNvBkpItmArray;
		m_pNvBkpItmArray = NULL;
	}

	if( NULL != m_pTempNvBkpItmArray)
	{
		delete []m_pTempNvBkpItmArray;
		m_pTempNvBkpItmArray = NULL;
	}
/*lint -restore */
}


BEGIN_MESSAGE_MAP(CNvBackupOptList, CSuperGridCtrl)
	//{{AFX_MSG_MAP(CNvBackupOptList)
	ON_WM_CREATE()
	//}}AFX_MSG_MAP
//	ON_NOTIFY_REFLECT(NM_CLICK, OnItemCheck)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CNvBackupOptList message handlers

int CNvBackupOptList::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CSuperGridCtrl::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO: Add your specialized creation code here
    m_pImageList = new CImageList;

	if(!m_pImageList->Create(IDB_BMP_FOLDER,16,1,RGB(0, 255, 255)))
		return -1;
	
	SetImageList(m_pImageList, LVSIL_SMALL);
	CImageList *pImageList = GetImageList(LVSIL_SMALL);
	if(pImageList)
		ImageList_GetIconSize(pImageList->m_hImageList, &m_cxImage, &m_cyImage);
	else
		return -1;

	Init();
	
	return 0;
}
void CNvBackupOptList::Init()
{
	SetExtendedStyle(LVS_EX_GRIDLINES|LVS_EX_CHECKBOXES);
	CString strClmn;
	VERIFY( strClmn.LoadString(IDS_NV_BKP_OPTION_COLUMN) );
		
	int     nLen   = strClmn.GetLength();
	LPTSTR  lpBuf  = strClmn.GetBuffer(nLen);
	LPTSTR  lpFind = _tcschr(lpBuf, _T(','));
	int     nIndex = 0;
	while(lpFind != NULL)
	{
		*lpFind = _T('\0');
		InsertColumn(nIndex++,lpBuf,LVCFMT_LEFT,308);
		lpBuf = lpFind + 1;
		lpFind = _tcschr(lpBuf, _T(','));
	}
	InsertColumn(nIndex, lpBuf, LVCFMT_LEFT, 80 );
}

void CNvBackupOptList::InitNvBackupInfo(PNV_BACKUP_ITEM_T pNvBkpItmInfo, int nCount)
{
	//ASSERT(pNvBkpItmInfo!=NULL && nCount>0);

	if(m_pNvBkpItmArray != NULL)
	{
		delete []m_pNvBkpItmArray;
		m_pNvBkpItmArray = NULL;
	}
	if(m_pTempNvBkpItmArray != NULL)
	{
		delete []m_pTempNvBkpItmArray;
		m_pTempNvBkpItmArray = NULL;
	}
	
	m_nNvBkpItmCount = 0;
	m_nTempNvBkpItmCount = 0;

	m_nNvBkpItmChkCount = 0;
	m_nTempNvBkpItmChkCount=0;

	m_strNotChkItemName.Empty();
	m_strTmpNotChkItemName.Empty();
	
	if(pNvBkpItmInfo != NULL && nCount > 0)
	{
		m_nNvBkpItmCount = nCount;
		m_nTempNvBkpItmCount = nCount;
		m_pNvBkpItmArray = new NV_BACKUP_ITEM_T[nCount];
		m_pTempNvBkpItmArray = new NV_BACKUP_ITEM_T[nCount];
		memcpy(m_pNvBkpItmArray,pNvBkpItmInfo, sizeof(NV_BACKUP_ITEM_T)*nCount);
		memcpy(m_pTempNvBkpItmArray,pNvBkpItmInfo, sizeof(NV_BACKUP_ITEM_T)*nCount);

		for(int i = 0; i< nCount; i++)
		{
			if(m_pNvBkpItmArray[i].wIsBackup)
			{
				m_nNvBkpItmChkCount++;
				m_nTempNvBkpItmChkCount++;
			}
			else
			{
				m_strNotChkItemName += m_pNvBkpItmArray[i].szItemName;
				m_strNotChkItemName += _T(",");
				
				m_strTmpNotChkItemName += m_pNvBkpItmArray[i].szItemName;
				m_strTmpNotChkItemName += _T(",");
			}
		}
		if(!m_strNotChkItemName.IsEmpty())
		{
			m_strNotChkItemName.TrimRight(_T(','));
			m_strTmpNotChkItemName.TrimRight(_T(','));
		}
	}
}
//override called when OnLButtondown
void CNvBackupOptList::OnControlLButtonDown(UINT nFlags, CPoint point, LVHITTESTINFO& ht)
{
	UNUSED_ALWAYS(nFlags);
	UNUSED_ALWAYS(point);
	UNUSED_ALWAYS(ht);
	return;
}

BOOL CNvBackupOptList::OnItemExpanding(CTreeItem *pItem, int iItem)
{
	UNUSED_ALWAYS(iItem);
	CItemInfo* lp = GetData(pItem);
	if(lp!= NULL && lp->GetCheck())
		return TRUE;
	return FALSE;
}


BOOL CNvBackupOptList::OnItemExpanded(CTreeItem* pItem, int iItem)
{
	UNUSED_ALWAYS(pItem);
	UNUSED_ALWAYS(iItem);
	return TRUE;
}


BOOL CNvBackupOptList::OnCollapsing(CTreeItem *pItem)
{
	UNUSED_ALWAYS(pItem);
	return TRUE;
}

BOOL CNvBackupOptList::OnItemCollapsed(CTreeItem *pItem)
{
	UNUSED_ALWAYS(pItem);
	return TRUE;
}


BOOL CNvBackupOptList::OnItemLButtonDown(LVHITTESTINFO& ht)
{
	UNUSED_ALWAYS(ht);
	return TRUE;
}

BOOL CNvBackupOptList::OnVkReturn()
{
	return TRUE;
}

BOOL CNvBackupOptList::OnDeleteItem(CTreeItem* pItem, int nIndex)
{
	UNUSED_ALWAYS(pItem);
	UNUSED_ALWAYS(nIndex);
	return 0;
}

COLORREF CNvBackupOptList::GetCellRGB()
{
	return RGB(192,0,0);
}

CImageList *CNvBackupOptList::CreateDragImageEx(int nItem)
{
	UNUSED_ALWAYS(nItem);
	return NULL;
}

int CNvBackupOptList::GetIcon(const CTreeItem* pItem)
{
	if(pItem!=NULL)
	{
		int n = GetData(pItem)->GetImage();
		if(n!=-1)
			return n;
		
		int iImage = 0;
		if(ItemHasChildren(pItem))
		{
			IsCollapsed(pItem) ? iImage = 1/*close icon*/:iImage = 0;/*open icon*/
		}
		else
			iImage = 2;//doc icon
		return iImage;
	}
	return 0;
}
void CNvBackupOptList::AddNvBackupItemInfo(PNV_BACKUP_ITEM_T pNvBkpItmInfo)
{
	ASSERT(pNvBkpItmInfo != NULL);
	if(pNvBkpItmInfo == NULL)
	{
		return;
	}

	CItemInfo* lp = new CItemInfo();
	lp->SetCheck(pNvBkpItmInfo->wIsBackup);
	lp->SetItemText(pNvBkpItmInfo->szItemName);	
	//add subitem text
	CString strValue;
	if(pNvBkpItmInfo->dwID != 0xFFFFFFFF)
		strValue.Format(_T("0x%X"),pNvBkpItmInfo->dwID);
	else
		strValue = _T("");
	lp->AddSubItemText(strValue);      // 0 zero based subitems...
    //Create root item
	CTreeItem * pRoot = InsertRootItem(lp);//previous on N.Y.P.D we call it CreateTreeCtrl(lp)
	if( pRoot == NULL )
	{
		/*lint -save -e429*/
		return;
		/*lint -restore*/
	}
	//insert items	
	int nSubCount = pNvBkpItmInfo->dwFlagCount;
	if(pNvBkpItmInfo->wIsUseFlag)
	{	
		CString strText;
		for(int i=0; i < nSubCount; i++)
		{
			CItemInfo* lpItemInfo = new CItemInfo();
			//add items text
			//lpItemInfo->SetItemText(pNvBkpItmInfo->nbftArray[i].szFlagName);
			if(_tcscmp(pNvBkpItmInfo->nbftArray[i].szFlagName,_T("Replace"))==0)
			{
				//strText.LoadString(IDS_NV_BKP_FLAG_REPLACE);
				strText.Format(IDS_NV_BKP_FLAG_REPLACE,pNvBkpItmInfo->szItemName);
			}
			else
			{
				//strText.LoadString(IDS_NV_BKP_FLAG_CONTINUE);
				strText.Format(IDS_NV_BKP_FLAG_CONTINUE,pNvBkpItmInfo->szItemName);
			}
		
			
			lpItemInfo->SetItemText(strText);
			lpItemInfo->SetCheck(pNvBkpItmInfo->nbftArray[i].dwCheck);
			//lpItemInfo->SetCheck(1);
			//insert the iteminfo with ParentPtr
			InsertItem(pRoot, lpItemInfo);
		/*lint -save -e429*/
		}
		/*lint -restore*/
	}
//	Expand(pRoot, 0 /*listview index 0*/); 
//	UINT uflag = LVIS_SELECTED | LVIS_FOCUSED;
//	SetItemState(0, uflag, uflag);

/*lint -save -e429*/
}/*lint -restore*/
void CNvBackupOptList::AddNvBackupItemInfo(PNV_BACKUP_ITEM_T pNvBkpItmInfo, int nCount)
{
	ASSERT(pNvBkpItmInfo!=NULL && nCount>0);

	DeleteAll();

	if(m_pTempNvBkpItmArray != NULL)
	{
		delete []m_pTempNvBkpItmArray;
		m_pTempNvBkpItmArray = NULL;
	}

	m_nTempNvBkpItmCount =0;

	if(pNvBkpItmInfo != NULL && nCount > 0)
	{
		m_pTempNvBkpItmArray = new NV_BACKUP_ITEM_T[nCount];
		memcpy(m_pTempNvBkpItmArray,pNvBkpItmInfo, sizeof(NV_BACKUP_ITEM_T)*nCount);
		m_nTempNvBkpItmCount = nCount;
		for(int i = 0; i<nCount; i++)
		{
			AddNvBackupItemInfo(pNvBkpItmInfo+i);
		}
	}

}
BOOL CNvBackupOptList::SaveSettings(LPCTSTR lpszIniFile)
{
	UNUSED_ALWAYS(lpszIniFile);
	return TRUE;
}

void CNvBackupOptList::Update()
{
	if(GetItemCount()==0  || m_pTempNvBkpItmArray == NULL)
		return;
	m_nNvBkpItmCount = m_nTempNvBkpItmCount;
	if(m_pNvBkpItmArray != NULL)
	{
		delete []m_pNvBkpItmArray;
		m_pNvBkpItmArray = NULL;
	}
	if(m_nNvBkpItmCount == 0)
	{
		return;
	}

	m_pNvBkpItmArray = new NV_BACKUP_ITEM_T[m_nNvBkpItmCount];
	if(m_pNvBkpItmArray == NULL)
	{
		return;
	}
	memcpy(m_pNvBkpItmArray,m_pTempNvBkpItmArray, sizeof(NV_BACKUP_ITEM_T)*m_nNvBkpItmCount);
	
//	if(m_pTempNvBkpItmArray != NULL)
//	{
//		delete []m_pTempNvBkpItmArray;
//		m_pTempNvBkpItmArray = NULL;
//		m_nTempNvBkpItmCount = 0;
// 	}
	
	//do a GetHeadPosition
	POSITION pos = GetRootHeadPosition();
	int nIndex = 0;
	DWORD dwFlag = 0x0;
	CItemInfo* lp = NULL;
	m_nNvBkpItmChkCount = 0;
	m_strNotChkItemName.Empty();
	while(pos != NULL)
	{
		CTreeItem *pParent = (CTreeItem*)GetNextRoot(pos); 
		CTreeItem *pItem = pParent;
		lp = GetData(pParent);
		if(lp->GetCheck())//very hard :)=
		{
			m_pNvBkpItmArray[nIndex].wIsBackup = TRUE;
			dwFlag |= 0x1;
			m_nNvBkpItmChkCount++;
		}
		else
		{
			m_pNvBkpItmArray[nIndex].wIsBackup = FALSE;
			m_strNotChkItemName += m_pNvBkpItmArray[nIndex].szItemName;
			m_strNotChkItemName += _T(",");
		}
		//GetNext ....loop through all children 
		for(int i=0;i<MAX_NV_BACKUP_FALG_NUM;i++)
		{
			CTreeItem *pCur = GetNext(pParent, pItem, TRUE, FALSE/*regardless of the item are hidden or not*/);	  
			if(!IsChildOf(pParent, pCur))
				break;
			else
				if(pCur==pItem)
					break;				
				
			lp = GetData(pCur);
			if(lp->GetCheck())
			{
				m_pNvBkpItmArray[nIndex].nbftArray[i].dwCheck = TRUE;
			}
			else
			{
				m_pNvBkpItmArray[nIndex].nbftArray[i].dwCheck = FALSE;
			}
			pItem=pCur;
		}
		nIndex++;
	}
	if(!m_strNotChkItemName.IsEmpty())
	{
		m_strNotChkItemName.TrimRight(_T(','));
	}
	if(dwFlag == 0x1)
		m_bBackNV = TRUE;
	else
		m_bBackNV = FALSE;
}

void CNvBackupOptList::UpdateTemp()
{
	if(!GetItemCount())
		return;
	
	if(m_pTempNvBkpItmArray == NULL)
	{
		return;
	}
	
	//do a GetHeadPosition
	POSITION pos = GetRootHeadPosition();
	int nIndex = 0;
	DWORD dwFlag = 0x0;
	CItemInfo* lp = NULL;
	m_nTempNvBkpItmChkCount = 0;
	m_strTmpNotChkItemName.Empty();
	while(pos != NULL)
	{
		CTreeItem *pParent = (CTreeItem*)GetNextRoot(pos); 
		CTreeItem *pItem = pParent;
		lp = GetData(pParent);
		if(lp->GetCheck())//very hard :)=
		{
			m_pTempNvBkpItmArray[nIndex].wIsBackup = TRUE;
			dwFlag |= 0x1;
			m_nTempNvBkpItmChkCount++;
		}
		else
		{
			m_pTempNvBkpItmArray[nIndex].wIsBackup = FALSE;
			m_strTmpNotChkItemName += m_pTempNvBkpItmArray[nIndex].szItemName;
			m_strTmpNotChkItemName += _T(",");
		}
		//GetNext ....loop through all children 
		for(int i=0;i<MAX_NV_BACKUP_FALG_NUM;i++)
		{
			CTreeItem *pCur = GetNext(pParent, pItem, TRUE, FALSE/*regardless of the item are hidden or not*/);	  
			if(!IsChildOf(pParent, pCur))
				break;
			else
				if(pCur==pItem)
					break;				
				
			lp = GetData(pCur);
			if(lp->GetCheck())
			{
				m_pTempNvBkpItmArray[nIndex].nbftArray[i].dwCheck = TRUE;
			}
			else
			{
				m_pTempNvBkpItmArray[nIndex].nbftArray[i].dwCheck = FALSE;
			}
			pItem=pCur;
		}
		nIndex++;
	}
	if(!m_strTmpNotChkItemName.IsEmpty())
	{
		m_strTmpNotChkItemName.TrimRight(_T(','));
	}

	if(dwFlag == 0x1)
		m_bTempBackNV = TRUE;
	else
		m_bTempBackNV = FALSE;
}

void CNvBackupOptList::OnItemCheck(LPNMITEMACTIVATE lpnmitem)
{
	CTreeItem * pTreeItem = GetTreeItem(lpnmitem->iItem);
	ASSERT(pTreeItem != NULL);
    CItemInfo* lp = GetData(pTreeItem);
	if(lp!=NULL && !lp->GetCheck() && ItemHasChildren(pTreeItem))
	{
		Collapse(pTreeItem);
	}
}

void CNvBackupOptList::FillList()
{
	this->DeleteAll();
	if(m_pNvBkpItmArray != NULL)
	{
		for(int i = 0; i<m_nNvBkpItmCount; i++)
		{
			AddNvBackupItemInfo(m_pNvBkpItmArray+i);
		}
	}
}
