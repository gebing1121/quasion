#if !defined(AFX_SETTINGSSHEET_H__3B16967D_5736_432E_A398_2FE0117D093C__INCLUDED_)
#define AFX_SETTINGSSHEET_H__3B16967D_5736_432E_A398_2FE0117D093C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SettingsSheet.h : header file
//
//#define CONCTRL_ELAPSE_DL         23 
//#define CONCTRL_ELAPSE_FLASH      25
#define MAX_FILE_COUNT	        40
#define MAX_BUF_SIZE            4096


/////////////////////////////////////////////////////////////////////////////
// CSettingsSheet
#include "MainPage.h"
#include "CalibrationPage.h"
#include "PageMultiLang.h"
#include "BMAGlobal.h"
#include "FlashOptPage.h"
#include "PageLcdConfig.h"
#include "PageMcp.h"
#include "PageOptions.h"

class CSettingsSheet : public CPropertySheet
{
	DECLARE_DYNAMIC(CSettingsSheet)

// Construction
public:
	CSettingsSheet(UINT nIDCaption, CWnd* pParentWnd = NULL, UINT iSelectPage = 0);
	CSettingsSheet(LPCTSTR pszCaption, CWnd* pParentWnd = NULL, UINT iSelectPage = 0);

    BOOL LoadSettings(LPCTSTR pFileName,CString &strErrorMsg);
	BOOL LoadPacket(LPCTSTR pFileName);
    
    CString GetCurProduct()
    {
        return m_pageMain.m_strCurProduct;
    }

public:
    int		GetBaudRate();
    int		GetComPort();
    int		GetDownloadFile( CStringArray& aryDownloadFile ); 

    int		GetRepartitionFlag();
    int		GetFlashPageType();
	int		GetNvNewBasePosition();
	int		GetFileInfo(LPCTSTR lpszFileID, LPDWORD ppFileInfo);
	int     GetAllFileInfo(LPDWORD ppFileInfo);
	CString GetDownloadFilePath(LPCTSTR lpszFileID);
	int		GetNvBkpItemCount();	
	PNV_BACKUP_ITEM_T GetNvBkpItemInfo(int nIndex);

	BOOL	IsNandFlash();  // if NAND return true, NOR return false
	BOOL	IsMainPageInit();
	BOOL	IsBackupNV();         //for NV page
	BOOL	IsNvBaseChange();
	BOOL	IsReadFlashInFDL2();
	
	BOOL    IsNVSaveToLocal();
	CString GetNVSavePath();

	void	Resize( int nChangeWidth, int nChangeHeight );

	BOOL    IsBackupLang();
	BOOL    IsHasLang();
	WORD    GetLangNVItemID();
	
	BOOL    IsNVOrgDownload();
	int     GetNVOrgBasePosition();
	
	BOOL    IsOmaDM();

	int     GetFlashOprFileInfo(CUIntArray &agFlashOpr);
	
	BOOL    IsEnableChipDspMap();
	
	BOOL    IsAutoGenSN();

	BOOL    IsHasLCD();
	
	BOOL    IsEnableMultiFileBuf();
	int     GetAllChipName(CStringArray &agChipNames,CUIntArray &agChipIDs);
	BOOL    GetChipName(DWORD dwChipID, CString &strName);
	int     GetAllFileID(CStringArray &agFileID);

	BOOL    FindLCDItem(LPCTSTR lpszFileName, VEC_LCD_CFIG &vLcdCfig);

	BOOL    IsEraseAll();	

	BOOL    IsCheckMCPType();
	CString GetMCPTypeDesc(LPCTSTR lpszMcpType,DWORD &dwPage,DWORD &dwOOB,BOOL &bMatch);
	
	BOOL    IsReadMcpType();

	BOOL    IsReset();

	BOOL    IsEnablePageOobFile();
	
	BOOL    IsNeedCheckNV();

	int     GetBackupFiles(CStringArray &agID);

	int     IsBackupFile(LPCTSTR lpszFileID);
	
	BOOL    IsNeedRebootByAT();

    CFont				m_fntPage; 
	CCalibrationPage    m_pageCalibration;
	CPageMultiLang		m_pageMultiLang;
	CPageLcdConfig      m_pageLCDCfig;

// Attributes
protected:
	CMainPage           m_pageMain;  
	CFlashOptPage       m_pageFlashOpt;
	CPageMcp            m_pageMcp;
	CPageOptions        m_pageOptions;

// Operations
public:
    RECT m_rctPage;
    
// Overrides
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CSettingsSheet)
	public:
    virtual BOOL OnInitDialog();
	protected:
    virtual BOOL OnNotify(WPARAM wParam, LPARAM lParam, LRESULT* pResult);
    virtual BOOL OnCommand(WPARAM wParam, LPARAM lParam);
	//}}AFX_VIRTUAL
    
// Implementation
public:
    virtual ~CSettingsSheet();
    
// Generated message map functions
protected:
    virtual void BuildPropPageArray ();
    //{{AFX_MSG(CSettingsSheet)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	//}}AFX_MSG
    afx_msg LONG OnResizePage (UINT, LONG);
//	afx_msg void OnPressButton(int nButtion);
	afx_msg void OnApply();
    DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SETTINGSSHEET_H__3B16967D_5736_432E_A398_2FE0117D093C__INCLUDED_)
