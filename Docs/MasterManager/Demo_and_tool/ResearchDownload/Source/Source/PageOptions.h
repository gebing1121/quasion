#if !defined(AFX_PAGEOPTIONS_H__CB7C2CB5_9F95_45ED_A67D_09A098439565__INCLUDED_)
#define AFX_PAGEOPTIONS_H__CB7C2CB5_9F95_45ED_A67D_09A098439565__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// PageOptions.h : header file
//

// Do repartition always
#define REPAR_STRATEGY_ALWAYS           0   
// Stop actions and report error when incompatible partition error occured
#define REPAR_STRATEGY_STOP             1
// Ignore incompatible partition error
#define REPAR_STRATEGY_IGNORE           2
// Do repartion action when imcompatible partition error occured
#define REPAR_STRATEGY_DO               3

/////////////////////////////////////////////////////////////////////////////
// CPageOptions dialog

class CPageOptions : public CPropertyPage
{
	DECLARE_DYNCREATE(CPageOptions)

// Construction
public:
	CPageOptions();
	~CPageOptions();

	BOOL LoadSettings(LPCTSTR pFileName,CString &strErrorMsg);
    BOOL SaveSettings(LPCTSTR pFileName);

	int  GetGUINandRepartStrategy();
	int  GetNandRepartStrategy();

	int  GetGUIFlashPageType() {return m_nFlashPageType;}
	int  GetFlashPageType(){return m_nFlashPageType;}

// Dialog Data
	//{{AFX_DATA(CPageOptions)
	enum { IDD = IDD_PROPPAGE_OPTIONS };
	BOOL	m_bTmpRepart;
	BOOL	m_bTmpReset;
	BOOL	m_bTmpReadMcpType;
	//}}AFX_DATA

	BOOL    m_bRepart;
	BOOL    m_bReset;

	int     m_nFlashPageType;
	BOOL    m_bReadMcpType;

// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CPageOptions)
	public:
	virtual void OnOK();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CPageOptions)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	CString m_strIniFile;

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PAGEOPTIONS_H__CB7C2CB5_9F95_45ED_A67D_09A098439565__INCLUDED_)
