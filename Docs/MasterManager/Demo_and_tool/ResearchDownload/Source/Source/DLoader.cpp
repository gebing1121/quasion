// DLoader.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "DLoader.h"

#include "MainFrm.h"
#include "DLoaderDoc.h"
#include "DLoaderView.h"
#include "XAboutDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

class CDLCmdLine : public CCommandLineInfo
{
public:
	CDLCmdLine() 
	{ 
		m_strPrdVersion = _T("");
		m_nParaCount = 0; 
		m_nMaxParaCount = 3; 
		m_bWait = FALSE;
	}
	virtual void ParseParam( LPCTSTR lpszParam, BOOL bFlag, BOOL bLast );

	CString m_strPrdVersion;

	BOOL    m_bWait;

protected:
	int m_nParaCount;
	int m_nMaxParaCount;
};



void CDLCmdLine::ParseParam( LPCTSTR lpszParam, BOOL bFlag, BOOL bLast )
{
	UNREFERENCED_PARAMETER(bLast);
//	if( bFlag )
//	{
		// Do not support any flag now
//		return;
//	}

//	if( m_nParaCount > m_nMaxParaCount )
//	{
//		// Only support three parameters
//		return;
//	}

	CString strParam = lpszParam;
	strParam.TrimLeft();
	strParam.TrimRight();

	if( 0 == m_nParaCount )
	{
		m_strFileName = strParam;
	}
	else if( 1 == m_nParaCount )
	{
		m_strPrdVersion = strParam;
	}
	else
	{
		if(bFlag && strParam.CompareNoCase(_T("w")) == 0)
		{
			m_bWait = TRUE;
		}	
	}
	m_nParaCount++;
}



/////////////////////////////////////////////////////////////////////////////
// CDLoaderApp

BEGIN_MESSAGE_MAP(CDLoaderApp, CWinApp)
	//{{AFX_MSG_MAP(CDLoaderApp)
	ON_COMMAND(ID_APP_ABOUT, OnAppAbout)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
	// Standard file based document commands
	ON_COMMAND(ID_FILE_NEW, CWinApp::OnFileNew)
	ON_COMMAND(ID_FILE_OPEN, CWinApp::OnFileOpen)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDLoaderApp construction

CDLoaderApp::CDLoaderApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
	m_pBMAFramework = NULL;
	m_bScriptCtrl = FALSE;
	m_bResultPathWithPort = FALSE;

#if defined(_SPUPGRADE) || defined(_DOWNLOAD_FOR_PRODUCTION)
	m_bShowOtherPage = FALSE;
	m_bShowMcpTypePage = FALSE;
#else
	m_bShowOtherPage = TRUE;
	m_bShowMcpTypePage = TRUE;
#endif
	
	m_bColorFlag = FALSE;
	m_bClosePortFlag = TRUE;
	m_bResultHolding = TRUE;
	m_bManual = FALSE;

	m_bCMDPackage = FALSE;
	m_strPacPath = _T("");
	m_strPrdVersion = _T("");
	m_bKeepPacNVState = FALSE;
	m_bNeedPassword = FALSE;	

	m_bFilterPort = TRUE;

	m_strFileFilter = _T("");

	m_nGSMCaliVaPolicy = FALSE;
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CDLoaderApp object

CDLoaderApp g_theApp;

/////////////////////////////////////////////////////////////////////////////
// CDLoaderApp initialization

BOOL CDLoaderApp::InitInstance()
{
	AfxEnableControlContainer();

	OleInitialize( NULL );

    CString strErrMsg;

	if(!CreateBMAFramework( &m_pBMAFramework))
	{
		AfxMessageBox(_T("Create BMAFramework fail."));    
        return FALSE;
	} 

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	//  of your final executable, you should remove from the following
	//  the specific initialization routines you do not need.

#ifdef _AFXDLL
	Enable3dControls();			// Call this when using MFC in a shared DLL
#else
	Enable3dControlsStatic();	// Call this when linking to MFC statically
#endif

	// Change the registry key under which our settings are stored.
	// TODO: You should modify this string to be something appropriate
	// such as the name of your company or organization.
	SetRegistryKey(_T("Spreadtrum"));

	LoadStdProfileSettings();  // Load standard INI file options (including MRU)

    //@ Liu Kai 2004-8-23
    GetVersion();

	
	_TCHAR szConfigPath[_MAX_PATH]={0};
	DWORD dwRet = ::GetModuleFileName(this->m_hInstance,szConfigPath,_MAX_PATH);
	if(dwRet == 0)
	{
		AfxMessageBox(_T("Load configure ini file failed!"));
		return FALSE;
	}
    LPTSTR pResult = _tcsrchr(szConfigPath,_T('.'));
    *pResult = 0;
    _tcscat(szConfigPath,_T(".ini"));

#if defined(_SPUPGRADE) || defined(_DOWNLOAD_FOR_PRODUCTION)	
	m_bShowOtherPage = GetPrivateProfileInt(_T("GUI"),_T("ShowOtherPage"),0,szConfigPath);
	m_bManual = GetPrivateProfileInt(_T("GUI"),_T("Manual"),0,szConfigPath);
	m_bResultHolding = GetPrivateProfileInt(_T("GUI"),_T("ResultHolding"),1,szConfigPath);
	m_bShowMcpTypePage = GetPrivateProfileInt(_T("GUI"),_T("ShowMcpTypePage"),0,szConfigPath);
#else
	m_bShowOtherPage = GetPrivateProfileInt(_T("GUI"),_T("ShowOtherPage"),1,szConfigPath);
	m_bShowMcpTypePage = GetPrivateProfileInt(_T("GUI"),_T("ShowMcpTypePage"),1,szConfigPath);
#endif	

	m_bColorFlag = GetPrivateProfileInt(_T("GUI"),_T("MarkLastState"),0,szConfigPath);
	m_bClosePortFlag = GetPrivateProfileInt(_T("GUI"),_T("ClosePortFlag"),1,szConfigPath);

	m_bScriptCtrl = GetPrivateProfileInt(_T("GUI"),_T("ScriptControl"),0,szConfigPath);
	m_bCMDPackage = GetPrivateProfileInt(_T("GUI"),_T("CmdPackage"),0,szConfigPath);
#if defined(_SPUPGRADE) 
	m_bKeepPacNVState = GetPrivateProfileInt(_T("GUI"),_T("KeepPacNVState"),0,szConfigPath);
#endif
	
	m_bNeedPassword = GetPrivateProfileInt(_T("GUI"),_T("NeedPassword"),0,szConfigPath);
	m_dwWaitTimeForNextChip = GetPrivateProfileInt(_T("Settings"),_T("WaitTimeForNextChip"),0,szConfigPath);

	m_bResultPathWithPort = GetPrivateProfileInt(_T("GUI"),_T("ResultPathWithPort"),0,szConfigPath);
	m_strResultPath = szConfigPath;
	int nFind = m_strResultPath.ReverseFind('\\');
	m_strResultPath = m_strResultPath.Left(nFind);
	CString strBMFileTypeConfig = m_strResultPath;
	m_strResultPath += _T("\\result.txt");


	_TCHAR szBuf[_MAX_PATH] = {0};
	GetPrivateProfileString(_T("GUI"),_T("FileFilter"),_T(""),szBuf,_MAX_PATH,szConfigPath);
	m_strFileFilter = szBuf;	

	LoadFilterPortSetting(szConfigPath);

	strBMFileTypeConfig+= _T("\\BMFileType.ini");

	m_nGSMCaliVaPolicy = GetPrivateProfileInt(_T("DownloadNV"),_T("GSMCaliVaPolicy"),0,strBMFileTypeConfig);
	if(m_nGSMCaliVaPolicy <0 || m_nGSMCaliVaPolicy>2)
	{
		AfxMessageBox(_T("GSMCaliVaPolicy configure error in BMFileType.ini!\n It must be 0,1,2."));
		return FALSE;
	}

	// Register the application's document templates.  Document templates
	//  serve as the connection between documents, frame windows and views.
	CSingleDocTemplate* pDocTemplate;
	pDocTemplate = new CSingleDocTemplate(
		IDR_MAINFRAME,
		RUNTIME_CLASS(CDLoaderDoc),
		RUNTIME_CLASS(CMainFrame),       // main SDI frame window
		RUNTIME_CLASS(CDLoaderView));
	AddDocTemplate(pDocTemplate);


	UNUSED_ALWAYS(IDR_DLOADETYPE);

	// Parse command line for standard shell commands, DDE, file open
	CDLCmdLine cmdInfo;
	ParseCommandLine(cmdInfo);


	// Dispatch commands specified on the command line
	if (!ProcessShellCommand(cmdInfo))
		return FALSE;

	// The one and only window has been initialized, so show and update it.	
	if(m_bCMDPackage)
	{
		m_pMainWnd->ShowWindow(SW_HIDE);
	}
	else
	{
		m_pMainWnd->ShowWindow(SW_SHOW);
	}
	m_pMainWnd->UpdateWindow();


	if(m_bScriptCtrl)
	{
		PostMessage(m_pMainWnd->m_hWnd,WM_COMMAND,ID_START,0);
	}
	else if(m_bCMDPackage)
	{
		m_strPacPath = cmdInfo.m_strFileName;
		m_strPrdVersion = cmdInfo.m_strPrdVersion;
		if(!m_strPacPath.IsEmpty() && !m_strPrdVersion.IsEmpty())
		{
			PostMessage(m_pMainWnd->m_hWnd,WM_COMMAND,ID_SETTINGS,0);
		}
		else
		{
			AfxMessageBox(_T("Usage: ResearchDownload.exe <pac path> <product version>"));
			return FALSE;
		}
	}

	return TRUE;
}


/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX }; //lint !e30
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

// App command to run the dialog
void CDLoaderApp::OnAppAbout()
{
	CXAboutDlg aboutDlg;

#ifdef _SPUPGRADE
	aboutDlg.SetProductName(_T("UpgradeDownload"));
#elif defined _DOWNLOAD_FOR_PRODUCTION 
	aboutDlg.SetProductName( _T("FactoryDownload"));
#else
	aboutDlg.SetProductName( _T("ResearchDownload"));
#endif
	aboutDlg.DoModal();
}

/////////////////////////////////////////////////////////////////////////////
// CDLoaderApp message handlers


BOOL CAboutDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

    //@ Liu Kai 2004-08-23
    GetDlgItem( IDC_STATIC_VERSION )->SetWindowText( g_theApp.m_strVersion );
    GetDlgItem( IDC_STATIC_BUILD )->SetWindowText( g_theApp.m_strBuild );
#ifdef _SPUPGRADE
	GetDlgItem( IDC_STATIC_PRODUCT_NAME )->SetWindowText(_T("Spreadtrum UpgradeDownload"));
	SetWindowText(_T("About Spreadtrum UpgradeDownload")); 
#elif defined _DOWNLOAD_FOR_PRODUCTION
    GetDlgItem( IDC_STATIC_PRODUCT_NAME )->SetWindowText(_T("Spreadtrum FactoryDownload"));
	SetWindowText(_T("About Spreadtrum FactoryDownload")); 
#else
	GetDlgItem( IDC_STATIC_PRODUCT_NAME )->SetWindowText(_T("Spreadtrum ResearchDownload"));
	SetWindowText(_T("About Spreadtrum ResearchDownload")); 
#endif
	
	return TRUE;
}

int CDLoaderApp::ExitInstance() 
{
	// TODO: Add your specialized code here and/or call the base class
	OleUninitialize();
	return CWinApp::ExitInstance();
}

void CDLoaderApp::LoadFilterPortSetting(LPCTSTR pszConfigFile)
{
	m_bFilterPort = GetPrivateProfileInt(_T("PortSetting"),_T("EnableFilter"),TRUE,pszConfigFile);
	
	m_mapFilterPort.RemoveAll();
	_TCHAR szBuf[_MAX_PATH] = {0};
	GetPrivateProfileString(_T("PortSetting"),_T("FilterPort"),_T("1,2"),szBuf,_MAX_PATH,pszConfigFile);

	DWORD dwPort = 0;
	if(_tcslen(szBuf) != 0)
    {	
		LPTSTR  lpBuf  = szBuf;
		LPTSTR  lpFind = _tcschr(lpBuf, _T(','));
		while(lpFind != NULL)
		{
			*lpFind = _T('\0');
			
			_stscanf(lpBuf,_T("%d"),&dwPort);
			m_mapFilterPort.SetAt(dwPort,dwPort);
			lpBuf = lpFind + 1;
			lpFind = _tcschr(lpBuf, _T(','));
		}		
		_stscanf(lpBuf,_T("%d"),&dwPort);
		m_mapFilterPort.SetAt(dwPort,dwPort);	
    }
}
