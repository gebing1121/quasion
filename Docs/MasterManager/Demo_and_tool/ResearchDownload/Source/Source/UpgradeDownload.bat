echo off

set COMPCP=..\..\..\common\bin\compcp.bat
set SRC_DIR=..\..\..\common\bin
set DST_DIR=..\..\..\Release\UpgradeDownload\Bin
set FILES=Channel.dll DiagChan.dll BMPlatform.dll UpgradeDownload.ini UpgradeDownload.exe BMFileType.ini BMError.ini BMTimeout.ini BMAConfig.xml BMAFrame.dll udl_bkmark.bmp MCPType.ini

set _COMP=ture

for %%f in (%FILES%) do (
@call "%COMPCP%" "%SRC_DIR%\%%f" "%DST_DIR%\%%f" "%_COMP%"
)

