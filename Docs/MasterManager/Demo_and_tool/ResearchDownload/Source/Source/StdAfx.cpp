// stdafx.cpp : source file that includes just the standard includes
//	DLoader.pch will be the pre-compiled header
//	stdafx.obj will contain the pre-compiled type information

#include "stdafx.h"

int EnumKeys(LPCTSTR pSection,CStringArray* pKeys)
{
    ASSERT(pSection != NULL);
    ASSERT(pKeys != NULL);
	if(pSection == NULL || pKeys == NULL)
	{
		return 0;
	}
	
    int number = 0;
    for(;;)
    {
        CString strLine = pSection;
        int nLen = strLine.GetLength();
        if(0 == nLen)
        {
            // All keys are read
            break;
        }
        
        int nIndex = strLine.Find(_T('='));
        if(-1 == nIndex)
        {
            // Not a valid key
            continue;
        }
        CString strKey = strLine.Left(nIndex);
        strKey.TrimLeft();
        strKey.TrimRight();
        pKeys->Add(strKey);
		
        CString strData = strLine.Right(nLen - nIndex - 1);
        strData.TrimLeft();
        strData.TrimRight();
        pKeys->Add(strData);
		
        number++;
		
        pSection += nLen + 1;
		
    }
	
    return number;
}

long GetDigit(LPCTSTR lpszText)
{
	if(lpszText == NULL)
		return 0;

    CString strText = lpszText;

    long lRet;
    if(!strText.Left(2).CompareNoCase(_T("0x")))
    {
        // Hex mode
        _stscanf(strText,_T("%x"),&lRet);
    }
    else
    {
        // Decimal mode
        _stscanf(strText,_T("%d"),&lRet);
    }

    return lRet;
}

void GetModuleFilePath( HMODULE hModule, LPTSTR lpszAppPath )
{
    //Get the current running path
    _TCHAR Path[MAX_PATH];
    _TCHAR *pFoundChar=NULL;
    
    GetModuleFileName( hModule, Path, MAX_PATH );
    
    pFoundChar = _tcsrchr(Path,_T('\\'));
	if(pFoundChar != NULL)
	{
		*pFoundChar = _T('\0');
	}
    
    _tcscpy( lpszAppPath,Path);
}

BOOL CreateDeepDirectory(LPCTSTR lpszDir)
{
	if(lpszDir == NULL)
		return FALSE;

	CString			strDir = lpszDir;
	HANDLE			hFile;  
    WIN32_FIND_DATA fileinfo;  
    CStringArray    agDir;  
    BOOL			bOK;  
    int				nCount = 0;  
    CString			strTemp= _T("");  
	
	
    hFile   =   FindFirstFile(strDir,&fileinfo);  
	
    // if the file exists and it is a directory  
    if(fileinfo.dwFileAttributes == FILE_ATTRIBUTE_DIRECTORY)  
    {  
		//     Directory   Exists   close   file   and   return  
		FindClose(hFile);  
		return   TRUE;  
    }  
    agDir.RemoveAll();  
    for( nCount = 0; nCount < strDir.GetLength(); nCount++ )  
    {  
		if( strDir.GetAt(nCount) != _T('\\') )  
			strTemp += strDir.GetAt(nCount);  
		else  
		{  
			agDir.Add(strTemp);  
			strTemp += _T("\\");  
		}  
		if(nCount ==   strDir.GetLength()-1) 
		{
			agDir.Add(strTemp);  
		}
    }  
	
	
    //   Close   the   file  
    FindClose(hFile);  
	
	
    // Now lets cycle through the String Array and create each directory in turn  
    for(nCount = 1; nCount < agDir.GetSize(); nCount++)  
    {  
		strTemp = agDir.GetAt(nCount);  
		bOK = CreateDirectory(strTemp,NULL);  
		
		// If the Directory exists it will return a false  
		if(bOK)
		{
			// If we were successful we set the attributes to normal  
			SetFileAttributes(strTemp,FILE_ATTRIBUTE_NORMAL);  
		}
		else
		{
			if( GetLastError() != ERROR_ALREADY_EXISTS )
 			{ 	
				agDir.RemoveAll(); 
				return FALSE;
 			}  
		}
		
    }  
    // Now lets see if the directory was successfully created  
    hFile   =   FindFirstFile(strDir,&fileinfo);  
	
    agDir.RemoveAll();  
    if(fileinfo.dwFileAttributes == FILE_ATTRIBUTE_DIRECTORY)  
    {  
		// Directory Exists close file and return  
		FindClose(hFile);  
		return   TRUE;  
    }  
    else  
    {  
		// For Some reason the Function Failed Return FALSE  
		FindClose(hFile);  
		return   FALSE;  
    }  
	
}

/*agInfo: file-path,file-name,file-ext */
BOOL GetFilePathInfo(LPCTSTR lpszFile, CStringArray &agInfo)
{
	agInfo.RemoveAll();
	if(lpszFile == NULL)
	{
		return FALSE;
	}
	
	CString strFilePath = lpszFile;
	int nIndx = strFilePath.ReverseFind('\\');
	CString strFileName = strFilePath;
	CString strExt = _T("");
	if(nIndx != -1)
	{
		strFileName = strFilePath.Right(strFilePath.GetLength()-nIndx -1);
		strFilePath = strFilePath.Left(nIndx);
	}
	else
	{
		strFilePath.Empty();
	}
	
	nIndx = strFileName.ReverseFind('.');
	if(nIndx != -1)
	{
		strExt = strFileName.Right(strFileName.GetLength()-nIndx);
		strFileName = strFileName.Left(nIndx);
	}
	
	agInfo.Add(strFilePath);
	agInfo.Add(strFileName);
	agInfo.Add(strExt);
	
	return TRUE;
}

int SplitStr(LPCTSTR lpszStr,CStringArray &agStrs,TCHAR chSeparate /*= _T(',')*/)
{
	agStrs.RemoveAll();
	
	int nLen = _tcslen(lpszStr);
	
	if(nLen == 0)
		return 0;
	
	LPTSTR  lpBuf  = (LPTSTR)lpszStr;
	LPTSTR  lpFind = _tcschr(lpBuf, chSeparate);
	CString strTmp;
	while(lpFind != NULL)
	{
		*lpFind = _T('\0');
		strTmp = lpBuf;
		strTmp.TrimLeft();
		strTmp.TrimRight();
		if(!strTmp.IsEmpty())
			agStrs.Add(strTmp);
		lpBuf = lpFind + 1;
		lpFind = _tcschr(lpBuf, chSeparate);
	}
	
	strTmp = lpBuf;
	strTmp.TrimLeft();
	strTmp.TrimRight();
	if(!strTmp.IsEmpty())
		agStrs.Add(strTmp);
	
	return agStrs.GetSize();
}