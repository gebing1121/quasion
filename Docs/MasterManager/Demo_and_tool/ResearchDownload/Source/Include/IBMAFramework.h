#ifndef _IBMAFRAMEWORK_H__
#define _IBMAFRAMEWORK_H__

#include "BMAFExport.h"
#include "BootModeitf.h"

class IBMAFramework
{
public:
    virtual ~IBMAFramework()=0;
	virtual HRESULT BMAF_StartOneWork(LPCTSTR lpProductName, 
							LPCTSTR *ppFileList,
							DWORD  dwFileCount,
							DWORD  pOpenArgument,
							BOOL bBigEndian, 
							DWORD dwOprCookie,
							BOOL bRcvThread, 
							const DWORD pReceiver,
							BSTR lpbstrProgID )=0;
	virtual HRESULT BMAF_StopAllWork()=0;
	virtual HRESULT BMAF_StopOneWork(DWORD dwOprCookie)=0;
	virtual HRESULT BMAF_GetBootModeObjInfo(DWORD dwOprCookie,LPDWORD ppBootModeObjT)=0;
	virtual HRESULT BMAF_RegisterBMPDll(LPCTSTR lpstrDllName)=0;
	virtual HRESULT BMAF_UnregisterBMPDll(LPCTSTR lpstrDllName)=0;
	virtual HRESULT BMAF_SubscribeObserver(THIS_ DWORD dwOprCookie,
											THIS_ IBMOprObserver* pSink, 
											ULONG uFlags )=0;
	virtual HRESULT BMAF_UnsubscribeObserver(THIS_ DWORD dwOprCookie)=0;
	virtual HRESULT BMAF_SetProperty(DWORD dwPropertyID, DWORD dwPropertyValue) = 0;
	virtual HRESULT BMAF_GetProperty(DWORD dwPropertyID, LPDWORD lpdwPropertyValue) = 0;	
	virtual HRESULT BMAF_SetCommunicateChannelPtr(DWORD dwOprCookie,LPVOID pCommunicateChannel ) = 0;
	virtual HRESULT BMAF_GetCommunicateChannelPtr(DWORD dwOprCookie, /*[out]*/LPVOID* ppCommunicateChannel ) =0 ;
	virtual HRESULT BMAF_Release()=0;
	
};

BMAF_EXPORT_API BOOL CreateBMAFramework( IBMAFramework ** pBMAFramework );

#endif //_IBMAFRAMEWORK_H__