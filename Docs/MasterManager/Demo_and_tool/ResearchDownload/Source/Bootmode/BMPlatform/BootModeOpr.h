/******************************************************************************
** File Name:      BootModeOpr.h                                            *
** Author:         Apple Gao                                                 *
** DATE:           07/08/2004                                                *
** Copyright:      Spreatrum, Incoporated. All Rights Reserved.              *
** Description:    This files contains dll interface.                        *
******************************************************************************

  ******************************************************************************
  **                        Edit History                                       *
  ** ------------------------------------------------------------------------- *
  ** DATE				NAME				DESCRIPTION                       *
  ** 07/08/2004			Apple Gao			Create.							  * 
******************************************************************************/

#ifndef _BOOTMODEOPR_H
#define	_BOOTMODEOPR_H

/**---------------------------------------------------------------------------*
**                         Dependencies                                      *
**---------------------------------------------------------------------------*/

#include <atlbase.h>
#include "DiagChan.h"

//#include "ICommChannel.h"
//#include "Channelguid.h"

#include "Global.h"

#define MAX_RECV_LEN  (0x8000)   //32K Bytes

class  CBootModeOpr
{
public:
    CBootModeOpr();
    virtual ~CBootModeOpr();
public:
    BOOL CheckBaud( DWORD dwTimeout = 1000 );
    BOOL Connect( DWORD dwTimeout = 1000 );
    BOOL Excute( DWORD dwTimeout = 1000 );
    BOOL Reset( DWORD dwTimeout = 1000 );
    BOOL ReadFlash(  DWORD dwBase,DWORD dwLength, DWORD dwTimeout = 1000  );
    BOOL ReadPartitionFlash( DWORD dwBase,DWORD dwLength,DWORD dwOffset,DWORD dwTimeout = 1000 );
    BOOL StartData( DWORD dwBase,DWORD dwLength ,
        LPBYTE lpDownLoadData, DWORD dwTimeout = 1000 );
    BOOL EndData( DWORD dwTimeout  = 1000 );
    BOOL MidstData( DWORD dwLength, LPBYTE lpDownLoadData,DWORD dwTimeout = 1000, DWORD dwTotal = 0  );
    BOOL ReadChipType( DWORD dwTimeout = 1000 );
    BOOL ReadNVItem( DWORD dwStartAddr, DWORD dwEndAddr, 
                    unsigned short uiItemID, DWORD dwTimeout = 1000 );
    BOOL ChangeBaud( DWORD dwTimeout = 1000 );
    BOOL EraseFlash( DWORD dwBase,DWORD dwLength , DWORD dwTimeout = 1000 );
    
    // For nand flash
    BOOL Repartition( DWORD dwTimeout = 1000 );
        
    BOOL AllocRecvBuffer( DWORD dwSize );
    LPBYTE GetRecvBuffer( ); 
    DWORD  GetRecvBufferSize();
    void FreeRecvBuffer();
    
    BOOL Initialize( LPCTSTR lpszChannelProgID, LPBYTE lpOpenParam,
        BOOL bBigEndian, DWORD dwCookie );
    
    void Uninitialize();
    
    DWORD GetLastErrorCode();
        
    void Log( LPCTSTR lpszLog );
    
    void GetLastErrorDescription( DWORD dwErrorCode,LPTSTR lpszErrorDescription, int nSize );
    
    void SetIsCheckCrc( BOOL bCheckCrc );

    void StopOpr();
    
    DWORD GetChannelPtr( );

    void SetChannel( DWORD pChannel );
    
    BOOL ReadFlashType( DWORD dwTimeout = 1000 );
	//BOOL ReadFlashInfo( DWORD dwTimeout = 1000 );
	//BOOL DownFlashSpec( DWORD dwLength, LPBYTE lpDownLoadData, DWORD dwTimeout = 1000 );
	BOOL SetDebugLogState(DWORD dwState);

	// @hongliang.xin 2009-3-17 add checksum at "StartData"
	void SetStartDataInfo(BOOL bHasCheckSum);
	
	BOOL ReadSectorSize( DWORD dwTimeout = 1000 );
	
	BOOL DoBeforeCheckBaud();
	BOOL DoAfterChackBaud();
    
protected:
	BOOL SendData( BM_PACKAGE* lpPkg,DWORD dwTimeout = 1000 );
    
    BOOL SendCommandData( pkt_type_s nCmdType , DWORD dwTimeout = 1000 );
    BOOL SendPacketData( pkt_type_s nCmdType, 
        const int nSendDataLen,
        LPBYTE lpPacketData,
        UINT uiDataLen,
        DWORD dwTimeout = 1000 );
    
private:
    BOOL DisconnectChannel( );
    BOOL ConnectChannel(  LPBYTE lpOpenParam );

    BOOL OpenLogFile( DWORD dwCookie );
    
    void CloseLogFile();

    void LogOpr( LPCTSTR lpszOperation, BOOL bSuccess );

	DWORD DoCheckSum(LPBYTE pDataBuf,DWORD dwSize);

private:
    IProtocolChannel*				    m_pChannel;
    BOOL                                m_bBigEndian;
//  LPBYTE                              m_lpCurrRead;    
    LPBYTE                              m_lpReceiveBuffer;
    DWORD                               m_dwReceiveLen;
    DWORD                               m_dwBufferSize;
    pkt_type_s                          m_iLastSentPktType;
    DWORD                               m_dwLastPktSize;
    BOOL                                m_bCheckCrc;

    DWORD                               m_dwLastErrorCode;
    BOOL                                m_bOperationSuccess;
    
    BYTE                                m_RecvData[ MAX_RECV_LEN ];
    ULONG                               m_ulRecvDataLen;
    
    DWORD                               m_dwRecvThreadID;
    HANDLE                              m_hRecvThreadState;
    HANDLE                              m_hRecvThread;    

    FILE                                *m_pLogFile;
    DWORD                               m_dwCookie;

    CRITICAL_SECTION                    m_csRecvBbuffer;   
	CRITICAL_SECTION                    m_csLastError;

//    HANDLE                            m_hOprEvent;
    UINT                                m_uiReadOffset;

	// @hongliang.xin 2009-3-17 for "StartData"
	BOOL								m_bHasCheckSum;
	DWORD                               m_dwBaud;	
};



#endif // _BOOTMODEOPR_H