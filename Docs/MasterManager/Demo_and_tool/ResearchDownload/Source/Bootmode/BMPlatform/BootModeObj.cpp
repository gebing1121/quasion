// BootModeObj.cpp : Defines the initialization routines for the DLL.
//

#include "stdafx.h"
//#include <initguid.h>
#include "BootModeObj.h"
#include <atlconv.h>
#include <process.h>

#include "OptionHelpper.h"

extern COptionHelpper ohObject;

#define DEFAULT_MAX_LENGTH          0x3000
#define MAX_UINT_SLEEP_TIME         500

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// CBootModeObj
CBootModeObj::CBootModeObj()
{
    m_hBMThread        = NULL;
    m_bExitThread      = FALSE;
    
    m_mapObserverRegs.RemoveAll();
    m_dwOprCookie  = 0; 
    
    m_dwWaitTime = 0;
    m_dwNextObserverCookie = 1;

    m_dwOprCookie = 0;

}

CBootModeObj::~CBootModeObj()
{
    POSITION pos = m_mapObserverRegs.GetStartPosition();
    DWORD dwCookie;
    IBMOprObserver* pReg;
    while( pos ){
        m_mapObserverRegs.GetNextAssoc( pos, dwCookie, pReg );
        pReg->Release();
    }            
    m_mapObserverRegs.RemoveAll();
}

BOOL CBootModeObj::GenerateEndNotification( DWORD dwOprCookie , DWORD dwResult )
{
	DWORD dwErrorCode = 1;
	if(dwResult == 1) // success
	{
		dwErrorCode = 0;
	}

    POSITION pos = m_mapObserverRegs.GetStartPosition();
    DWORD dwCookie;
    IBMOprObserver* pReg = NULL;
    while( pos ){
        pReg = NULL;
        dwCookie = 0;
        m_mapObserverRegs.GetNextAssoc( pos, dwCookie, pReg );
        if( pReg == NULL )
            continue;
        HRESULT hr = pReg->OnEnd( dwOprCookie ,dwErrorCode );
        if( FAILED(hr) )
        {
            return FALSE;
        }
		break;
    }
    return TRUE;
}

BOOL CBootModeObj::GenerateStartNotification( DWORD dwOprCookie, DWORD dwResult  )
{  
	DWORD dwErrorCode = 1; //fail
	if(dwResult == 1) // success
	{
		dwErrorCode = 0;
	}

    POSITION pos = m_mapObserverRegs.GetStartPosition();
    DWORD dwCookie;
    IBMOprObserver* pReg = NULL;
    while( pos ){
        pReg = NULL;
        dwCookie = 0;
        m_mapObserverRegs.GetNextAssoc( pos, dwCookie, pReg );
        if( pReg == NULL )
            continue;
        HRESULT hr = pReg->OnStart( dwOprCookie , dwErrorCode );
        if( FAILED(hr) )
        {
            return FALSE;
        }
		break;
    }   
    return TRUE;
}


BOOL CBootModeObj::GenerateEndFileOprNotification( DWORD dwOprCookie, 
												  LPCTSTR lpszFileId,
                                                  LPCTSTR lpszFileType,
                                                  DWORD dwResult )
{
	USES_CONVERSION;

	DWORD dwErrorCode = 1; // fail
	if(dwResult == 1) // success
	{
		dwErrorCode = 0;
	}
    
    POSITION pos = m_mapObserverRegs.GetStartPosition();
    DWORD dwCookie = 0;
    IBMOprObserver* pReg = NULL;
    while( pos ){
        dwCookie = 0;
        pReg = NULL;
        m_mapObserverRegs.GetNextAssoc( pos, dwCookie, pReg );
        if( pReg == NULL )
            continue;
        HRESULT hr = pReg->OnFileOprEnd( dwOprCookie, T2OLE((_TCHAR *)lpszFileId),T2OLE((_TCHAR *)lpszFileType), dwErrorCode );
        if( FAILED(hr) )
        {				
			return FALSE;		
        }
		break;
    }    
    return TRUE;
}

BOOL CBootModeObj::GenerateStartFileOprNotification( DWORD dwOprCookie, 
													LPCTSTR lpszFileId,
                                                    LPCTSTR lpszFileType  )
{
    USES_CONVERSION;
    
    POSITION pos = m_mapObserverRegs.GetStartPosition();
    DWORD dwCookie;
    IBMOprObserver* pReg = NULL;
    while( pos ){
        dwCookie = 0;
        pReg = NULL;
        m_mapObserverRegs.GetNextAssoc( pos, dwCookie, pReg );
        if( pReg == NULL )
            continue;
        HRESULT hr = pReg->OnFileOprStart( dwOprCookie, T2OLE((_TCHAR *)lpszFileId),
                        T2OLE((_TCHAR *)lpszFileType), (DWORD)(&m_BMFileImpl) );
        if( FAILED(hr) )
        {
            return FALSE;
        }
		break;
    }        
    return TRUE;
}

BOOL CBootModeObj::GenerateEndOprNotification( DWORD dwOprCookie, 
											  LPCTSTR lpszFileId,
                                              LPCTSTR lpszFileType, 
                                              LPCTSTR lpszOperationType,
                                              DWORD dwResult )
{
    USES_CONVERSION;

	DWORD dwErrorCode = 1;
	if(dwResult == 1) // success
	{
		dwErrorCode = 0;
	}
	else // get operation error code
	{
		dwErrorCode = m_OprDriver.GetOprLastErrorCode();
	}
    
    POSITION pos = m_mapObserverRegs.GetStartPosition();
    DWORD dwCookie;
    IBMOprObserver* pReg = NULL;

    while( pos ){
        dwCookie = 0;
        pReg = NULL;
        m_mapObserverRegs.GetNextAssoc( pos, dwCookie, pReg );
        if( pReg == NULL )
            continue;
        HRESULT hr = pReg->OnOperationEnd( dwOprCookie,T2OLE((_TCHAR *)lpszFileId), T2OLE((_TCHAR *)lpszFileType),
                    T2OLE((_TCHAR *)lpszOperationType) , dwErrorCode , (DWORD)(&m_BMFileImpl) );
        if( FAILED(hr) )
        {
            return FALSE;
        }
		break;
    }        
    return TRUE;
}

BOOL CBootModeObj::GenerateStartOprNotification( DWORD dwOprCookie, 
												LPCTSTR lpszFileID,
                                                LPCTSTR lpszFileType, 
                                                LPCTSTR lpszOperationType )
{
    USES_CONVERSION;
    
    POSITION pos = m_mapObserverRegs.GetStartPosition();
    DWORD dwCookie;
    IBMOprObserver* pReg = NULL;
    while( pos ){
        dwCookie = 0;
        pReg = NULL;
        m_mapObserverRegs.GetNextAssoc( pos, dwCookie, pReg );
        if( pReg == NULL )
            continue;
        HRESULT hr = pReg->OnOperationStart( dwOprCookie, T2OLE((_TCHAR *)lpszFileID),T2OLE((_TCHAR *)lpszFileType),
            T2OLE((_TCHAR *)lpszOperationType), (DWORD)(&m_BMFileImpl) );
        if( FAILED(hr) )
        {
            return FALSE;
        }
		break;
    }        
    return TRUE;
}

void CBootModeObj::EndProc(  BOOL bEndSuccess  )
{
	BOOL bEndNotify = FALSE;
	DWORD dwEnd = bEndSuccess;

    if( m_bExitThread )
    {
		bEndNotify = GenerateEndNotification( m_dwOprCookie, dwEnd );  
		if( bEndNotify && bEndSuccess )
		{
			Log( _T("Finish Successful."));
		}
		else
		{
			Log( _T("Finish Failed."));
		}
        return;
    }        
    
    if( m_dwWaitTime == 0 )
    {
        m_bExitThread = TRUE;
        bEndNotify = GenerateEndNotification( m_dwOprCookie, dwEnd );      
    }
    else
    {
		Log( _T("Download files move to the first."));
        m_BMFileImpl.MoveFirst(); 
        ICommChannel* pChannel;
        HRESULT hr = GetCommunicateChannelPtr( (LPVOID*)&pChannel );
        if( SUCCEEDED(hr) )
        {
			DWORD dwBaud = DEFAULT_BAUDRATE;
			pChannel->SetProperty(0,CH_PROP_BAUD,(LPCVOID)&dwBaud);
        }
//@hongliang.xin 2010-1-25  not used 
//      dwEnd |= FACTORY_MODE;
        bEndNotify = GenerateEndNotification( m_dwOprCookie, dwEnd );    
        UINT uiSleepCount = m_dwWaitTime / MAX_UINT_SLEEP_TIME;
        for( unsigned int i = 0; i < uiSleepCount && !m_bExitThread; i++)
        {
            Sleep( MAX_UINT_SLEEP_TIME );        
        }
		// @hongliang.xin 2010-7-19
		if(!m_bExitThread && (m_dwWaitTime % MAX_UINT_SLEEP_TIME) != 0 )
		{
			Sleep( m_dwWaitTime % MAX_UINT_SLEEP_TIME );   
		}
    }
    if( bEndNotify && bEndSuccess )
    {
        Log( _T("Finish Successful."));
    }
    else
    {
        Log( _T("Finish Failed."));
    }

}

BOOL CBootModeObj::CreateBMThread( )
{
    Log( _T("Create boot mode thread."));
    /*m_hBMThread = (HANDLE)_beginthreadex(NULL,
								0,
                                (PBEGINTHREADEX_THREADFUNC)GetBMThreadFunc,
                                this,
                                NULL,
                                NULL);*/

	m_hBMThread = CreateThread( NULL, 0, (LPTHREAD_START_ROUTINE)GetBMThreadFunc, this, 0, NULL );

    if(m_hBMThread == NULL)
    {
        return FALSE;		
    }
    m_bExitThread = FALSE;

    return TRUE;
}

void CBootModeObj::DestroyBMThread()
{
    if(m_hBMThread != NULL )
    {
        m_bExitThread = TRUE;        
	
		m_OprDriver.StopOpr();

        Log( _T("End boot mode thread."));

        WaitForSingleObject( m_hBMThread, INFINITE );  
        
        CloseHandle( m_hBMThread );
        m_hBMThread = NULL;
    }
}

DWORD WINAPI CBootModeObj::GetBMThreadFunc( LPVOID lpParam )
{
    CBootModeObj *pThis = (CBootModeObj *)lpParam;
    
    return pThis->BMThreadFunc();
}

DWORD CBootModeObj::BMThreadFunc( )
{
    m_BMFileImpl.MoveFirst();
    CString strLog;

	//add by hongliang.xin 2009-3-2
	VARIANT varTimes;
	ohObject.GetProperty(0,_T("CheckNVTimes"),&varTimes);
	UINT nCheckNVTimes = varTimes.uintVal;	// check nv and redownload times
	UINT nReadNVCount = 0; // checked nv times, begin to check NV after downloaded all files
	BOOL bCheckNVSuccess = TRUE; // check nv success flag


    while( !m_bExitThread )
	{
		//if( m_BMFileImpl.GetCurFileIndex() == 0)
		//	m_OprDriver.OnBegin();

        if( m_BMFileImpl.GetCurFileIndex() == 0 &&
            !GenerateStartNotification( m_dwOprCookie, 1 ) ) 
        {
            Log( _T("Fail at start notification.") );
            EndProc( FALSE );            
            continue;
        }

        BMFileInfo* pFileInfo = m_BMFileImpl.GetCurrentBMFileInfo();

        CStringArray aryKeyAndData;
        if( !ohObject.GetFileOperations( pFileInfo->szFileType,&aryKeyAndData ) )   
        {
            strLog.Format( _T("Can not read file operations for file type %s "), pFileInfo->szFileType );
            Log( strLog );
            EndProc( FALSE );
            continue;
        }

        if( !GenerateStartFileOprNotification( m_dwOprCookie, pFileInfo->szFileID, 
			                                   pFileInfo->szFileType) )
        {
            GenerateEndFileOprNotification( m_dwOprCookie, pFileInfo->szFileID,
				                            pFileInfo->szFileType ,FALSE);
            strLog.Format( _T("Fail at start [ID:%s] %s  file notification"), 
                            pFileInfo->szFileID,pFileInfo->szFileType );
            Log( strLog );
            EndProc( FALSE );           
            continue;
        }

        DWORD dwMaxLength = ohObject.GetPacketLength( pFileInfo->szFileType );
		if( _tcsicmp(pFileInfo->szFileType,_T("PAGE")) == 0 || 
			_tcsicmp(pFileInfo->szFileType,_T("PAGE_OOB")) == 0)
		{
			DWORD dwMax = m_BMFileImpl.GetCurMaxLength();
			if(dwMax != 0 && dwMaxLength <= 24)
			{
				dwMaxLength *= dwMax;
			}
		}
	
		m_BMFileImpl.SetCurMaxLength( dwMaxLength );
		
        
    	static BYTE parms[] =  VTS_I4;
		BOOL result = TRUE;  
		BOOL bIsCheckNV = FALSE;
        
		if(_tcsicmp(pFileInfo->szFileType,_T("CHECK_NV")) == 0)
		{
			bIsCheckNV = TRUE;
			nReadNVCount = 0;
			bCheckNVSuccess = TRUE;
		}
		
        for( int i = 0; i < aryKeyAndData.GetSize(); i+=2 )
        { 
			if(bIsCheckNV)
			{
				if(nCheckNVTimes == 0) {result = TRUE; break;}
				if(0 == i) {bCheckNVSuccess = TRUE; nReadNVCount++;}	
			}			

            CString str;
            
            //OLECHAR FAR* szName = aryKeyAndData.GetAt( i + 1).AllocSysString() ;
			CString strName = aryKeyAndData.GetAt( i + 1);
			strName.TrimLeft();
			strName.TrimRight();
            DISPID dispid;
            
            dispid = m_OprDriver.GetIDsOfNames( strName );
            if( dispid == -1 )
            {
                strLog.Format( _T("%s---%s operation is not implemented"), 
                    pFileInfo->szFileType, aryKeyAndData.GetAt( i + 1 ));
                Log( strLog );
                continue;
            }

            if( !GenerateStartOprNotification( m_dwOprCookie,pFileInfo->szFileID,
                pFileInfo->szFileType, aryKeyAndData.GetAt( i + 1 ) ) )
            {
                GenerateEndOprNotification( m_dwOprCookie, pFileInfo->szFileID,
                    pFileInfo->szFileType, aryKeyAndData.GetAt( i + 1 ) , FALSE ) ;
                strLog.Format( _T("Fail at start [ID:%s]%s---%s operation"), 
                    pFileInfo->szFileID, pFileInfo->szFileType, aryKeyAndData.GetAt( i + 1 ) );
                Log( strLog );
                break;
            }
                
            result = m_OprDriver.Invoke( dispid, (long)pFileInfo );   
            
            if( !GenerateEndOprNotification( m_dwOprCookie,pFileInfo->szFileID, 
                pFileInfo->szFileType, aryKeyAndData.GetAt( i + 1 ) , result ) )
            {				
				strLog.Format( _T("Fail at end %s---%s operation"), 
					pFileInfo->szFileType, aryKeyAndData.GetAt( i + 1 ) );
				Log( strLog ); 
				
				if(0 == i && bIsCheckNV) //read flash and check NV failed
				{			
					bCheckNVSuccess = FALSE;
					strLog.Format( _T("Check NV failed and download %s again"), pFileInfo->szFileID );
					Log( strLog ); 
				}
				else
				{
					break;
				}	
            }

            if( !result )
            {				
                strLog.Format( _T("Fail at %s---%s operation"), 
                    pFileInfo->szFileType, aryKeyAndData.GetAt( i + 1 ) );
                Log( strLog );
			
				if( bIsCheckNV && (nReadNVCount <= nCheckNVTimes))			
				{
					// read or download NV failed
					// recheck and download again					
					strLog.Format( _T("Recheck and download %s again"), pFileInfo->szFileID );
				    Log( strLog );
					if( i == 2 ) //download
					{
						i = -2; // i= i+2;  turn back
					}
				}
				else
				{
					break;   
				}				 
            }	
			else
			{
			    /**
				 * @hongliang.xin 2009-3-2
				 * "i==0  && bCheckNVSuccess" means read NV and check successs
				 * notice: maybe uplevel need not backup NV, so callback always return S_OK
				 * so that there is no check action in uplevel actually
				 */
				if(bIsCheckNV)
				{	
					if (0 == i && bCheckNVSuccess)
					{
						i=aryKeyAndData.GetSize(); // set NV BMFiles process over		   
						strLog.Format( _T("Check %s success"), pFileInfo->szFileID );
						Log( strLog ); 
						break; //check success, end the rest process
					}

					if( 2 == i && (nReadNVCount <= nCheckNVTimes) )
					{
						i = -2; // i= i+2;  turn back
					}
					
				}//else bCheckNVSuccess == false, download again
			}
			
        }

        BOOL bSuccessful = TRUE;
        if( i < aryKeyAndData.GetSize())
        {
            bSuccessful = FALSE;
        }

        if( !GenerateEndFileOprNotification( m_dwOprCookie, pFileInfo->szFileID,
                    pFileInfo->szFileType, bSuccessful )  )
        {
            strLog.Format( _T("Fail at end %s file notification"), 
                pFileInfo->szFileType );
            Log( strLog );   
            EndProc( FALSE );
            continue;
        }

        if( !bSuccessful )
        {
            EndProc( FALSE );
            continue;
        }

		
		m_BMFileImpl.MoveNext();    

		if( m_BMFileImpl.IsEOF() )
		{
			// All file have been downloaded
			EndProc( TRUE );		
		}
	}	
    	
    return 0L;
}

// Format of pSection
// "Key1 = data1\0Key2 = data2\0....Keyn = datan\0\0"
int CBootModeObj::EnumKeys(char* pSection,CStringArray* pKeys)
{
    ASSERT(pSection != NULL);
    ASSERT(pKeys != NULL);
    
    int number = 0;
    while(1)
    {
        CString strLine = pSection;
        int nLen = strLine.GetLength();
        if(0 == nLen)
        {
            // All keys are read
            break;
        }
        
        int nIndex = strLine.Find('=');
        if(-1 == nIndex)
        {
            // Not a valid key
            continue;
        }
        CString strKey = strLine.Left(nIndex);
        strKey.TrimLeft();
        strKey.TrimRight();
        pKeys->Add(strKey);
        
        CString strData = strLine.Right(nLen - nIndex - 1);
        strData.TrimLeft();
        strData.TrimRight();
        pKeys->Add(strData);
        
        number++;
        
        pSection += nLen + 1;        
    }
    
    return number;
}

void CBootModeObj::Log( LPCTSTR lpszLog )
{
    m_OprDriver.Log( lpszLog );    
}

STDMETHODIMP CBootModeObj::StartBootModeOperation(   DWORD lpBMFileInfo, 
                                                     UINT uFileCount, 
                                                     DWORD pOpenArgument, 
                                                     BOOL bBigEndian, 
                                                     DWORD dwOprCookie,
                                                     BOOL bRcvThread, 
                                                     const DWORD pReceiver,
                                                     BSTR lpbstrProgID)
{
    USES_CONVERSION;

    ASSERT( lpBMFileInfo != NULL );
    ASSERT( uFileCount > 0 );    
    
    m_dwOprCookie = dwOprCookie;    
 
    BOOL result = m_OprDriver.Initialize(OLE2T(lpbstrProgID),
											pOpenArgument,
											bBigEndian,
											dwOprCookie,
											bRcvThread,
											pReceiver);
    
    if( !result )
    {
        GenerateStartNotification( m_dwOprCookie, 0 );
        Log( _T("The channel occurs error. ") );
        return BM_E_CHANNEL_FAILED;
    }

	if(!m_BMFileImpl.InitBMFiles( (PBMFileInfo)lpBMFileInfo,uFileCount))
    {
        GenerateStartNotification( m_dwOprCookie, 0 );
		CString str = m_BMFileImpl.GetLastErrMsg();
		Log(str);
        Log( _T("The using file( FDL, NV ,etc ) is error. ") );	
        return BM_E_FILEINFO_ERROR;
    } 
	else
	{
		 Log( _T("load file success.") );
	}
 
    if(!CreateBMThread( ))
    {
        GenerateStartNotification( m_dwOprCookie, 0 );
        Log( _T("Create download  thread fail. ") );   
		m_OprDriver.Uninitialize();
        m_BMFileImpl.ClearUpBMFiles();
        return BM_E_CREATETHREAD_FAILED;
    }
    
    Log( _T("Start Boot Mode Operation success.") ); 
    return BM_S_OK;    
}

STDMETHODIMP CBootModeObj::StopBootModeOperation()
{    
    DestroyBMThread();

    Log( _T("Stop Boot Mode Operation") );    

	m_OprDriver.Uninitialize();

    m_BMFileImpl.ClearUpBMFiles();    
        
//    SAFE_FREE(m_pOprDriver);   

    return BM_S_OK;
}

STDMETHODIMP CBootModeObj::SetWaitTimeForNextChip( DWORD dwWaitTime /* = CHANGE_CHIP_TIMEOUT  */)
{
    m_dwWaitTime = dwWaitTime ;
    
    return BM_S_OK;
}

STDMETHODIMP CBootModeObj::SetCommunicateChannelPtr( LPVOID pCommunicateChannel  )
{
	m_OprDriver.SetChannel((long)pCommunicateChannel);    
    return S_OK;
}

STDMETHODIMP CBootModeObj::GetCommunicateChannelPtr( LPVOID* ppCommunicateChannel )
{
    long result = m_OprDriver.GetChannelPtr();
    *ppCommunicateChannel = (LPVOID)result;

    if( result ==  NULL )
    {
        return BM_E_FAILED;
    }
    return BM_S_OK ;
}

STDMETHODIMP CBootModeObj::SubscribeOperationObserver(THIS_ IBMOprObserver* pSink, 
                                                         ULONG uFlags,
                                                         LPDWORD lpdwCookie )
{  
	UNUSED_ALWAYS(uFlags);
    CSingleLock _lock( &m_CS, TRUE);
    
    DWORD dwCookie = m_dwNextObserverCookie;
    IBMOprObserver* p = NULL;
    do{
        p = NULL;
        if( !m_mapObserverRegs.Lookup( dwCookie, p) )
            break;
        dwCookie++;
        if( dwCookie == 0 )
            dwCookie++;
    }while( dwCookie != m_dwNextObserverCookie );
    
    if( p != NULL )
    {
        Log( _T("Subscribe Operation Observer fail."));
        return BM_E_REG_OBSERVER_FAILED;
    }
    
    m_mapObserverRegs.SetAt( dwCookie, pSink );
    pSink->AddRef();

    m_dwNextObserverCookie = dwCookie + 1;
    if( m_dwNextObserverCookie == 0 )
        m_dwNextObserverCookie++;
    
    *lpdwCookie = dwCookie;

    Log( _T("Subscribe Operation Observer success."));
    return BM_S_OK;
}

STDMETHODIMP CBootModeObj::UnsubscribeOperationObserver( THIS_ DWORD dwCookie )
{
    CSingleLock _lock( &m_CS, TRUE);

    IBMOprObserver* pObserver;
    if( !m_mapObserverRegs.Lookup( dwCookie, pObserver ) )
        return BM_E_FAILED;
        
    pObserver->Release();
    m_mapObserverRegs.RemoveKey( dwCookie );
	/* can not invoke log, maybe some object is not created */
//    Log( _T("Unsubscribe Operation Observer."));
    return BM_S_OK;
}

STDMETHODIMP_(const LPBYTE) CBootModeObj::GetReadBuffer( THIS )
{    
    long rlt = m_OprDriver.GetRecvBuffer();    
    return (const LPBYTE)rlt;
}

STDMETHODIMP_( DWORD) CBootModeObj::GetReadBufferSize( THIS )
{
    long dwSize = m_OprDriver.GetRecvBufferSize(); 
    return dwSize;    
}

