// BootModeObject.h : Declaration of the CBootModeObject

#ifndef __BOOTMODEOBJECT_H_
#define __BOOTMODEOBJECT_H_

#include "resource.h"       // main symbols
#include "BootModeObj.h"


/////////////////////////////////////////////////////////////////////////////
// CBootModeObject
class /*ATL_NO_VTABLE*/ CBootModeObject : 
//public CComObjectRootEx<CComSingleThreadModel>,
//public CComCoClass<CBootModeObject, &CLSID_BootModeObject>,
//public IDispatchImpl<IBootModeObject, &IID_IBootModeObject, &LIBID_BootModePlatform>,
public IBootModeHandler,
public IBMOprSubscriber,
public IBMOprBuffer,
public IBMSettings
{
public:
    CBootModeObject()
    {
		m_nRefCount = 0;
    }
    
//    DECLARE_REGISTRY_RESOURCEID(IDR_BOOTMODEOBJECT)
        
//         DECLARE_PROTECT_FINAL_CONSTRUCT()
//         
//         BEGIN_COM_MAP(CBootModeObject)
//         COM_INTERFACE_ENTRY(IBootModeObject)
//         COM_INTERFACE_ENTRY(IDispatch)
//          COM_INTERFACE_ENTRY_IID( IID_IBootModeHandler,IBootModeHandler )    
//          COM_INTERFACE_ENTRY_IID( IID_IBMOprSubscriber,IBMOprSubscriber )    
//          COM_INTERFACE_ENTRY_IID( IID_IBMOprBuffer,IBMOprBuffer )   
//          COM_INTERFACE_ENTRY_IID( IID_IBMSettings,IBMSettings )
//         END_COM_MAP()
        
        // IBootModeObject
public:
	STDMETHOD(QueryInterface) (THIS_ REFIID riid, LPVOID * ppvObj);
    STDMETHOD_(ULONG,AddRef) (THIS);
    STDMETHOD_(ULONG,Release) (THIS);

    // IBootModeHandler    
    STDMETHOD(SetWaitTimeForNextChip)(/*[in]*/ DWORD dwWaitTime);
    
    STDMETHOD(StopBootModeOperation)();
    
    STDMETHOD(StartBootModeOperation)(
        /*[in]*/ DWORD lpBMFileInfo, 
        /*[in]*/ UINT uFileCount, 
        /*[in]*/ DWORD pOpenArgument, 
        /*[in]*/ BOOL bBigEndian, 
        /*[in]*/ DWORD dwOprCookie,
        /*[in]*/ BOOL bRcvThread,  
        /*[in]*/ const DWORD pReceiver,
        /*[in]*/ BSTR lpbstrProgID );
        
    STDMETHOD( SetCommunicateChannelPtr)( /*[in]*/LPVOID pCommunicateChannel );
    
    
    STDMETHOD( GetCommunicateChannelPtr)( /*[out]*/LPVOID* ppCommunicateChannel );
    
    
    //IBMOprSubscriber
    STDMETHOD(SubscribeOperationObserver) (THIS_ IBMOprObserver* pSink,
        ULONG uFlags,
        LPDWORD lpdwCookie );
    
    STDMETHOD(UnsubscribeOperationObserver) (THIS_ DWORD dwCookie);
    
    //IBMOprBuffer
    STDMETHOD_(const LPBYTE, GetReadBuffer) ( THIS ) ;
    
    STDMETHOD_( DWORD, GetReadBufferSize) ( THIS );

    // IBMSettings
    STDMETHOD( SetCheckBaudTimes ) ( THIS_ DWORD dwTimes );
    
    STDMETHOD( SetRepartitionFlag ) ( THIS_ DWORD dwFlag );   
	
    STDMETHOD( SetReadFlashBefRepFlag ) ( THIS_ DWORD dwFlag ); 
	
    STDMETHOD_( DWORD, GetPacketLength ) ( const BSTR bstrFileType );
	
	STDMETHOD( GetProperty )( THIS_ LONG  lFlags, const BSTR cbstrName,  VARIANT *  pvarValue );  

    STDMETHOD( SetProperty )( THIS_ LONG lFlags, const BSTR cbstrName,  const VARIANT * pcvarValue );
private:
    CBootModeObj     m_BootModeObj;
	long             m_nRefCount;
    /*lint -e1509 -e1510*/
};
/*lint restore*/

#endif //__BOOTMODEOBJECT_H_
