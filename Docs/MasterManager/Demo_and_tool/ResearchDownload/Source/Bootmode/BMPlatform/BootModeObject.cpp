// BootModeObject.cpp : Implementation of CBootModeObject
#include "stdafx.h"
#include "BMPlatform.h"
#include "BootModeObject.h"
#include "OptionHelpper.h"

#include <initguid.h>
#include "BootModeGuid.h"

/////////////////////////////////////////////////////////////////////////////
// CBootModeObject

STDMETHODIMP CBootModeObject::StartBootModeOperation(
                                                     DWORD lpBMFileInfo, 
                                                     UINT uFileCount, 
                                                     DWORD pOpenArgument, 
                                                     BOOL bBigEndian, 
                                                     DWORD dwOprCookie,
                                                     BOOL bRcvThread, 
                                                     const DWORD pReceiver,
                                                     BSTR lpbstrProgID)
{
    AFX_MANAGE_STATE(AfxGetStaticModuleState())
        
    return m_BootModeObj.StartBootModeOperation( lpBMFileInfo, 
        uFileCount, pOpenArgument, bBigEndian, 
        dwOprCookie, bRcvThread, pReceiver, lpbstrProgID );
    
}

STDMETHODIMP CBootModeObject::StopBootModeOperation()
{
    AFX_MANAGE_STATE(AfxGetStaticModuleState())
        
    return m_BootModeObj.StopBootModeOperation();
}

STDMETHODIMP CBootModeObject::SetWaitTimeForNextChip(DWORD dwWaitTime)
{
    AFX_MANAGE_STATE(AfxGetStaticModuleState())
        
    return m_BootModeObj.SetWaitTimeForNextChip( dwWaitTime );
}

STDMETHODIMP CBootModeObject::SetCommunicateChannelPtr( LPVOID pCommunicateChannel )
{
    AFX_MANAGE_STATE(AfxGetStaticModuleState())
        
    return m_BootModeObj.SetCommunicateChannelPtr( pCommunicateChannel );    
}

STDMETHODIMP CBootModeObject::GetCommunicateChannelPtr( LPVOID* ppCommunicateChannel )
{
    AFX_MANAGE_STATE(AfxGetStaticModuleState())
        
    return m_BootModeObj.GetCommunicateChannelPtr( ppCommunicateChannel );
}

STDMETHODIMP CBootModeObject::SubscribeOperationObserver(THIS_ IBMOprObserver* pSink, 
                                                         ULONG uFlags,                                                       
                                                         LPDWORD lpdwCookie )
{
    AFX_MANAGE_STATE(AfxGetStaticModuleState())
        
    return m_BootModeObj.SubscribeOperationObserver( pSink, uFlags, lpdwCookie );
}

STDMETHODIMP CBootModeObject::UnsubscribeOperationObserver( THIS_ DWORD dwCookie )
{
    AFX_MANAGE_STATE(AfxGetStaticModuleState())
        
    return m_BootModeObj.UnsubscribeOperationObserver( dwCookie );
}

STDMETHODIMP_(const LPBYTE) CBootModeObject::GetReadBuffer( THIS )
{
    AFX_MANAGE_STATE(AfxGetStaticModuleState())
    return m_BootModeObj.GetReadBuffer(  );
}

STDMETHODIMP_( DWORD) CBootModeObject::GetReadBufferSize( THIS )
{
    AFX_MANAGE_STATE(AfxGetStaticModuleState())
        
    return m_BootModeObj.GetReadBufferSize(  );
}

STDMETHODIMP CBootModeObject::SetCheckBaudTimes( THIS_ DWORD dwTimes )
{
    AFX_MANAGE_STATE(AfxGetStaticModuleState())

	extern COptionHelpper ohObject;
    ohObject.SetCheckBaudTimes( dwTimes );

	return S_OK;
}

STDMETHODIMP CBootModeObject::SetRepartitionFlag( THIS_ DWORD dwFlag )
{
    AFX_MANAGE_STATE(AfxGetStaticModuleState())
        
    extern COptionHelpper ohObject;
    ohObject.SetRepartitionFlag( dwFlag );
    
    return S_OK;
}

STDMETHODIMP CBootModeObject::SetReadFlashBefRepFlag( THIS_ DWORD dwFlag )
{
    AFX_MANAGE_STATE(AfxGetStaticModuleState())
        
    extern COptionHelpper ohObject;
    ohObject.SetReadFlashBefRepFlag( dwFlag );
    
    return S_OK;
}
STDMETHODIMP_( DWORD)  CBootModeObject::GetPacketLength(const BSTR bstrFileType)
{
    AFX_MANAGE_STATE(AfxGetStaticModuleState())
        
    extern COptionHelpper ohObject;
	
	return ohObject.GetPacketLength(bstrFileType);
}

STDMETHODIMP CBootModeObject::GetProperty( THIS_ LONG  lFlags, const BSTR cbstrName,  VARIANT *  pvarValue )
{
    AFX_MANAGE_STATE(AfxGetStaticModuleState())
        
    extern COptionHelpper ohObject;
    if(ohObject.GetProperty(lFlags,cbstrName,pvarValue))
	{
		return S_OK;
	}
	else
	{
		return S_FALSE;
	}    
}

STDMETHODIMP CBootModeObject::SetProperty( THIS_ LONG lFlags, const BSTR cbstrName,  const VARIANT * pcvarValue )
{
    AFX_MANAGE_STATE(AfxGetStaticModuleState())
        
    extern COptionHelpper ohObject;
    if(ohObject.SetProperty(lFlags,cbstrName,pcvarValue))
	{
		return S_OK;
	}
	else
	{
		return S_FALSE;
	}  
}


STDMETHODIMP CBootModeObject::QueryInterface (THIS_ REFIID riid, LPVOID * ppvObj)
{
	if(IID_IBootModeHandler == riid)
	{
		*ppvObj = (LPVOID)dynamic_cast<IBootModeHandler*>(this);
	}
	else if(IID_IBMOprSubscriber == riid)
	{
		*ppvObj = (LPVOID)dynamic_cast<IBMOprSubscriber*>(this);
	}
	else if(IID_IBMOprBuffer == riid)
	{
		*ppvObj = (LPVOID)dynamic_cast<IBMOprBuffer*>(this);
	}
	else if(IID_IBMSettings == riid)
	{
		*ppvObj = (LPVOID)dynamic_cast<IBMSettings*>(this);
	}
	else
	{
		*ppvObj = NULL;
	}

	if(*ppvObj == NULL)
	{
		return S_FALSE;
	}

	return S_OK;
}

STDMETHODIMP_(ULONG) CBootModeObject::AddRef (THIS)
{
	m_nRefCount++;
	return S_OK;
}

STDMETHODIMP_(ULONG) CBootModeObject::Release (THIS)
{
	m_nRefCount--;

	if(m_nRefCount == 0)
	{
		delete this;
	}
	return S_OK;
}

