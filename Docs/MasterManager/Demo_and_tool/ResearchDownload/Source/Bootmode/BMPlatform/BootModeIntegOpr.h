#if !defined(AFX_BOOTMODEINTEGOPR_H__2D9F1612_91A5_4C80_BBD8_B54D552EC426__INCLUDED_)
#define AFX_BOOTMODEINTEGOPR_H__2D9F1612_91A5_4C80_BBD8_B54D552EC426__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// BootModeIntegOpr.h : header file
//

#include "BootModeOpr.h"

typedef BOOL (AFX_MSG_CALL CCmdTarget::*OPR_PFUN)(long);

typedef struct _BM_OPR_ENTRY{
	LPCTSTR  lpszName;
    OPR_PFUN pFun;
}BM_OPR_ENTRY;

#define DECLARE_DISP_MAP() \
private:\
	const static BM_OPR_ENTRY m_oprEntries[];\
	const static long  m_oprCount;


#define BEGIN_DISP_MAP(theClass, baseClass) \
	const BM_OPR_ENTRY theClass::m_oprEntries[] = \
	{ 

#define DISP_FUNC(theClass, szExternalName, pfnMember) \
	{ _T(szExternalName), \
		(OPR_PFUN)(BOOL (theClass::*)(long))&pfnMember }, 

#define END_DISP_MAP(theClass) \
	{_T(""),(OPR_PFUN)NULL} }; \
const long theClass::m_oprCount = sizeof(theClass::m_oprEntries)/sizeof(theClass::m_oprEntries[0]);

/////////////////////////////////////////////////////////////////////////////
// CBootModeIntegOpr command target

class CBootModeIntegOpr : public CCmdTarget
{
	DECLARE_DYNCREATE(CBootModeIntegOpr)

	CBootModeIntegOpr();           // protected constructor used by dynamic creation

// Attributes
public:

// Operations
public:
	long GetIDsOfNames(LPCTSTR lpszFunName);
	BOOL Invoke(long id,long pFileInfo);
	
	DWORD GetOprLastErrorCode();

	void  OnBegin();
	void  OnEnd();
    
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CBootModeIntegOpr)
	public:
	virtual void OnFinalRelease();
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CBootModeIntegOpr();


	// Generated message map functions
	//{{AFX_MSG(CBootModeIntegOpr)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()

public:

	//[[ Special notice
	// If you want to add a boot mode operation, this operation must be like
	// afx_msg BOOL XXXXX(long);
	// after you add this operation, you must add DISP_FUN map in the cpp file.

	// Generated OLE dispatch map functions
	//{{AFX_DISPATCH(CBootModeIntegOpr)
	afx_msg BOOL CheckBaud(long pFileInfo);
	afx_msg BOOL Connect(long pFileInfo);
	afx_msg BOOL Download(long pFileInfo);
	afx_msg BOOL EraseFlash(long pFileInfo);
	afx_msg BOOL ReadFlash(long pFileInfo);
	afx_msg BOOL Excute(long pFileInfo);
	afx_msg BOOL Reset(long pFileInfo);
    afx_msg BOOL ReadChipType( long pFileInfo );
    afx_msg BOOL ReadNVItem( long pFileInfo );
    afx_msg BOOL ChangeBaud( long pFileInfo );
    afx_msg BOOL EraseFlash2( long pFileInfo );
    afx_msg BOOL Repartition( long pFileInfo );
    afx_msg BOOL ExecNandInit( long pFileInfo );
    afx_msg BOOL ReadFlashType( long pFileInfo );
//	afx_msg BOOL ReadFlashInfo( long pFileInfo );
//	afx_msg BOOL DownFlashSpec( long pFileInfo );	
	afx_msg BOOL ReadSectorSize( long pFileInfo );
	afx_msg BOOL ReadFlashAndSave( long pFileInfo );
	afx_msg BOOL DoNothing(long pFileInfo);
	afx_msg BOOL SetCRC(long pFileInfo);
	afx_msg BOOL ResetCRC(long pFileInfo);
	afx_msg BOOL CheckBaudRom(long pFileInfo);
	afx_msg BOOL ConnectRom(long pFileInfo);
	//}}AFX_DISPATCH	

	//]] Special notice


	afx_msg BOOL Initialize(LPCTSTR lpszProgID, long pOpenArgument, BOOL bBigEndian, long dwOprCookie, BOOL bRcvThread, long pReceiver);
	afx_msg void Uninitialize();
	afx_msg void Log(LPCTSTR lpszLog);
	//afx_msg void SetIsCheckCrc(BOOL bCheckCrc);
	afx_msg long GetRecvBuffer();
	afx_msg void FreeRecvBuffer();
	afx_msg void StopOpr();
	afx_msg long GetRecvBufferSize();
	afx_msg long GetChannelPtr();
	afx_msg void SetChannel(long pChannel);
	afx_msg BOOL SetDebugLogState(long lState);

	DECLARE_DISP_MAP()

private:
    void SetMessageReceiver( BOOL bRcvThread,  const LPVOID pReceiver,DWORD dwCookie);

    void PostMessageToUplevel( opr_status_msg msgID, 
            WPARAM wParam, LPARAM lParam );

	BOOL ReadFlashToFile(long pFileInfo, HANDLE hFile);

	BOOL _CheckBaud(long pFileInfo,BOOL bRom);
	BOOL _Connect(long pFileInfo,BOOL bRom);

private:
    CBootModeOpr        m_bmOpr;
    BOOL                m_bCheckBaudRate;
    
    DWORD               m_dwOprCookie;
    // Indicates the up-level receiver is a thread or a window
    BOOL    m_bRcvThread;
    // ID of the up-level receiving thread
    DWORD   m_dwRcvThreadID;
    // Handle of the up-level receiving window
    HWND    m_hRcvWindow;   
    
    BOOL    m_bStopOpr;
	
	DWORD   m_bRepartion;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BOOTMODEINTEGOPR_H__2D9F1612_91A5_4C80_BBD8_B54D552EC426__INCLUDED_)
