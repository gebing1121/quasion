// stdafx.cpp : source file that includes just the standard includes
//	BMPlatform.pch will be the pre-compiled header
//	stdafx.obj will contain the pre-compiled type information

#include "stdafx.h"

void GetModuleFilePath( HMODULE hModule, LPTSTR lpszAppPath )
{
    //Get the current running path
    _TCHAR Path[MAX_PATH];
    _TCHAR *pFoundChar,*pNextChar;
    
    GetModuleFileName( hModule, Path, MAX_PATH );
    
    pFoundChar = pNextChar = Path;
    while(pFoundChar)
    {
        //Search the first char '\'
        pFoundChar = _tcschr( pNextChar + 1,_T('\\'));
        //find the char '\'
        if(pFoundChar != NULL)
        {
            //move to the next char pointer to the current address 
            pNextChar = pFoundChar;
            continue;
        }
    }
    
    //escape the program name 
    *pNextChar = _T('\0');
    
    _tcscpy( lpszAppPath,Path);
}


