// BootModeOpr.cpp : Defines the entry point for the DLL application.
//

#include "stdafx.h"
#include "BMPlatform.h"
//#include <initguid.h>
#include "BootModeOpr.h"
#include <process.h>
#include "OptionHelpper.h"

extern "C"
{
#include "bmpacket.h"
#include "crc16.h"
}

#define  WM_RCV_CHANNEL_DATA              WM_APP + 301 // channel data received
#define  WM_CHANNEL_CLOSE                 WM_RCV_CHANNEL_DATA + 1

#define LOG_FILENAME_PREFIX     _T("BootModeOpr_")
#define MAX_LOG_LEN             256

extern COptionHelpper ohObject;
/////////////////////////////////////////////////////////////////////////////
// CBootModeOpr
CBootModeOpr::CBootModeOpr()
{
    //m_lpCurrRead = NULL;
    m_bCheckCrc  = TRUE;
    m_bBigEndian = TRUE;

    ZeroMemory( m_RecvData, MAX_RECV_LEN );
    m_ulRecvDataLen = 0;
    m_iLastSentPktType = BSL_PKT_TYPE_MAX; 
    m_dwLastErrorCode = 0; 
    m_bOperationSuccess = FALSE;
    m_pLogFile  = NULL;    
    m_pChannel = NULL;

    m_dwRecvThreadID = 0;
    m_hRecvThread = NULL;
    m_hRecvThreadState = NULL;
//    m_hOprEvent = NULL;

    m_lpReceiveBuffer = NULL;
    m_dwReceiveLen = 0;
    m_dwBufferSize = 0;
    m_dwLastPktSize = 0;
    m_uiReadOffset = 0;
	m_dwCookie = 0;

	m_bHasCheckSum  = FALSE;

	m_dwBaud = DEFAULT_BAUDRATE;



    ::InitializeCriticalSection( &m_csRecvBbuffer );
	::InitializeCriticalSection( &m_csLastError );

}

CBootModeOpr::~CBootModeOpr()
{
    FreeRecvBuffer();
    ::DeleteCriticalSection( &m_csRecvBbuffer );
	::DeleteCriticalSection( &m_csLastError );

}

BOOL CBootModeOpr::Initialize( LPCTSTR lpszChannelProgID, LPBYTE lpOpenParam,
                              BOOL bBigEndian, DWORD dwCookie  ) 
{
   OpenLogFile( dwCookie );

//   HRESULT hr = E_FAIL;
   CString strLog;
//    hr =  CoInitialize(NULL);
//     if( FAILED( hr ) )
//     {
//         strLog.Format( _T("Call CoInitialize() function fail. [0x%08X]"), hr );
//         Log( strLog );
//         return FALSE;
//     }

    USES_CONVERSION;
    CString strChannel = lpszChannelProgID;
	if(strChannel.CompareNoCase(_T(""))==0)
    {
        strLog.Format( _T("Unknown channel type.[%s]"),lpszChannelProgID);
        Log( strLog );
        return FALSE;
    }

    if( !CreateBmChannel((IProtocolChannel **)&m_pChannel) )
    {
		strLog.Format( _T("Create channel object fail.[0x%08X]"), GetLastErrorCode() );
        Log( strLog );
        return FALSE;
    }
   
    m_bBigEndian = bBigEndian;
    ZeroMemory( m_RecvData, MAX_RECV_LEN );
    m_ulRecvDataLen = 0;
    m_iLastSentPktType = BSL_PKT_TYPE_MAX; 
    m_dwLastErrorCode = 0; 
    m_bOperationSuccess = FALSE;

	m_pChannel->SetProperty( 0,PPI_Endian,(void*)m_bBigEndian );

//    if( FALSE == CreateRecvThread() )
//    {
//        Log( _T("Create receive thread fail."));
//        return FALSE;
//    }

   return ConnectChannel( lpOpenParam );  
}

void CBootModeOpr::Uninitialize()
{
    DisconnectChannel(); 
    
    //m_lpCurrRead       = NULL;
    m_bCheckCrc  = TRUE;

    ZeroMemory( m_RecvData, MAX_RECV_LEN );
    m_ulRecvDataLen = 0;
    
    m_iLastSentPktType = BSL_PKT_TYPE_MAX; 

    CloseLogFile();

	ReleaseBmChannel(m_pChannel);
	m_pChannel = NULL;

 //   CoUninitialize();    
}

BOOL CBootModeOpr::CheckBaud( DWORD dwTimeout /* = 1000  */)
{
    BYTE btSendData[ PACHET_HDR_LEN*2 ];    
    memset( btSendData, 0x7E, PACHET_HDR_LEN*2);
	
	int nSize = ohObject.Get7ENumOnce();
	if(nSize<0)
	{
		nSize = 1;
	}
	if(nSize > PACHET_HDR_LEN*2 )
	{
		nSize = PACHET_HDR_LEN*2;
	}

	//Clear received data, maybe there exist dirty data.
	m_pChannel->SetProperty(0,PPI_CLEAR_KEEP_PACKAGE,(LPCVOID)true);
	m_pChannel->Clear();
	m_pChannel->SetProperty(0,PPI_CLEAR_KEEP_PACKAGE,(LPCVOID)false);

    BOOL bSuccess = SendPacketData( BSL_CMD_CHECK_BAUD,nSize,NULL,nSize,dwTimeout );
	if( m_dwLastErrorCode == BSL_UART_SEND_ERROR)
	{
		Sleep( 500 );
	}
    LogOpr( _T("Check Baudrate"), bSuccess );

    return bSuccess;
}

BOOL CBootModeOpr::Connect( DWORD dwTimeout /* = 1000  */)
{
   BOOL bSuccess = SendCommandData( BSL_CMD_CONNECT, dwTimeout  );
   LogOpr( _T("Connect to Module"), bSuccess );
   
   return bSuccess;
}

BOOL CBootModeOpr::Excute( DWORD dwTimeout /* = 1000  */ )
{
    BOOL bSuccess = SendCommandData( BSL_CMD_EXEC_DATA , dwTimeout );
    LogOpr( _T("Excute  downloaded file"), bSuccess );
    
    return bSuccess;
}

BOOL CBootModeOpr::Reset( DWORD dwTimeout /* = 1000  */ )
{
    BOOL bSuccess = SendCommandData( BSL_CMD_NORMAL_RESET , dwTimeout );
    LogOpr( _T("Reset"), bSuccess );
    
    return bSuccess;
}

BOOL CBootModeOpr::ReadChipType( DWORD dwTimeout /* = 1000  */ )
{
    BOOL bSuccess = SendCommandData( BSL_CMD_READ_CHIP_TYPE , dwTimeout );
    LogOpr( _T("Read Chip Type"), bSuccess);

    return bSuccess;
}

BOOL CBootModeOpr::ReadFlashType( DWORD dwTimeout /* = 1000 */ )
{
    BOOL bSuccess = SendCommandData( BSL_CMD_READ_FLASH_TYPE, dwTimeout );
    LogOpr( _T("Read Flash Type"),bSuccess );
    return bSuccess;
}


// BOOL CBootModeOpr::ReadFlashInfo( DWORD dwTimeout /* = 1000 */ )
// {
//     BOOL bSuccess = SendCommandData( BSL_CMD_READ_FLASH_INFO, dwTimeout );
//     LogOpr( _T("Read Flash Info"),bSuccess );
//     return bSuccess;
// }

// BOOL CBootModeOpr::DownFlashSpec( DWORD dwLength, LPBYTE lpDownLoadData, DWORD dwTimeout /*= 1000*/ )
// {
// 	BM_PACKAGE bp;
// 	bp.header.type = BSL_CMD_DOWN_FLASH_SPEC;
// 	bp.header.len = (unsigned short)dwLength;
// 	bp.data = lpDownLoadData;
// 	
//     BOOL bState = SendData( &bp,dwTimeout );    
// 
//     if( m_dwLastErrorCode == BSL_REP_FLASH_SPEC_ERROR )
//     {
//         Log( _T("DownFlashSpec:Flash Spec error") );
//     }
//     else
//     {
//         LogOpr( _T("DownFlashSpec"), bState );
//     }
// 
//     
//     return bState;
// }

BOOL CBootModeOpr::EndData( DWORD dwTimeout /* = 1000  */ )
{
    BOOL bSuccess = SendCommandData( BSL_CMD_END_DATA, dwTimeout );
    if( !bSuccess &&  m_dwLastErrorCode == BSL_REP_DOWN_DEST_ERROR )
    {
        Log( _T("End Data:Doownload error in previous command") );
    }
    else
    {
        LogOpr( _T("End Data"), bSuccess );
    }
    
    return bSuccess;
}

BOOL CBootModeOpr::ChangeBaud( DWORD dwTimeout /* = 1000  */ )
{
	ICommChannel *pPhyChannel = NULL;
	m_pChannel->GetLowerChannel(&pPhyChannel,FALSE);
	if(pPhyChannel== NULL)
	{
		return FALSE;
	}
    DWORD dwBaud = 0;
    BOOL bOK  = pPhyChannel->GetProperty( 0, CH_PROP_BAUD, &dwBaud );
    if( !bOK )
    {
        return TRUE;
    }   
	
    if( dwBaud == CHANGE_BAUD_NOT_AVAILABLE || dwBaud == m_dwBaud )
    {
        return TRUE;
    }

    UINT uiDataLen = sizeof( DWORD );
    LPBYTE lpPackData = new BYTE[uiDataLen];
    if( lpPackData == NULL )
        return FALSE; 
    
    *(DWORD *)&lpPackData[ 0 ] = m_dwBaud;    
    
    BOOL bSuccess = SendPacketData( BSL_CMD_CHANGE_BAUD, 
                        uiDataLen  + PACHET_HDR_LEN , 
                        lpPackData, uiDataLen, dwTimeout  );
    delete []lpPackData;

    _TCHAR szOpr[ MAX_LOG_LEN ];
    _stprintf( szOpr, _T("FDL:Change Baudrate( %d )"), m_dwBaud );
    LogOpr( szOpr, bSuccess );
    
    if( bSuccess )
    {
        bSuccess = pPhyChannel->SetProperty( 0, CH_PROP_BAUD, (LPCVOID)&m_dwBaud );
        Sleep( 500 );
    }
    
    _stprintf( szOpr, _T("BMPlatform:Change Baudrate( %d )"), dwBaud );
    LogOpr( szOpr, bSuccess );            
    
    return bSuccess;
}

BOOL CBootModeOpr::ReadFlash( DWORD dwBase,DWORD dwLength,
                             DWORD dwTimeout /* = 1000  */  )
{
    UINT uiDataLen =  START_BSL_PKT_LEN - PACHET_HDR_LEN;
    LPBYTE lpPackData = new BYTE[uiDataLen];
    if( lpPackData == NULL )
        return FALSE; 
    
    m_uiReadOffset = dwBase % 4;
    dwLength += m_uiReadOffset;    
	
	BOOL bRetried = FALSE;

ReadFlash_Retry:

    *(DWORD *)&lpPackData[ 0 ] = dwBase;
    *(DWORD *)&lpPackData[ sizeof(DWORD) ] = dwLength;
    m_dwLastPktSize = dwLength;

    //m_lpCurrRead = NULL;

    BOOL bSuccess = SendPacketData( BSL_CMD_READ_FLASH, START_BSL_PKT_LEN, 
        lpPackData, uiDataLen , dwTimeout  );

	if( !bSuccess && !bRetried )
	{
		// log the current failed information
		_TCHAR szOpr[ MAX_LOG_LEN ];
		_stprintf( szOpr,_T("%s( Base:0x%08X, Size:0x%08X )"), _T("Read Flash"), dwBase, dwLength );
		LogOpr( szOpr, bSuccess );

		bRetried = TRUE;
		// Clear channel dirty data and prepare for repeat reading.
		Log( _T("Clear channel buffer and retry ReadFlash."));
		m_pChannel->Clear();
		goto ReadFlash_Retry;
	}
	
    delete []lpPackData;

    _TCHAR szOpr[ MAX_LOG_LEN ];
    _stprintf( szOpr,_T("%s( Base:0x%08X, Size:0x%08X )"), _T("Read Flash"), dwBase, dwLength );
    LogOpr( szOpr, bSuccess );
    
    return bSuccess;
}

// dwBase gives partition id,dwOffset gives offset in this partition
BOOL CBootModeOpr::ReadPartitionFlash( DWORD dwBase,DWORD dwLength,DWORD dwOffset,DWORD dwTimeout /* = 1000 */)
{
    UINT uiDataLen =  START_BSL_PKT_LEN - PACHET_HDR_LEN + sizeof( DWORD );
    LPBYTE lpPackData = new BYTE[uiDataLen];
    if( lpPackData == NULL )
        return FALSE; 
    
    m_uiReadOffset = 0;
	BOOL bRetried = FALSE;

ReadPartitionFlash_Retry:
    
    *(DWORD *)&lpPackData[ 0 ] = dwBase;
    *(DWORD *)&lpPackData[ sizeof(DWORD) ] = dwLength;
    *(DWORD *)&lpPackData[ sizeof( DWORD ) * 2 ] = dwOffset;
    m_dwLastPktSize = dwLength;
    
    //m_lpCurrRead = NULL;
    
    BOOL bSuccess = SendPacketData( BSL_CMD_READ_FLASH, START_BSL_PKT_LEN + sizeof( DWORD ), 
        lpPackData, uiDataLen , dwTimeout  );

	if( !bSuccess && !bRetried )
	{
		// log the current failed information
		_TCHAR szOpr[ MAX_LOG_LEN ];
		_stprintf( szOpr,_T("%s( Base:0x%08X, Size:0x%08X, Offset:0x%08X )"), _T("Read Flash"), dwBase, dwLength,dwOffset );
		LogOpr( szOpr, bSuccess );

		bRetried = TRUE;
		// Clear channel dirty data and prepare for repeat reading.
		Log( _T("Clear channel buffer and retry ReadPartitionFlash."));
		m_pChannel->Clear();
		goto ReadPartitionFlash_Retry;
	}
	
	delete []lpPackData;
    
    _TCHAR szOpr[ MAX_LOG_LEN ];
    _stprintf( szOpr,_T("%s( Base:0x%08X, Size:0x%08X, Offset:0x%08X )"), _T("Read Flash"), dwBase, dwLength,dwOffset );
    LogOpr( szOpr, bSuccess );
    
    return bSuccess;
}

BOOL CBootModeOpr::ReadNVItem( DWORD dwStartAddr, DWORD dwEndAddr, 
                              unsigned short uiItemID, DWORD dwTimeout /* = 1000  */ )
{
    BYTE lpSendData[ READ_NVITEM_PKT_LEN];
    
    //Fill The Packet Type         
//     *(short *)&lpSendData[PKT_TYPE_POS]   = BSL_CMD_READ_NVITEM;
//     *(short *)&lpSendData[PKT_LEN_POS]    = READ_NVITEM_PKT_LEN ;  

    if( m_bBigEndian )
    {
        DWORD dwSoruceValue, dwDestValue;
        dwSoruceValue =  dwStartAddr;    
        dwDestValue   = 0;
        CONVERT_INT( dwSoruceValue, dwDestValue);
        *(DWORD *)&lpSendData[0] = dwDestValue;

        dwSoruceValue = dwEndAddr;    
        dwDestValue   = 0;
        CONVERT_INT( dwSoruceValue, dwDestValue);
        *(DWORD *)&lpSendData[sizeof( DWORD) ] = dwDestValue;

        unsigned short uiSourceValue, uiDestValue;        
        uiSourceValue =  uiItemID;    
        uiDestValue   = 0;
        CONVERT_SHORT( uiSourceValue, uiDestValue);
        *(unsigned short *)&lpSendData[ 2 * sizeof( DWORD) ] = uiDestValue;
    }
    else
    {
        *(DWORD *)&lpSendData[ 0 ] = dwStartAddr;
        *(DWORD *)&lpSendData[ sizeof( DWORD)  ] = dwEndAddr;
        *(unsigned short *)&lpSendData[ 2 * sizeof( DWORD)  ] = uiItemID;        
    }    
    
	BM_PACKAGE bp;
	bp.header.type = BSL_CMD_READ_NVITEM;
	bp.header.len = READ_NVITEM_PKT_LEN;
	bp.data = lpSendData;

    BOOL bSuccess = SendData( &bp,dwTimeout );    

    _TCHAR szOpr[ MAX_LOG_LEN ];
    _stprintf( szOpr,_T("%s( Start:0x%08X, End:0x%08X, Item ID:%d )"), _T("Read NV Item"), 
        dwStartAddr, dwEndAddr, uiItemID );
    LogOpr( szOpr, bSuccess );
    
    return bSuccess;
}

BOOL CBootModeOpr::EraseFlash( DWORD dwBase,DWORD dwLength , DWORD dwTimeout /* = 1000  */ )
{
    DWORD dwSize = dwLength;
    if( (dwSize != 0xFFFFFFFF) && (dwSize % 2)  )
    {
        dwSize++;
    }
    
    UINT uiDataLen =  2 * sizeof(DWORD) ;
    LPBYTE lpPackData = new BYTE[uiDataLen];
    if( lpPackData == NULL )
        return FALSE;    
    
    *(DWORD *)&lpPackData[ 0 ] = dwBase;
    *(DWORD *)&lpPackData[ sizeof(DWORD) ] = dwSize;
        
    BOOL bSuccess = SendPacketData( BSL_CMD_ERASE_FLASH,
        ERASE_FLASH_PKT_LEN, lpPackData, 
        uiDataLen , dwTimeout );
    delete []lpPackData;
    
    _TCHAR szOpr[ MAX_LOG_LEN ];
    _stprintf( szOpr,_T("%s( Base:0x%08X, Size:0x%08X)"), _T("Erase Flash"), dwBase, dwLength );
    LogOpr( szOpr, bSuccess );
    
    return bSuccess;    
}

BOOL CBootModeOpr::StartData( DWORD dwBase,DWORD dwLength , 
                                 LPBYTE lpDownLoadData,
                                  DWORD dwTimeout /* = 1000  */ )
{
    DWORD dwSize = dwLength;
    if( dwSize % 2 )
    {
        dwSize++;
    }

	_TCHAR szOpr[ MAX_LOG_LEN ] = {0};
	
	if(dwSize == 0)
	{
		m_dwLastErrorCode = BSL_REP_SIZE_ZERO;
		_stprintf( szOpr,_T("Start Data: ( Base:0x%08X ) size is zero."),dwBase);
		LogOpr( szOpr, FALSE );
		return FALSE;
	}
	
	int nSendDataLen = START_BSL_PKT_LEN;
    
    UINT uiDataLen =  2 * sizeof(DWORD) ;
	DWORD dwCheckSum = 0;
	if(m_bHasCheckSum)
	{
		uiDataLen += sizeof(DWORD);
		nSendDataLen += sizeof(DWORD);
		dwCheckSum = DoCheckSum(lpDownLoadData,dwLength);
		if( 0 == dwCheckSum )
		{
			_stprintf( szOpr,_T("%s( Base:0x%08X, Size:0x%08X, CheckSum:0x%08X)"), _T("Start Data"),
					   dwBase, dwLength, dwCheckSum);
			LogOpr( szOpr, FALSE );
			return FALSE;
		}
	}	
	
    LPBYTE lpPackData = new BYTE[uiDataLen];
    if( lpPackData == NULL )
        return FALSE;    
    
    *(DWORD *)&lpPackData[ 0 ] = dwBase;
    *(DWORD *)&lpPackData[ sizeof(DWORD) ] = dwSize;

	if(m_bHasCheckSum)
	{
		*(DWORD *)&lpPackData[ sizeof(DWORD)*2 ] = dwCheckSum;
	}

    //m_lpCurrRead = lpDownLoadData;

    
    BOOL bSuccess = SendPacketData( BSL_CMD_START_DATA,
                        nSendDataLen, lpPackData, 
                        uiDataLen , dwTimeout );
    delete []lpPackData;
    
    
	if(!m_bHasCheckSum)
	{
		_stprintf( szOpr,_T("%s( Base:0x%08X, Size:0x%08X)"), _T("Start Data"), dwBase, dwLength );
	}
	else
	{
		_stprintf( szOpr,_T("%s( Base:0x%08X, Size:0x%08X, CheckSum:0x%08X)"), _T("Start Data"),
			       dwBase, dwLength, dwCheckSum);
	}
    LogOpr( szOpr, bSuccess );
    
    return bSuccess;
}

BOOL CBootModeOpr::MidstData( DWORD dwLength, 
							 LPBYTE lpDownLoadData,
                             DWORD dwTimeout /* = 1000  */, 
							 DWORD dwTotal /*= 0*/)
{
	BM_PACKAGE bp;
	bp.header.type = BSL_CMD_MIDST_DATA;
	bp.header.len = (unsigned short)dwLength;
	bp.data = lpDownLoadData;

	BOOL bRetried = FALSE;

MidstData_Retry:
    
    BOOL bState = SendData( &bp,dwTimeout );  

	if( !bState && !bRetried && m_dwLastErrorCode == BSL_REP_VERIFY_ERROR )
	{
		// log the current failed information
		CString strLog;
		strLog.Format(_T("Download (0x%08X)"),dwTotal);
        LogOpr( strLog, bState );

		bRetried = TRUE;
		// Clear channel dirty data and prepare for repeat reading.
		Log( _T("Clear channel buffer and retry MidstData."));
		m_pChannel->Clear();
		goto MidstData_Retry;
	}
    
    //m_lpCurrRead += dwLength;    

    if( m_dwLastErrorCode == BSL_REP_DOWN_DEST_ERROR )
    {
        Log( _T("Download:Download error in previous command") );
    }
    else
    {
		CString strLog;
		strLog.Format(_T("Download (0x%08X)"),dwTotal);
        LogOpr( strLog, bState );
    }
    
    return bState;
}

BOOL CBootModeOpr::Repartition( DWORD dwTimeout )
{
    BOOL bSuccess = SendCommandData( BSL_CMD_REPARTITION , dwTimeout );
    LogOpr( _T("Repartition"), bSuccess );
    
    return bSuccess;
}

BOOL CBootModeOpr::SendData( BM_PACKAGE* lpPkg,DWORD dwTimeout )
{
	if( NULL == lpPkg || m_dwLastErrorCode == BSL_USER_CANCEL || m_pChannel == NULL )
	{
		return FALSE;
	}

    //Save the last send packet type
    m_iLastSentPktType = (pkt_type_s)lpPkg->header.type;   
    if( m_iLastSentPktType == BSL_CMD_MIDST_DATA || m_iLastSentPktType == BSL_CMD_END_DATA )
    {
        if( m_dwLastErrorCode == BSL_REP_DOWN_DEST_ERROR )
        {
            return FALSE;
        }
    }
    
	PRT_WRITE_T pwt;
	pwt.action = PRT_WRITE_normal;
	pwt.nCond = -1;
	pwt.lpProtocolData = lpPkg;

	if(lpPkg->header.type == BSL_CMD_CHECK_BAUD)
	{
		pwt.action = PRT_WRITE_no_respond;
	}

	::EnterCriticalSection( &m_csLastError);
	if(m_dwLastErrorCode == BSL_USER_CANCEL)
	{
		::LeaveCriticalSection( &m_csLastError);
		return FALSE;
	}

	if( !m_pChannel->Write( &pwt,1 ) )
	{		
		m_dwLastErrorCode = BSL_UART_SEND_ERROR;
        Log( _T("Write channel fail.") );
		::LeaveCriticalSection( &m_csLastError);
        return FALSE;
	}

	PRT_READ_T prt;
	prt.lpCond = NULL;
	prt.nCond = pwt.nCond;
	PRT_BUFF* lpBuff;
	prt.lppBuff = &lpBuff;
	if( !m_pChannel->Read( &prt,1,dwTimeout ) )
	{
		PACKAGE_INFO info;
		m_pChannel->GetProperty( 0,PPI_GetError,&info );
		if( info.bCrcError || info.bInvalidLen )
		{
			m_dwLastErrorCode = BSL_REP_DECODE_ERROR;
		}
		else if( info.bNoTail )
		{
			m_dwLastErrorCode = BSL_REP_INCOMPLETE_DATA;
		}
		else
		{
			m_dwLastErrorCode = BSL_PHONE_WAIT_INPUT_TIMEOUT;
		}			
		::LeaveCriticalSection( &m_csLastError);
		return FALSE;
	}
	
	P_Packet_Header tmpPktHeader = (P_Packet_Header)lpBuff->lpData;    
    m_dwLastErrorCode = tmpPktHeader->PacketType;

	::LeaveCriticalSection( &m_csLastError);

	if( m_dwLastErrorCode == BSL_USER_CANCEL )
	{
		return FALSE;
	}

    m_bOperationSuccess = FALSE;
    //According the Response message type implement operation
    if(tmpPktHeader->PacketType == BSL_REP_ACK)
    {
        m_bOperationSuccess = TRUE;
    }
    else if(tmpPktHeader->PacketType == BSL_REP_VER)
    {
        m_bOperationSuccess = TRUE;
    }    
    else if(tmpPktHeader->PacketType == BSL_REP_READ_FLASH)
    {
        m_bOperationSuccess = TRUE;
        if( NULL != lpBuff->lpData  &&  m_iLastSentPktType == BSL_CMD_READ_FLASH )
        {
            // The buffer is read from module
            if( lpBuff->size <= PACHET_HDR_LEN  ||
                m_dwLastPktSize != lpBuff->size - PACHET_HDR_LEN)
            {
                // Data read error
				lpBuff->free_prt( lpBuff );
                m_dwLastErrorCode = BSL_REP_OPERATION_FAILED;
                return FALSE;
            }
            if( m_lpReceiveBuffer != NULL )
            {
                if( m_dwReceiveLen + lpBuff->size - PACHET_HDR_LEN - m_uiReadOffset <= m_dwBufferSize  )
                {
                    memcpy( m_lpReceiveBuffer + m_dwReceiveLen,
                        lpBuff->lpData + PACHET_HDR_LEN + m_uiReadOffset,
                        lpBuff->size - PACHET_HDR_LEN - m_uiReadOffset );
                    m_dwReceiveLen += ( lpBuff->size - PACHET_HDR_LEN - m_uiReadOffset );
                    m_uiReadOffset = 0;
                }
            }
        }

    } else if( tmpPktHeader->PacketType == BSL_REP_READ_CHIP_TYPE || 
        tmpPktHeader->PacketType == BSL_REP_READ_NVITEM ||
        tmpPktHeader->PacketType == BSL_REP_READ_FLASH_TYPE ||
		/*tmpPktHeader->PacketType == BSL_REP_READ_FLASH_INFO||*/
		tmpPktHeader->PacketType == BSL_REP_READ_SECTOR_SIZE ||
		tmpPktHeader->PacketType == BSL_CHIPID_NOT_MATCH)
    { 
		if(tmpPktHeader->PacketType == BSL_CHIPID_NOT_MATCH)
		{
			m_bOperationSuccess = FALSE; 
		}
		else
		{
			m_bOperationSuccess = TRUE; 
		}

        if(  NULL != lpBuff->lpData  )
        {
            ::EnterCriticalSection( &m_csRecvBbuffer );
            if( m_lpReceiveBuffer != NULL )
            {
                m_dwReceiveLen = 0; 
                if( lpBuff->size - PACHET_HDR_LEN  == m_dwBufferSize )
                {
                    memcpy( m_lpReceiveBuffer ,lpBuff->lpData + PACHET_HDR_LEN, m_dwBufferSize );
                    m_dwReceiveLen = m_dwBufferSize;
                }
            }
            ::LeaveCriticalSection( &m_csRecvBbuffer );
        }
    }

    lpBuff->free_prt( lpBuff );   

	
   
    return m_bOperationSuccess;
}

BOOL CBootModeOpr::SendCommandData( pkt_type_s nCmdType, DWORD dwTimeout /* = 1000  */ )
{
    /*
	BYTE btSendData[PACHET_HDR_LEN];
    
    memset(&btSendData, 0, PACHET_HDR_LEN);    
    *(short *)&btSendData[PKT_TYPE_POS] = (short)nCmdType;
	return SendData( btSendData, PACHET_HDR_LEN , TRUE, dwTimeout );    
	*/
	BM_PACKAGE bp;
	bp.header.type = (unsigned short)nCmdType;
	bp.header.len = 0;
	bp.data = NULL;

	return SendData( &bp,dwTimeout );
}

BOOL CBootModeOpr::SendPacketData( pkt_type_s nCmdType, 
                                  const int /*nSendDataLen*/,
                                  LPBYTE lpPacketData,
                                  UINT uiDataLen,
                                  DWORD dwTimeout /* = 1000  */ )
{
	if( m_bBigEndian && NULL != lpPacketData )
	{
		for( UINT i = 0; i < uiDataLen / sizeof( DWORD ); i++ )
		{
            DWORD dwSoruceValue, dwDestValue;
			
            dwSoruceValue =  *(DWORD *)&lpPacketData[ i * sizeof( DWORD) ];    
            dwDestValue   = 0;
            CONVERT_INT( dwSoruceValue, dwDestValue);
            *(DWORD *)&lpPacketData[ i * sizeof( DWORD) ] = dwDestValue;
        }
    }

	BM_PACKAGE bp;
	bp.header.type = (unsigned short)nCmdType;
	bp.header.len = (unsigned short)uiDataLen;
	bp.data = lpPacketData;
    
    return SendData( &bp, dwTimeout );
}

BOOL CBootModeOpr::AllocRecvBuffer( DWORD dwSize )
{
    ::EnterCriticalSection( &m_csRecvBbuffer );
    if( m_lpReceiveBuffer != NULL )
    {
        if( m_dwBufferSize == dwSize )
        {
            ZeroMemory( m_lpReceiveBuffer, m_dwBufferSize );
            m_dwReceiveLen = 0;
        }
        else
        {
            delete[] m_lpReceiveBuffer;
            m_lpReceiveBuffer = NULL;
            m_dwReceiveLen = 0;
            m_dwBufferSize = 0;            
        }
    }
    if( m_lpReceiveBuffer == NULL )
    {
        m_lpReceiveBuffer = new BYTE [ dwSize ];
        if( m_lpReceiveBuffer == NULL )
            return FALSE;
        m_dwReceiveLen = 0;
        m_dwBufferSize = dwSize;
    }
    
    ::LeaveCriticalSection( &m_csRecvBbuffer );
    
    return TRUE;
}

LPBYTE CBootModeOpr::GetRecvBuffer()
{
    return m_lpReceiveBuffer;
}

DWORD CBootModeOpr::GetRecvBufferSize()
{
    return m_dwReceiveLen;
}

void CBootModeOpr::FreeRecvBuffer()
{
    ::EnterCriticalSection( &m_csRecvBbuffer );
    if( m_lpReceiveBuffer != NULL )
    {
        delete[] m_lpReceiveBuffer;
        m_lpReceiveBuffer = NULL;
        m_dwReceiveLen = 0;    
        m_dwBufferSize = 0;
    }
    ::LeaveCriticalSection( &m_csRecvBbuffer );

}

BOOL CBootModeOpr::ConnectChannel( LPBYTE lpOpenParam )
{
//    m_pChannel->SetReceiver( WM_RCV_CHANNEL_DATA, TRUE, (LPVOID)m_dwRecvThreadID );
	DWORD dwPort = *((DWORD*)lpOpenParam);
	DWORD dwBaud = *((DWORD*)(lpOpenParam+sizeof(DWORD)));
	m_dwBaud = dwBaud;

	CHANNEL_ATTRIBUTE ca;
	ca.ChannelType = CHANNEL_TYPE_COM;
	ca.Com.dwPortNum = dwPort;
	ca.Com.dwBaudRate = ohObject.GetDefaultBaudrate();
         
    BOOL bOK  = m_pChannel->Open( &ca );
    
    if( !bOK )
    {
        Log( _T("Open Channel fail.") );
        return FALSE;
    }    
    return TRUE;    
}

BOOL CBootModeOpr::DisconnectChannel()
{
    if( m_pChannel == NULL )
        return FALSE;

    m_pChannel->Close();
//  m_pChannel = NULL;
    return TRUE;
}

DWORD CBootModeOpr::GetLastErrorCode()
{
    return m_dwLastErrorCode;
}


void CBootModeOpr::GetLastErrorDescription(
                                           DWORD dwErrorCode,
                                           LPTSTR lpszErrorDescription, 
                                           int nSize )
{
    if( lpszErrorDescription == NULL || nSize == 0 )
        return;
    
    _TCHAR szErrorIniFile[MAX_PATH];
    
    GetModuleFilePath( g_theApp.m_hInstance, szErrorIniFile );
    _tcscat( szErrorIniFile,  _T("\\BMError.ini") );
    
    _TCHAR szKey[ 10 ];
    _itot( dwErrorCode, szKey, 10 );
    
    GetPrivateProfileString(  _T("ErrorDescription"), szKey,
        _T("Unknown Error"), lpszErrorDescription, 
        nSize, szErrorIniFile );  
    
    return;
}

BOOL CBootModeOpr::OpenLogFile( DWORD dwCookie )
{
    CloseLogFile();

	int nLogFlag = ohObject.GetLogFlag();
	if(0 == nLogFlag)
	{
		m_pLogFile = NULL;
		return TRUE;
	}
	
    m_dwCookie = dwCookie;
    _TCHAR szLogFileName[ MAX_PATH ];
    GetModuleFilePath( NULL, szLogFileName );

    SYSTEMTIME  currentTime ;
    GetLocalTime( &currentTime);
    _stprintf( szLogFileName, _T("%s\\%sCOM_%d_%d_%d_%d.log"), 
                        szLogFileName, LOG_FILENAME_PREFIX, 
                        dwCookie,
                        currentTime.wYear,
                        currentTime.wMonth,
                        currentTime.wDay );

    m_pLogFile = _tfopen( szLogFileName, _T("a+") );  
    if( m_pLogFile != NULL )
    {
        GetModuleFileName( g_theApp.m_hInstance, 
                        szLogFileName, MAX_PATH );
        _ftprintf( m_pLogFile, _T("===============================%s\n"),             
            szLogFileName );   
        return TRUE;
    }
    return FALSE;
}

void CBootModeOpr::CloseLogFile()
{
    if( m_pLogFile != NULL )
    {
        fclose( m_pLogFile );
        m_pLogFile = NULL;
    }    
}

void CBootModeOpr::Log( LPCTSTR lpszLog )
{
    if( m_pLogFile != NULL )
    {
        SYSTEMTIME  currentTime;
        GetLocalTime( &currentTime);
        _ftprintf( m_pLogFile, _T("%02d:%02d:%02d:%03d  %s\n"),  
                            currentTime.wHour,
                            currentTime.wMinute,
                            currentTime.wSecond,
							currentTime.wMilliseconds,
                            lpszLog );
        /*TRACE( _T("COM%d    %02d:%02d:%02d:%03d  %s\n"),  
                    m_dwCookie,
                    currentTime.wHour,
                    currentTime.wMinute,
                    currentTime.wSecond,
					currentTime.wMilliseconds,
                    lpszLog); */     
    }
}

void CBootModeOpr::LogOpr( LPCTSTR lpszOperation, BOOL bSuccess )
{
    if( m_pLogFile != NULL)
    {
        _TCHAR szLog[ MAX_LOG_LEN ];
        _tcscpy( szLog, lpszOperation );
        if( bSuccess )
        {
            _tcscat( szLog, _T(": Success.") );
            Log( szLog );
        }
        else
        {
            _TCHAR szDescription[ MAX_LOG_LEN ];
            DWORD dwErrorCode = m_dwLastErrorCode;
            GetLastErrorDescription( m_dwLastErrorCode, szDescription, MAX_LOG_LEN );
            _stprintf( szLog, _T("%s: Fail. [%d:%s]"), szLog, dwErrorCode, szDescription );
            Log( szLog );
            fflush( m_pLogFile );
        }
    }    
}

void CBootModeOpr::SetIsCheckCrc( BOOL bCheckCrc  )
{
    m_bCheckCrc = bCheckCrc;
	m_pChannel->SetProperty( 0,PPI_BM_CRC_Type,(void*)m_bCheckCrc );
}

void CBootModeOpr::StopOpr()
{
	if(m_pChannel)
	{
		m_pChannel->Close();
	}

	::EnterCriticalSection(&m_csLastError);
    m_dwLastErrorCode = BSL_USER_CANCEL;
	::LeaveCriticalSection(&m_csLastError);

//  SetEvent( m_hOprEvent );
}

DWORD CBootModeOpr::GetChannelPtr()
{
	ICommChannel* lpChannel;
    m_pChannel->GetLowerChannel( &lpChannel,false );
	return (DWORD)lpChannel;
}

void CBootModeOpr::SetChannel( DWORD pChannel )
{
    m_pChannel->SetLowerChannel((ICommChannel*)pChannel,false);
}

BOOL CBootModeOpr::SetDebugLogState(DWORD dwState)
{
	ICommChannel* lpChannel = NULL;
    m_pChannel->GetLowerChannel( &lpChannel,false );

	if( lpChannel )
	{
		return lpChannel->SetProperty( 0, CH_PROP_DEBUGMODE, (LPCVOID)&dwState );
	}

	return FALSE;
}

void  CBootModeOpr::SetStartDataInfo(BOOL bHasCheckSum)
{
	m_bHasCheckSum  = bHasCheckSum;
}

DWORD CBootModeOpr::DoCheckSum(LPBYTE pDataBuf,DWORD dwSize)
{
	//////////////////////////////////////////////////////////////////////////
	//make crc and add to the first two byte of data
	//notice: pDataBuf is bigendian, and do CRC not including the first WORD
	WORD wUpdateCrc = crc16(0,pDataBuf+sizeof(WORD),dwSize-sizeof(WORD));
	*pDataBuf = HIBYTE(wUpdateCrc);
	*(pDataBuf+1) = LOBYTE(wUpdateCrc);
					
	
	DWORD dwSum = 0;
	for(UINT i= 0; i< dwSize; i++)
	{
		dwSum += (BYTE)(*(pDataBuf+i));
	}

	return dwSum;
}

BOOL CBootModeOpr::ReadSectorSize( DWORD dwTimeout/* = 1000*/ )
{
    BOOL bSuccess = SendCommandData( BSL_CMD_READ_SECTOR_SIZE, dwTimeout );
    LogOpr( _T("Read sector size"),bSuccess );
    return bSuccess;
}

BOOL CBootModeOpr::DoBeforeCheckBaud()
{
	int nPacCap = 1;
	m_pChannel->SetProperty(0,PPI_INTERNAL_PACKAGE_CAPACITY,(LPCVOID)nPacCap);
	return TRUE;

}

BOOL CBootModeOpr::DoAfterChackBaud()
{
	int nPacCap = 0;
	m_pChannel->SetProperty(0,PPI_INTERNAL_PACKAGE_CAPACITY,(LPCVOID)nPacCap);
	return TRUE;
}