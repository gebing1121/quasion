package com.quasion.MasterManager.data;

import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.TextView;

import com.quasion.MasterManager.R;
import com.quasion.MasterManager.bean.Sales;
import com.quasion.MasterManager.ui.SalesAddActivity;
import com.quasion.MasterManager.ui.SalesListActivity;

public class SalesAdapter extends BaseAdapter {
	private SalesListActivity mActivity;
	private LayoutInflater mInflater;
	private List<Sales> mSalesData;

	public SalesAdapter(SalesListActivity context, List<Sales> salesData) {
		mActivity = context;
		mInflater = LayoutInflater.from(context);
		mSalesData = salesData;
	}

	public List<Sales> getItems() {
		return mSalesData;
	}

	@Override
	public int getCount() {
		return mSalesData.size();
	}

	@Override
	public Sales getItem(int position) {
		return mSalesData.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.sales_list_item, null);
		}
		final Sales sales = mSalesData.get(position);
		TextView productText = (TextView) convertView.findViewById(R.id.sales_list_item_text_product);
		TextView salesText = (TextView) convertView.findViewById(R.id.sales_list_item_text_sales);
		View editBtn = convertView.findViewById(R.id.sales_list_item_btn_edit);
		productText.setText(sales.getBrand() + " " + sales.getModel());
		salesText.setText(String.valueOf(sales.getSales()));
		editBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				mActivity.invokeSalesAddActivity(sales);
			}
		});
		return convertView;
	}
}
