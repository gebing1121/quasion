package com.quasion.MasterManager.data;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.quasion.MasterManager.R;
import com.quasion.MasterManager.bean.AlarmReport;

public class AlarmAdapter extends BaseAdapter {

	private LayoutInflater mInflater;
	private List<AlarmReport> mTableData;

	public AlarmAdapter(Context context, List<AlarmReport> data) {
		mInflater = LayoutInflater.from(context);
		this.mTableData = data;
	}

	@Override
	public int getCount() {
		return mTableData.size();
	}

	@Override
	public Object getItem(int position) {
		return mTableData.get(position);
	}

	@Override
	public long getItemId(int id) {
		return id;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.alarm_report_item, null);
		}
		AlarmReport rowData = mTableData.get(position);
		((TextView) convertView.findViewById(R.id.alarm_report_item_date)).setText(rowData.getAlarmDate());
		((TextView) convertView.findViewById(R.id.alarm_report_item_time)).setText(rowData.getAlarmTime());
		((TextView) convertView.findViewById(R.id.alarm_report_item_key)).setText(rowData.getUnlockCardId());
		((TextView) convertView.findViewById(R.id.alarm_report_item_owner)).setText(rowData.getUnlockOwnerName() != null ? rowData.getUnlockOwnerName() : "---");
		return convertView;
	}
}
