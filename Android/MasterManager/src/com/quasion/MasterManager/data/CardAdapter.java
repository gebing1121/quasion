package com.quasion.MasterManager.data;

import java.util.ArrayList;
import java.util.List;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.quasion.MasterManager.R;
import com.quasion.MasterManager.bean.NfcCard;
import com.quasion.MasterManager.ui.KeyManagementActivity;
import com.quasion.MasterManager.util.DateUtil;

public class CardAdapter extends BaseAdapter {
	private KeyManagementActivity mActivity;
	private LayoutInflater mInflater;
	private List<NfcCard> mCardList = new ArrayList<NfcCard>();

	public CardAdapter(KeyManagementActivity activity, List<NfcCard> entries) {
		mActivity = activity;
		mCardList = entries;
		mInflater = LayoutInflater.from(mActivity);
	}

	public List<NfcCard> getItems() {
		return mCardList;
	}

	@Override
	public int getCount() {
		return mCardList.size();
	}

	@Override
	public NfcCard getItem(int position) {
		return mCardList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, final ViewGroup parent) {
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.card_list_item, parent, false);
		}
		TextView cardIdText = (TextView) convertView.findViewById(R.id.card_list_item_text_card_id);
		TextView ownerText = (TextView) convertView.findViewById(R.id.card_list_item_text_owner);
		TextView status = (TextView) convertView.findViewById(R.id.card_list_item_text_card_status);
		TextView addTime = (TextView) convertView.findViewById(R.id.card_list_item_text_add_time);
		ImageButton deleteBtn = (ImageButton) convertView.findViewById(R.id.card_list_item_btn_delete);
		ImageButton assignBtn = (ImageButton) convertView.findViewById(R.id.card_list_item_btn_assign);

		final NfcCard nfcCard = getItem(position);
		cardIdText.setText(nfcCard.getKeyId());
		ownerText.setText(nfcCard.getOwnerEnglishName());
		switch (nfcCard.getStatus()) {
		case NfcCard.STATUS_ADDED:
			status.setText(R.string.nfc_card_text_status_added);
			break;
		case NfcCard.STATUS_ASSIGNED:
			status.setText(R.string.nfc_card_text_status_assigned);
			break;
		case NfcCard.STATUS_LOST:
			status.setText(R.string.nfc_card_text_status_lost);
			break;
		default:
			status.setText("");
			break;
		}
		addTime.setText(DateUtil.formatTime(nfcCard.getAddTime(), "yyyy/MM/dd"));
		deleteBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
				builder.setTitle(R.string.key_management_title_delete)
						.setMessage(mActivity.getString(R.string.key_management_text_delete_message) + nfcCard.getKeyId())
						.setPositiveButton(R.string.key_management_btn_delete_confirm, new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								mActivity.requestDeleteNfcCard(nfcCard.getKeyId());
							}
						})
						.setNegativeButton(R.string.key_management_btn_delete_cancel, null)
						.setCancelable(false).create().show();
			}
		});
		assignBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				mActivity.requestCardAssignActivity(nfcCard);
			}
		});
		return convertView;
	}
}
