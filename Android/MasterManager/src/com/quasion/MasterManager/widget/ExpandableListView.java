package com.quasion.MasterManager.widget;

import android.content.Context;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;

/**
 * Simple subclass of ListView which does nothing more than wrap
 * any ListAdapter in a ExpandableListAdapter
 */
public class ExpandableListView extends ListView {
	private ExpandableListAdapter adapter;
	private OnItemClickListener userItemClickListener = null;
	private OnItemClickListener systemItemClickListener = new OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
			ExpandableListAdapter adapter = (ExpandableListAdapter) getAdapter();
			adapter.getExpandToggleButton(view).performClick();
		}
	};

	public ExpandableListView(Context context) {
		super(context);
	}

	public ExpandableListView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public ExpandableListView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	public void setAdapter(ListAdapter adapter) {
		this.adapter = new ExpandableListAdapter(adapter);
		super.setAdapter(this.adapter);
	}

	public void setAdapter(ListAdapter adapter, int toggle_button_id, int expandable_view_id) {
		this.adapter = new ExpandableListAdapter(adapter, toggle_button_id, expandable_view_id);
		super.setAdapter(this.adapter);
	}

	public void setOnItemExpandCollapseListener(ExpandableListAdapter.OnItemExpandCollapseListener listener) {
		if (adapter != null) {
			adapter.setOnItemExpandCollapseListener(listener);
		}
	}

	/**
	 * Expend the specific item view.
	 *
	 * @param position item position
	 * @return true if a item view was expanded, false if position is invalid.
	 */
	public boolean expand(int position) {
		if (adapter == null) return false;
		return adapter.expandItem(position);
	}

	/**
	 * Collapses the currently open item view.
	 *
	 * @return true if a item view was collapsed, false if there was no open item view.
	 */
	public boolean collapse() {
		if (adapter == null) return false;
		return adapter.collapseLastOpen();
	}

	/**
	 * Enable or disable automatic expand/collapse the clicked item
	 * for this ListView
	 *
	 * @param enabled
	 */
	public void enableExpandOnItemClick(boolean enabled) {
		if (enabled) {
			super.setOnItemClickListener(systemItemClickListener);
		} else {
			super.setOnItemClickListener(userItemClickListener);
		}
	}

	@Override
	public void setOnItemClickListener(OnItemClickListener listener) {
		userItemClickListener = listener;
		if (getOnItemClickListener() != systemItemClickListener) {
			super.setOnItemClickListener(listener);
		}
	}

	@Override
	public Parcelable onSaveInstanceState() {
		if (adapter != null) return adapter.onSaveInstanceState(super.onSaveInstanceState());
		else return super.onSaveInstanceState();
	}

	@Override
	public void onRestoreInstanceState(Parcelable state) {
		if (state instanceof ExpandableListAdapter.SavedState) {
			ExpandableListAdapter.SavedState ss = (ExpandableListAdapter.SavedState) state;
			super.onRestoreInstanceState(ss.getSuperState());
			if (adapter != null) adapter.onRestoreInstanceState(ss);
		} else {
			super.onRestoreInstanceState(state);
		}
	}
}
