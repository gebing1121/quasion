package com.quasion.MasterManager.widget;

import java.util.BitSet;

import android.database.DataSetObserver;
import android.graphics.Rect;
import android.os.Parcel;
import android.os.Parcelable;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.WrapperListAdapter;

import com.quasion.MasterManager.R;

/**
 * Wraps a ListAdapter to give it expandable list view functionality.
 * The main thing it does is add a listener to the getToggleButton
 * which expands the getExpandableView for each list item.
 */
public class ExpandableListAdapter implements WrapperListAdapter {
	/**
	 * Interface for callback to be invoked whenever an item is expanded or
	 * collapsed in the list view.
	 */
	public interface OnItemExpandCollapseListener {
		/**
		 * Called when an item is expanded.
		 *
		 * @param itemView
		 *            the view of the list item
		 * @param position
		 *            the position in the list view
		 */
		public void onExpand(View itemView, int position);

		/**
		 * Called when an item is collapsed.
		 *
		 * @param itemView
		 *            the view of the list item
		 * @param position
		 *            the position in the list view
		 */
		public void onCollapse(View itemView, int position);
	}

	/**
	 * Reference to the last expanded list item.
	 * Since lists are recycled this might be null if
	 * though there is an expanded list item
	 */
	private View lastOpenItemView = null;
	/**
	 * The position of the last expanded list item.
	 * If -1 there is no list item expanded.
	 * Otherwise it points to the position of the last expanded list item
	 */
	private int lastOpenPosition = -1;
	/**
	 * Default Animation duration
	 * Set animation duration with @see setAnimationDuration
	 */
	private int animationDuration = 330;
	/**
	 * List item expand/collapse event listener
	 */
	private OnItemExpandCollapseListener expandCollapseListener;
	/**
	 * A list of positions of all list items that are expanded.
	 * Normally only one is expanded. But a mode to expand
	 * multiple will be added soon.
	 *
	 * If an item onj position x is open, its bit is set
	 */
	private BitSet openItems = new BitSet();
	/**
	* Will point to the ListView
	*/
	private ViewGroup parent;
	/**
	 * Will point to the ListAdapter
	 */
	private ListAdapter wrapped;
	/**
	 * ID for expand/collapse toggle button
	 */
	private int toggle_button_id;
	/**
	 * ID for expandable view
	 */
	private int expandable_view_id;

	//********************************************************************************
	// constructor
	//********************************************************************************

	public ExpandableListAdapter(ListAdapter wrapped) {
		this(wrapped, R.id.ExpandableList_toggle_button, R.id.ExpandableList_expandable_view);
	}

	public ExpandableListAdapter(ListAdapter wrapped, int toggle_button_id, int expandable_view_id) {
		this.wrapped = wrapped;
		this.toggle_button_id = toggle_button_id;
		this.expandable_view_id = expandable_view_id;
	}

	//********************************************************************************
	// implement for WrapperListAdapter
	//********************************************************************************

	@Override
	public ListAdapter getWrappedAdapter() {
		return wrapped;
	}

	@Override
	public boolean areAllItemsEnabled() {
		return wrapped.areAllItemsEnabled();
	}

	@Override
	public boolean isEnabled(int i) {
		return wrapped.isEnabled(i);
	}

	@Override
	public void registerDataSetObserver(DataSetObserver dataSetObserver) {
		wrapped.registerDataSetObserver(dataSetObserver);
	}

	@Override
	public void unregisterDataSetObserver(DataSetObserver dataSetObserver) {
		wrapped.unregisterDataSetObserver(dataSetObserver);
	}

	@Override
	public boolean isEmpty() {
		return wrapped.isEmpty();
	}

	@Override
	public int getCount() {
		return wrapped.getCount();
	}

	@Override
	public Object getItem(int i) {
		return wrapped.getItem(i);
	}

	@Override
	public long getItemId(int i) {
		return wrapped.getItemId(i);
	}

	@Override
	public boolean hasStableIds() {
		return wrapped.hasStableIds();
	}

	@Override
	public int getItemViewType(int i) {
		return wrapped.getItemViewType(i);
	}

	@Override
	public int getViewTypeCount() {
		return wrapped.getViewTypeCount();
	}

	@Override
	public View getView(int position, View view, ViewGroup viewGroup) {
		parent = viewGroup;
		view = wrapped.getView(position, view, viewGroup);
		enableFor(view, position);
		return view;
	}

	//********************************************************************************
	// public functions
	//********************************************************************************

	/**
	 * Sets a listener which gets call on item expand or collapse
	 *
	 * @param listener
	 *            the listener which will be called when an item is expanded or
	 *            collapsed. null for remove listener
	 */
	public void setOnItemExpandCollapseListener(OnItemExpandCollapseListener listener) {
		expandCollapseListener = listener;
	}

	/**
	 * This method is used to get the Button view that should
	 * expand or collapse the Expandable View.
	 * A listener will be attached to the button which will
	 * either expand or collapse the expandable view
	 *
	 * @see #getExpandableView(View)
	 * @param listItem the list view item
	 * @ensure return!=null
	 * @return a child of listItem which is a button
	 */
	public View getExpandToggleButton(View listItem) {
		if (listItem == null) return null;
		return listItem.findViewById(toggle_button_id);
	}

	/**
	 * This method is used to get the view that will be hidden
	 * initially and expands or collapse when the ExpandToggleButton
	 * is pressed @see getExpandToggleButton
	 *
	 * @see #getExpandToggleButton(View)
	 * @param listItem the list view item
	 * @ensure return!=null
	 * @return a child of listItem which is a view (or often ViewGroup)
	 *  that can be collapsed and expanded
	 */
	public View getExpandableView(View listItem) {
		if (listItem == null) return null;
		return listItem.findViewById(expandable_view_id);
	}

	/**
	 * Check's if any position is currently Expanded
	 * To collapse the open item @see collapseLastOpen
	 *
	 * @return boolean True if there is currently an item expanded, otherwise false
	 */
	public boolean isAnyItemExpanded() {
		return lastOpenPosition != -1;
	}

	/**
	 * Expanded the specific item
	 *
	 * @param position item position
	 * @return true if an item was expanded, false otherwise
	 */
	public boolean expandItem(int position) {
		if (position < 0 || position >= getCount()) {
			return false;
		}
		if (lastOpenPosition == position) {
			return true;
		}
		// collapse last open item
		collapseLastOpen();
		// save expand state
		lastOpenPosition = position;
		openItems.set(lastOpenPosition, true);
		// if specific item view is visible, animate and expand
		if (parent instanceof ListView) {
			ListView listView = (ListView) parent;
			if (position >= listView.getFirstVisiblePosition() && position <= listView.getLastVisiblePosition()) {
				lastOpenItemView = listView.getChildAt(position - listView.getFirstVisiblePosition());
				animateView(getExpandableView(lastOpenItemView), ExpandableListAnimation.EXPAND);
				notifyExpandCollapseListener(ExpandableListAnimation.EXPAND, lastOpenItemView, position);
			}
		}
		return true;
	}

	/**
	 * Closes the current open item.
	 * If it is current visible it will be closed with an animation.
	 *
	 * @return true if an item was closed, false otherwise
	 */
	public boolean collapseLastOpen() {
		if (!isAnyItemExpanded()) {
			return false;
		}
		// if expanded item view is visible, animate and collapse
		if (lastOpenItemView != null) {
			animateView(getExpandableView(lastOpenItemView), ExpandableListAnimation.COLLAPSE);
			notifyExpandCollapseListener(ExpandableListAnimation.COLLAPSE, lastOpenItemView, lastOpenPosition);
		}
		// save collapse state
		openItems.set(lastOpenPosition, false);
		lastOpenPosition = -1;
		lastOpenItemView = null;
		return true;
	}

	/**
	 * Gets the duration of the collapse animation in ms.
	 * Default is 330ms. Override this method to change the default.
	 *
	 * @return the duration of the anim in ms
	 */
	public int getAnimationDuration() {
		return animationDuration;
	}

	/**
	 * Set's the Animation duration for the Expandable animation
	 *
	 * @param duration The duration as an integer in MS (duration > 0)
	 * @exception IllegalArgumentException if parameter is less than zero
	 */
	public void setAnimationDuration(int duration) {
		if(duration < 0) {
			throw new IllegalArgumentException("Duration is less than zero");
		}
		animationDuration = duration;
	}

	//********************************************************************************
	// private functions
	//********************************************************************************

	private void enableFor(final View itemView, final int position) {
		final View toggleButton = getExpandToggleButton(itemView);
		final View expandableView = getExpandableView(itemView);
		// save last open item view
		if (lastOpenPosition == position) {
			// re reference to the last item view, so when can animate it when collapsed
			lastOpenItemView = itemView;
		} else if (lastOpenItemView == itemView && lastOpenPosition != position) {
			// lastOpenItemView is recycled, so its reference is false
			lastOpenItemView = null;
		}
		// measure and save expandable view height
		expandableView.measure(itemView.getWidth(), itemView.getHeight());
		LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) expandableView.getLayoutParams();
		if (openItems.get(position)) {
			expandableView.setVisibility(View.VISIBLE);
			params.bottomMargin = 0;
		} else {
			expandableView.setVisibility(View.GONE);
			params.bottomMargin = -expandableView.getMeasuredHeight();
		}
		expandableView.setLayoutParams(params);
		expandableView.requestLayout();
		// setup toggle click listener
		toggleButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(final View view) {
				Animation anim = expandableView.getAnimation();
				if (anim != null && anim.hasStarted() && !anim.hasEnded()) {
					anim.setAnimationListener(new Animation.AnimationListener() {
						@Override
						public void onAnimationStart(Animation animation) {
						}
						@Override
						public void onAnimationEnd(Animation animation) {
							view.performClick();
						}
						@Override
						public void onAnimationRepeat(Animation animation) {
						}
					});
				} else {
					expandableView.setAnimation(null);
					int type = expandableView.getVisibility() == View.VISIBLE ? ExpandableListAnimation.COLLAPSE : ExpandableListAnimation.EXPAND;
					// remember the state
					openItems.set(position, type == ExpandableListAnimation.EXPAND ? true : false);
					// check if we need to collapse a different view
					if (type == ExpandableListAnimation.EXPAND) {
						collapseLastOpen();
						lastOpenPosition = position;
						lastOpenItemView = itemView;
					} else {
						lastOpenPosition = -1;
						lastOpenItemView = null;
					}
					animateView(expandableView, type);
					notifyExpandCollapseListener(type, itemView, position);
				}
			}
		});
	}

	private void notifyExpandCollapseListener(int type, View view, int position) {
		if (expandCollapseListener != null) {
			if (type == ExpandableListAnimation.EXPAND) {
				expandCollapseListener.onExpand(view, position);
			} else if (type == ExpandableListAnimation.COLLAPSE) {
				expandCollapseListener.onCollapse(view, position);
			}
		}
	}

	/**
	 * Performs either COLLAPSE or EXPAND animation on the target view
	 * @param target the view to animate
	 * @param type the animation type, either ExpandCollapseAnimation.COLLAPSE
	 *			 or ExpandCollapseAnimation.EXPAND
	 */
	private void animateView(final View target, final int type) {
		if (target == null) {
			return;
		}
		Animation anim = new ExpandableListAnimation(target, type);
		anim.setDuration(getAnimationDuration());
		anim.setAnimationListener(new AnimationListener() {
			@Override
			public void onAnimationStart(Animation animation) {}
			@Override
			public void onAnimationRepeat(Animation animation) {}
			@Override
			public void onAnimationEnd(Animation animation) {
				if (type == ExpandableListAnimation.EXPAND) {
					if (parent instanceof ListView) {
						ListView listView = (ListView) parent;
						int movement = target.getBottom();

						Rect r1 = new Rect();
						boolean visible = target.getGlobalVisibleRect(r1);
						Rect r2 = new Rect();
						listView.getGlobalVisibleRect(r2);

						if (!visible) {
							listView.smoothScrollBy(movement, getAnimationDuration());
						} else if (r2.bottom == r1.bottom) {
							listView.smoothScrollBy(movement, getAnimationDuration());
						}
					}
				}
			}
		});
		target.startAnimation(anim);
	}

	//********************************************************************************
	// instance save state functions
	//********************************************************************************

	public Parcelable onSaveInstanceState(Parcelable parcelable) {
		SavedState ss = new SavedState(parcelable);
		ss.lastOpenPosition = this.lastOpenPosition;
		ss.openItems = this.openItems;
		return ss;
	}

	public void onRestoreInstanceState(SavedState state) {
		if (state != null) {
			this.lastOpenPosition = state.lastOpenPosition;
			this.openItems = state.openItems;
		}
	}

	/**
	 * The actual state class
	 */
	static class SavedState extends View.BaseSavedState {
		public BitSet openItems = null;
		public int lastOpenPosition = -1;

		SavedState(Parcelable superState) {
			super(superState);
		}

		private SavedState(Parcel in) {
			super(in);
			lastOpenPosition = in.readInt();
			openItems = readBitSet(in);
		}

		@Override
		public void writeToParcel(Parcel out, int flags) {
			super.writeToParcel(out, flags);
			out.writeInt(lastOpenPosition);
			writeBitSet(out, openItems);
		}

		// utility methods to read and write a bitset from and to a Parcel
		private static BitSet readBitSet(Parcel src) {
			BitSet set = new BitSet();
			if (src == null) {
				return set;
			}
			int cardinality = src.readInt();
			for (int i = 0; i < cardinality; i++) {
				set.set(src.readInt());
			}
			return set;
		}

		private static void writeBitSet(Parcel dst, BitSet set) {
			if (dst == null || set == null) {
				return;
			}
			dst.writeInt(set.cardinality());
			int nextSetBit = -1;
			while ((nextSetBit = set.nextSetBit(nextSetBit + 1)) != -1) {
				dst.writeInt(nextSetBit);
			}
		}

		// required field that makes Parcelables from a Parcel
		public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator<SavedState>() {
			public SavedState createFromParcel(Parcel in) {
				return new SavedState(in);
			}
			public SavedState[] newArray(int size) {
				return new SavedState[size];
			}
		};
	}
}
