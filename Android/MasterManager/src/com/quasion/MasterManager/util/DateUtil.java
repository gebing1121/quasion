package com.quasion.MasterManager.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DateUtil {
	/**
	 * 格式化输出日期/时间为字符串.
	 *
	 * @param time Date类型时间数据。如果为null，则使用系统当前时间
	 * @param format (可选) 输出的格式。如果未提供或者为null，则使用缺省格式："yyyy-MM-dd HH:mm:ss".
	 */
	public static String formatTime(Date time, String... format) {
		String dateFormat = (format.length == 0 || format[0] == null) ? "yyyy-MM-dd HH:mm:ss" : format[0];
		if (time == null) time = new Date(System.currentTimeMillis());
		return new SimpleDateFormat(dateFormat).format(time);
	}

	/**
	 * 格式化输出日期/时间为字符串.
	 *
	 * @param time long型时间数据。.
	 * @param format (可选) 输出的格式。如果未提供或者为null，则使用缺省格式："yyyy-MM-dd HH:mm:ss".
	 */
	public static String formatTime(long time, String... format) {
		return formatTime(new Date(time), format);
	}

	/**
	 * 格式化输出当前日期/时间为字符串.
	 *
	 * @param format (可选) 输出的格式。如果未提供或者为null，则使用缺省格式："yyyy-MM-dd HH:mm:ss".
	 */
	public static String formatCurrentTime(String... format) {
		return formatTime(null, format);
	}
}
