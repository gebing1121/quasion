package com.quasion.MasterManager.util;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.util.Log;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.support.DatabaseConnection;
import com.j256.ormlite.table.TableUtils;

import java.io.File;
import java.sql.SQLException;

/**
 * Created by Gebing on 2014/7/18.
 */
public class DatabaseUtil extends OrmLiteSqliteOpenHelper {

	private static String DATABASE_NAME = "default.db";
	private static int DATABASE_VERSION = 1;
	private static String DATABASE_CONFIG;

	static public void setDatabaseParameter(String databaseName, Integer databaseVersion, String databaseConfig) {
		if (databaseName != null && databaseName.length() != 0 ) DATABASE_NAME = databaseName;
		if (databaseVersion != null) DATABASE_VERSION = databaseVersion;
		if (databaseConfig != null) DATABASE_CONFIG = databaseConfig;
	}

	static public int deleteForAll(Dao dao) throws SQLException {
		return dao.delete(dao.deleteBuilder().prepare());
	}

	static public int deleteForAll(RuntimeExceptionDao dao) {
		try {
			return dao.delete(dao.deleteBuilder().prepare());
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	static public <T> int createForAll(Dao dao, T[] list) throws SQLException {
		if (dao == null || list == null || list.length == 0) return 0;
		DatabaseConnection connection = null;
		try {
			connection = dao.startThreadConnection();
			dao.setAutoCommit(connection, false);
			int count = 0;
			for (int i = 0; i < list.length; i++) count += dao.create(list[i]);
			dao.commit(connection);
			return count;
		} finally {
			if (connection != null) dao.endThreadConnection(connection);
		}
	}

	static public <T> int createForAll(RuntimeExceptionDao dao, T[] list) {
		if (dao == null || list == null || list.length == 0) return 0;
		DatabaseConnection connection = null;
		try {
			connection = dao.startThreadConnection();
			dao.setAutoCommit(connection, false);
			int count = 0;
			for (int i = 0; i < list.length; i++) count += dao.create(list[i]);
			dao.commit(connection);
			return count;
		} finally {
			if (connection != null) dao.endThreadConnection(connection);
		}
	}

	public DatabaseUtil(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION, DATABASE_CONFIG != null && DATABASE_CONFIG.length() != 0 ? new File(DATABASE_CONFIG) : null);
	}

	@Override
	public void onCreate(SQLiteDatabase sqliteDatabase, ConnectionSource connectionSource) {
		// 由于getDao()会自动创建表，因此此处不需要其他操作。
	}

	@Override
	public void onUpgrade(SQLiteDatabase sqliteDatabase, ConnectionSource connectionSource, int oldVer, int newVer) {
		// 当数据升级时，删除原数据库中所有表。
		try {
			SQLiteQueryBuilder query = new SQLiteQueryBuilder();
			query.setTables("sqlite_master");
			Cursor cursor = query.query(sqliteDatabase, null, null, null, null, null, null);
			cursor.moveToFirst();
			int num = cursor.getCount();
			for (int i = 0; i < num; i++, cursor.moveToNext()) {
				try {
					String type = cursor.getString(cursor.getColumnIndex("type"));
					String name = cursor.getString(cursor.getColumnIndex("name"));
					sqliteDatabase.execSQL("DROP " + type + " " + name);
				} catch (Exception e) {
				}
			}
			cursor.close();
		} catch (Exception e) {
			Log.e(DatabaseUtil.class.getName(), "Unable to upgrade database from version " + oldVer + " to new " + newVer, e);
		}
	}

	@Override
	public <D extends Dao<T, ?>, T> D getDao(Class<T> clazz) throws SQLException {
		D dao = super.getDao(clazz);
		TableUtils.createTableIfNotExists(dao.getConnectionSource(), clazz);
		return dao;
	}
}
