package com.quasion.MasterManager.util;

/**
 * 字符串操作工具包
 *
 * @author Gebing
 */
public class StringUtil {
	/**
	 * 判断给定字符串是否空白串。 空白串是指由空格、制表符、回车符、换行符组成的字符串 若输入字符串为null或空字符串，返回true
	 *
	 * @param input
	 * @return boolean
	 */
	public static boolean isEmpty(String input) {
		if (input != null && !"".equals(input)) {
			for (int i = 0; i < input.length(); i++) {
				char c = input.charAt(i);
				if (c != ' ' && c != '\t' && c != '\r' && c != '\n') {
					return false;
				}
			}
		}
		return true;
	}

	public static String fill(char ch, int cnt) {
		StringBuffer sb = new StringBuffer(cnt);
		for (int i = 0; i < cnt; i++) {
			sb.append(ch);
		}
		return sb.toString();
	}

	public static String fill(String str, int cnt) {
		if (str == null || str.length() == 0) {
			return "";
		}
		StringBuffer sb = new StringBuffer(cnt * str.length());
		for (int i = 0; i < cnt; i++) {
			sb.append(str);
		}
		return sb.toString();
	}

	/**
	 * Convert bytes to hex string
	 *
	 * @param data input number
	 * @param digit number of digit for hex string
	 * @return hex sting, "" if input is null or empty
	 */
	public static String toHex(long data, int digit) {
		digit = digit/2;
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < digit; i++) {
			sb.insert(0, toHex((byte) data));
			data >>= 8;
		}
		return sb.toString();
	}

	/**
	 * Convert bytes to hex string
	 *
	 * @param data input bytes
	 * @return hex sting, "" if input is null or empty
	 */
	public static String toHex(byte[] data) {
		if (data == null || data.length == 0) return "";

		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < data.length; i++) {
			sb.append(toHex(data[i]));
		}
		return sb.toString();
	}

	/**
	 * Convert a byte to 2 digit hex string
	 *
	 * @param data input byte
	 * @return 2 digit hex sting
	 */
	public static String toHex(byte data) {
		String result = Integer.toHexString(0xff & data);
		if (result.length() == 1) result = "0" + result;
		return result.toUpperCase();
	}

	/**
	 * Convert hex string to bytes
	 *
	 * @param text input hex string
	 * @return bytes, null if input is null or invalid length
	 */
	public static byte[] fromHex(String text) {
		try {
			if (text == null || (text.length() % 2) != 0) return null;
			byte[] result = new byte[text.length() / 2];
			for (int i = 0, j = 0; i < text.length(); i += 2, j++) {
				result[j] = (byte) Integer.parseInt(text.substring(i, i + 2), 16);
			}
			return result;
		} catch (Throwable t) {
			return null;
		}
	}
}
