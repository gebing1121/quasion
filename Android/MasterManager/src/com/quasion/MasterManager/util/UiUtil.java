package com.quasion.MasterManager.util;

import android.content.Context;
import android.widget.Toast;

/**
 * 应用程序UI工具包：封装UI相关的一些操作
 *
 * @author philipZhou
 * @version 1.0
 * @created 2013-12-04
 */
public class UiUtil {
	/**
	 * 弹出Toast消息
	 *
	 * @param msg
	 */
	public static void showToastMessage(Context context, String msg) {
		Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
	}

	/**
	 * 弹出Toast消息
	 *
	 * @param msg
	 */
	public static void showToastMessage(Context context, int msg) {
		Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
	}

	public static int dp2px(Context context, float dpValue) {
		final float scale = context.getResources().getDisplayMetrics().density;
		return (int) (dpValue * scale + 0.5f);
	}

	public static int px2dp(Context context, float pxValue){
		final float scale = context.getResources().getDisplayMetrics().density;
		return (int)(pxValue / scale + 0.5f);
	}
}
