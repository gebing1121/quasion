package com.quasion.MasterManager.ui;

import java.util.List;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.*;

import com.quasion.MasterManager.R;
import com.quasion.MasterManager.bean.AlarmReport;
import com.quasion.MasterManager.data.AlarmAdapter;
import com.quasion.MasterManager.server.ServerApiClient;

public class AlarmReportActivity extends BaseActivity {
	public static final String EXTRA_NFC_BASE_ID = "nfcBaseId";

	private TextView mTitle;
	private View mBackBtn;
	private ListView mAlarmDataList;
	private TextView mBaseIdText;
	private Spinner mDateRangeSpinner;
	private String mBaseId;

	private Handler mHandler = new Handler() {
		public void handleMessage(Message msg) {
			hideLoadingPage();
			switch (msg.what) {
			case BaseActivity.INVOKE_SUCCESS:
				AlarmAdapter adapter = new AlarmAdapter(getApplicationContext(), (List<AlarmReport>) msg.obj);
				mAlarmDataList.setAdapter(adapter);
				break;
			case BaseActivity.INVOKE_FAIL:
				onInvokeFailed((String) msg.obj);
				break;
			case BaseActivity.SESSION_EXPIRE:
				onSessionExpired((String) msg.obj);
				break;
			}
		};
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mBaseId = getIntent().getStringExtra(EXTRA_NFC_BASE_ID);
		setContentView(R.layout.alarm_report);
		initView();
	}

	private void initView() {
		// activity title
		mTitle = (TextView) findViewById(R.id.common_title_text_title);
		mTitle.setText(R.string.alarm_report_title);
		// NFC Base id
		mBaseIdText = (TextView) findViewById(R.id.alarm_report_text_base_id);
		mBaseIdText.setText(mBaseId);
		// date range spinner
		mDateRangeSpinner = (Spinner) findViewById(R.id.alarm_report_spinner_date_range);
		String[] dateRanges = getResources().getStringArray(R.array.alarm_report_array_date_range);
		ArrayAdapter adapter = new ArrayAdapter(this, R.layout.report_spinner_date_range, dateRanges);
		adapter.setDropDownViewResource(R.layout.common_spinner_dropdown);
		mDateRangeSpinner.setAdapter(adapter);
		mDateRangeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> adapterView, View view, final int position, long id) {
				// request alarm data from server
				showLoadingPage();
				new Thread() {
					public void run() {
						ServerApiClient.getAlarmData(mHandler, BaseActivity.INVOKE_SUCCESS, mBaseId, position + 1, 0, ServerApiClient.PAGE_DEFAULT_SIZE);
					}
				}.start();
			}
			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
			}
		});
		mDateRangeSpinner.setSelection(0);
		// alarm data list
		mAlarmDataList = (ListView) findViewById(R.id.alarm_data_list_alarm);
		// back button
		mBackBtn = findViewById(R.id.common_title_btn_back);
		mBackBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
	}
}
