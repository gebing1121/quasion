package com.quasion.MasterManager.ui;

import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.AnimationDrawable;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.quasion.MasterManager.R;
import com.quasion.MasterManager.server.ServerApiClient;
import com.quasion.MasterManager.util.StringUtil;
import com.quasion.MasterManager.util.UiUtil;

import java.util.Arrays;

/**
 * 添加NFC card 界面
 *
 * @author philipZhou
 * @version 1.0
 * @created 2013-12-06
 *
 */
public class NfcCardAddActivity extends BaseActivity {
	private static final String TAG = NfcCardAddActivity.class.getSimpleName();

	private TextView mTitle;
	private View mBackBtn;
	private View mSubmitBtn;
	private TextView mNfcCardLabelView;
	private TextView mNfcCardIdView;
	private ImageView mSearchAnimationView;
	private AnimationDrawable mSearchAnimationDrawable;

	private NfcAdapter mAdapter;
	private IntentFilter[] intentFilter;
	private PendingIntent pendingIntent;

	private Handler mHandler = new Handler() {
		public void handleMessage(Message msg) {
			hideLoadingPage();
			switch (msg.what) {
			case BaseActivity.INVOKE_SUCCESS:
				UiUtil.showToastMessage(getApplicationContext(), R.string.nfc_card_add_toast_add_success);
				finish();
				break;
			case BaseActivity.INVOKE_FAIL:
				onInvokeFailed((String) msg.obj);
				break;
			case BaseActivity.SESSION_EXPIRE:
				onSessionExpired((String) msg.obj);
				break;
			}
		};
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.nfc_card_add);
		initView();
		prepareNfc();
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (mAdapter != null) mAdapter.enableForegroundDispatch(this, pendingIntent, intentFilter, null);
	}

	@Override
	protected void onPause() {
		super.onPause();
		if (mAdapter != null) mAdapter.disableForegroundDispatch(this);
	}

	@Override
	public void onNewIntent(Intent intent) {
		// 当检测到NFC Card时，调用onNewIntent()
		Tag nfcTag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
		String nfcUid = StringUtil.toHex(Arrays.copyOf(nfcTag.getId(), 4));
		Log.d(TAG, "Found NFC card, QuasionId=" + nfcUid + ", Tag=" + StringUtil.toHex(nfcTag.getId()) + "," + nfcTag.toString());
		// set and show nfc card id
		mNfcCardLabelView.setVisibility(View.VISIBLE);
		mNfcCardIdView.setVisibility(View.VISIBLE);
		mNfcCardIdView.setText(nfcUid);
		// stop and hide search NFC card animation
		mSearchAnimationDrawable.stop();
		mSearchAnimationView.setVisibility(View.GONE);
		// show and enable submit button
		mSubmitBtn.setVisibility(View.VISIBLE);
	}

	private void initView() {
		// activity title
		mTitle = (TextView) findViewById(R.id.common_title_text_title);
		mTitle.setText(R.string.nfc_card_add_title);
		// NFC card id
		mNfcCardLabelView = (TextView) findViewById(R.id.nfc_card_add_label_card_id);
		mNfcCardIdView = (TextView) findViewById(R.id.nfc_card_add_text_card_id);
		// search NFC card animation
		mSearchAnimationView = (ImageView) findViewById(R.id.nfc_card_add_anim_search);
		mSearchAnimationDrawable = (AnimationDrawable) mSearchAnimationView.getDrawable();
		mSearchAnimationDrawable.start();
		// submit button
		mSubmitBtn = findViewById(R.id.nfc_card_add_btn_submit);
		mSubmitBtn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				final String nfcId = mNfcCardIdView.getText().toString();
				showLoadingPage();
				new Thread() {
					public void run() {
						ServerApiClient.addNfcCard(mHandler, BaseActivity.INVOKE_SUCCESS, nfcId);
					}
				}.start();
			}
		});
		// back button
		mBackBtn = findViewById(R.id.common_title_btn_back);
		mBackBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
	}

	private void prepareNfc() {
		// 获得NfcAdapter，以检测是否支持NFC
		mAdapter = NfcAdapter.getDefaultAdapter(this);
		if (mAdapter == null) {
			UiUtil.showToastMessage(getApplicationContext(), R.string.msg_nfc_not_support);
			finish();
			return;
		}
		// 检查NFC是否被打开
		if (!mAdapter.isEnabled()) {
			UiUtil.showToastMessage(getApplicationContext(), R.string.msg_nfc_not_enabled);
			finish();
			return;
		}
		// 创建techList和intentFilters
		intentFilter = new IntentFilter[] { new IntentFilter(NfcAdapter.ACTION_TAG_DISCOVERED), };
		// 创建PendingIntent对象, 这样Android系统就能在一个tag被检测到时定位到这个对象
		pendingIntent = PendingIntent.getActivity(this, 0, new Intent(this, getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
		Log.d(TAG, "prepareNfc() success");
	}
}
