package com.quasion.MasterManager.ui;

import java.util.Calendar;
import java.util.List;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.TextView;

import com.quasion.MasterManager.R;
import com.quasion.MasterManager.bean.Sales;
import com.quasion.MasterManager.data.SalesAdapter;
import com.quasion.MasterManager.server.ServerApiClient;
import com.quasion.MasterManager.util.DateUtil;
import com.quasion.MasterManager.util.StringUtil;
import com.quasion.MasterManager.util.UiUtil;

public class SalesListActivity extends BaseActivity implements DatePickerDialog.OnDateSetListener {
	private static final int GET_SALES_DATA = 100;
	private static final int SET_SALES_DATA = 101;
	private static final int REQUEST_ADD_SALES = 100;

	private TextView mTitle;
	private View mBackBtn;
	private View mAddBtn;
	private View mSalesDateBtn;
	private View mSubmitBtn;
	private TextView mSalesDateText;
	private ListView mSalesList;
	private SalesAdapter mSalesAdapter;
	private Calendar mSalesDate;

	private Handler mHandler = new Handler() {
		public void handleMessage(Message msg) {
			hideLoadingPage();
			switch (msg.what) {
			case GET_SALES_DATA:
				mSalesAdapter = new SalesAdapter(SalesListActivity.this, (List<Sales>) msg.obj);
				mSalesList.setAdapter(mSalesAdapter);
				break;
			case SET_SALES_DATA:
				UiUtil.showToastMessage(getApplication(), R.string.sales_list_toast_submit_success);
				break;
			case BaseActivity.INVOKE_FAIL:
				onInvokeFailed((String) msg.obj);
				break;
			case BaseActivity.SESSION_EXPIRE:
				onSessionExpired((String) msg.obj);
				break;
			}
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.sales_list);
		initView();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == REQUEST_ADD_SALES && resultCode == RESULT_OK) {
			// 执行增加销售量界面后，成功返回
			Sales newSales = (Sales) data.getSerializableExtra(SalesAddActivity.EXTRA_SALES_DATA);
			String brand = newSales.getBrand();
			String model = newSales.getModel();
			// 查找原有销售数据中匹配的记录
			List<Sales> salesList = mSalesAdapter.getItems();
			for (int i = 0; i < salesList.size(); i++) {
				Sales sales = salesList.get(i);
				if (sales.getBrand().equals(brand) && sales.getModel().equals(model)) {
					sales.setSales(newSales.getSales());
					mSalesAdapter.notifyDataSetChanged();
					return;
				}
			}
			// 新增加的销售数据
			salesList.add(newSales);
			mSalesAdapter.notifyDataSetChanged();
		}
	}

	private void initView() {
		// activity title
		mTitle = (TextView) findViewById(R.id.common_title_text_title);
		mTitle.setText(R.string.sales_list_title);
		// sales date text and change button
		mSalesDateText = (TextView) findViewById(R.id.sales_list_text_date);
		mSalesDateBtn = findViewById(R.id.sales_list_btn_date);
		mSalesDateBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				DatePickerDialog dlg = new DatePickerDialog(SalesListActivity.this, SalesListActivity.this, 
						mSalesDate.get(Calendar.YEAR), mSalesDate.get(Calendar.MONTH), mSalesDate.get(Calendar.DAY_OF_MONTH));
				dlg.getDatePicker().setMaxDate(System.currentTimeMillis());
				dlg.show();
			}
		});
		// sales data list view
		mSalesList = (ListView) findViewById(R.id.sales_list_list_data);
		// add button
		mAddBtn = findViewById(R.id.common_title_btn_add);
		mAddBtn.setVisibility(View.VISIBLE);
		mAddBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (mSalesAdapter == null) return;
				invokeSalesAddActivity(new Sales());
			}
		});
		// submit button
		mSubmitBtn = findViewById(R.id.sales_list_btn_submit);
		mSubmitBtn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if (mSalesAdapter == null) return;
				// submit data to server
				if (mSalesAdapter.getCount() > 0) {
					showLoadingPage();
					new Thread() {
						public void run() {
							ServerApiClient.setSalesData(mHandler, SET_SALES_DATA, mSalesDate.getTimeInMillis(), mSalesAdapter.getItems());
						}
					}.start();
				} else {
					finish();
				}
			}
		});
		// back button
		mBackBtn = findViewById(R.id.common_title_btn_back);
		mBackBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
		// request sales data fro current day
		mSalesDate = Calendar.getInstance();
		onDateSet(null, mSalesDate.get(Calendar.YEAR), mSalesDate.get(Calendar.MONTH), mSalesDate.get(Calendar.DAY_OF_MONTH));
	}

	@Override
	public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
		// set new sales date
		mSalesDate.set(year, monthOfYear, dayOfMonth, 0, 0, 0);
		mSalesDateText.setText(DateUtil.formatTime(mSalesDate.getTimeInMillis(), "yyyy/MM/dd"));
		// request sales data from server();
		showLoadingPage();
		new Thread() {
			public void run() {
				ServerApiClient.getSalesData(mHandler, GET_SALES_DATA, mSalesDate.getTimeInMillis(), 0, ServerApiClient.PAGE_DEFAULT_SIZE);
			}
		}.start();
	}

	public void invokeSalesAddActivity(Sales sales) {
		Intent intent = new Intent(getApplicationContext(), SalesAddActivity.class);
		intent.putExtra(SalesAddActivity.EXTRA_SALES_DATA, sales);
		startActivityForResult(intent, REQUEST_ADD_SALES);
	}
}
