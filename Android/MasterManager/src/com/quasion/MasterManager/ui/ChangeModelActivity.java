package com.quasion.MasterManager.ui;

import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.MediaPlayer;
import android.nfc.FormatException;
import android.nfc.NdefMessage;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.nfc.tech.NfcA;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.util.Log;
import android.view.View;
import android.widget.*;

import com.quasion.MasterManager.R;
import com.quasion.MasterManager.app.AppContext;
import com.quasion.MasterManager.bean.Product;
import com.quasion.MasterManager.nfc.*;
import com.quasion.MasterManager.server.ServerApiClient;
import com.quasion.MasterManager.util.StringUtil;
import com.quasion.MasterManager.util.UiUtil;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 *
 * 修改手机品牌和型号的界面
 *
 * @author philip zhou
 *
 *
 */
public class ChangeModelActivity extends BaseActivity {
	private static final String TAG = ChangeModelActivity.class.getSimpleName();

	private static final String EMPTY = "None";
	public static final String EXTRA_NFC_TAG_ID = "nfcTagId";
	public static final String EXTRA_PHONE_BRAND = "brand";
	public static final String EXTRA_PHONE_MODEL = "model";

	private TextView mTitle;
	private View mBackBtn;
	private View mSaveBtn;
	private Spinner mBrandSpinner, mModelSpinner;
	private ArrayAdapter<String> mBrandAdapter, mModelAdapter;

	private NfcAdapter mAdapter;
	private String[][] mTechList;
	private IntentFilter[] mIntentFilter;
	private PendingIntent mPendingIntent;
	private byte[] mNfcTagId;
	private String mPhoneBrand, mPhoneModel;
	private NfcSetPhoneModel mNfcSetModel = null;
	private boolean mNfcSetSend = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mNfcTagId = getIntent().getByteArrayExtra(EXTRA_NFC_TAG_ID);
		mPhoneBrand = getIntent().getStringExtra(EXTRA_PHONE_BRAND);
		mPhoneModel = getIntent().getStringExtra(EXTRA_PHONE_MODEL);
		setContentView(R.layout.change_model);
		initView();
		prepareNfc();
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (mAdapter != null) mAdapter.enableForegroundDispatch(this, mPendingIntent, mIntentFilter, mTechList);
	}

	@Override
	protected void onPause() {
		super.onPause();
		if (mAdapter != null) mAdapter.disableForegroundDispatch(this);
	}

	@Override
	protected void onNewIntent(Intent intent) {
		// 当检测到NFC Based时，调用onNewIntent()
		Tag nfcTag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
		Parcelable[] nfcMessages = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
		if (nfcTag == null) return;
		Ndef ndef = Ndef.get(nfcTag);
		Log.d(TAG, "Found NFC device, Action=" + intent.getAction() + ", Tag=" + StringUtil.toHex(nfcTag.getId()) + "," + nfcTag.toString());
		Log.d(TAG, "Ndef=" + ndef.getType() + ", MaxSize=" + ndef.getMaxSize() + ", isWritable=" + ndef.isWritable());
		if (!ndef.isWritable() || nfcMessages == null) return;
		receiveNfcMessage(nfcTag, (Parcelable[]) nfcMessages);
	}

	private void initView() {
		// activity title
		mTitle = (TextView) findViewById(R.id.common_title_text_title);
		mTitle.setText(R.string.change_model_title);
		// brand and model edit
		mBrandAdapter = new ArrayAdapter<String>(this, R.layout.common_spinner_text);
		mBrandAdapter.setDropDownViewResource(R.layout.common_spinner_dropdown);
		mBrandSpinner = (Spinner) findViewById(R.id.change_model_brand_id);
		mBrandSpinner.setAdapter(mBrandAdapter);
		mModelAdapter = new ArrayAdapter<String>(this, R.layout.common_spinner_text);
		mModelAdapter.setDropDownViewResource(R.layout.common_spinner_dropdown);
		mModelSpinner = (Spinner) findViewById(R.id.change_model_model_id);
		mModelSpinner.setAdapter(mModelAdapter);
		// save button
		mSaveBtn = findViewById(R.id.change_model_btn_save);
		mSaveBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				String brand = mBrandSpinner.getSelectedItem().toString().trim();
				String model = mModelSpinner.getSelectedItem().toString().trim();
				if (StringUtil.isEmpty(brand) || EMPTY.equals(brand) || brand.indexOf(' ') >= 0) {
					UiUtil.showToastMessage(getApplicationContext(), R.string.change_model_toast_empty_brand);
				} else if (StringUtil.isEmpty(model) || EMPTY.equals(model)) {
					UiUtil.showToastMessage(getApplicationContext(), R.string.change_model_toast_empty_model);
				} else if (brand.length() + model.length() > NfcSetPhoneModel.PHONE_MODEL_LENGTH - 1) {
					UiUtil.showToastMessage(getApplicationContext(), getString(R.string.change_model_toast_invalid_length, NfcSetPhoneModel.PHONE_MODEL_LENGTH - 1));
				} else if (brand.equals(mPhoneBrand) && model.equals(mPhoneModel)) {
					mBackBtn.performClick();
				} else {
					startWriteNfcBase(brand, model);
				}
			}
		});
		// back button
		mBackBtn = findViewById(R.id.common_title_btn_back);
		mBackBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				setResult(RESULT_CANCELED);
				finish();
			}
		});
		// init product
		initData();
	}

	private void initData() {
		// make sure product list has been fetch from server
		if (AppContext.productList == null) {
			finish();
			return;
		}
		// clear brand and model adapter
		mBrandAdapter.clear();
		mBrandAdapter.add(EMPTY);
		mBrandSpinner.setSelection(0);
		mBrandAdapter.notifyDataSetChanged();
		mModelAdapter.clear();
		mModelAdapter.add(EMPTY);
		mModelSpinner.setSelection(0);
		mModelAdapter.notifyDataSetChanged();
		// set data of brand adapter and model adapter
		for (int i = 0; i < AppContext.productList.size(); i++) {
			// add brand data
			String brand = AppContext.productList.get(i).getBrand();
			mBrandAdapter.add(brand);
			// if brand is selected?
			if (brand != null && brand.equals(mPhoneBrand)) {
				mBrandSpinner.setSelection(i + 1, false);
				// add model data
				List<String> models = AppContext.productList.get(i).getModel();
				mModelAdapter.addAll(models);
				// if model is selected?
				for (int j = 0; j < models.size(); j++) {
					if (models.get(j).equals(mPhoneModel)) {
						mModelSpinner.setSelection(j + 1, false);
						break;
					}
				}
			}
		}
		// setup OnItemSelectedListener
		mBrandSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				mModelAdapter.clear();
				mModelAdapter.add(EMPTY);
				mModelSpinner.setSelection(0);
				if (position > 0) mModelAdapter.addAll(AppContext.productList.get(position - 1).getModel());
				mModelAdapter.notifyDataSetChanged();
			}
			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				onItemSelected(parent, null, -1, -1);
			}
		});
	}

	private void prepareNfc() {
		// 获得NfcAdapter，以检测是否支持NFC
		mAdapter = NfcAdapter.getDefaultAdapter(this);
		if (mAdapter == null) {
			UiUtil.showToastMessage(getApplicationContext(), R.string.msg_nfc_not_support);
			finish();
			return;
		}
		// 检查NFC是否被打开
		if (!mAdapter.isEnabled()) {
			UiUtil.showToastMessage(getApplicationContext(), R.string.msg_nfc_not_enabled);
			finish();
			return;
		}
		// 创建techList和intentFilters
		mTechList = new String[][]{new String[]{NfcA.class.getName(), Ndef.class.getName()}};
		mIntentFilter = new IntentFilter[]{new IntentFilter(NfcAdapter.ACTION_TECH_DISCOVERED)};
		// 创建PendingIntent对象, 这样Android系统就能在一个tag被检测到时定位到这个对象
		mPendingIntent = PendingIntent.getActivity(this, 0, new Intent(this, this.getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
		Log.d(TAG, "prepareNfc() success");
	}

	private boolean sendNfcMessage(Tag nfcTag, NdefMessage nfcMessage) {
		// 获取NDEF对象
		Ndef ndef = Ndef.get(nfcTag);
		if (ndef == null) return false;
		// 连接NDEF设备
		try {
			ndef.close();
		} catch (IOException e) {
		}
		try {
			ndef.connect();
		} catch (IOException e) {
			Log.w(TAG, "sendNfcMessage() failed for connect!");
			e.printStackTrace();
			return false;
		}
		// 发送NDEF数据
		try {
			ndef.writeNdefMessage(nfcMessage);
			Log.d(TAG, "sendNfcMessage() success.");
			return true;
		} catch (IOException e) {
			Log.w(TAG, "sendNfcMessage() with IOException!");
			// 发送数据到NFC Base时，有时成功发送也会产生IOException
			return true;
		}
		catch (FormatException e) {
			Log.w(TAG, "sendNfcMessage() failed for FormatException!");
			e.printStackTrace();
			return false;
		}
		finally {
			try {
				if (ndef != null && ndef.isConnected()) ndef.close();
			}
			catch (IOException e) {
			}
		}
	}

	private void receiveNfcMessage(Tag nfcTag, Parcelable[] nfcMessages) {
		// 解析接收到的NFC数据
		if (nfcMessages == null || nfcMessages.length == 0) return;
		NdefMessage[] ndefMessages = new NdefMessage[nfcMessages.length];
		for (int i = 0; i < nfcMessages.length; i++) ndefMessages[i] = (NdefMessage) nfcMessages[i];
		List<NfcBaseMessage> baseMessages = NfcBaseMessage.fromNdefMessages(ndefMessages);
		// 如果解析成功，则表示连接的是NFC Base设备
		Log.d(TAG, "receiveNfcMessage()=" + baseMessages);
		if (baseMessages == null || baseMessages.size() == 0) return;
		// 判断是否启动改写NFC Base设备？
		if (!timerActive()) return;
		resetTimer();
		// 判断是否为需要改写的NFC Base设备？
		if (!Arrays.equals(nfcTag.getId(), mNfcTagId)) {
			stopWriteNfcBase(false);
			UiUtil.showToastMessage(getApplication(), R.string.change_model_toast_wrong_tag);
			return;
		}
		// 发送设置手机型号的命令到NFC Base
		if (!mNfcSetSend) {
			NdefMessage ndefMessage = NfcBaseMessage.toNdefMessages(mNfcSetModel, NfcBaseMessage.MSG_GET_BASE_ID, NfcBaseMessage.MSG_GET_MODEL);
			if (sendNfcMessage(nfcTag, ndefMessage)) mNfcSetSend = true;
			return;
		}
		// 处理接收到的NFC Base数据
		for (NfcBaseMessage baseMessage : baseMessages) {
			if (baseMessage instanceof NfcGetPhoneModel) {
				// 验证是否写入成功
				String brand = ((NfcGetPhoneModel) baseMessage).phoneBrand;
				String model = ((NfcGetPhoneModel) baseMessage).phoneModel;
				if (mNfcSetModel.phoneBrand.equals(brand) && mNfcSetModel.phoneModel.equals(model)) {
					stopWriteNfcBase(true);
					UiUtil.showToastMessage(getApplication(), R.string.change_model_toast_write_success);
					Intent intent = new Intent();
					intent.putExtra(EXTRA_PHONE_BRAND, brand);
					intent.putExtra(EXTRA_PHONE_MODEL, model);
					setResult(RESULT_OK, intent);
					finish();
				}
				else {
					stopWriteNfcBase(false);
					UiUtil.showToastMessage(getApplication(), R.string.change_model_toast_write_failed);
				}
				return;
			}
		}
	}

	private void startWriteNfcBase(String brand, String model) {
		mNfcSetSend = false;
		mNfcSetModel = new NfcSetPhoneModel(brand, model);
		if (!mNfcSetModel.isValid()) return;
		showLoadingPage(getString(R.string.change_model_toast_writing_base));
		timerStart();
	}

	private void stopWriteNfcBase(boolean success) {
		mNfcSetSend = false;
		mNfcSetModel = null;
		playSound(success ? R.raw.success : R.raw.failed);
		hideLoadingPage();
		timerStop();
	}

	private static final long NFC_CHECK_DELAY = 1000;
	private static final long NFC_CHECK_TIMEOUT = 6000;
	private boolean mTimerActive = false;
	private long mTimerLastTime;
	private final Runnable mCheckNfcConnection = new Runnable() {
		@Override
		public void run() {
			if (System.currentTimeMillis() - mTimerLastTime >= NFC_CHECK_TIMEOUT) {
				stopWriteNfcBase(false);
				UiUtil.showToastMessage(getApplication(), R.string.change_model_toast_write_timeout);
			} else {
				getWindow().getDecorView().postDelayed(this, NFC_CHECK_DELAY);
			}
		}
	};

	private void timerStart() {
		timerStop();
		mTimerActive = true;
		mTimerLastTime = System.currentTimeMillis();
		getWindow().getDecorView().postDelayed(mCheckNfcConnection, NFC_CHECK_DELAY);
	}

	private void timerStop() {
		mTimerActive = false;
		getWindow().getDecorView().removeCallbacks(mCheckNfcConnection);
	}

	private void resetTimer() {
		mTimerLastTime = System.currentTimeMillis();
	}

	private boolean timerActive() {
		return mTimerActive;
	}

	private void playSound(int resId) {
		MediaPlayer mediaPlayer = MediaPlayer.create(this, resId);
		mediaPlayer.setVolume(1.0f, 1.0f);
		mediaPlayer.start();
	}
}