package com.quasion.MasterManager.ui;

import android.app.AlertDialog;
import android.app.DownloadManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.quasion.MasterManager.R;
import com.quasion.MasterManager.app.AppContext;
import com.quasion.MasterManager.bean.Update;
import com.quasion.MasterManager.server.ServerApiClient;
import com.quasion.MasterManager.util.StringUtil;

import java.io.File;

public class SettingsActivity extends BaseActivity {
	private static final int CHECK_UPDATE = 100;
	private static final int LOG_OUT = 101;

	private TextView mTitle;
	private View mBackBtn;
	private View mQuitBtn;
	private RelativeLayout mSecurityLayout;
	private RelativeLayout mFeedbackLayout;
	private RelativeLayout mChangePasswordLayout;
	private RelativeLayout mAboutQuasionLayout;
	private RelativeLayout mCheckUpdateLayout;

	private Handler mHandler = new Handler() {
		public void handleMessage(Message msg) {
			hideLoadingPage();
			switch (msg.what) {
			case CHECK_UPDATE:
				Update update = (Update) msg.obj;
				if (update != null && !StringUtil.isEmpty(update.getClientFile())) {
					alertUpdate(update);
				}
				break;
			case LOG_OUT:
				// logout success, return to login activity
				onSessionExpired(null);
				break;
			case BaseActivity.INVOKE_FAIL:
				onInvokeFailed((String) msg.obj);
				break;
			case BaseActivity.SESSION_EXPIRE:
				onSessionExpired((String) msg.obj);
				break;
			}
		};
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.settings);
		initView();
	}

	private void initView() {
		// activity title
		mTitle = (TextView) findViewById(R.id.common_title_text_title);
		mTitle.setText(R.string.settings_title);
		// security question button
		mSecurityLayout = (RelativeLayout) findViewById(R.id.security_layout_id);
		mSecurityLayout.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getApplicationContext(), ChangeQuestionActivity.class);
				startActivity(intent);
			}
		});
		// change password button
		mChangePasswordLayout = (RelativeLayout) findViewById(R.id.password_layout_id);
		mChangePasswordLayout.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getApplicationContext(), ChangePasswordActivity.class);
				startActivity(intent);
			}
		});
		// feedback button
		mFeedbackLayout = (RelativeLayout) findViewById(R.id.feedback_layout_id);
		mFeedbackLayout.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getApplicationContext(), FeedbackActivity.class);
				startActivity(intent);
			}
		});
		// about quasion button
		mAboutQuasionLayout = (RelativeLayout) findViewById(R.id.about_layout_id);
		mAboutQuasionLayout.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getApplicationContext(), AboutQuasionActivity.class);
				startActivity(intent);
			}
		});
		// check update button
		mCheckUpdateLayout =  (RelativeLayout) findViewById(R.id.update_layout_id);
		mCheckUpdateLayout.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				showLoadingPage();
				new Thread() {
					public void run() {
						PackageInfo packageInfo = AppContext.getAppContext().getPackageInfo();
						ServerApiClient.checkUpdate(mHandler, CHECK_UPDATE, packageInfo.versionCode);
					}
				}.start();
			}
		});
		// logout button
		mQuitBtn = findViewById(R.id.quit_btn_id);
		mQuitBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				showLoadingPage();
				new Thread() {
					public void run() {
						ServerApiClient.logout(mHandler, LOG_OUT);
					}
				}.start();
			}
		});
		// back button
		mBackBtn = findViewById(R.id.common_title_btn_back);
		mBackBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				onMenuActivity();
			}
		});
	}

	private void alertUpdate(final Update updateInfo) {
		// 提示用户是否更新
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(R.string.settings_update_alert_title).setMessage(updateInfo.getRecentChange()).setCancelable(true);
		builder.setPositiveButton(R.string.settings_update_alert_btn_ok, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				startUpdate(updateInfo.getClientFile());
			}
		});
		builder.setNegativeButton(R.string.settings_update_alert_btn_cancel, null);
		AlertDialog dialog = builder.create();
		dialog.setCanceledOnTouchOutside(false);
		dialog.show();
	}

	private void startUpdate(String downloadUrl) {
		// start download
		DownloadManager downloadManager = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
		DownloadManager.Request request = new DownloadManager.Request(Uri.parse(downloadUrl));
		request.setDestinationInExternalPublicDir("quasion", "MasterManager.apk");
		request.setTitle(getString(R.string.settings_update_download_title));
		request.setNotificationVisibility(request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
		request.setVisibleInDownloadsUi(true);
		downloadManager.enqueue(request);
	}
}
