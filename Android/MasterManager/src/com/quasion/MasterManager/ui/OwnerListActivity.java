package com.quasion.MasterManager.ui;

import java.util.ArrayList;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.*;

import com.quasion.MasterManager.R;
import com.quasion.MasterManager.util.StringUtil;

public class OwnerListActivity extends BaseActivity {
	public static final String EXTRA_OWNER_LIST = "ownerList";

	private static final int REQUEST_ADD_OWNER = 100;

	private TextView mTitle;
	private View mBackBtn;
	private View mAddBtn;
	private ListView mOwnerList;
	private ArrayAdapter<String> mOwnerAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		ArrayList<String> owners = getIntent().getStringArrayListExtra(EXTRA_OWNER_LIST);
		setContentView(R.layout.owner_list);
		initView(owners);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == REQUEST_ADD_OWNER && resultCode == RESULT_OK) {
			// 执行增加领用人界面后，成功返回
			String owner = data.getStringExtra(OwnerAddActivity.EXTRA_OWNER_NAME);
			if (!StringUtil.isEmpty(owner)) {
				mOwnerAdapter.add(owner);
			}
		}
	}

	private void initView(ArrayList<String> owners) {
		// activity title
		mTitle = (TextView) findViewById(R.id.common_title_text_title);
		mTitle.setText(R.string.owner_list_title);
		// owner list
		mOwnerAdapter = new ArrayAdapter<String>(this, R.layout.owner_list_item, owners);
		mOwnerList = (ListView) findViewById(R.id.owner_list_id);
		mOwnerList.setAdapter(mOwnerAdapter);
		// add owner button
		mAddBtn = findViewById(R.id.common_title_btn_add);
		mAddBtn.setVisibility(View.VISIBLE);
		mAddBtn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getApplicationContext(), OwnerAddActivity.class);
				startActivityForResult(intent, REQUEST_ADD_OWNER);
			}
		});
		// back button
		mBackBtn = findViewById(R.id.common_title_btn_back);
		mBackBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
	}
}
