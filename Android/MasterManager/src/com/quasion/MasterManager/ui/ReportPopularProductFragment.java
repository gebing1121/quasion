package com.quasion.MasterManager.ui;

import java.util.List;

import android.app.Fragment;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import android.widget.AdapterView.OnItemSelectedListener;

import com.quasion.MasterManager.R;
import com.quasion.MasterManager.bean.ReportAvgPickupTime;
import com.quasion.MasterManager.bean.ReportPickupRate;
import com.quasion.MasterManager.bean.SalesReport;
import com.quasion.MasterManager.server.ServerApiClient;
import com.quasion.MasterManager.util.DateUtil;
import com.quasion.MasterManager.util.UiUtil;
import org.achartengine.GraphicalView;
import org.achartengine.chart.BarChart;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

/**
 * Most Popluar Product
 *
 * @author philip zhou
 *
 */
public class ReportPopularProductFragment extends Fragment {
	private static final String TAG = ReportPopularProductFragment.class.getSimpleName();

	// handler code
	private static final int GET_PICKUP_RATE = 100;
	private static final int GET_AVERAGE_PICKUP_TIME = 101;
	private static final int GET_SALES_VOLUME = 102;
	// date range code
	public static final int RANGE_SEVEN_DAY = 0;
	public static final int RANGE_THIRTY_DAY = 1;
	public static final int RANGE_TODAY = 2;
	public static final int RANGE_OTHER = 3;

	private TextView mDateRangeText;
	private Spinner mDateRangeSpinner;
	private TextView mLegendText;
	private RadioGroup mButtonsGroup;
	private ViewGroup mReportLayout;
	private BarChart mChart;

	private Handler mHandler = new Handler() {
		public void handleMessage(Message msg) {
			((BaseActivity) getActivity()).hideLoadingPage();
			switch (msg.what) {
			case GET_PICKUP_RATE:
				showReportPickupRate((List<ReportPickupRate>) msg.obj);
				break;
			case GET_AVERAGE_PICKUP_TIME:
				showReportAvgPickupTime((List<ReportAvgPickupTime>) msg.obj);
				break;
			case GET_SALES_VOLUME:
				showReportSaleVolume((List<SalesReport>) msg.obj);
				break;
			case BaseActivity.INVOKE_FAIL:
				((BaseActivity) getActivity()).onInvokeFailed((String) msg.obj);
				break;
			case BaseActivity.SESSION_EXPIRE:
				((BaseActivity) getActivity()).onSessionExpired((String) msg.obj);
			}
		}
	};

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.report_popular_product, null);
		initView(view);
		return view;
	}

	private void initView(View view) {
		// data range selection
		mDateRangeText = (TextView) view.findViewById(R.id.report_text_date_range);
		mDateRangeSpinner = (Spinner) view.findViewById(R.id.report_spinner_date_range);
		String[] dateRanges = getResources().getStringArray(R.array.report_array_date_range);
		ArrayAdapter adapter = new ArrayAdapter(getActivity(), R.layout.report_spinner_date_range, dateRanges);
		adapter.setDropDownViewResource(R.layout.common_spinner_dropdown);
		mDateRangeSpinner.setAdapter(adapter);
		mDateRangeSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
				long today = System.currentTimeMillis();
				switch (position) {
				case RANGE_SEVEN_DAY:
					long time1 = today - 7l * 24l * 60l * 60l * 1000l;
					mDateRangeText.setText(DateUtil.formatTime(time1, "yyyy/MM/dd") + "~" + DateUtil.formatTime(today, "MM/dd"));
					break;
				case RANGE_THIRTY_DAY:
					long time2 = today - 30l * 24l * 60l * 60l * 1000l;
					mDateRangeText.setText(DateUtil.formatTime(time2, "yyyy/MM/dd") + "~" + DateUtil.formatTime(today, "MM/dd"));
					break;
				case RANGE_TODAY:
					mDateRangeText.setText(DateUtil.formatTime(today, "yyyy/MM/dd"));
					break;
				case RANGE_OTHER:
					mDateRangeText.setText((String) adapterView.getSelectedItem());
					break;
				}
				requestReportFromServer(mButtonsGroup.getCheckedRadioButtonId(), position);
			}
			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
			}
		});
		// report layout
		mLegendText = (TextView) view.findViewById(R.id.report_text_legend);
		mReportLayout = (ViewGroup) view.findViewById(R.id.report_layout_chart);
		mChart = getBarChart();
		GraphicalView chartView = new GraphicalView(getActivity(), mChart);
		ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
		mReportLayout.addView(chartView, layoutParams);
		// report byte button on bottom
		mButtonsGroup = (RadioGroup) view.findViewById(R.id.report_layout_buttons);
		mButtonsGroup.check(R.id.report_btn_pickup_rate);
		mButtonsGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				if (mDateRangeSpinner.getSelectedItemPosition() == RANGE_SEVEN_DAY) {
					requestReportFromServer(checkedId, RANGE_SEVEN_DAY);
				} else {
					mDateRangeSpinner.setSelection(RANGE_SEVEN_DAY);
				}
			}
		});
	}

	private void requestReportFromServer(int reportId, int rangeId) {
		Log.i(TAG, "requestReportFromServer(reportId=" + reportId + ",rangeId=" + rangeId + ")");
		((BaseActivity) getActivity()).showLoadingPage();
		final int rangeType = rangeId + 1;
		switch(reportId) {
		case R.id.report_btn_pickup_rate:
			mLegendText.setText(R.string.report_text_popular_pickup_rate);
			((BaseActivity) getActivity()).showLoadingPage();
			new Thread() {
				public void run() {
					ServerApiClient.getReportPickupRates(mHandler, GET_PICKUP_RATE, rangeType);
				}
			}.start();
			break;
		case R.id.report_btn_avg_pickup_time:
			mLegendText.setText(R.string.report_text_popular_avg_pickup_time);
			((BaseActivity) getActivity()).showLoadingPage();
			new Thread() {
				public void run() {
					ServerApiClient.getReportPickupAvgTimes(mHandler, GET_AVERAGE_PICKUP_TIME, rangeType);
				}
			}.start();
			break;
		case R.id.report_btn_sales_volume:
			mLegendText.setText(R.string.report_text_popular_sales_volume);
			((BaseActivity) getActivity()).showLoadingPage();
			new Thread() {
				public void run() {
					ServerApiClient.getReportSalesVolume(mHandler, GET_SALES_VOLUME, rangeType);
				}
			}.start();
			break;
		default:
			((BaseActivity) getActivity()).hideLoadingPage();
			break;
		}
	}

	private void showReportPickupRate(List<ReportPickupRate> reportData) {
		if (reportData == null) return;
		int size = reportData.size();
		String[] labels = new String[size];
		double[] values = new double[size];
		for (int i = 0; i < size; i++) {
			labels[i] = reportData.get(i).toString();
			values[i] = reportData.get(i).getPickUpTimes();
		}
		setBarChart(mChart, labels, values);
		mReportLayout.getChildAt(0).invalidate();
	}

	private void showReportAvgPickupTime(List<ReportAvgPickupTime> reportData) {
		if (reportData == null) return;
		int size = reportData.size();
		String[] labels = new String[size];
		double[] values = new double[size];
		for (int i = 0; i < size; i++) {
			labels[i] = reportData.get(i).toString();
			values[i] = reportData.get(i).getAvgPickUpTime();
		}
		setBarChart(mChart, labels, values);
		mReportLayout.getChildAt(0).invalidate();
	}

	private void showReportSaleVolume(List<SalesReport> reportData) {
		if (reportData == null) return;
		int size = reportData.size();
		String[] labels = new String[size];
		double[] values = new double[size];
		for (int i = 0; i < size; i++) {
			labels[i] = reportData.get(i).toString();
			values[i] = reportData.get(i).getSales();
		}
		setBarChart(mChart, labels, values);
		mReportLayout.getChildAt(0).invalidate();
	}

	private final int    CHART_LABEL_MAX = 10;
	private final double CHART_SPACING_BAR = 0.5;
	private final double CHART_SPACING_VALUE = 0.1;
	private final float  CHART_MARGIN_VALUE = 5;
	private final int    CHART_MARGIN_TOP = 5;
	private final int    CHART_MARGIN_LEFT = 80;
	private final int    CHART_MARGIN_BOTTOM = 5;
	private final int    CHART_MARGIN_RIGHT = 5;

	private BarChart getBarChart() {
		// init data set
		XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
		XYSeries xySeries = new XYSeries(null);
		dataset.addSeries(xySeries);
		// init renderer
		XYMultipleSeriesRenderer renderer = new XYMultipleSeriesRenderer();
		renderer.setOrientation(XYMultipleSeriesRenderer.Orientation.VERTICAL);		// 横向BarChart
		renderer.setPanEnabled(true, true);				// 允许X/Y平移
		renderer.setZoomEnabled(false, true);				// 只允许数据左右移动
		renderer.setShowLegend(false);						// 不显示Legend
		renderer.setShowLabels(true);						// 显示ChartTitle和Labels
		renderer.setShowAxes(false);						// 不显示X/Y轴线
		renderer.setShowTickMarks(false);					// 不显示X/Y轴线上的刻度线
		renderer.setXLabels(-1);							// X轴显示数值个数为的-1，即不显示数值
		renderer.setYLabels(-1);							// Y轴显示数值个数为的-1，即不显示数值
		renderer.setBarSpacing(CHART_SPACING_BAR);		// Bar间隔比率（1.0：Bar连续）
		// set chart margins
		renderer.setMargins(new int[] { 
				UiUtil.dp2px(getActivity(), CHART_MARGIN_RIGHT),
				UiUtil.dp2px(getActivity(), CHART_MARGIN_TOP),
				UiUtil.dp2px(getActivity(), CHART_MARGIN_LEFT),
				UiUtil.dp2px(getActivity(), CHART_MARGIN_BOTTOM)
		});
		// set color and text size for labels
		renderer.setLabelsTextSize(mDateRangeText.getTextSize());
		renderer.setXLabelsColor(getResources().getColor(R.color.report_label_color));
		renderer.setXLabelsStyle(UiUtil.dp2px(getActivity(), CHART_MARGIN_LEFT), 0, Paint.Align.RIGHT, Paint.Align.CENTER, true);
		// set color and text size of value series
		XYSeriesRenderer seriesRenderer = new XYSeriesRenderer();
		seriesRenderer.setDisplayChartValues(true);
		seriesRenderer.setColor(getResources().getColor(R.color.report_bar_color));
		seriesRenderer.setChartValuesTextSize(mLegendText.getTextSize());
		seriesRenderer.setChartValuesSpacing(UiUtil.dp2px(getActivity(), CHART_MARGIN_VALUE));
		seriesRenderer.setChartValuesTextAlign(Paint.Align.LEFT);
		renderer.addSeriesRenderer(seriesRenderer);
		return new BarChart(dataset, renderer, BarChart.Type.DEFAULT);
	}

	private BarChart setBarChart(BarChart chart, String[] labels, double[] values) {
		XYMultipleSeriesRenderer renderer = chart.getRenderer();
		XYSeries xySeries = chart.getDataset().getSeriesAt(0);
		// set chart label and values
		renderer.clearXTextLabels();
		xySeries.clear();
		int cnt = labels.length;
		double valueMax = 0;
		for (int i = 0; i < cnt; i++) {
			renderer.addXTextLabel(i, labels[i]);
			xySeries.add(i, values[i]);
			if (values[i] > valueMax) valueMax = values[i];
		}
		// set chart x/y limits
		double[] limits = new double[]{ -CHART_SPACING_BAR, cnt - CHART_SPACING_BAR, 0, valueMax * (1 + CHART_SPACING_VALUE)};
		renderer.setPanLimits(limits);
		renderer.setXAxisMin(limits[0]);
		renderer.setXAxisMax(CHART_LABEL_MAX - CHART_SPACING_BAR);
		renderer.setYAxisMin(limits[2]);
		renderer.setYAxisMax(limits[3]);
		return chart;
	}
}
