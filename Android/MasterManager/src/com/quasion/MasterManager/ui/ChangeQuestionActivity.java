package com.quasion.MasterManager.ui;

import java.util.List;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.*;

import com.quasion.MasterManager.R;
import com.quasion.MasterManager.bean.SafeQuestion;
import com.quasion.MasterManager.server.ServerApiClient;
import com.quasion.MasterManager.util.StringUtil;
import com.quasion.MasterManager.util.UiUtil;

public class ChangeQuestionActivity extends BaseActivity {
	private final static int GET_QUESTIONS = 100;
	private final static int SET_QUESTIONS = 101;

	private TextView mTitle;
	private View mBackBtn;
	private View mSubmitBtn;
	private Spinner mQuestion1Spinner, mQuestion2Spinner, mQuestion3Spinner;
	private ArrayAdapter<SafeQuestion> mQuestion1Adapter, mQuestion2Adapter, mQuestion3Adapter;
	private EditText mAnswer1Edit, mAnswer2Edit, mAnswer3Edit;

	private String mQuestion1, mQuestion2, mQuestion3;

	private Handler mHandler = new Handler() {
		public void handleMessage(Message msg) {
			hideLoadingPage();
			switch (msg.what) {
			case GET_QUESTIONS:
				// get all security questions success from server
				List<SafeQuestion> safeQuestions = (List<SafeQuestion>) msg.obj;
				// at least 3 security questions
				if (safeQuestions.size() < 3) {
					UiUtil.showToastMessage(getApplicationContext(), R.string.first_login_toast_no_questions);
					break;
				}
				// set question spinners data
				initQuestions(safeQuestions);
				break;
			case SET_QUESTIONS:
				// set user's security questions success from server
				UiUtil.showToastMessage(getApplicationContext(), R.string.question_toast_change_success);
				finish();
				break;
			case BaseActivity.INVOKE_FAIL:
				onInvokeFailed((String) msg.obj);
				break;
			case BaseActivity.SESSION_EXPIRE:
				onSessionExpired((String) msg.obj);
				break;
			}
		};
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.change_question);
		initView();
	}

	private void initView() {
		// activity title
		mTitle = (TextView) findViewById(R.id.common_title_text_title);
		mTitle.setText(R.string.security_question_title);
		// set security question layout
		mAnswer1Edit = (EditText) findViewById(R.id.answer1_id);
		mAnswer2Edit = (EditText) findViewById(R.id.answer2_id);
		mAnswer3Edit = (EditText) findViewById(R.id.answer3_id);
		mQuestion1Adapter = new ArrayAdapter<SafeQuestion>(this, R.layout.common_spinner_text);
		mQuestion1Adapter.setDropDownViewResource(R.layout.common_spinner_dropdown);
		mQuestion1Spinner = (Spinner) findViewById(R.id.question1_id);
		mQuestion1Spinner.setAdapter(mQuestion1Adapter);
		mQuestion1Spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				mQuestion1 = mQuestion1Adapter.getItem(position).getId();
			}
			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				mQuestion1 = null;
			}
		});
		mQuestion2Adapter = new ArrayAdapter<SafeQuestion>(this, R.layout.common_spinner_text);
		mQuestion2Adapter.setDropDownViewResource(R.layout.common_spinner_dropdown);
		mQuestion2Spinner = (Spinner) findViewById(R.id.question2_id);
		mQuestion2Spinner.setAdapter(mQuestion2Adapter);
		mQuestion2Spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				mQuestion2 = mQuestion2Adapter.getItem(position).getId();
			}
			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				mQuestion2 = null;
			}
		});
		mQuestion3Adapter = new ArrayAdapter<SafeQuestion>(this, R.layout.common_spinner_text);
		mQuestion3Adapter.setDropDownViewResource(R.layout.common_spinner_dropdown);
		mQuestion3Spinner = (Spinner) findViewById(R.id.question3_id);
		mQuestion3Spinner.setAdapter(mQuestion3Adapter);
		mQuestion3Spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				mQuestion3 = mQuestion3Adapter.getItem(position).getId();
			}
			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				mQuestion3 = null;
			}
		});
		// submit button
		mSubmitBtn = findViewById(R.id.security_question_btn_submit);
		mSubmitBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				requestSetQuestion();
			}
		});
		// back button
		mBackBtn = findViewById(R.id.common_title_btn_back);
		mBackBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
		// request all question list from server
		requestAllQuestions();
	}

	private void initQuestions(List<SafeQuestion> questions) {
		// set question spinners data
		mQuestion1Adapter.clear();
		mQuestion2Adapter.clear();
		mQuestion3Adapter.clear();
		mQuestion1Adapter.addAll(questions);
		mQuestion2Adapter.addAll(questions);
		mQuestion3Adapter.addAll(questions);
		mQuestion1Adapter.notifyDataSetChanged();
		mQuestion2Adapter.notifyDataSetChanged();
		mQuestion3Adapter.notifyDataSetChanged();
		// set question spinners selection
		mQuestion1Spinner.setSelection(0);
		mQuestion2Spinner.setSelection(1);
		mQuestion3Spinner.setSelection(2);
		// enable submit button
		mSubmitBtn.setEnabled(true);
	}

	private void requestAllQuestions() {
		// disable submit button util get question list
		mSubmitBtn.setEnabled(false);
		// execute request
		showLoadingPage();
		new Thread() {
			public void run() {
				ServerApiClient.getAllSafeQuestionsByToken(mHandler, GET_QUESTIONS);
			}
		}.start();
	}

	private void requestSetQuestion() {
		final String answer1 = mAnswer1Edit.getText().toString().trim();
		final String answer2 = mAnswer2Edit.getText().toString().trim();
		final String answer3 = mAnswer3Edit.getText().toString().trim();
		if (StringUtil.isEmpty(mQuestion1) || StringUtil.isEmpty(mQuestion2) || StringUtil.isEmpty(mQuestion3)) {
			UiUtil.showToastMessage(getApplicationContext(), R.string.question_toast_empty_questions);
		} else if (mQuestion1.equals(mQuestion2) || mQuestion1.equals(mQuestion3) || mQuestion2.equals(mQuestion3)) {
			UiUtil.showToastMessage(getApplicationContext(), R.string.question_toast_same_questions);
		} else if (StringUtil.isEmpty(answer1) || StringUtil.isEmpty(answer2) || StringUtil.isEmpty(answer3)) {
			UiUtil.showToastMessage(getApplicationContext(), R.string.question_toast_empty_answers);
		} else {
			showLoadingPage();
			new Thread() {
				public void run() {
					ServerApiClient.setSafeQuestionsByToken(mHandler, SET_QUESTIONS,
							mQuestion1, answer1, mQuestion2, answer2, mQuestion3, answer3);
				}
			}.start();
		}
	}
}
