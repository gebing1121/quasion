package com.quasion.MasterManager.ui;

import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.*;

import com.quasion.MasterManager.R;
import com.quasion.MasterManager.app.AppContext;
import com.quasion.MasterManager.bean.Sales;
import com.quasion.MasterManager.util.StringUtil;
import com.quasion.MasterManager.util.UiUtil;

public class SalesAddActivity extends BaseActivity {
	public static final String EXTRA_SALES_DATA = "sales";
	private static final String EMPTY = "None";

	private TextView mTitle;
	private View mBackBtn;
	private View mSaveBtn;
	private Spinner mBrandSpinner, mModelSpinner;
	private ArrayAdapter<String> mBrandAdapter, mModelAdapter;
	private EditText mSalesEdit;
	private Sales mSalesData;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mSalesData = (Sales) getIntent().getSerializableExtra(EXTRA_SALES_DATA);
		setContentView(R.layout.sales_add);
		initView();
	}

	private void initView() {
		// activity title
		mTitle = (TextView) findViewById(R.id.common_title_text_title);
		mTitle.setText(R.string.sales_add_title);
		// brand and model edit
		mBrandAdapter = new ArrayAdapter<String>(this, R.layout.common_spinner_text);
		mBrandAdapter.setDropDownViewResource(R.layout.common_spinner_dropdown);
		mBrandSpinner = (Spinner) findViewById(R.id.sales_add_brand_id);
		mBrandSpinner.setAdapter(mBrandAdapter);
		mModelAdapter = new ArrayAdapter<String>(this, R.layout.common_spinner_text);
		mModelAdapter.setDropDownViewResource(R.layout.common_spinner_dropdown);
		mModelSpinner = (Spinner) findViewById(R.id.sales_add_model_id);
		mModelSpinner.setAdapter(mModelAdapter);
		mSalesEdit = (EditText) findViewById(R.id.sales_add_sales_id);
		// save button
		mSaveBtn = findViewById(R.id.sales_add_btn_save);
		mSaveBtn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				String brand = mBrandSpinner.getSelectedItem().toString().trim();
				String model = mModelSpinner.getSelectedItem().toString().trim();
				String sales = mSalesEdit.getText().toString().trim();
				if (StringUtil.isEmpty(brand) || EMPTY.equals(brand) || brand.indexOf(' ') >= 0) {
					UiUtil.showToastMessage(getApplicationContext(), R.string.sales_add_toast_empty_brand);
				} else if (StringUtil.isEmpty(model) || EMPTY.equals(model)) {
					UiUtil.showToastMessage(getApplicationContext(), R.string.sales_add_toast_empty_model);
				} else if (StringUtil.isEmpty(sales) || Integer.valueOf(sales) < 0) {
					UiUtil.showToastMessage(getApplicationContext(), R.string.sales_add_toast_empty_sales);
				} else {
					mSalesData.setBrand(brand);
					mSalesData.setModel(model);
					mSalesData.setSales(Integer.valueOf(sales));
					Intent intent = new Intent();
					intent.putExtra(EXTRA_SALES_DATA, mSalesData);
					setResult(RESULT_OK, intent);
					finish();
				}
			}
		});
		// back button
		mBackBtn = findViewById(R.id.common_title_btn_back);
		mBackBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				setResult(RESULT_CANCELED);
				finish();
			}
		});
		// init product
		initData();
	}

	private void initData() {
		// make sure product list has been fetch from server
		if (AppContext.productList == null) {
			finish();
			return;
		}
		// set data of sales
		mSalesEdit.setText(String.valueOf(mSalesData.getSales()));
		// clear brand and model adapter
		mBrandAdapter.clear();
		mBrandAdapter.add(EMPTY);
		mBrandSpinner.setSelection(0);
		mBrandSpinner.requestFocus();
		mBrandAdapter.notifyDataSetChanged();
		mModelAdapter.clear();
		mModelAdapter.add(EMPTY);
		mModelSpinner.setSelection(0);
		mModelAdapter.notifyDataSetChanged();
		// set data of brand adapter and model adapter
		for (int i = 0; i < AppContext.productList.size(); i++) {
			// add brand data
			String brand = AppContext.productList.get(i).getBrand();
			mBrandAdapter.add(brand);
			// if brand is selected?
			if (brand != null && brand.equals(mSalesData.getBrand())) {
				mBrandSpinner.setSelection(i + 1, false);
				// add model data
				List<String> models = AppContext.productList.get(i).getModel();
				mModelAdapter.addAll(models);
				// if model is selected?
				for (int j = 0; j < models.size(); j++) {
					if (models.get(j).equals(mSalesData.getModel())) {
						mModelSpinner.setSelection(j + 1, false);
						mSalesEdit.requestFocus();
						break;
					}
				}
			}
		}
		// setup OnItemSelectedListener
		mBrandSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				mModelAdapter.clear();
				mModelAdapter.add(EMPTY);
				mModelSpinner.setSelection(0);
				if (position > 0) mModelAdapter.addAll(AppContext.productList.get(position - 1).getModel());
				mModelAdapter.notifyDataSetChanged();
			}
			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				onItemSelected(parent, null, -1, -1);
			}
		});
	}
}
