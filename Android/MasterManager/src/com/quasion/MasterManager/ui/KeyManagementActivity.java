package com.quasion.MasterManager.ui;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.*;

import com.quasion.MasterManager.R;
import com.quasion.MasterManager.app.AppPreference;
import com.quasion.MasterManager.bean.NfcCard;
import com.quasion.MasterManager.data.CardAdapter;
import com.quasion.MasterManager.server.ServerApiClient;
import com.quasion.MasterManager.util.UiUtil;
import com.quasion.MasterManager.widget.ExpandableListAdapter;
import com.quasion.MasterManager.widget.ExpandableListView;

public class KeyManagementActivity extends BaseActivity {
	private static final String TAG = KeyManagementActivity.class.getSimpleName();

	private static final int GET_CARD_LIST = 100;
	private static final int DELETE_NFC_CARD = 101;

	private TextView mTitle;
	private View mBackBtn;
	private View mAddBtn;
	private View mSyncBtn;
	private ExpandableListView mKeyListView;
	private CardAdapter mCardAdapter;

	private Handler mHandler = new Handler() {
		public void handleMessage(Message msg) {
			hideLoadingPage();
			switch (msg.what) {
			case DELETE_NFC_CARD:
				// delete nfc card success from server
				UiUtil.showToastMessage(getApplicationContext(), R.string.key_management_toast_delete_success);
				requestGetNfcCardList();
				break;
			case GET_CARD_LIST:
				// get nfc card list success from server
				List<NfcCard> nfcCards = (List<NfcCard>) msg.obj;
				mCardAdapter = new CardAdapter(KeyManagementActivity.this, nfcCards);
				mKeyListView.setAdapter(mCardAdapter, R.id.card_list_item_btn_toggle, R.id.card_list_item_layout_expand);
				mKeyListView.setOnItemExpandCollapseListener(new ExpandableListAdapter.OnItemExpandCollapseListener() {
					@Override
					public void onExpand(View itemView, int position) {
						ImageButton toggleButton = (ImageButton) itemView.findViewById(R.id.card_list_item_btn_toggle);
						toggleButton.setImageResource(R.drawable.card_list_item_toggle_collapse);
					}
					@Override
					public void onCollapse(View itemView, int position) {
						ImageButton toggleButton = (ImageButton) itemView.findViewById(R.id.card_list_item_btn_toggle);
						toggleButton.setImageResource(R.drawable.card_list_item_toggle_expand);
					}
				});
				mSyncBtn.setEnabled(nfcCards.size() > 0 ? true : false);
				// save nfc card list to preference
				StringBuffer sbBuffer = new StringBuffer();
				for (NfcCard nfcCard : nfcCards) {
					sbBuffer.append(nfcCard.getKeyId());
				}
				AppPreference.saveNfcCards(sbBuffer.toString());
				Log.d(TAG, "NfcCardList=" + sbBuffer.toString());
				break;
			case BaseActivity.INVOKE_FAIL:
				onInvokeFailed((String) msg.obj);
				break;
			case BaseActivity.SESSION_EXPIRE:
				onSessionExpired((String) msg.obj);
				break;
			}
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.key_management);
		initView();
	}

	@Override
	protected void onResume() {
		super.onResume();
		requestGetNfcCardList();
	}

	private void initView() {
		// activity title
		mTitle = (TextView) findViewById(R.id.common_title_text_title);
		mTitle.setText(R.string.key_management_title);
		// NFC key list
		mKeyListView = (ExpandableListView) findViewById(R.id.key_management_card_list);
		// add NFC key button
		mAddBtn = findViewById(R.id.common_title_btn_add);
		mAddBtn.setVisibility(View.VISIBLE);
		mAddBtn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getApplicationContext(), NfcCardAddActivity.class);
				startActivity(intent);
			}
		});
		// NFC data synchronize button
		mSyncBtn = findViewById(R.id.key_management_btn_sync);
		mSyncBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getApplicationContext(), DataSynchronizationActivity.class);
				startActivity(intent);
				finish();
			}
		});
		// back button
		mBackBtn = findViewById(R.id.common_title_btn_back);
		mBackBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
	}

	private void requestGetNfcCardList() {
		if (mCardAdapter != null) {
			mCardAdapter.getItems().clear();
			mCardAdapter.notifyDataSetChanged();
		}
		showLoadingPage();
		new Thread() {
			public void run() {
				ServerApiClient.getNfcCardList(mHandler, GET_CARD_LIST, 0, ServerApiClient.PAGE_DEFAULT_SIZE);
			}
		}.start();
	}

	public void requestDeleteNfcCard(final String keyId) {
		showLoadingPage();
		new Thread() {
			public void run() {
				ServerApiClient.deleteNfcCard(mHandler, DELETE_NFC_CARD, keyId);
			}
		}.start();
	}

	public void requestCardAssignActivity(NfcCard nfcCard) {
		Intent intent = new Intent(this, NfcCardAssignActivity.class);
		intent.putExtra(NfcCardAssignActivity.EXTRA_NFC_CARD, nfcCard);
		startActivity(intent);
	}
}
