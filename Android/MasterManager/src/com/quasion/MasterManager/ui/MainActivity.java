package com.quasion.MasterManager.ui;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.TranslateAnimation;
import android.widget.*;

import com.crashlytics.android.Crashlytics;
import com.quasion.MasterManager.R;
import com.quasion.MasterManager.app.AppPreference;
import com.quasion.MasterManager.bean.User;
import com.quasion.MasterManager.server.ServerApiClient;
import com.quasion.MasterManager.util.StringUtil;
import com.quasion.MasterManager.util.UiUtil;

/**
 * loading page
 *
 * @author Philip Zhou
 *
 */
public class MainActivity extends BaseActivity {
	private static final String TAG = MainActivity.class.getSimpleName();

	private static final int LOGIN_SUCCESS = 1;
	private static final int LOGO_ANIMATION_START = 0;
	private static final int LOGO_ANIMATION_END = 1;
	private static final int LOGIN_ANIMATION_END = 2;

	private ImageView mLogoImageView;
	private Button mLoginBtn;
	private LinearLayout mLoginLayout;
	private Animation mTranslateAnimationToUp;
	private Animation mTranslateAnimationToLeft;
	private EditText mUsernameEdit;
	private EditText mPasswordEdit;
	private TextView mForgetPasswordBtn;

	private Handler mHandler = new Handler() {
		public void handleMessage(Message msg) {
			hideLoadingPage();
			if (msg.what == LOGIN_SUCCESS && msg.obj != null) {
				// 清空原先cookie
				ServerApiClient.cleanCookie();
				// 提示登陆成功
				UiUtil.showToastMessage(MainActivity.this, R.string.sign_in_toast_login_success);
				// 保存用户信息
				User user = (User) msg.obj;
				AppPreference.saveUserInfo(user);
				// 跳转到主菜单或设置安全问题界面
				Intent intent = new Intent(MainActivity.this, user.isFirstLogin() ? FirstLoginActivity.class : MenuActivity.class);
				intent.putExtra(FirstLoginActivity.INIT_PASSWORD,  mPasswordEdit.getText().toString().trim());
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intent);
				finish();
			} else {
				UiUtil.showToastMessage(MainActivity.this, String.valueOf(msg.obj));
			}
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Crashlytics.start(this);
		setContentView(R.layout.main);
		initView();
	}

	private void initView() {
		mLogoImageView = (ImageView) findViewById(R.id.logo_img_id);
		mLoginLayout = (LinearLayout) findViewById(R.id.login_layout_id);
		mUsernameEdit = (EditText) findViewById(R.id.username_id);
		String username = AppPreference.getUsername();
		if (!StringUtil.isEmpty(username)) {
			mUsernameEdit.setText(username.trim());
		}
		mPasswordEdit = (EditText) findViewById(R.id.password_id);
		mLoginBtn = (Button) findViewById(R.id.login_btn_id);
		mLoginBtn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				final String username = mUsernameEdit.getText().toString().trim();
				final String password = mPasswordEdit.getText().toString().trim();
				if (StringUtil.isEmpty(username)) {
					UiUtil.showToastMessage(getApplicationContext(), R.string.sign_in_toast_username_empty);
				} else if (StringUtil.isEmpty(password)) {
					UiUtil.showToastMessage(getApplicationContext(), R.string.sign_in_toast_password_empty);
				} else {
					showLoadingPage();
					new Thread() {
						public void run() {
							ServerApiClient.login(mHandler, LOGIN_SUCCESS, username, password);
						}
					}.start();
				}
			}
		});

		mForgetPasswordBtn = (TextView) findViewById(R.id.forget_password_link_id);
		mForgetPasswordBtn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View view) {
				Intent intent = new Intent(getApplicationContext(), ForgetPasswordActivity.class);
				startActivity(intent);
			}
		});

		viewAnimationToUp(mLogoImageView);
	}

	private int getMultipleByDpi() {
		DisplayMetrics dm = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(dm);
		return dm.densityDpi / 160;
	}

	private void viewAnimationToUp(View view) {
		if (mTranslateAnimationToUp != null) {
			mTranslateAnimationToUp = null;
		}
		mTranslateAnimationToUp = new TranslateAnimation(0, 0, 0, -150 * getMultipleByDpi());
		mTranslateAnimationToUp.setDuration(700);
		mTranslateAnimationToUp.setFillAfter(true);
		mTranslateAnimationToUp.setFillEnabled(true);
		view.setAnimation(mTranslateAnimationToUp);
		mTranslateAnimationToUp.setAnimationListener(new AnimationListener() {
			@Override
			public void onAnimationStart(Animation animation) {
				Message msg = new Message();
				msg.what = LOGO_ANIMATION_END;
				mAnimationHandler.sendMessage(msg);
			}

			@Override
			public void onAnimationRepeat(Animation animation) {
			}

			@Override
			public void onAnimationEnd(Animation animation) {
			}
		});

		mTranslateAnimationToUp.startNow();
	}

	private void viewAnimationToLeft(View view) {
		if (mTranslateAnimationToLeft != null) {
			mTranslateAnimationToUp = null;
		}
		mTranslateAnimationToLeft = new TranslateAnimation(720, 0, 0, 0);
		mTranslateAnimationToLeft.setDuration(1000);
		mTranslateAnimationToLeft.setFillAfter(true);
		mTranslateAnimationToLeft.setFillEnabled(true);
		view.setAnimation(mTranslateAnimationToLeft);
		mTranslateAnimationToLeft.setAnimationListener(new AnimationListener() {
			@Override
			public void onAnimationStart(Animation animation) {
			}

			@Override
			public void onAnimationRepeat(Animation animation) {
			}

			@Override
			public void onAnimationEnd(Animation animation) {
				Message msg = new Message();
				msg.what = LOGIN_ANIMATION_END;
				mAnimationHandler.sendMessage(msg);
			}
		});
		mTranslateAnimationToLeft.startNow();
	}

	private Handler mAnimationHandler = new Handler() {
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case LOGO_ANIMATION_START:
				viewAnimationToUp(mLogoImageView);
				break;
			case LOGO_ANIMATION_END:
				mLoginLayout.setVisibility(View.VISIBLE);
				viewAnimationToLeft(mLoginLayout);
				break;
			case LOGIN_ANIMATION_END:
				break;
			}
		}
	};

}
