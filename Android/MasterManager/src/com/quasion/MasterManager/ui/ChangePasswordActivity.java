package com.quasion.MasterManager.ui;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.*;

import com.quasion.MasterManager.R;
import com.quasion.MasterManager.server.ServerApiClient;
import com.quasion.MasterManager.util.StringUtil;
import com.quasion.MasterManager.util.UiUtil;

public class ChangePasswordActivity extends BaseActivity {
	private TextView mTitle;
	private View mBackBtn;
	private View mSubmitBtn;
	private EditText mOldPasswordEdit;
	private EditText mNewPasswordEdit;
	private EditText mConfirmPasswordEdit;

	private Handler mHandler = new Handler() {
		public void handleMessage(Message msg) {
			hideLoadingPage();
			switch (msg.what) {
			case BaseActivity.INVOKE_SUCCESS:
				// change password success
				UiUtil.showToastMessage(getApplicationContext(), R.string.password_toast_change_success);
				// return to login activity
				onSessionExpired(null);
				break;
			case BaseActivity.INVOKE_FAIL:
				onInvokeFailed((String) msg.obj);
				break;
			case BaseActivity.SESSION_EXPIRE:
				onSessionExpired((String) msg.obj);
				break;
			}
		};
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.change_password);
		initView();
	}

	private void initView() {
		// activity title
		mTitle = (TextView) findViewById(R.id.common_title_text_title);
		mTitle.setText(R.string.change_password_title);
		// password content
		mOldPasswordEdit = (EditText) findViewById(R.id.old_password_id);
		mNewPasswordEdit = (EditText) findViewById(R.id.new_password_id);
		mConfirmPasswordEdit = (EditText) findViewById(R.id.confirm_password_id);
		// submit button
		mSubmitBtn = findViewById(R.id.submit_btn_id);
		mSubmitBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				final String oldPassword = mOldPasswordEdit.getText().toString();
				final String newPassword = mNewPasswordEdit.getText().toString();
				final String confirmPassword = mConfirmPasswordEdit.getText().toString();
				if (StringUtil.isEmpty(oldPassword)) {
					UiUtil.showToastMessage(getApplicationContext(), R.string.password_toast_empty_old_pwd);
				} else if (StringUtil.isEmpty(newPassword)) {
					UiUtil.showToastMessage(getApplicationContext(), R.string.password_toast_empty_new_pwd);
				} else if (StringUtil.isEmpty(confirmPassword)) {
					UiUtil.showToastMessage(getApplicationContext(), R.string.password_toast_empty_confirm_pwd);
				} else if (!newPassword.equals(confirmPassword)) {
					UiUtil.showToastMessage(getApplicationContext(), R.string.password_toast_not_same_pwd);
				} else {
					showLoadingPage();
					new Thread() {
						public void run() {
							ServerApiClient.modifyPassword(mHandler, BaseActivity.INVOKE_SUCCESS, oldPassword, newPassword);
						}
					}.start();
				}
			}
		});
		// back button
		mBackBtn = findViewById(R.id.common_title_btn_back);
		mBackBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
	}
}
