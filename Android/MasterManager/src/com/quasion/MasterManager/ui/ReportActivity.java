package com.quasion.MasterManager.ui;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.v13.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.quasion.MasterManager.R;
import com.viewpagerindicator.TabPageIndicator;

public class ReportActivity extends BaseActivity {
	private static final String TAG = ReportActivity.class.getSimpleName();
	private static final Class[] VIEW_CLASS = new Class[] {
			ReportPopularProductFragment.class, ReportDecliningProductFragment.class, ReportRaisingProductFragment.class, ReportAffectingSalesFragment.class
	};
	private static final int[] VIEW_TITLES = new int[] {
			R.string.report_title_popular_product, R.string.report_title_raising_product, R.string.report_title_declining_product, R.string.report_title_affecting_sales
	};
	private TextView mTitle;
	private View mBackBtn;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.report);
		initView();
	}

	private void initView() {
		// activity title
		mTitle = (TextView) findViewById(R.id.common_title_text_title);
		mTitle.setText(getString(R.string.report_title));
		// fragment view page and tab indicator
		final Fragment[] fragments = new Fragment[VIEW_CLASS.length];
		final String[] titles = new String[VIEW_TITLES.length];
		for (int i = 0; i < fragments.length; i++) {
			titles[i] = getString(VIEW_TITLES[i]);
			fragments[i] = Fragment.instantiate(this, VIEW_CLASS[i].getName());
		}
		FragmentPagerAdapter adapter = new FragmentPagerAdapter(getFragmentManager()) {
			@Override
			public int getCount() {
				return fragments.length;
			}
			@Override
			public Fragment getItem(int position) {
				return fragments[position % fragments.length];
			}
			@Override
			public CharSequence getPageTitle(int position) {
				return titles[position % titles.length];
			}
		};
		ViewPager pager = (ViewPager) findViewById(R.id.report_page_view);
		pager.setAdapter(adapter);
		TabPageIndicator indicator = (TabPageIndicator) findViewById(R.id.report_page_tab);
		indicator.setViewPager(pager);
		// back button
		mBackBtn = findViewById(R.id.common_title_btn_back);
		mBackBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
	}
}
