package com.quasion.MasterManager.ui;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.*;
import android.widget.AdapterView.OnItemSelectedListener;

import com.quasion.MasterManager.R;
import com.quasion.MasterManager.bean.NfcCard;
import com.quasion.MasterManager.bean.Owner;
import com.quasion.MasterManager.server.ServerApiClient;
import com.quasion.MasterManager.util.DateUtil;
import com.quasion.MasterManager.util.UiUtil;

public class NfcCardAssignActivity extends BaseActivity {
	public static final String EXTRA_NFC_CARD = "nfcCard";

	private static final int GET_OWNER_LIST = 100;
	private static final int ASSIGN_NFC_CARD = 101;

	private TextView mTitle;
	private View mBackBtn;
	private View mOwnerBtn;
	private View mSubmitBtn;
	private TextView mCardIdText;
	private TextView mCardStatusText;
	private TextView mAddTimeText;
	private Spinner mOwnerSpinner;
	private ArrayAdapter<Owner> mOwnerAdapter;

	private NfcCard mNfcCard;
	private String mCurrentOwner;

	private Handler mHandler = new Handler() {
		public void handleMessage(Message msg) {
			hideLoadingPage();
			switch (msg.what) {
			case GET_OWNER_LIST:
				// get owner list success from server
				List<Owner> owners = (List<Owner>) msg.obj;
				// set owner spinner data
				Owner none = new Owner();
				none.setId("");
				none.setEnglishName(getString(R.string.owner_list_text_none));
				mOwnerAdapter.clear();
				mOwnerAdapter.add(none);
				mOwnerAdapter.addAll(owners);
				mOwnerAdapter.notifyDataSetChanged();
				// select owner spinner for nfc key's current owner
				mOwnerSpinner.setSelection(0);
				for (int i = 1; i < mOwnerAdapter.getCount(); i++) {
					if (mOwnerAdapter.getItem(i).getEnglishName().equals(mNfcCard.getOwnerEnglishName())) {
						mOwnerSpinner.setSelection(i);
						break;
					}
				}
				// enable submit button
				mSubmitBtn.setEnabled(true);
				break;
			case ASSIGN_NFC_CARD:
				// set nfc card's owner success from server
				UiUtil.showToastMessage(getApplicationContext(), R.string.nfc_card_assign_toast_assign_success);
				finish();
				break;
			case BaseActivity.INVOKE_FAIL:
				onInvokeFailed((String) msg.obj);
				break;
			case BaseActivity.SESSION_EXPIRE:
				onSessionExpired((String) msg.obj);
				break;
			}
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mNfcCard = (NfcCard) getIntent().getSerializableExtra(EXTRA_NFC_CARD);
		setContentView(R.layout.nfc_card_assign);
		initView();
	}

	@Override
	protected void onResume() {
		super.onResume();
		requestGetOwnerList();
	}

	private void initView() {
		// activity title
		mTitle = (TextView) findViewById(R.id.common_title_text_title);
		mTitle.setText(R.string.nfc_card_assign_title);
		// nfc card information
		mCardIdText = (TextView) findViewById(R.id.nfc_key_assign_text_card_id);
		mCardIdText.setText(mNfcCard.getKeyId());
		mCardStatusText = (TextView) findViewById(R.id.nfc_key_assign_text_card_status);
		switch(mNfcCard.getStatus()) {
		case NfcCard.STATUS_ADDED:
			mCardStatusText.setText(R.string.nfc_card_text_status_added);
			break;
		case NfcCard.STATUS_ASSIGNED:
			mCardStatusText.setText(R.string.nfc_card_text_status_assigned);
			break;
		case NfcCard.STATUS_LOST:
			mCardStatusText.setText(R.string.nfc_card_text_status_lost);
			break;
		default:
			mCardStatusText.setText("");
			break;
		}
		mAddTimeText = (TextView) findViewById(R.id.nfc_key_assign_text_add_time);
		mAddTimeText.setText(DateUtil.formatTime(mNfcCard.getAddTime(), "yyyy/MM/dd"));
		mOwnerAdapter = new ArrayAdapter<Owner>(this, R.layout.nfc_card_assign_spinner_item);
		mOwnerSpinner = (Spinner) findViewById(R.id.nfc_key_assign_spinner_owner);
		mOwnerSpinner.setAdapter(mOwnerAdapter);
		mOwnerSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> AdapterView, View view, int position, long id) {
				mCurrentOwner = mOwnerAdapter.getItem(position).getId();
			}
			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				mCurrentOwner = null;
			}
		});
		// manage owner list button
		mOwnerBtn = findViewById(R.id.common_title_btn_owner);
		mOwnerBtn.setVisibility(View.VISIBLE);
		mOwnerBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				ArrayList<String> owners = new ArrayList<String>();
				for (int i = 1; i < mOwnerAdapter.getCount(); i++) {
					owners.add(mOwnerAdapter.getItem(i).getEnglishName());
				}
				Intent intent = new Intent(getApplicationContext(), OwnerListActivity.class);
				intent.putExtra(OwnerListActivity.EXTRA_OWNER_LIST, owners);
				startActivity(intent);
			}
		});
		// submit button
		mSubmitBtn = findViewById(R.id.nfc_key_assign_btn_submit);
		mSubmitBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				requestAssignNfcCard();
			}
		});
		// back button
		mBackBtn = findViewById(R.id.common_title_btn_back);
		mBackBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
	}

	private void requestGetOwnerList() {
		// clear owner spinner data
		mOwnerAdapter.clear();
		mOwnerAdapter.notifyDataSetChanged();
		// disable submit button util get owner list
		mSubmitBtn.setEnabled(false);
		showLoadingPage();
		new Thread() {
			public void run() {
				ServerApiClient.getOwnerList(mHandler, GET_OWNER_LIST);
			}
		}.start();
	}

	private void requestAssignNfcCard() {
		showLoadingPage();
		new Thread() {
			public void run() {
				ServerApiClient.addOwnerToNfcCard(mHandler, ASSIGN_NFC_CARD, mCurrentOwner, mNfcCard.getKeyId());
			}
		}.start();
	}
}
