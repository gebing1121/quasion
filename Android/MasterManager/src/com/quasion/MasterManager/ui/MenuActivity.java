package com.quasion.MasterManager.ui;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.LinearLayout;

import com.quasion.MasterManager.R;
import com.quasion.MasterManager.app.AppContext;
import com.quasion.MasterManager.bean.Product;
import com.quasion.MasterManager.server.ServerApiClient;

import java.util.List;

public class MenuActivity extends BaseActivity {
	private LinearLayout mKeyManagementItem;
	private LinearLayout mDataSynchronizationItem;
	private LinearLayout mReportsItem;
	private LinearLayout mSettingsItem;

	private Handler mHandler = new Handler() {
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case INVOKE_SUCCESS:
				AppContext.productList = (List<Product>) msg.obj;
				break;
			case BaseActivity.INVOKE_FAIL:
				onInvokeFailed((String) msg.obj);
				break;
			case BaseActivity.SESSION_EXPIRE:
				onSessionExpired((String) msg.obj);
				break;
			}
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.menu);
		initView();
	}

	private void initView() {
		mKeyManagementItem = (LinearLayout) findViewById(R.id.key_management_item_id);
		mKeyManagementItem.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getApplicationContext(), KeyManagementActivity.class);
				startActivity(intent);
			}
		});

		mDataSynchronizationItem = (LinearLayout) findViewById(R.id.data_synchronization_item_id);
		mDataSynchronizationItem.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getApplicationContext(), DataSynchronizationActivity.class);
				startActivity(intent);
			}
		});

		mReportsItem = (LinearLayout) findViewById(R.id.reports_item_id);
		mReportsItem.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent reportIntent = new Intent(getApplicationContext(), ReportActivity.class);
				startActivity(reportIntent);
			}
		});

		mSettingsItem = (LinearLayout) findViewById(R.id.settings_item_id);
		mSettingsItem.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent reportIntent = new Intent(getApplicationContext(), SettingsActivity.class);
				startActivity(reportIntent);
			}
		});

		new Thread() {
			public void run() {
				ServerApiClient.getProducts(mHandler, INVOKE_SUCCESS, "");
			}
		}.start();
	}
}
