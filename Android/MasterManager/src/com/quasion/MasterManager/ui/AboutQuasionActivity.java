package com.quasion.MasterManager.ui;

import android.content.pm.PackageInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.quasion.MasterManager.R;
import com.quasion.MasterManager.app.AppContext;

public class AboutQuasionActivity extends BaseActivity {
	private TextView mTitle;
	private TextView mVersion;
	private View mBackBtn;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.about_quasion);
		initView();
	}

	private void initView() {
		mTitle = (TextView) findViewById(R.id.common_title_text_title);
		mTitle.setText(R.string.about_quason_title);

		mVersion = (TextView) findViewById(R.id.about_quasion_text_version);
		PackageInfo pkgInfo = AppContext.getAppContext().getPackageInfo();
		mVersion.setText(pkgInfo.versionName + " Build " + pkgInfo.versionCode);

		mBackBtn = findViewById(R.id.common_title_btn_back);
		mBackBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
	}
}
