package com.quasion.MasterManager.ui;

import java.io.IOException;
import java.sql.SQLException;
import java.util.*;

import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.media.MediaPlayer;
import android.nfc.*;
import android.nfc.tech.*;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.quasion.MasterManager.R;
import com.quasion.MasterManager.app.AppPreference;
import com.quasion.MasterManager.bean.*;
import com.quasion.MasterManager.nfc.*;
import com.quasion.MasterManager.server.ServerApiClient;
import com.quasion.MasterManager.util.DatabaseUtil;
import com.quasion.MasterManager.util.StringUtil;
import com.quasion.MasterManager.util.UiUtil;

public class DataSynchronizationActivity extends BaseActivity {
	private static final String TAG = DataSynchronizationActivity.class.getSimpleName();

	private static final int ADD_ALARM_DATA = 100;
	private static final int ADD_HOOK_DATA = 101;
	private static final int REQUEST_CHANGE_MODEL = 100;

	private TextView mTitle;
	private View mBackBtn;
	private View mSalesBtn;
	private View mViewAlarmBtn;
	private View mChangeModelBtn;
	private TextView mBaseIdText;
	private TextView mBaseDataText;
	private TextView mPhoneModelText;
	private ImageView mSearchAnimationView;
	private AnimationDrawable mSearchAnimationDrawable;
	private ViewGroup mBaseInfoLayout, mButtonLayout;

	private NfcAdapter mAdapter;
	private String[][] mTechList;
	private IntentFilter[] mIntentFilter;
	private PendingIntent mPendingIntent;
	private Tag mNfcBaseTag;
	private String mNfcCardIds;
	private String mNfcBaseId;
	private String mNfcPhoneBrand;
	private String mNfcPhoneModel;
	private NfcGetHookData mNfcHookData;
	private NfcGetAlarmData mNfcAlarmData;
	private NfcSetKeyIds mNfcSetKeyIds;
	private AlarmTimeData mSubmitAlarmData;
	private OffhookTimeData mSubmitHookData;
	private RuntimeExceptionDao<AlarmTimeData, String> mAlarmTimeDao;
	private RuntimeExceptionDao<OffhookTimeData, String> mHookTimeDao;

	private Handler mHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			hideLoadingPage();
			switch (msg.what) {
			case ADD_ALARM_DATA:
				submitDataToServer();
				break;
			case ADD_HOOK_DATA:
				submitDataToServer();
				break;
			case BaseActivity.INVOKE_FAIL:
				onInvokeFailed((String) msg.obj);
				break;
			case BaseActivity.SESSION_EXPIRE:
				onSessionExpired((String) msg.obj);
				break;
			}
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.data_synchronization);
		DatabaseUtil helper = OpenHelperManager.getHelper(getApplicationContext(), DatabaseUtil.class);
		mHookTimeDao = helper.getRuntimeExceptionDao(OffhookTimeData.class);
		mAlarmTimeDao = helper.getRuntimeExceptionDao(AlarmTimeData.class);
		initView();
		prepareNfc();
	}

	@Override
	protected void onDestroy() {
		OpenHelperManager.releaseHelper();
		super.onDestroy();
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (mAdapter != null) mAdapter.enableForegroundDispatch(this, mPendingIntent, mIntentFilter, mTechList);
	}

	@Override
	protected void onPause() {
		super.onPause();
		if (mAdapter != null) mAdapter.disableForegroundDispatch(this);
	}

	@Override
	protected void onNewIntent(Intent intent) {
		// 当检测到NFC Based时，调用onNewIntent()
		Tag nfcTag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
		Parcelable[] nfcMessages = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
		if (nfcTag == null) return;
		Ndef ndef = Ndef.get(nfcTag);
		Log.d(TAG, "Found NFC device, Action=" + intent.getAction() + ", Tag=" + StringUtil.toHex(nfcTag.getId()) + "," + nfcTag.toString());
		Log.d(TAG, "Ndef=" + ndef.getType() + ", MaxSize=" + ndef.getMaxSize() + ", isWritable=" + ndef.isWritable());
		if (!ndef.isWritable() || nfcMessages == null) return;
		receiveNfcMessage(nfcTag, nfcMessages);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == REQUEST_CHANGE_MODEL && resultCode == RESULT_OK) {
			// 执行修改手机品牌/型号界面后，成功返回
			mNfcPhoneBrand = data.getStringExtra(ChangeModelActivity.EXTRA_PHONE_BRAND);
			mNfcPhoneModel = data.getStringExtra(ChangeModelActivity.EXTRA_PHONE_MODEL);
			mPhoneModelText.setText(mNfcPhoneBrand + " " + mNfcPhoneModel);
			mPhoneModelText.setTextColor(Color.parseColor("#0086DC"));
			// 保存NFC Base的报警和摘机数据到数据库，并将数据库中数据提交到服务器
			initDataToServer();
		}
	}

	private void initView() {
		// activity title
		mTitle = (TextView) findViewById(R.id.common_title_text_title);
		mTitle.setText(getString(R.string.data_synchronize_title));
		// NFC Base information
		mBaseInfoLayout = (ViewGroup) findViewById(R.id.data_synchronize_layout_info);
		mButtonLayout = (ViewGroup) findViewById(R.id.data_synchronize_layout_button);
		mBaseIdText = (TextView) findViewById(R.id.data_synchronize_text_base_id);
		mBaseDataText = (TextView) findViewById(R.id.data_synchronize_text_base_data);
		mPhoneModelText = (TextView) findViewById(R.id.data_synchronize_text_phone_model);
		// search NFC base animation
		mSearchAnimationView = (ImageView) findViewById(R.id.data_synchronize_anim_search);
		mSearchAnimationDrawable = (AnimationDrawable) mSearchAnimationView.getDrawable();
		// modify phone model button
		mChangeModelBtn = findViewById(R.id.data_synchronize_btn_model);
		mChangeModelBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getApplicationContext(), ChangeModelActivity.class);
				intent.putExtra(ChangeModelActivity.EXTRA_NFC_TAG_ID, mNfcBaseTag.getId());
				intent.putExtra(ChangeModelActivity.EXTRA_PHONE_BRAND, mNfcPhoneBrand);
				intent.putExtra(ChangeModelActivity.EXTRA_PHONE_MODEL, mNfcPhoneModel);
				startActivityForResult(intent, REQUEST_CHANGE_MODEL);
			}
		});
		// view alarm data button
		mViewAlarmBtn = findViewById(R.id.data_synchronize_btn_alarm);
		mViewAlarmBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getApplicationContext(), AlarmReportActivity.class);
				intent.putExtra(AlarmReportActivity.EXTRA_NFC_BASE_ID, mNfcBaseId);
				startActivity(intent);
			}
		});
		// sales volume button
		mSalesBtn = findViewById(R.id.common_title_btn_edit);
		mSalesBtn.setVisibility(View.VISIBLE);
		mSalesBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getApplicationContext(), SalesListActivity.class);
				startActivity(intent);
			}
		});
		// back button
		mBackBtn = findViewById(R.id.common_title_btn_back);
		mBackBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
		// show searching nfc
		showNfcSearching();
	}

	private void showNfcSearching() {
		mSearchAnimationDrawable.start();
		mSearchAnimationView.setVisibility(View.VISIBLE);
		mBaseInfoLayout.setVisibility(View.GONE);
		mButtonLayout.setVisibility(View.GONE);
	}

	private void showNfcContent() {
		int alarmCnt = mNfcAlarmData.alarmData.size();
		int hookCnt = mNfcHookData.hookData.size();
		mSearchAnimationDrawable.stop();
		mSearchAnimationView.setVisibility(View.GONE);
		mBaseInfoLayout.setVisibility(View.VISIBLE);
		mButtonLayout.setVisibility(View.VISIBLE);
		mBaseIdText.setText(mNfcBaseId);
		mBaseDataText.setText("" + alarmCnt + " / " + hookCnt);
		if (StringUtil.isEmpty(mNfcPhoneBrand) || StringUtil.isEmpty(mNfcPhoneModel)) {
			mPhoneModelText.setText(R.string.data_synchronize_text_empty_model);
			mPhoneModelText.setTextColor(Color.RED);
			// 提示Base尚未设置手机型号，并进入设置手机型号界面
			UiUtil.showToastMessage(getApplication(), R.string.data_synchronize_toast_model_empty);
			mChangeModelBtn.performClick();
		} else {
			mPhoneModelText.setText(mNfcPhoneBrand + " " + mNfcPhoneModel);
			mPhoneModelText.setTextColor(Color.parseColor("#0086DC"));
			// 保存NFC Base的报警和摘机数据到数据库，并将数据库中数据提交到服务器
			initDataToServer();
		}
	}

	private void prepareNfc() {
		// 读取保存的NFC Card Id列表，如果为空则跳转到KeyManagementActivity
		String keyIds = AppPreference.getNfcCards();
		if (StringUtil.isEmpty(keyIds)) {
			UiUtil.showToastMessage(getApplicationContext(), R.string.data_synchronize_toast_empty_keys);
			Intent intent = new Intent(getApplicationContext(), KeyManagementActivity.class);
			startActivity(intent);
			finish();
			return;
		}
		mNfcSetKeyIds = new NfcSetKeyIds(keyIds);
		// 获得NfcAdapter，以检测是否支持NFC
		mAdapter = NfcAdapter.getDefaultAdapter(this);
		if (mAdapter == null) {
			UiUtil.showToastMessage(getApplicationContext(), R.string.msg_nfc_not_support);
			finish();
			return;
		}
		// 检查NFC是否被打开
		if (!mAdapter.isEnabled()) {
			UiUtil.showToastMessage(getApplicationContext(), R.string.msg_nfc_not_enabled);
			finish();
			return;
		}
		// 创建techList和intentFilters
		mTechList = new String[][] { new String[] { NfcA.class.getName(), Ndef.class.getName() } };
		mIntentFilter = new IntentFilter[] { new IntentFilter(NfcAdapter.ACTION_TECH_DISCOVERED) };
		// 创建PendingIntent对象, 这样Android系统就能在一个tag被检测到时定位到这个对象
		mPendingIntent = PendingIntent.getActivity(this, 0, new Intent(this, this.getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
		Log.d(TAG, "prepareNfc() success");
	}

	private boolean sendNfcMessage(Tag nfcTag, NdefMessage nfcMessage) {
		// 获取NDEF对象
		Ndef ndef = Ndef.get(nfcTag);
		if (ndef == null) return false;
		// 连接NDEF设备
		try {
			ndef.close();
		} catch (IOException e) {
		}
		try {
			ndef.connect();
		} catch (IOException e) {
			Log.w(TAG, "sendNfcMessage() failed for connect!");
			e.printStackTrace();
			return false;
		}
		// 发送NDEF数据
		try {
			ndef.writeNdefMessage(nfcMessage);
			Log.d(TAG, "sendNfcMessage() success.");
			return true;
		} catch (IOException e) {
			Log.w(TAG, "sendNfcMessage() with IOException!");
			// 发送数据到NFC Base时，有时成功发送也会产生IOException
			return true;
		} catch (FormatException e) {
			Log.w(TAG, "sendNfcMessage() failed for FormatException!");
			e.printStackTrace();
			return false;
		} finally {
			try {
				if (ndef != null && ndef.isConnected()) ndef.close();
			} catch (IOException e) {
			}
		}
	}

	private boolean receiveNfcMessage(Tag nfcTag, Parcelable[] nfcMessages) {
		// 解析接收到的NFC数据
		if (nfcMessages == null || nfcMessages.length == 0) return false;
		NdefMessage[] ndefMessages = new NdefMessage[nfcMessages.length];
		for (int i = 0; i < nfcMessages.length; i++) ndefMessages[i] = (NdefMessage) nfcMessages[i];
		List<NfcBaseMessage> baseMessages = NfcBaseMessage.fromNdefMessages(ndefMessages);
		// 如果解析成功，则表示连接的是NFC Base设备
		Log.d(TAG, "receiveNfcMessage()=" + baseMessages);
		if (baseMessages == null || baseMessages.size() == 0) return false;
		boolean previousSyncFinished = onNfcBaseConnected(nfcTag);
		// 处理接收到的NFC Base数据
		for (NfcBaseMessage baseMessage : baseMessages) {
			if (baseMessage instanceof NfcGetBaseId) {
				mNfcBaseId = ((NfcGetBaseId) baseMessage).nfcBaseId;
			} else if (baseMessage instanceof NfcGetPhoneModel) {
				mNfcPhoneBrand = ((NfcGetPhoneModel) baseMessage).phoneBrand;
				mNfcPhoneModel = ((NfcGetPhoneModel) baseMessage).phoneModel;
			} else if (baseMessage instanceof NfcGetKeyIds) {
				mNfcCardIds = ((NfcGetKeyIds) baseMessage).nfcKeyIds;
			} else if (baseMessage instanceof NfcGetAlarmData) {
				mNfcAlarmData = (NfcGetAlarmData) baseMessage;
			} else if (baseMessage instanceof NfcGetHookData) {
				mNfcHookData = (NfcGetHookData) baseMessage;
			}
		}
		// 根据已经获取的数据，发送下一个命令到NFC Base
		boolean currentSyncFinished = false;
		if (mNfcBaseId == null || mNfcPhoneBrand == null) {
			NdefMessage ndefMessage = NfcBaseMessage.toNdefMessages(NfcBaseMessage.MSG_GET_BASE_ID, NfcBaseMessage.MSG_GET_MODEL, NfcBaseMessage.MSG_GET_BASE_TIME);
			sendNfcMessage(mNfcBaseTag, ndefMessage);
		} else if (mNfcCardIds == null || !mNfcCardIds.equals(mNfcSetKeyIds.nfcKeyIds)) {
			NdefMessage ndefMessage = NfcBaseMessage.toNdefMessages(mNfcSetKeyIds, NfcBaseMessage.MSG_GET_BASE_ID, NfcBaseMessage.MSG_GET_KEY_ID, NfcBaseMessage.MSG_GET_BASE_TIME);
			sendNfcMessage(mNfcBaseTag, ndefMessage);
		} else if (mNfcAlarmData == null) {
			NdefMessage ndefMessage = NfcBaseMessage.toNdefMessages(NfcBaseMessage.MSG_GET_ALARM_DATA);
			sendNfcMessage(mNfcBaseTag, ndefMessage);
		} else if (mNfcHookData == null) {
			NdefMessage ndefMessage = NfcBaseMessage.toNdefMessages(NfcBaseMessage.MSG_GET_HOOK_DATA);
			sendNfcMessage(mNfcBaseTag, ndefMessage);
		} else {
			currentSyncFinished = true;
		}
		// 根据同步情况，显示同步开始/结束的信息
		if (currentSyncFinished && !previousSyncFinished) {
			// 完成同步NFC Base所有需要的数据
			onNfcBaseStopSync(true);
			// 保证最后发送给NFC Base的消息是获取BaseId消息。防止再次连接NFC Base时收到上次传送的Alarm/Hook的数据
			NdefMessage ndefMessage = NfcBaseMessage.toNdefMessages(NfcBaseMessage.MSG_GET_BASE_ID);
			sendNfcMessage(mNfcBaseTag, ndefMessage);
		} else if (!currentSyncFinished && previousSyncFinished) {
			// 开始同步NFC Base的数据
			onNfcBaseStartSync();
		}
		return true;
	}

	private boolean onNfcBaseConnected(Tag nfcTag) {
		// 判断是否为上次连接的NFC Base
		if (mNfcBaseTag == null || !Arrays.equals(nfcTag.getId(), mNfcBaseTag.getId())) {
			// 清除保存的相关数据
			mNfcBaseTag = nfcTag;
			mNfcBaseId = null;
			mNfcCardIds = null;
			mNfcPhoneBrand = null;
			mNfcPhoneModel = null;
			mNfcHookData = null;
			mNfcAlarmData = null;
		}
		timerReset();
		return !timerActive();
	}

	private void onNfcBaseStartSync() {
		// 显示等待对话框
		showLoadingPage(getString(R.string.data_synchronize_toast_read_base));
		// 显示搜索NFC Base的界面
		showNfcSearching();
		// 启动检查NFC是否断开的定时器
		timerStart();
		Log.d(TAG, "onNfcBaseStartSync()");
	}

	private void onNfcBaseStopSync(boolean syncSuccess) {
		if (syncSuccess) {
			// 播放成功声效
			playSound(R.raw.success);
			// 显示成功提示
			UiUtil.showToastMessage(this, R.string.data_synchronize_toast_read_success);
			// 隐藏等待对话框
			hideLoadingPage();
			// 显示NFC Base的相关信息
			showNfcContent();
		} else {
			// 播放失败声效
			playSound(R.raw.failed);
			// 显示失败提示
			UiUtil.showToastMessage(this, R.string.data_synchronize_toast_read_failed);
			// 隐藏等待对话框
			hideLoadingPage();
			// 显示搜索NFC Base的界面
			showNfcSearching();
		}
		// 停止检查NFC是否断开的定时器
		timerStop();
		Log.d(TAG, "onNfcBaseStopSync(syncSuccess=" + syncSuccess + ")");
	}

	private void playSound(int resId) {
		MediaPlayer mediaPlayer = MediaPlayer.create(this, resId);
		mediaPlayer.setVolume(1.0f, 1.0f);
		mediaPlayer.start();
	}

	private static final long NFC_CHECK_DELAY = 1000;
	private static final long NFC_CHECK_TIMEOUT = 4000;
	private boolean mTimerActive = false;
	private long mTimerLastTime;
	private final Runnable mCheckNfcConnection = new Runnable() {
		@Override
		public void run() {
			if (System.currentTimeMillis() - mTimerLastTime >= NFC_CHECK_TIMEOUT) {
				onNfcBaseStopSync(false);
			} else {
				mHandler.postDelayed(this, NFC_CHECK_DELAY);
			}
		}
	};
	private void timerStart() {
		timerStop();
		mTimerActive = true;
		mTimerLastTime = System.currentTimeMillis();
		mHandler.postDelayed(mCheckNfcConnection, NFC_CHECK_DELAY);
	}

	private void timerStop() {
		mTimerActive = false;
		mHandler.removeCallbacks(mCheckNfcConnection);
	}

	private void timerReset() {
		mTimerLastTime = System.currentTimeMillis();
	}

	private boolean timerActive() {
		return mTimerActive;
	}

	private void saveHookTimeData(String baseId, String brand, String model, String imei, NfcGetHookData data) {
		if (data == null || data.hookData == null || data.hookData.size() == 0) return;
		OffhookTimeData hookTimeData = new OffhookTimeData();
		hookTimeData.uuid = UUID.randomUUID().toString();
		hookTimeData.nfcBaseId = baseId;
		hookTimeData.deviceBrand = brand;
		hookTimeData.deviceModel = model;
		hookTimeData.deviceImei = imei;
		hookTimeData.offhookTimes = new OffhookTimeItem(data.hookData);
		mHookTimeDao.createOrUpdate(hookTimeData);
	}

	private void saveAlarmTimeData(String baseId, String imei, NfcGetAlarmData data) {
		if (data == null || data.alarmData == null || data.alarmData.size() == 0) return;
		AlarmTimeData alarmTimeData = new AlarmTimeData();
		alarmTimeData.uuid = UUID.randomUUID().toString();
		alarmTimeData.nfcBaseId = baseId;
		alarmTimeData.deviceImei = imei;
		alarmTimeData.alarmTimes = new AlarmTimeItem(data.alarmData);
		mAlarmTimeDao.createOrUpdate(alarmTimeData);
	}

	private AlarmTimeData queryAlarmTimeData() {
		try {
			return mAlarmTimeDao.queryForFirst(mAlarmTimeDao.queryBuilder().prepare());
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	private OffhookTimeData queryHookTimeData() {
		try {
			return mHookTimeDao.queryForFirst(mHookTimeDao.queryBuilder().prepare());
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	private void initDataToServer() {
		// 保存NFC Base的报警和摘机数据到数据库
		saveAlarmTimeData(mNfcBaseId, null, mNfcAlarmData);
		saveHookTimeData(mNfcBaseId, mNfcPhoneBrand, mNfcPhoneModel, null, mNfcHookData);
		if (mNfcAlarmData != null  && mNfcAlarmData.alarmData != null) mNfcAlarmData.alarmData.clear();
		if (mNfcHookData != null  && mNfcHookData.hookData != null) mNfcHookData.hookData.clear();
		// 提交数据库中的报警和摘机数据到服务器
		mSubmitAlarmData = null;
		mSubmitHookData = null;
		if (mAlarmTimeDao.countOf() > 0 || mHookTimeDao.countOf() > 0) {
			submitDataToServer();
		}
	}

	private void submitDataToServer() {
		// 如果mSubmitAlarmData不为null，则表示上次的操作是将mSubmitAlarmData提交到服务器，
		// 且提交成功，因此将本地数据库中保存的mSubmitAlarmData删除，防止数据再次提交到服务器
		mAlarmTimeDao.delete(mSubmitAlarmData);
		// 重新从数据库中提取未提交的Alarm数据，如果为null，则表示无未提交的Alarm数据
		mSubmitAlarmData = queryAlarmTimeData();
		if (mSubmitAlarmData != null) {
			// 将未提交的Alarm数据提交到服务器
			showLoadingPage(getString(R.string.data_synchronize_toast_submit_alarm));
			new Thread() {
				public void run() {
					ServerApiClient.addAlarmDataByToken(mHandler, ADD_ALARM_DATA, mSubmitAlarmData.uuid, mSubmitAlarmData.nfcBaseId, mSubmitAlarmData.alarmTimes);
				}
			}.start();
			return;
		}
		// 如果mHookTimeDao不为null，则表示上次的操作是将mHookTimeDao提交到服务器，
		// 且提交成功，因此将本地数据库中保存的mHookTimeDao删除，防止数据再次提交到服务器
		mHookTimeDao.delete(mSubmitHookData);
		// 重新从数据库中提取未提交的Hook数据，如果为null，则表示无未提交的Hook数据
		mSubmitHookData = queryHookTimeData();
		if (mSubmitHookData != null) {
			// 将未提交的Hook数据提交到服务器
			showLoadingPage(getString(R.string.data_synchronize_toast_submit_hook));
			new Thread() {
				public void run() {
					ServerApiClient.addOffhookTimesByToken(mHandler, ADD_HOOK_DATA, mSubmitHookData.uuid, mSubmitHookData.nfcBaseId,
							mSubmitHookData.deviceBrand, mSubmitHookData.deviceModel, mSubmitHookData.deviceImei, mSubmitHookData.offhookTimes);
				}
			}.start();
			return;
		}
		// 无未提交的Alarm/Hook数据，表示所有数据都已经提交到服务器
		UiUtil.showToastMessage(getApplication(), R.string.data_synchronize_toast_submit_success);
	}
}
