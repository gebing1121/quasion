package com.quasion.MasterManager.ui;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.*;

import com.quasion.MasterManager.R;
import com.quasion.MasterManager.server.ServerApiClient;
import com.quasion.MasterManager.util.StringUtil;
import com.quasion.MasterManager.util.UiUtil;

public class OwnerAddActivity extends BaseActivity {
	public static final String EXTRA_OWNER_NAME = "owner";

	private TextView mTitle;
	private View mBackBtn;
	private View mSaveBtn;
	private EditText mOwnerEdit;

	private Handler mHandler = new Handler() {
		public void handleMessage(Message msg) {
			hideLoadingPage();
			switch (msg.what) {
			case BaseActivity.INVOKE_SUCCESS:
				UiUtil.showToastMessage(getApplicationContext(), R.string.owner_add_toast_add_success);
				Intent intent = new Intent();
				intent.putExtra(EXTRA_OWNER_NAME, mOwnerEdit.getText().toString().trim());
				setResult(RESULT_OK, intent);
				finish();
				break;
			case BaseActivity.INVOKE_FAIL:
				onInvokeFailed((String) msg.obj);
				break;
			case BaseActivity.SESSION_EXPIRE:
				setResult(RESULT_CANCELED);
				onSessionExpired((String) msg.obj);
				break;
			}
		};
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.owner_add);
		initView();
	}

	private void initView() {
		// activity title
		mTitle = (TextView) findViewById(R.id.common_title_text_title);
		mTitle.setText(R.string.owner_add_title);
		// owner name
		mOwnerEdit = (EditText) findViewById(R.id.owner_add_edit_name);
		// save button
		mSaveBtn = findViewById(R.id.owner_add_btn_save);
		mSaveBtn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				final String ownerName = mOwnerEdit.getText().toString().trim();
				if (StringUtil.isEmpty(ownerName)) {
					UiUtil.showToastMessage(getApplicationContext(), R.string.owner_add_toast_name_empty);
				} else {
					showLoadingPage();
					new Thread() {
						public void run() {
							ServerApiClient.addOwner(mHandler, BaseActivity.INVOKE_SUCCESS, ownerName);
						}
					}.start();
				}
			}
		});
		// back button
		mBackBtn = findViewById(R.id.common_title_btn_back);
		mBackBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				setResult(RESULT_CANCELED);
				finish();
			}
		});
	}
}
