package com.quasion.MasterManager.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.quasion.MasterManager.R;
import com.quasion.MasterManager.util.StringUtil;
import com.quasion.MasterManager.util.UiUtil;
import com.quasion.MasterManager.widget.LoadingDialog;

public class BaseActivity extends Activity {
	/* 会话过期 */
	public static final int SESSION_EXPIRE = 999;
	/* 调用URL返回成功 */
	public static final int INVOKE_SUCCESS = 200;
	/* 调用URL返回失败 */
	public static final int INVOKE_FAIL = 500;

	private LoadingDialog mLoadingDialog;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mLoadingDialog = new LoadingDialog(this, R.style.LoadingDialog);
		mLoadingDialog.setCancelable(false);
	}

	public void showLoadingPage(String ...message) {
		mLoadingDialog.show();
		if (message != null && message.length > 0) mLoadingDialog.setDialogMessage(message[0]);
	}

	public void hideLoadingPage() {
		mLoadingDialog.dismiss();
	}

	public boolean isLoadingDialogShowing() {
		return mLoadingDialog.isShowing();
	}

	public void setLoadingProgress(int progress) {
		if (mLoadingDialog != null) mLoadingDialog.setDialogProgress(progress);
	}

	public void setLoadingProgress(String progress) {
		if (mLoadingDialog != null) mLoadingDialog.setDialogProgress(progress);
	}

	public void setLoadingMessage(String message) {
		if (mLoadingDialog != null) mLoadingDialog.setDialogMessage(message);
	}

	public void onInvokeFailed(String errorMsg) {
		UiUtil.showToastMessage(getApplicationContext(), errorMsg);
	}

	public void onSessionExpired(String expireMsg) {
		if (!StringUtil.isEmpty(expireMsg)) {
			UiUtil.showToastMessage(getApplicationContext(), expireMsg);
		}
		// 清除当前Activity Stack中的全部Activity
		Intent intent = new Intent(getApplicationContext(), MainActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
		startActivity(intent);
		finish();
	}

	public void onMenuActivity() {
		// 清除当前Activity Stack中MenuActivity之上的全部Activity
		Intent intent = new Intent(getApplicationContext(), MenuActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(intent);
		finish();
	}
}
