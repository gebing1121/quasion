package com.quasion.MasterManager.ui;

import java.util.List;

import com.quasion.MasterManager.util.StringUtil;
import com.quasion.MasterManager.util.UiUtil;
import org.json.JSONObject;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;

import com.quasion.MasterManager.R;
import com.quasion.MasterManager.bean.SafeQuestion;
import com.quasion.MasterManager.server.ServerApiClient;

public class ForgetPasswordActivity extends BaseActivity {
	private final static int INPUT_USERNAME = 100;
	private final static int VERIFY_QUESTION = 101;
	private final static int RESET_PASSWORD = 102;

	private TextView mTitle;
	private View mBackBtn;
	private View mNextBtn;
	private ViewGroup mInputUsernameLayout, mVerifyQuestionLayout, mResetPasswordLayout;
	private EditText mLoginAccountEdit;
	private Spinner mQuestion1Spinner, mQuestion2Spinner, mQuestion3Spinner;
	private ArrayAdapter<String> mQuestion1Adapter, mQuestion2Adapter, mQuestion3Adapter;
	private EditText mAnswer1Edit, mAnswer2Edit, mAnswer3Edit;
	private EditText mNewPasswordEdit;
	private EditText mConfirmPasswordEdit;

	private int mCurrentStep;
	private String mUsername;
	private String mQuestion1, mQuestion2, mQuestion3;

	private Handler mHandler = new Handler() {
		public void handleMessage(Message msg) {
			hideLoadingPage();
			switch (msg.what) {
			case INPUT_USERNAME:
				// get user's security questions from server
				if (mCurrentStep != msg.what) break;
				List<SafeQuestion> safeQuestions = (List<SafeQuestion>) msg.obj;
				// at least 3 security questions
				if (safeQuestions.size() < 3) {
					UiUtil.showToastMessage(getApplicationContext(), R.string.forget_password_toast_no_questions);
					break;
				}
				// set question spinners data
				initQuestions(safeQuestions);
				// show question verify page
				showContent(VERIFY_QUESTION);
				break;
			case VERIFY_QUESTION:
				// verify user's security questions from server
				if (mCurrentStep != msg.what) break;
				boolean isVerify = SafeQuestion.parseVerifyQuestions((JSONObject) msg.obj);
				// verify success or failed
				if (!isVerify) {
					UiUtil.showToastMessage(getApplicationContext(), R.string.forget_password_toast_verify_failed);
					break;
				}
				UiUtil.showToastMessage(getApplicationContext(), R.string.forget_password_toast_verify_success);
				// show reset password page
				showContent(RESET_PASSWORD);
				break;
			case RESET_PASSWORD:
				// success reset user's password from server
				if (mCurrentStep != msg.what) break;
				UiUtil.showToastMessage(getApplicationContext(), R.string.forget_password_toast_reset_success);
				// return to login activity
				onSessionExpired(null);
				break;
			case BaseActivity.INVOKE_FAIL:
				onInvokeFailed((String) msg.obj);
				break;
			case BaseActivity.SESSION_EXPIRE:
				onSessionExpired((String) msg.obj);
				break;
			}
		};
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.forget_password);
		initView();
	}

	private void initView() {
		// activity title
		mTitle = (TextView) findViewById(R.id.common_title_text_title);
		// input username layout
		mInputUsernameLayout = (ViewGroup) findViewById(R.id.input_username_layout);
		mLoginAccountEdit = (EditText) findViewById(R.id.username_id);
		// verify security question layout
		mVerifyQuestionLayout = (ViewGroup) findViewById(R.id.verify_question_layout);
		mAnswer1Edit = (EditText) findViewById(R.id.answer1_id);
		mAnswer2Edit = (EditText) findViewById(R.id.answer2_id);
		mAnswer3Edit = (EditText) findViewById(R.id.answer3_id);
		mQuestion1Adapter = new ArrayAdapter<String>(this, R.layout.common_spinner_text);
		mQuestion1Adapter.setDropDownViewResource(R.layout.common_spinner_dropdown);
		mQuestion1Spinner = (Spinner) findViewById(R.id.question1_id);
		mQuestion1Spinner.setAdapter(mQuestion1Adapter);
		mQuestion1Spinner.setEnabled(false);
		mQuestion2Adapter = new ArrayAdapter<String>(this, R.layout.common_spinner_text);
		mQuestion2Adapter.setDropDownViewResource(R.layout.common_spinner_dropdown);
		mQuestion2Spinner = (Spinner) findViewById(R.id.question2_id);
		mQuestion2Spinner.setAdapter(mQuestion2Adapter);
		mQuestion2Spinner.setEnabled(false);
		mQuestion3Adapter = new ArrayAdapter<String>(this, R.layout.common_spinner_text);
		mQuestion3Adapter.setDropDownViewResource(R.layout.common_spinner_dropdown);
		mQuestion3Spinner = (Spinner) findViewById(R.id.question3_id);
		mQuestion3Spinner.setAdapter(mQuestion3Adapter);
		mQuestion3Spinner.setEnabled(false);
		// reset password layout
		mResetPasswordLayout = (ViewGroup) findViewById(R.id.reset_password_layout);
		mNewPasswordEdit = (EditText) findViewById(R.id.new_password_id);
		mConfirmPasswordEdit = (EditText) findViewById(R.id.confirm_password_id);
		// next button
		mNextBtn = findViewById(R.id.next_btn_id);
		mNextBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				processContent();
			}
		});
		// back button
		mBackBtn = findViewById(R.id.common_title_btn_back);
		mBackBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (mCurrentStep == INPUT_USERNAME) {
					finish();
				} else {
					showContent(mCurrentStep - 1);
				}
			}
		});
		// show first page
		showContent(INPUT_USERNAME);
	}

	private void initQuestions(final List<SafeQuestion> questions) {
		// set question spinners data
		mQuestion1Adapter.clear();
		mQuestion2Adapter.clear();
		mQuestion3Adapter.clear();
		mQuestion1Adapter.add(questions.get(0).getQuestion());
		mQuestion2Adapter.add(questions.get(1).getQuestion());
		mQuestion3Adapter.add(questions.get(2).getQuestion());
		mQuestion1Adapter.notifyDataSetChanged();
		mQuestion2Adapter.notifyDataSetChanged();
		mQuestion3Adapter.notifyDataSetChanged();
		// get question ids
		mQuestion1 = questions.get(0).getId();
		mQuestion2 = questions.get(1).getId();
		mQuestion3 = questions.get(2).getId();
	}

	private void showContent(int currentStep) {
		mCurrentStep = currentStep;
		switch (mCurrentStep) {
		case INPUT_USERNAME:
			mInputUsernameLayout.setVisibility(View.VISIBLE);
			mVerifyQuestionLayout.setVisibility(View.GONE);
			mResetPasswordLayout.setVisibility(View.GONE);
			mTitle.setText(R.string.forget_password_title1);
			((TextView) mNextBtn).setText(R.string.btn_next);
			break;
		case VERIFY_QUESTION:
			mInputUsernameLayout.setVisibility(View.GONE);
			mVerifyQuestionLayout.setVisibility(View.VISIBLE);
			mResetPasswordLayout.setVisibility(View.GONE);
			mTitle.setText(R.string.forget_password_title2);
			((TextView) mNextBtn).setText(R.string.btn_next);
			break;
		case RESET_PASSWORD:
			mInputUsernameLayout.setVisibility(View.GONE);
			mVerifyQuestionLayout.setVisibility(View.GONE);
			mResetPasswordLayout.setVisibility(View.VISIBLE);
			mTitle.setText(R.string.forget_password_title3);
			((TextView) mNextBtn).setText(R.string.btn_submit);
			break;
		}
	}

	private void processContent() {
		switch (mCurrentStep) {
		case INPUT_USERNAME:
			// request user's security question from server
			mUsername = mLoginAccountEdit.getText().toString().trim();
			if (StringUtil.isEmpty(mUsername)) {
				UiUtil.showToastMessage(getApplicationContext(), R.string.forget_password_toast_empty_username);
			} else {
				showLoadingPage();
				new Thread() {
					public void run() {
						ServerApiClient.getSafeQuestionsByUsername(mHandler, INPUT_USERNAME, mUsername);
					}
				}.start();
			}
			break;
		case VERIFY_QUESTION:
			// request verify user's security question from server
			final String answer1 = mAnswer1Edit.getText().toString().trim();
			final String answer2 = mAnswer2Edit.getText().toString().trim();
			final String answer3 = mAnswer3Edit.getText().toString().trim();
			if (StringUtil.isEmpty(answer1) || StringUtil.isEmpty(answer2) || StringUtil.isEmpty(answer3)) {
				UiUtil.showToastMessage(getApplicationContext(), R.string.question_toast_empty_answers);
			} else {
				showLoadingPage();
				new Thread() {
					public void run() {
						ServerApiClient.verifySafeQuestionsByUsername(mHandler, VERIFY_QUESTION, mUsername,
								mQuestion1, answer1, mQuestion2, answer2, mQuestion3, answer3);
					}
				}.start();
			}
			break;
		case RESET_PASSWORD:
			// request reset user's password from server
			final String newPassword = mNewPasswordEdit.getText().toString().trim();
			String confirmPassword = mConfirmPasswordEdit.getText().toString().trim();
			if (StringUtil.isEmpty(newPassword)) {
				UiUtil.showToastMessage(getApplicationContext(), R.string.password_toast_empty_new_pwd);
			} else if (StringUtil.isEmpty(confirmPassword)) {
				UiUtil.showToastMessage(getApplicationContext(), R.string.password_toast_empty_confirm_pwd);
			} else if (!newPassword.equals(confirmPassword)) {
				UiUtil.showToastMessage(getApplicationContext(), R.string.password_toast_not_same_pwd);
			} else {
				showLoadingPage();
				new Thread() {
					public void run() {
						ServerApiClient.resetPassword(mHandler, RESET_PASSWORD, mUsername, newPassword);
					}
				}.start();
			}
			break;
		}
	}
}
