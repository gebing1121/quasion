package com.quasion.MasterManager.ui;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.*;

import com.quasion.MasterManager.R;
import com.quasion.MasterManager.server.ServerApiClient;
import com.quasion.MasterManager.util.StringUtil;
import com.quasion.MasterManager.util.UiUtil;

public class FeedbackActivity extends BaseActivity {
	private TextView mTitle;
	private View mBackBtn;
	private View mSubmitBtn;
	private EditText mFeedback;
	private EditText mContact;

	private Handler mHandler = new Handler() {
		public void handleMessage(Message msg) {
			hideLoadingPage();
			switch (msg.what) {
			case BaseActivity.INVOKE_SUCCESS:
				UiUtil.showToastMessage(getApplicationContext(), R.string.feed_back_toast_submit_success);
				mFeedback.setText("");
				mContact.setText("");
				finish();
				break;
			case BaseActivity.INVOKE_FAIL:
				onInvokeFailed((String) msg.obj);
				break;
			case BaseActivity.SESSION_EXPIRE:
				onSessionExpired((String) msg.obj);
				break;
			}
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.feedback);
		initView();
	}

	private void initView() {
		// activity title
		mTitle = (TextView) findViewById(R.id.common_title_text_title);
		mTitle.setText(R.string.feed_back_title);
		// feed back content
		mFeedback = (EditText) findViewById(R.id.feedback_text_content);
		mContact = (EditText) findViewById(R.id.feedback_text_contact);
		// submit button
		mSubmitBtn = findViewById(R.id.feedback_btn_submit);
		mSubmitBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				final String feedback = mFeedback.getText().toString().trim();
				final String contact = mContact.getText().toString().trim();
				if (StringUtil.isEmpty(feedback)) {
					UiUtil.showToastMessage(getApplicationContext(), R.string.feed_back_toast_feedback_empty);
				}
				else if (StringUtil.isEmpty(contact)) {
					UiUtil.showToastMessage(getApplicationContext(), R.string.feed_back_toast_contact_empty);
				}
				else {
					showLoadingPage();
					new Thread() {
						public void run() {
							ServerApiClient.addUserFeedback(mHandler, BaseActivity.INVOKE_SUCCESS, feedback, contact);
						}
					}.start();
				}
			}
		});
		// back button
		mBackBtn = findViewById(R.id.common_title_btn_back);
		mBackBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
	}
}
