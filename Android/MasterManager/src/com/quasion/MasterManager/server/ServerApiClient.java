package com.quasion.MasterManager.server;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import org.apache.commons.httpclient.*;
import org.apache.commons.httpclient.cookie.CookiePolicy;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.multipart.MultipartRequestEntity;
import org.apache.commons.httpclient.methods.multipart.Part;
import org.apache.commons.httpclient.methods.multipart.StringPart;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Handler;
import android.util.Log;

import com.quasion.MasterManager.app.AppContext;
import com.quasion.MasterManager.app.AppException;
import com.quasion.MasterManager.app.AppPreference;
import com.quasion.MasterManager.bean.*;

public class ServerApiClient {
	public static final String TAG = ServerApiClient.class.getSimpleName();

	private static final String UTF_8 = "UTF-8";
	private final static int TIMEOUT_CONNECTION = 20000;
	private final static int TIMEOUT_SOCKET = 20000;
	private final static int RETRY_TIME = 3;

	private static String appCookie;
	private static String appUserAgent;

	/* 对应哪一页 */
	public static final String PAGE_NO = "pageNo";
	/* 每页最多显示数量 */
	public static final String PAGE_SIZE = "pageSize";
	/* 缺省每页显示数量 */
	public static final int PAGE_DEFAULT_SIZE = 100;

	private static BaseBean.Parser safeQuestionParser = new BaseBean.Parser() {
		@Override
		public Object parse(JSONObject data) throws JSONException {
			return SafeQuestion.parseSafeQuestions(data);
		}
	};

	private static BaseBean.Parser nfcCardParser = new BaseBean.Parser() {
		@Override
		public Object parse(JSONObject data) throws JSONException {
			return NfcCard.parseNfcCards(data);
		}
	};

	private static BaseBean.Parser ownerListParser = new BaseBean.Parser() {
		@Override
		public Object parse(JSONObject data) throws JSONException {
			return Owner.parseOwnerList(data);
		}
	};

	private static BaseBean.Parser alarmListParser = new BaseBean.Parser() {
		@Override
		public Object parse(JSONObject data) throws JSONException {
			return AlarmReport.parseAlarmDataList(data);
		}
	};

	private static BaseBean.Parser productListParser = new BaseBean.Parser() {
		@Override
		public Object parse(JSONObject data) throws JSONException {
			return Product.parseProductList(data);
		}
	};

	private static BaseBean.Parser salesListParser = new BaseBean.Parser() {
		@Override
		public Object parse(JSONObject data) throws JSONException {
			return Sales.parseSalesList(data);
		}
	};

	private static BaseBean.Parser pickupRateParser = new BaseBean.Parser() {
		@Override
		public Object parse(JSONObject data) throws JSONException {
			return ReportPickupRate.parseReportPickupRate(data);
		}
	};

	private static BaseBean.Parser pickupTimeParser = new BaseBean.Parser() {
		@Override
		public Object parse(JSONObject data) throws JSONException {
			return ReportAvgPickupTime.parseReportPickupTime(data);
		}
	};

	private static BaseBean.Parser salesRateParser = new BaseBean.Parser() {
		@Override
		public Object parse(JSONObject data) throws JSONException {
			return SalesReport.parseReportSalesRate(data);
		}
	};

	private static BaseBean.Parser updateParser = new BaseBean.Parser() {
		@Override
		public Object parse(JSONObject data) throws JSONException {
			return Update.parseUpdate(data);
		}
	};

	/**
	 * 登录， 自动处理cookie
	 *
	 * @param mHandler
	 * @param msgWhat
	 * @param loginAccount
	 * @param password
	 */
	public static void login(Handler mHandler, int msgWhat, String loginAccount, String password) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(ServerApiURLs.LOGIN_ACCOUNT, loginAccount);
		params.put(ServerApiURLs.LOGIN_PASSWORD, password);
		try {
			String responseData = doPost(ServerApiURLs.LOGIN_URL, params);
			User user = new User();
			user.setLoginAccount(loginAccount);
			user.setLoginPassword(password);
			BaseBean.handleResponse(mHandler, msgWhat, responseData, user);
		} catch (Exception ex) {
			BaseBean.handleException(mHandler, ex);
		}
	}

	/**
	 * 退出
	 *
	 * @param mHandler
	 * @param msgWhat
	 */
	public static void logout(Handler mHandler, int msgWhat) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(ServerApiURLs.SESSION_TOKEN, AppPreference.getSessionToken());

		try {
			String responseData = doPost(ServerApiURLs.LOGOUT_URL, params);
			BaseBean.handleResponse(mHandler, msgWhat, responseData, null);
		} catch (Exception ex) {
			BaseBean.handleException(mHandler, ex);
		}
	}

	/**
	 * 修改密码
	 *
	 * @param mHandler
	 * @param msgWhat
	 * @param oldPassword
	 * @param newPassword
	 */
	public static void modifyPassword(Handler mHandler, int msgWhat, String oldPassword, String newPassword) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(ServerApiURLs.SESSION_TOKEN, AppPreference.getSessionToken());
		params.put(ServerApiURLs.OLD_PASSWORD, oldPassword);
		params.put(ServerApiURLs.NEW_PASSWORD, newPassword);

		try {
			String responseData = doPost(ServerApiURLs.MODIFY_PASSWORD_URL, params);
			BaseBean.handleResponse(mHandler, msgWhat, responseData, null);
		} catch (Exception ex) {
			BaseBean.handleException(mHandler, ex);
		}
	}

	/**
	 * 重置密码
	 *
	 * @param mHandler
	 * @param msgWhat
	 * @param newPassword
	 */
	public static void resetPassword(Handler mHandler, int msgWhat, String loginAccount, String newPassword) {
		Map<String, Object> params = new HashMap<String, Object>();
 		params.put(ServerApiURLs.LOGIN_ACCOUNT, loginAccount);
		params.put(ServerApiURLs.NEW_PASSWORD, newPassword);

		try {
			String responseData = doPost(ServerApiURLs.RESET_PASSWORD_URL, params);
			BaseBean.handleResponse(mHandler, msgWhat, responseData, null);
		} catch (Exception ex) {
			BaseBean.handleException(mHandler, ex);
		}
	}

	/**
	 * 根据客户的token获取安全问题
	 *
	 * @param mHandler
	 * @param msgWhat
	 */
	public static void getSafeQuestionsByToken(Handler mHandler, int msgWhat) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(ServerApiURLs.SESSION_TOKEN, AppPreference.getSessionToken());

		try {
			String responseData = doPost(ServerApiURLs.GET_SAFE_QUESTION_BY_TOKEN_URL, params);
			BaseBean.handleResponse(mHandler, msgWhat, responseData, safeQuestionParser);
		} catch (Exception ex) {
			BaseBean.handleException(mHandler, ex);
		}
	}

	/**
	 * 根据token获取所有的安全问题
	 *
	 * @param mHandler
	 * @param msgWhat
	 */
	public static void getAllSafeQuestionsByToken(Handler mHandler, int msgWhat) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(ServerApiURLs.SESSION_TOKEN, AppPreference.getSessionToken());

		try {
			String responseData = doPost(ServerApiURLs.GET_ALL_SAFE_QUESTION_BY_TOKEN, params);
			BaseBean.handleResponse(mHandler, msgWhat, responseData, safeQuestionParser);
		} catch (Exception ex) {
			BaseBean.handleException(mHandler, ex);
		}
	}

	/**
	 * 根据token验证客户的安全问题
	 *
	 * @param mHandler
	 * @param msgWhat
	 * @param secretQuestion1
	 * @param answer1
	 * @param secretQuestion2
	 * @param answer2
	 * @param secretQuestion3
	 * @param answer3
	 */
	public static void verifySafeQuestionsByToken(Handler mHandler, int msgWhat,
												  String secretQuestion1, String answer1,
												  String secretQuestion2, String answer2,
												  String secretQuestion3, String answer3) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(ServerApiURLs.SESSION_TOKEN, AppPreference.getSessionToken());
		params.put(ServerApiURLs.SECRET_QUESTION_ID1, secretQuestion1);
		params.put(ServerApiURLs.SECRET_ANSWER1, answer1);
		params.put(ServerApiURLs.SECRET_QUESTION_ID2, secretQuestion2);
		params.put(ServerApiURLs.SECRET_ANSWER2, answer2);
		params.put(ServerApiURLs.SECRET_QUESTION_ID3, secretQuestion3);
		params.put(ServerApiURLs.SECRET_ANSWER3, answer3);

		try {
			String responseData = doPost(ServerApiURLs.VERIFY_SAFE_QUESTION_BY_TOKEN_URL, params);
			BaseBean.handleResponse(mHandler, msgWhat, responseData, null);
		} catch (Exception ex) {
			BaseBean.handleException(mHandler, ex);
		}
	}

	/**
	 * 根据登录账户 设置客户的安全问题
	 *
	 * @param mHandler
	 * @param msgWhat
	 * @param secretQuestion1
	 * @param answer1
	 * @param secretQuestion2
	 * @param answer2
	 * @param secretQuestion3
	 * @param answer3
	 */
	public static void setSafeQuestionsByToken(Handler mHandler, int msgWhat,
											   String secretQuestion1, String answer1,
											   String secretQuestion2, String answer2,
											   String secretQuestion3, String answer3) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(ServerApiURLs.SESSION_TOKEN, AppPreference.getSessionToken());
		params.put(ServerApiURLs.SECRET_QUESTION_ID1, secretQuestion1);
		params.put(ServerApiURLs.SECRET_ANSWER1, answer1);
		params.put(ServerApiURLs.SECRET_QUESTION_ID2, secretQuestion2);
		params.put(ServerApiURLs.SECRET_ANSWER2, answer2);
		params.put(ServerApiURLs.SECRET_QUESTION_ID3, secretQuestion3);
		params.put(ServerApiURLs.SECRET_ANSWER3, answer3);

		try {
			String responseData = doPost(ServerApiURLs.SET_SAFE_QUESTION_BY_TOKEN_URL, params);
			BaseBean.handleResponse(mHandler, msgWhat, responseData, null);
		} catch (Exception ex) {
			BaseBean.handleException(mHandler, ex);
		}
	}

	/**
	 * 根据登录账户获取客户的安全问题
	 *
	 * @param mHandler
	 * @param msgWhat
	 * @param loginAccount
	 */
	public static void getSafeQuestionsByUsername(Handler mHandler, int msgWhat, String loginAccount) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(ServerApiURLs.LOGIN_ACCOUNT, loginAccount);

		try {
			String responseData = doPost(ServerApiURLs.GET_SAFE_QUESTION_BY_USERNAME_URL, params);
			BaseBean.handleResponse(mHandler, msgWhat, responseData, safeQuestionParser);
		} catch (Exception ex) {
			BaseBean.handleException(mHandler, ex);
		}
	}

	/**
	 * 根据登录账户验证客户的安全问题
	 *
	 * @param mHandler
	 * @param msgWhat
	 * @param loginAccount
	 * @param secretQuestion1
	 * @param answer1
	 * @param secretQuestion2
	 * @param answer2
	 * @param secretQuestion3
	 * @param answer3
	 */
	public static void verifySafeQuestionsByUsername(Handler mHandler, int msgWhat, String loginAccount,
													 String secretQuestion1, String answer1,
													 String secretQuestion2, String answer2,
													 String secretQuestion3, String answer3) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(ServerApiURLs.LOGIN_ACCOUNT, loginAccount);
		params.put(ServerApiURLs.SECRET_QUESTION_ID1, secretQuestion1);
		params.put(ServerApiURLs.SECRET_ANSWER1, answer1);
		params.put(ServerApiURLs.SECRET_QUESTION_ID2, secretQuestion2);
		params.put(ServerApiURLs.SECRET_ANSWER2, answer2);
		params.put(ServerApiURLs.SECRET_QUESTION_ID3, secretQuestion3);
		params.put(ServerApiURLs.SECRET_ANSWER3, answer3);

		try {
			String responseData = doPost(ServerApiURLs.VERIFY_SAFE_QUESTION_BY_USERNAME_URL, params);
			BaseBean.handleResponse(mHandler, msgWhat, responseData, null);
		} catch (Exception ex) {
			BaseBean.handleException(mHandler, ex);
		}
	}

	/**
	 * 添加用户反馈
	 *
	 * @param mHandler
	 * @param msgWhat
	 * @param feedback
	 * @param contact
	 */
	public static void addUserFeedback(Handler mHandler, int msgWhat, String feedback, String contact) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(ServerApiURLs.SESSION_TOKEN, AppPreference.getSessionToken());
		params.put(ServerApiURLs.FEEDBACK, feedback);
		params.put(ServerApiURLs.CONTACT, contact);

		try {
			String responseData = doPost(ServerApiURLs.USER_FEEDBACK_URL, params);
			BaseBean.handleResponse(mHandler, msgWhat, responseData, null);
		} catch (Exception ex) {
			BaseBean.handleException(mHandler, ex);
		}
	}

	/**
	 * 添加nfc card
	 *
	 * @param mHandler
	 * @param msgWhat
	 * @param keyId
	 */
	public static void addNfcCard(Handler mHandler, int msgWhat, String keyId) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(ServerApiURLs.SESSION_TOKEN, AppPreference.getSessionToken());
		params.put(ServerApiURLs.NFC_CARD_KEY_ID, keyId);

		try {
			String responseData = doPost(ServerApiURLs.NFC_CARD_ADD_URL, params);
			BaseBean.handleResponse(mHandler, msgWhat, responseData, null);
		} catch (Exception ex) {
			BaseBean.handleException(mHandler, ex);
		}
	}

	/**
	 *
	 * 获取nfc card的列表
	 *
	 * @param mHandler
	 * @param msgWhat
	 * @param pageNo
	 * @param pageSize
	 */
	public static void getNfcCardList(Handler mHandler, int msgWhat, int pageNo, int pageSize) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(ServerApiURLs.SESSION_TOKEN, AppPreference.getSessionToken());
		params.put(PAGE_NO, "" + pageNo);
		params.put(PAGE_SIZE, "" + pageSize);

		try {
			String responseData = doPost(ServerApiURLs.NFC_CARD_LIST_URL, params);
			BaseBean.handleResponse(mHandler, msgWhat, responseData, nfcCardParser);
		} catch (Exception ex) {
			BaseBean.handleException(mHandler, ex);
		}
	}

	/**
	 *
	 * 删除nfc card的列表
	 *
	 * @param mHandler
	 * @param msgWhat
	 * @param keyId
	 */
	public static void deleteNfcCard(Handler mHandler, int msgWhat, String keyId) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(ServerApiURLs.SESSION_TOKEN, AppPreference.getSessionToken());
		params.put(ServerApiURLs.NFC_CARD_KEY_ID, keyId);

		try {
			String responseData = doPost(ServerApiURLs.NFC_CARD_DELETE_URL, params);
			BaseBean.handleResponse(mHandler, msgWhat, responseData, null);
		} catch (Exception ex) {
			BaseBean.handleException(mHandler, ex);
		}
	}

	/**
	 * 获取领用人列表
	 *
	 * @param mHandler
	 * @param msgWhat
	 */
	public static void getOwnerList(Handler mHandler, int msgWhat) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(ServerApiURLs.SESSION_TOKEN, AppPreference.getSessionToken());

		try {
			String responseData = doPost(ServerApiURLs.OWNER_LIST_URL, params);
			BaseBean.handleResponse(mHandler, msgWhat, responseData, ownerListParser);
		} catch (Exception ex) {
			BaseBean.handleException(mHandler, ex);
		}
	}

	/**
	 * 添加领用人
	 *
	 * @param mHandler
	 * @param msgWhat
	 * @param englishName
	 */
	public static void addOwner(Handler mHandler, int msgWhat, String englishName) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(ServerApiURLs.SESSION_TOKEN, AppPreference.getSessionToken());
		params.put(ServerApiURLs.ENGLISH_NAME, englishName);

		try {
			String responseData = doPost(ServerApiURLs.ADD_OWNER_URL, params);
			BaseBean.handleResponse(mHandler, msgWhat, responseData, null);
		} catch (Exception ex) {
			BaseBean.handleException(mHandler, ex);
		}
	}

	/**
	 * 领用人领用卡
	 *
	 * @param mHandler
	 * @param msgWhat
	 * @param ownerId
	 * @param keyId
	 */
	public static void addOwnerToNfcCard(Handler mHandler, int msgWhat, String ownerId, String keyId) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(ServerApiURLs.SESSION_TOKEN, AppPreference.getSessionToken());
		params.put(ServerApiURLs.OWNER_ID, ownerId);
		params.put(ServerApiURLs.NFC_CARD_KEY_ID, keyId);

		try {
			String responseData = doPost(ServerApiURLs.ADD_OWNER_TO_NFCCARD_URL, params);
			BaseBean.handleResponse(mHandler, msgWhat, responseData, null);
		} catch (Exception ex) {
			BaseBean.handleException(mHandler, ex);
		}
	}

	/**
	 * 添加摘机数据列表
	 *
	 * @param mHandler
	 * @param msgWhat
	 * @param uuid
	 * @param baseId
	 * @param deviceBrand
	 * @param deviceModel
	 * @param deviceImei
	 * @param offhookTimes
	 */
	public static void addOffhookTimesByToken(Handler mHandler, int msgWhat,
											  String uuid, String baseId, String deviceBrand, String deviceModel,
											  String deviceImei, List<OffhookTime> offhookTimes) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(ServerApiURLs.SESSION_TOKEN, AppPreference.getSessionToken());
		params.put(ServerApiURLs.UUID, uuid);
		params.put(ServerApiURLs.BASE_ID, baseId);
		params.put(ServerApiURLs.DEVICE_BRAND, deviceBrand);
		params.put(ServerApiURLs.DEVICE_MODEL, deviceModel);
		params.put(ServerApiURLs.DEVICE_IMEI, deviceImei);
		params.put(ServerApiURLs.OFFHOOKE_TIMES, JSON.toJSONString(offhookTimes));

		try {
			String responseData = doPost(ServerApiURLs.ADD_OFFHOOK_TIMES_URL, params);
			BaseBean.handleResponse(mHandler, msgWhat, responseData, null);
		} catch (Exception ex) {
			BaseBean.handleException(mHandler, ex);
		}
	}

	/**
	 * 新增报警数据
	 *
	 * @param mHandler
	 * @param msgWhat
	 * @param uuid
	 * @param baseId
	 * @param alarms
	 */
	public static void addAlarmDataByToken(Handler mHandler, int msgWhat, String uuid, String baseId, List<AlarmTime> alarms) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(ServerApiURLs.SESSION_TOKEN, AppPreference.getSessionToken());
		params.put(ServerApiURLs.UUID, uuid);
		params.put(ServerApiURLs.BASE_ID, baseId);
		params.put(ServerApiURLs.ALARM_TIMES, JSON.toJSONString(alarms));

		try {
			String responseData = doPost(ServerApiURLs.ADD_ALARM_DATA_URL, params);
			BaseBean.handleResponse(mHandler, msgWhat, responseData, null);
		} catch (Exception ex) {
			BaseBean.handleException(mHandler, ex);
		}
	}

	/**
	 * 获取报警数据列表
	 *
	 * @param mHandler
	 * @param msgWhat
	 * @param baseId
	 * @param timeType
	 */
	public static void getAlarmData(Handler mHandler, int msgWhat, String baseId, int timeType, int pageNo, int pageSize) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(ServerApiURLs.SESSION_TOKEN, AppPreference.getSessionToken());
		params.put(ServerApiURLs.BASE_ID, baseId);
		params.put(ServerApiURLs.TIME_TYPE, timeType);
		params.put(PAGE_NO, String.valueOf(pageNo));
		params.put(PAGE_SIZE, String.valueOf(pageSize));

		try {
			String responseData = doPost(ServerApiURLs.GET_ALARM_DATA_LIST_URL, params);
			BaseBean.handleResponse(mHandler, msgWhat, responseData, alarmListParser);
		} catch (Exception ex) {
			BaseBean.handleException(mHandler, ex);
		}
	}

	/**
	 * 获取销售商品列表
	 *
	 * @param mHandler
	 * @param msgWhat
	 * @param searchContent
	 */
	public static void getProducts(Handler mHandler, int msgWhat, String searchContent) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(ServerApiURLs.SESSION_TOKEN, AppPreference.getSessionToken());
		params.put(ServerApiURLs.SEARCH_CONTENT, searchContent);

		try {
			String responseData = doPost(ServerApiURLs.GET_PRODUCT_LIST_URL, params);
			BaseBean.handleResponse(mHandler, msgWhat, responseData, productListParser);
		} catch (Exception ex) {
			BaseBean.handleException(mHandler, ex);
		}
	}

	/**
	 * 查询商品销售数据
	 *
	 * @param mHandler
	 * @param msgWhat
	 * @param queryDate
	 */
	public static void getSalesData(Handler mHandler, int msgWhat, long queryDate, int pageNo, int pageSize) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(ServerApiURLs.SESSION_TOKEN, AppPreference.getSessionToken());
		params.put(ServerApiURLs.SALES_DATE, queryDate);
		params.put(PAGE_NO, String.valueOf(pageNo));
		params.put(PAGE_SIZE, String.valueOf(pageSize));

		try {
			String responseData = doPost(ServerApiURLs.GET_SALES_DATA_URL, params);
			BaseBean.handleResponse(mHandler, msgWhat, responseData, salesListParser);
		} catch (Exception ex) {
			BaseBean.handleException(mHandler, ex);
		}
	}

	/**
	 * 批量修改门店商品销量
	 *
	 * @param mHandler
	 * @param msgWhat
	 * @param sales
	 */
	public static void setSalesData(Handler mHandler, int msgWhat, long salesDate, List<Sales> sales) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(ServerApiURLs.SESSION_TOKEN, AppPreference.getSessionToken());
		params.put(ServerApiURLs.SALES_DATE, salesDate);
		params.put(ServerApiURLs.PRODUCT_SALES, JSON.toJSONString(sales));

		try {
			String responseData = doPost(ServerApiURLs.SET_SALES_DATA_URL, params);
			BaseBean.handleResponse(mHandler, msgWhat, responseData, null);
		} catch (Exception ex) {
			BaseBean.handleException(mHandler, ex);
		}
	}

	/**
	 * 报表 获取摘机数据
	 *
	 * @param mHandler
	 * @param msgWhat
	 * @param timeType
	 */
	public static void getReportPickupRates(Handler mHandler, int msgWhat, int timeType) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(ServerApiURLs.SESSION_TOKEN, AppPreference.getSessionToken());
		params.put(ServerApiURLs.TIME_TYPE, timeType);

		try {
			String responseData = doPost(ServerApiURLs.REPORT_GET_PICKUP_RATE_URL, params);
			BaseBean.handleResponse(mHandler, msgWhat, responseData, pickupRateParser);
		} catch (Exception ex) {
			BaseBean.handleException(mHandler, ex);
		}
	}

	/**
	 * 报表 获取摘机时间数据
	 *
	 * @param mHandler
	 * @param msgWhat
	 * @param timeType
	 */
	public static void getReportPickupAvgTimes(Handler mHandler, int msgWhat, int timeType) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(ServerApiURLs.SESSION_TOKEN, AppPreference.getSessionToken());
		params.put(ServerApiURLs.TIME_TYPE, timeType);

		try {
			String responseData = doPost(ServerApiURLs.REPORT_GET_AVERAGE_PICKUP_TIME_URL, params);
			BaseBean.handleResponse(mHandler, msgWhat, responseData, pickupTimeParser);
		} catch (Exception ex) {
			BaseBean.handleException(mHandler, ex);
		}
	}

	/**
	 * 报表 获取商品销量数据
	 *
	 * @param mHandler
	 * @param msgWhat
	 * @param timeType
	 */
	public static void getReportSalesVolume(Handler mHandler, int msgWhat, int timeType) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(ServerApiURLs.SESSION_TOKEN, AppPreference.getSessionToken());
		params.put(ServerApiURLs.TIME_TYPE, timeType);

		try {
			String responseData = doPost(ServerApiURLs.REPORT_GET_SALES_RATE_URL, params);
			BaseBean.handleResponse(mHandler, msgWhat, responseData, salesRateParser);
		} catch (Exception ex) {
			BaseBean.handleException(mHandler, ex);
		}
	}

	/**
	 * 更新 检查客户端版本
	 *
	 * @param mHandler
	 * @param msgWhat
	 * @param versionCode
	 */
	public static void checkUpdate(Handler mHandler, int msgWhat, int versionCode) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(ServerApiURLs.SESSION_TOKEN, AppPreference.getSessionToken());
		params.put(ServerApiURLs.VERSION_CODE, versionCode);

		try {
			String responseData = doPost(ServerApiURLs.CHECK_UPDATE_URL, params);
			BaseBean.handleResponse(mHandler, msgWhat, responseData, updateParser);
		} catch (Exception ex) {
			BaseBean.handleException(mHandler, ex);
		}
	}

	public static void cleanCookie() {
		appCookie = "";
	}

	private static String getCookie(AppContext appContext) {
		if (appCookie == null || appCookie == "") {
			appCookie = appContext.getProperty("cookie", "");
		}
		return appCookie;
	}

	private static String getUserAgent(AppContext appContext) {
		if (appUserAgent == null || appUserAgent == "") {
			StringBuilder ua = new StringBuilder("OSChina.NET");
			ua.append('/' + appContext.getPackageInfo().versionName + '_' + appContext.getPackageInfo().versionCode);// App版本
			ua.append("/Android");// 手机系统平台
			ua.append("/" + android.os.Build.VERSION.RELEASE);// 手机系统版本
			ua.append("/" + android.os.Build.MODEL); // 手机型号
			ua.append("/" + appContext.getAppId());// 客户端唯一标识
			appUserAgent = ua.toString();
		}
		return appUserAgent;
	}

	private static HttpClient getHttpClient() {
		HttpClient httpClient = new HttpClient();
		// 设置 HttpClient 接收 Cookie,用与浏览器一样的策略
		httpClient.getParams().setCookiePolicy(CookiePolicy.BROWSER_COMPATIBILITY);
		// 设置 默认的超时重试处理策略
		httpClient.getParams().setParameter(HttpMethodParams.RETRY_HANDLER, new DefaultHttpMethodRetryHandler());
		// 设置 连接超时时间
		httpClient.getHttpConnectionManager().getParams().setConnectionTimeout(TIMEOUT_CONNECTION);
		// 设置 读数据超时时间
		httpClient.getHttpConnectionManager().getParams().setSoTimeout(TIMEOUT_SOCKET);
		// 设置 字符集
		httpClient.getParams().setContentCharset(UTF_8);
		return httpClient;
	}

	private static GetMethod getHttpGet(String url, String cookie, String userAgent) {
		GetMethod httpGet = new GetMethod(url);
		// 设置 请求超时时间
		httpGet.getParams().setSoTimeout(TIMEOUT_SOCKET);
		httpGet.setRequestHeader("Host", ServerApiURLs.HOST);
		httpGet.setRequestHeader("Connection", "Keep-Alive");
		httpGet.setRequestHeader("Cookie", cookie);
		httpGet.setRequestHeader("User-Agent", userAgent);
		return httpGet;
	}

	private static PostMethod getHttpPost(String url, String cookie, String userAgent) {
		PostMethod httpPost = new PostMethod(url);
		// 设置 请求超时时间
		httpPost.getParams().setSoTimeout(TIMEOUT_SOCKET);
		httpPost.setRequestHeader("Host", ServerApiURLs.HOST);
		httpPost.setRequestHeader("Connection", "Keep-Alive");
		httpPost.setRequestHeader("Cookie", cookie);
		httpPost.setRequestHeader("User-Agent", userAgent);
		return httpPost;
	}

	public static String doPost(String url, Map<String, Object> params) throws AppException {
		AppContext appContext = AppContext.getAppContext();
		String cookie = getCookie(appContext);
		String userAgent = getUserAgent(appContext);

		HttpClient httpClient = null;
		PostMethod httpPost = null;
		Part[] parts = new Part[0];
		String responseBody = "";

		// post表单参数处理
		Log.d(TAG, "doPost: url=" + url);
		if (params != null) {
			parts = new Part[params.size()];
			int i = 0;
			for (String name : params.keySet()) {
				parts[i++] = new StringPart(name, String.valueOf(params.get(name)), UTF_8);
				Log.d(TAG, "doPost: post_key=" + name + ",value=" + String.valueOf(params.get(name)));
			}
		}

		for (int time = 0; time < RETRY_TIME;) {
			try {
				httpClient = getHttpClient();
				httpPost = getHttpPost(url, cookie, userAgent);
				httpPost.setRequestEntity(new MultipartRequestEntity(parts, httpPost.getParams()));
				int statusCode = httpClient.executeMethod(httpPost);
				Log.d(TAG, "doPost: statusCode:" + statusCode);
				if (statusCode != HttpStatus.SC_OK) {
					throw AppException.http(statusCode);
				} else if (statusCode == HttpStatus.SC_OK) {
					Cookie[] cookies = httpClient.getState().getCookies();
					String tmpcookies = "";
					for (Cookie ck : cookies) {
						tmpcookies += ck.toString() + ";";
					}
					// 保存cookie
					if (appContext != null && tmpcookies != "") {
						appContext.setProperty("cookie", tmpcookies);
						appCookie = tmpcookies;
					}
				}
				responseBody = httpPost.getResponseBodyAsString();
				break;
			} catch (HttpException e) {
				if (++time < RETRY_TIME) {
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e1) {
					}
					continue;
				}
				// 发生致命的异常，可能是协议不对或者返回的内容有问题
				e.printStackTrace();
				throw AppException.http(e);
			} catch (IOException e) {
				if (++time < RETRY_TIME) {
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e1) {
					}
					continue;
				}
				// 发生网络异常
				e.printStackTrace();
				throw AppException.network(e);
			} finally {
				// 释放连接
				httpPost.releaseConnection();
				httpClient = null;
			}
		}

		responseBody = responseBody.replaceAll("\\p{Cntrl}", "");
		Log.d(TAG, "doPost: responseBody:" + responseBody);
		return responseBody;
	}

	public static String doGet(String url) throws AppException {
		AppContext appContext = AppContext.getAppContext();
		String cookie = getCookie(appContext);
		String userAgent = getUserAgent(appContext);
		HttpClient httpClient = null;
		GetMethod httpGet = null;
		String responseBody = "";
		for (int time = 0; time < RETRY_TIME;) {
			try {
				httpClient = getHttpClient();
				httpGet = getHttpGet(url, cookie, userAgent);
				int statusCode = httpClient.executeMethod(httpGet);
				if (statusCode != HttpStatus.SC_OK) {
					throw AppException.http(statusCode);
				}
				responseBody = httpGet.getResponseBodyAsString();
				break;
			} catch (HttpException e) {
				time++;
				if (time < RETRY_TIME) {
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e1) {
					}
					continue;
				}
				// 发生致命的异常，可能是协议不对或者返回的内容有问题
				e.printStackTrace();
				throw AppException.http(e);
			} catch (IOException e) {
				time++;
				if (time < RETRY_TIME) {
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e1) {
					}
					continue;
				}
				// 发生网络异常
				e.printStackTrace();
				throw AppException.network(e);
			} finally {
				// 释放连接
				httpGet.releaseConnection();
				httpClient = null;
			}
		}

		responseBody = responseBody.replaceAll("\\p{Cntrl}", "");
		Log.d(TAG, "doGet: responseBody:" + responseBody);
		return responseBody;
	}

}
