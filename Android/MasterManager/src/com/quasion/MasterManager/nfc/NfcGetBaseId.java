package com.quasion.MasterManager.nfc;

/**
 * Created by Gebing on 2014/7/12.
 */
public class NfcGetBaseId extends NfcBaseMessage {
	static protected final int BASE_ID_LENGTH = 4*2;
	public String nfcBaseId;

	@Override
	public boolean isValid() {
		return nfcBaseId != null && nfcBaseId.length() == BASE_ID_LENGTH;
	}

	@Override
	protected int fromNdefData(String ndefData) {
		// 处理NFC Base Id数据
		if (ndefData != null && CMD_FIELD_LENGTH + BASE_ID_LENGTH <= ndefData.length()) {
			nfcBaseId = ndefData.substring(CMD_FIELD_LENGTH, CMD_FIELD_LENGTH + BASE_ID_LENGTH);
			return CMD_FIELD_LENGTH + BASE_ID_LENGTH;
		}
		// 无效数据
		nfcBaseId = null;
		return -1;
	}

	@Override
	protected String toNdefData() {
		return CMD_TO_BASE_GET_BASE_ID;
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + '{' +
				(isValid() ? "nfcBaseId='" + nfcBaseId + '\'' : "") +
				'}';
	}
}
