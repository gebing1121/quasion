package com.quasion.MasterManager.nfc;

import com.quasion.MasterManager.util.StringUtil;

/**
 * Created by Gebing on 2014/7/12.
 */
public class NfcSetKeyIds extends NfcGetKeyIds {

	public NfcSetKeyIds(String nfcKeyIds) {
		if (StringUtil.isEmpty(nfcKeyIds)) {
			this.nfcKeyCnt = 0;
			this.nfcKeyIds = "";
		} else {
			this.nfcKeyCnt = (nfcKeyIds.length() / KEY_ID_LENGTH) & KEY_ID_MAXIMUM;
			this.nfcKeyIds = nfcKeyIds.substring(0, nfcKeyCnt * KEY_ID_LENGTH);
		}
	}

	@Override
	protected int fromNdefData(String ndefData) {
		// NFC Base不会返回该类型的数据
		return -1;
	}

	@Override
	protected String toNdefData() {
		if (!isValid()) return "";
		// 命令类型同时表示KeyId的数量
		return StringUtil.toHex((byte) nfcKeyCnt) + nfcKeyIds;
	}
}
