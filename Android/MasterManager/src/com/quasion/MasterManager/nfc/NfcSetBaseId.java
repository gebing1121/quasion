package com.quasion.MasterManager.nfc;

/**
 * Created by Gebing on 2014/7/12.
 */
public class NfcSetBaseId extends NfcGetBaseId {
	public NfcSetBaseId(String nfcBaseId) {
		if (nfcBaseId == null || nfcBaseId.length() < BASE_ID_LENGTH) {
			this.nfcBaseId = null;
		} else {
			this.nfcBaseId = nfcBaseId.substring(0, BASE_ID_LENGTH);
		}
	}

	@Override
	protected int fromNdefData(String ndefData) {
		// NFC Base不会返回该类型的数据
		return -1;
	}

	@Override
	protected String toNdefData() {
		if (!isValid()) return "";
		return CMD_TO_BASE_SET_BASE_ID + nfcBaseId;
	}
}
