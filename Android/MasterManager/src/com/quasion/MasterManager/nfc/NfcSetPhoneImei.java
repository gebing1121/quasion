package com.quasion.MasterManager.nfc;

import com.quasion.MasterManager.util.StringUtil;

/**
 * Created by Gebing on 2014/7/12.
 */
public class NfcSetPhoneImei extends NfcGetPhoneImei {
	public NfcSetPhoneImei(String phoneImei) {
		if (phoneImei == null || phoneImei.length() == 0) {
			this.phoneImei = null;
		} else {
			String fill = StringUtil.fill(' ', PHONE_IMEI_LENGTH - phoneImei.length());
			this.phoneImei = phoneImei.concat(fill).substring(0, PHONE_IMEI_LENGTH);
		}
	}

	@Override
	protected int fromNdefData(String ndefData) {
		// NFC Base不会返回该类型的数据
		return -1;
	}

	@Override
	protected String toNdefData() {
		if (!isValid()) return "";
		return CMD_TO_BASE_SET_IMEI + phoneImei;
	}
}
