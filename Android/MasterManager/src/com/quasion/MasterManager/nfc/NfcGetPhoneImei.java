package com.quasion.MasterManager.nfc;

/**
 * Created by Gebing on 2014/7/12.
 */
public class NfcGetPhoneImei extends NfcBaseMessage {
	static protected final int PHONE_IMEI_LENGTH = 14;
	public String phoneImei;

	@Override
	public boolean isValid() {
		return phoneImei != null && phoneImei.length() == PHONE_IMEI_LENGTH;
	}

	@Override
	protected int fromNdefData(String ndefData) {
		// 处理手机IMEI数据
		if (ndefData != null && CMD_FIELD_LENGTH + PHONE_IMEI_LENGTH <= ndefData.length()) {
			phoneImei = ndefData.substring(CMD_FIELD_LENGTH, CMD_FIELD_LENGTH + PHONE_IMEI_LENGTH);
			return CMD_FIELD_LENGTH + PHONE_IMEI_LENGTH;
		}
		// 无效数据
		phoneImei = null;
		return -1;
	}

	@Override
	protected String toNdefData() {
		return CMD_TO_BASE_GET_IMEI;
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + '{' +
				(isValid() ? "phoneImei='" + phoneImei + '\'' : "") +
				'}';
	}
}
