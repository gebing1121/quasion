package com.quasion.MasterManager.nfc;

import com.quasion.MasterManager.bean.AlarmTime;
import com.quasion.MasterManager.util.StringUtil;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Gebing on 2014/7/12.
 */
public class NfcGetAlarmData extends NfcBaseMessage {
	static protected final int ALARM_TIME_LENGTH = 4*2;
	static protected final int ALARM_DURATION_LENGTH = 2*2;
	static protected final int DATA_UNLOCK_LENGTH = 4*2;
	static protected final int ALARM_DATA_LENGTH = ALARM_TIME_LENGTH + ALARM_DURATION_LENGTH + DATA_UNLOCK_LENGTH;
	static protected final String ALARM_DATA_END = StringUtil.fill('F', ALARM_DATA_LENGTH);

	public List<AlarmTime> alarmData;

	@Override
	public boolean isValid() {
		return alarmData != null;
	}

	@Override
	protected int fromNdefData(String ndefData) {
		// 处理报警数据
		if (ndefData != null) {
			alarmData = new ArrayList<AlarmTime>();
			for (int pos = CMD_FIELD_LENGTH; pos + ALARM_DATA_LENGTH <= ndefData.length(); pos += ALARM_DATA_LENGTH) {
				String data = ndefData.substring(pos, pos + ALARM_DATA_LENGTH);
				// 必须以10个FF结束
				if (ALARM_DATA_END.equalsIgnoreCase(data)) {
					return pos + ALARM_DATA_LENGTH;
				}
				// 获取报警数据
				AlarmTime alarmTime = new AlarmTime();
				alarmTime.setAlarmTime(Long.valueOf(data.substring(0, ALARM_TIME_LENGTH), 16) * 1000);		// 转换秒到毫秒
				alarmTime.setTimes(Integer.valueOf(data.substring(ALARM_TIME_LENGTH, ALARM_TIME_LENGTH + ALARM_DURATION_LENGTH), 16));
				alarmTime.setUnlockNfccardId(data.substring(ALARM_TIME_LENGTH + ALARM_DURATION_LENGTH));
				alarmData.add(alarmTime);
				// 判断是否结束？
				if (pos + ALARM_DATA_LENGTH == ndefData.length()) {
					return pos + ALARM_DATA_LENGTH;
				}
			}
		}
		// 无效数据
		alarmData = null;
		return -1;
	}

	@Override
	protected String toNdefData() {
		return CMD_TO_BASE_GET_ALARM_DATA;
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + '{' +
				(isValid() ? "alarmData=" + alarmData : "") +
				'}';
	}
}
