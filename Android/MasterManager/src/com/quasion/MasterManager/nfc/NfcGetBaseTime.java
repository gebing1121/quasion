package com.quasion.MasterManager.nfc;

import com.quasion.MasterManager.util.DateUtil;

/**
 * Created by Gebing on 2014/7/12.
 */
public class NfcGetBaseTime extends NfcBaseMessage {
	static protected final int BASE_TIME_LENGTH = 4*2;
	public long nfcBaseTime = -1;

	@Override
	public boolean isValid() {
		return nfcBaseTime > 0;
	}

	@Override
	protected int fromNdefData(String ndefData) {
		// 处理NFC Base Id数据
		if (ndefData != null && CMD_FIELD_LENGTH + BASE_TIME_LENGTH <= ndefData.length()) {
			String data = ndefData.substring(CMD_FIELD_LENGTH, CMD_FIELD_LENGTH + BASE_TIME_LENGTH);
			nfcBaseTime = Long.valueOf(data, 16);
			nfcBaseTime *= 1000;
			return CMD_FIELD_LENGTH + BASE_TIME_LENGTH;
		}
		// 无效数据
		nfcBaseTime = -1;
		return -1;
	}

	@Override
	protected String toNdefData() {
		return CMD_TO_BASE_GET_BASE_TIME;
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + '{' +
				(isValid() ? "nfcBaseTime=" + nfcBaseTime + ',' + DateUtil.formatTime(nfcBaseTime) : "" ) +
				'}';
	}
}
