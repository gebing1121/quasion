package com.quasion.MasterManager.nfc;

import com.quasion.MasterManager.util.StringUtil;

/**
 * Created by Gebing on 2014/7/12.
 */
public class NfcSetBaseTime {
	static protected String toNdefData(long baseTime) {
		return NfcBaseMessage.CMD_TO_BASE_SET_BASE_TIME + StringUtil.toHex(baseTime / 1000, 8);
	}
}
