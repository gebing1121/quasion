package com.quasion.MasterManager.app;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Application;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;

import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import com.quasion.MasterManager.bean.Product;
import com.quasion.MasterManager.util.DatabaseUtil;
import com.quasion.MasterManager.util.DateUtil;
import com.quasion.MasterManager.util.StringUtil;

public class AppContext extends Application {
	private static final String TAG = AppContext.class.getSimpleName();
	private static final String DB_NAME = "quasion.db";
	private static final int DB_VERSION = 1;
	private static AppContext appContext;

	// 全局常量
	public static List<Product> productList;

	@Override
	public void onCreate() {
		super.onCreate();
		appContext = this;
		DatabaseUtil.setDatabaseParameter(DB_NAME, DB_VERSION, null);
//		registerActivityLifecycleCallbacks(new ActivityLifecycleLogger());
	}

	/**
	 * 返回ApplicationContext
	 * @return
	 */
	public static AppContext getAppContext() {
		return appContext;
	}

	/**
	 * 获取App安装包信息
	 *
	 * @return
	 */
	public PackageInfo getPackageInfo() {
		PackageInfo info = null;
		try {
			info = getPackageManager().getPackageInfo(getPackageName(), 0);
		} catch (NameNotFoundException e) {
			e.printStackTrace(System.err);
		}
		if (info == null)
			info = new PackageInfo();
		return info;
	}

	/**
	 * 获取App唯一标识
	 *
	 * @return
	 */
	public String getAppId() {
		String appId = getProperty(AppPreference.APP_ID, null);
		if (StringUtil.isEmpty(appId)) {
			appId = UUID.randomUUID().toString();
			setProperty(AppPreference.APP_ID, appId);
		}
		return appId;
	}

	public void setProperty(String key, String value) {
		AppPreference.setProperty(key, value);
	}

	public String getProperty(String key, String defaultValue) {
		return AppPreference.getProperty(key, defaultValue);
	}

	@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
	static class ActivityLifecycleLogger implements ActivityLifecycleCallbacks {
		@Override
		public void onActivityCreated(Activity activity , Bundle bundle) {
			Log.d(TAG, "onActivityCreated(" + activity + "," + bundle + ")");
		}
		@Override
		public void onActivityDestroyed(Activity activity) {
			Log.d(TAG, "onActivityDestroyed(" + activity + ")");
		}
		@Override
		public void onActivityPaused(Activity activity) {
			Log.d(TAG, "onActivityPaused(" + activity + ")");
		}
		@Override
		public void onActivityResumed(Activity activity) {
			Log.d(TAG, "onActivityResumed(" + activity + ")");
		}
		@Override
		public void onActivitySaveInstanceState(Activity activity , Bundle bundle) {
			Log.d(TAG, "onActivitySaveInstanceState(" + activity + "," + bundle + ")");
		}
		@Override
		public void onActivityStarted(Activity activity) {
			Log.d(TAG, "onActivityStarted(" + activity + ")");
		}
		@Override
		public void onActivityStopped(Activity activity) {
			Log.d(TAG, "onActivityStopped(" + activity + ")");
		}
	}
}
