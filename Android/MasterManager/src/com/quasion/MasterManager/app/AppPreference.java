package com.quasion.MasterManager.app;

import android.content.Context;
import android.content.SharedPreferences;

import com.quasion.MasterManager.bean.User;

public class AppPreference {
	private static final String PREFERENCE_NAME = "quasion";
	private static final String USER_NAME = "UserName";
	private static final String SESSION_TOKEN = "SessionToken";
	private static final String NFC_CARDS = "NfcCardIds";

	public static final String APP_ID = "AppId";

	public static SharedPreferences getSharedPreference() {
		AppContext appContext = AppContext.getAppContext();
		SharedPreferences sharedPreferences = appContext.getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE);
		return sharedPreferences;
	}

	public static SharedPreferences.Editor getSharedPreferenceEditor() {
		SharedPreferences sharedPreferences = getSharedPreference();
		return sharedPreferences.edit();
	}

	public static void saveUserInfo(User user) {
		SharedPreferences.Editor editor = getSharedPreferenceEditor();
		editor.putString(USER_NAME, user.getLoginAccount());
		editor.putString(SESSION_TOKEN, user.getSessionId());
		editor.commit();
	}

	public static String getUsername() {
		SharedPreferences sharedPreferences = getSharedPreference();
		String username = sharedPreferences.getString(USER_NAME, null);
		return username;
	}

	public static String getSessionToken() {
		SharedPreferences sharedPreferences = getSharedPreference();
		String sessionToken = sharedPreferences.getString(SESSION_TOKEN, null);
		return sessionToken;
	}

	// 保存NFC CARD 的长度和cardId的字符串
	public static void saveNfcCards(String cards) {
		SharedPreferences.Editor editor = getSharedPreferenceEditor();
		editor.putString(NFC_CARDS, cards);
		editor.commit();
	}

	public static String getNfcCards() {
		SharedPreferences sharedPreferences = getSharedPreference();
		String nfcCards = sharedPreferences.getString(NFC_CARDS, null);
		return nfcCards;
	}

	public static void setProperty(String key, String value) {
		SharedPreferences.Editor editor = getSharedPreferenceEditor();
		editor.putString(key, value);
		editor.commit();
	}

	public static String getProperty(String key, String defaultValue) {
		SharedPreferences sharedPreferences = getSharedPreference();
		return sharedPreferences.getString(key, defaultValue);
	}
}