package com.quasion.MasterManager.app;

import java.io.IOException;
import java.lang.Thread.UncaughtExceptionHandler;
import java.net.ConnectException;
import java.net.SocketException;
import java.net.UnknownHostException;

import org.apache.commons.httpclient.HttpException;

import android.content.Context;

import com.quasion.MasterManager.R;

/**
 * 应用程序异常类：用于捕获异常和提示错误信息
 *
 * @author liux (http://my.oschina.net/liux)
 * @version 1.0
 * @created 2012-3-21
 */
public class AppException extends Exception implements UncaughtExceptionHandler {
	/** 定义异常类型 */
	public final static int TYPE_NETWORK = 0x01;
	public final static int TYPE_SOCKET = 0x02;
	public final static int TYPE_HTTP_CODE = 0x03;
	public final static int TYPE_HTTP_ERROR = 0x04;
	public final static int TYPE_XML = 0x05;
	public final static int TYPE_IO = 0x06;
	public final static int TYPE_RUN = 0x07;

	private int type;
	private int code;

	/** 系统默认的UncaughtException处理类 */
	private Thread.UncaughtExceptionHandler mDefaultHandler;

	private AppException() {
		this.mDefaultHandler = Thread.getDefaultUncaughtExceptionHandler();
	}

	private AppException(int type, int code, Exception ex) {
		super(ex);
		this.type = type;
		this.code = code;
	}

	public int getCode() {
		return code;
	}

	public int getType() {
		return type;
	}

	public String getMessage(Context ctx) {
		switch (getType()) {
		case TYPE_HTTP_CODE:
			return ctx.getString(R.string.http_status_error, this.getCode());
		case TYPE_HTTP_ERROR:
			return ctx.getString(R.string.http_exception_error);
		case TYPE_SOCKET:
			return ctx.getString(R.string.socket_exception_error);
		case TYPE_NETWORK:
			return ctx.getString(R.string.network_not_connected);
		case TYPE_XML:
			return ctx.getString(R.string.xml_parser_failed);
		case TYPE_IO:
			return ctx.getString(R.string.io_exception_error);
		case TYPE_RUN:
		default:
			return ctx.getString(R.string.app_run_code_error);
		}
	}

	public static AppException http(int code) {
		return new AppException(TYPE_HTTP_CODE, code, null);
	}

	public static AppException http(Exception e) {
		return new AppException(TYPE_HTTP_ERROR, 0, e);
	}

	public static AppException socket(Exception e) {
		return new AppException(TYPE_SOCKET, 0, e);
	}

	public static AppException io(Exception e) {
		if (e instanceof UnknownHostException || e instanceof ConnectException) {
			return new AppException(TYPE_NETWORK, 0, e);
		} else if (e instanceof IOException) {
			return new AppException(TYPE_IO, 0, e);
		}
		return run(e);
	}

	public static AppException xml(Exception e) {
		return new AppException(TYPE_XML, 0, e);
	}

	public static AppException network(Exception e) {
		if (e instanceof UnknownHostException || e instanceof ConnectException) {
			return new AppException(TYPE_NETWORK, 0, e);
		} else if (e instanceof HttpException) {
			return http(e);
		} else if (e instanceof SocketException) {
			return socket(e);
		}
		return http(e);
	}

	public static AppException run(Exception e) {
		return new AppException(TYPE_RUN, 0, e);
	}

	public static AppException getAppExceptionHandler() {
		return new AppException();
	}

	@Override
	public void uncaughtException(Thread thread, Throwable ex) {
		if (mDefaultHandler != null) {
			mDefaultHandler.uncaughtException(thread, ex);
		}
	}
}
