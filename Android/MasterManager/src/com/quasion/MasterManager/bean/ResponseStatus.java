package com.quasion.MasterManager.bean;

public class ResponseStatus {
	private String code;
	private boolean state;
	private Integer type;
	private String message;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		if (code != null) code = code.trim();
		this.code = code;
	}

	public boolean getState() {
		return state;
	}

	public void setState(boolean state) {
		this.state = state;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		if (message != null) message = message.trim();
		this.message = message;
	}

	@Override
	public String toString() {
		return "ResponseStatus{" +
				"code='" + code + '\'' +
				", state=" + state +
				", type=" + type +
				", message='" + message + '\'' +
				'}';
	}
}
