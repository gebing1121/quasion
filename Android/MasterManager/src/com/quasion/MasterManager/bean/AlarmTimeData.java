package com.quasion.MasterManager.bean;

import java.util.List;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;

/**
 * Created by Gebing on 2014/7/23.
 */
public class AlarmTimeData {
	@DatabaseField(id = true)
	public String uuid;
	@DatabaseField
	public String nfcBaseId;
	@DatabaseField
	public String deviceImei;
	@DatabaseField(dataType = DataType.SERIALIZABLE)
	public AlarmTimeItem alarmTimes;
}
