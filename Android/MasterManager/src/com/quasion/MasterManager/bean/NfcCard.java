package com.quasion.MasterManager.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class NfcCard extends BaseBean implements Serializable {
	public static final int STATUS_ADDED = 1;
	public static final int STATUS_ASSIGNED = 2;
	public static final int STATUS_LOST = 9;

	private static final String NFC_CARD_ID = "id";
	private static final String NFC_CARD_KEY_ID = "keyId";
	private static final String NFC_CARD_STATUS = "status";
	private static final String NFC_CARD_ADD_TIME = "addTime";
	private static final String NFC_CARD_ISSUE_TIME = "issueTime";
	private static final String NFC_OWNER_ID = "ownerId";
	private static final String NFC_OWNER_CHINESE_NAME = "ownerChineseName";
	private static final String NFC_OWNER_ENGLISH_NAME = "ownerEnglishName";

	/* 数据库id 唯一标识 */
	private String id = "";
	/* key的唯一标识 */
	private String keyId = "";
	/* 当前card的状态 */
	private int status;
	/* card 的添加时间 */
	private long addTime;
	/* 领用时间 */
	private long issueTime;
	/* 领用者Id */
	private String ownerId = "";
	/* 领用者英文名称 */
	private String ownerEnglishName = "";
	/* 领用者中文名称 */
	private String ownerChineseName = "";

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getKeyId() {
		return keyId;
	}

	public void setKeyId(String keyId) {
		this.keyId = keyId;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public long getAddTime() {
		return addTime;
	}

	public void setAddTime(long addTime) {
		this.addTime = addTime;
	}

	public long getIssueTime() {
		return issueTime;
	}

	public void setIssueTime(long issueTime) {
		this.issueTime = issueTime;
	}

	public String getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}

	public String getOwnerEnglishName() {
		return ownerEnglishName;
	}

	public void setOwnerEnglishName(String ownerEnglishName) {
		this.ownerEnglishName = ownerEnglishName;
	}

	public String getOwnerChineseName() {
		return ownerChineseName;
	}

	public void setOwnerChineseName(String ownerChineseName) {
		this.ownerChineseName = ownerChineseName;
	}

	/* 解析多个nfc列表数据 */
	private static final String DATA_TAG_NAME = "nfcCards";
	public static List<NfcCard> parseNfcCards(JSONObject data) throws JSONException {
		JSONArray dataArray = data.getJSONArray(DATA_TAG_NAME);
		List<NfcCard> nfcCardList = new ArrayList<NfcCard>();
		for (int i = 0; i < dataArray.length(); i++) {
			JSONObject dataObject = dataArray.getJSONObject(i);
			NfcCard nfcCard = new NfcCard();
			if (dataObject.has(NFC_CARD_ID) && !dataObject.isNull(NFC_CARD_ID)) {
				nfcCard.setId(dataObject.getString(NFC_CARD_ID).trim());
			}
			if (dataObject.has(NFC_CARD_KEY_ID) && !dataObject.isNull(NFC_CARD_KEY_ID)) {
				nfcCard.setKeyId(dataObject.getString(NFC_CARD_KEY_ID).trim());
			}
			if (dataObject.has(NFC_CARD_STATUS) && !dataObject.isNull(NFC_CARD_STATUS)) {
				nfcCard.setStatus(dataObject.getInt(NFC_CARD_STATUS));
			}
			if (dataObject.has(NFC_CARD_ADD_TIME) && !dataObject.isNull(NFC_CARD_ADD_TIME)) {
				nfcCard.setAddTime(dataObject.getLong(NFC_CARD_ADD_TIME));
			}
			if (dataObject.has(NFC_CARD_ISSUE_TIME) && !dataObject.isNull(NFC_CARD_ISSUE_TIME)) {
				nfcCard.setIssueTime(dataObject.getLong(NFC_CARD_ISSUE_TIME));
			}
			if (dataObject.has(NFC_OWNER_ID) && !dataObject.isNull(NFC_OWNER_ID)) {
				nfcCard.setOwnerId(dataObject.getString(NFC_OWNER_ID).trim());
			}
			if (dataObject.has(NFC_OWNER_CHINESE_NAME) && !dataObject.isNull(NFC_OWNER_CHINESE_NAME)) {
				nfcCard.setOwnerChineseName(dataObject.getString(NFC_OWNER_CHINESE_NAME).trim());
			}
			if (dataObject.has(NFC_OWNER_ENGLISH_NAME) && !dataObject.isNull(NFC_OWNER_ENGLISH_NAME)) {
				nfcCard.setOwnerEnglishName(dataObject.getString(NFC_OWNER_ENGLISH_NAME).trim());
			}
			nfcCardList.add(nfcCard);
		}
		return nfcCardList;
	}
}
