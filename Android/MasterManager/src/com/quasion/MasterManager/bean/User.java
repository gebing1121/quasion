package com.quasion.MasterManager.bean;

import com.quasion.MasterManager.nfc.NfcBaseMessage;
import org.json.JSONException;
import org.json.JSONObject;

public class User extends BaseBean implements BaseBean.Parser {
	private String sessionToken;
	private String loginAccount;
	private String loginPassword;
	private String name;
	private boolean isFirstLogin;

	public boolean isFirstLogin() {
		return isFirstLogin;
	}

	public void setFirstLogin(boolean isFirstLogin) {
		this.isFirstLogin = isFirstLogin;
	}

	public String getSessionId() {
		return sessionToken;
	}

	public void setSessionId(String sessionId) {
		this.sessionToken = sessionId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLoginAccount() {
		return loginAccount;
	}

	public void setLoginAccount(String loginAccount) {
		this.loginAccount = loginAccount;
	}

	public String getLoginPassword() {
		return loginPassword;
	}

	public void setLoginPassword(String loginPassword) {
		this.loginPassword = loginPassword;
	}

	private static final String SESSION_TOKEN = "sessionToken";
	private static final String MASTER_KEY = "masterKey";
	private static final String FIRST_LOGIN = "isFirstLogin";
	public Object parse(JSONObject data) throws JSONException {
		NfcBaseMessage.MASTER_KEY_UID = null;
		if (data != null) {
			if (data.has(FIRST_LOGIN)) setFirstLogin(data.getBoolean(FIRST_LOGIN));
			if (data.has(SESSION_TOKEN)) setSessionId(data.getString(SESSION_TOKEN));
			if (data.has(MASTER_KEY)) NfcBaseMessage.MASTER_KEY_UID = data.getString(MASTER_KEY).trim();
		}
		if (NfcBaseMessage.MASTER_KEY_UID == null || NfcBaseMessage.MASTER_KEY_UID.length() != NfcBaseMessage.MASTER_KEY_LENGTH) {
			throw new JSONException("Invalid Master Key!");
		}
		return this;
	}
}
