package com.quasion.MasterManager.bean;

import org.json.JSONException;
import org.json.JSONObject;

import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.quasion.MasterManager.R;
import com.quasion.MasterManager.app.AppContext;
import com.quasion.MasterManager.app.AppException;
import com.quasion.MasterManager.ui.BaseActivity;

public class BaseBean {
	private static final String TAG = BaseBean.class.getSimpleName();
	public static final String EXPIRE_CODE = "999999";		// 会话过期 的code

	// 处理服务器成功返回的数据接口
	public static interface Parser {
		public Object parse(JSONObject data) throws JSONException;
	}

	public static class StaticParse implements Parser {
		private Object data;
		public StaticParse(Object obj) {
			data = obj;
		}
		@Override
		public Object parse(JSONObject data) throws JSONException {
			return data;
		}
	}

	public static void handleResponse(Handler mHandler, int msgWhat, String responseData, Parser parser) throws JSONException {
		ResponseInfo jsonResponseInfo = ResponseInfo.convertJsonStrToJsonObj(responseData);
		ResponseStatus responseStatus = jsonResponseInfo.status;
		JSONObject data = jsonResponseInfo.data;
		Log.d(TAG, "status: " + responseStatus);
		Message msg = new Message();
		if (responseStatus.getState()) {
			msg.what = msgWhat;
			if (parser != null) msg.obj = parser.parse(data);
			else msg.obj = data;
		} else if (EXPIRE_CODE.equals(responseStatus.getCode())) {
			msg.what = BaseActivity.SESSION_EXPIRE;
			msg.obj = AppContext.getAppContext().getString(R.string.msg_session_expired);
		} else {
			msg.what = BaseActivity.INVOKE_FAIL;
			msg.obj = responseStatus.getMessage();
		}
		mHandler.sendMessage(msg);
	}

	public static void handleException(Handler mHandler, Exception exception) {
		Log.d(TAG, "exception:" + exception);
		if (exception instanceof JSONException) {
			exception = AppException.xml(exception);
		} else if (!(exception instanceof AppException)) {
			exception = AppException.network(exception);
		}
		Message msg = new Message();
		msg.what = BaseActivity.INVOKE_FAIL;
		msg.obj = ((AppException) exception).getMessage(AppContext.getAppContext());
		mHandler.sendMessage(msg);
	}
}
