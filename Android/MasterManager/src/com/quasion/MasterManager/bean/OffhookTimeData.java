package com.quasion.MasterManager.bean;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;

import java.util.List;

/**
 * Created by Gebing on 2014/7/23.
 */
public class OffhookTimeData {
	@DatabaseField(id = true)
	public String uuid;
	@DatabaseField
	public String nfcBaseId;
	@DatabaseField
	public String deviceBrand;
	@DatabaseField
	public String deviceModel;
	@DatabaseField
	public String deviceImei;
	@DatabaseField(dataType = DataType.SERIALIZABLE)
	public OffhookTimeItem offhookTimes;
}
