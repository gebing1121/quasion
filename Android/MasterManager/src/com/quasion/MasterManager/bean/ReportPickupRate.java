package com.quasion.MasterManager.bean;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ReportPickupRate extends BaseBean {
	/* 返回参数 */
	private static final String PRODUCT_BRAND = "brand";
	private static final String PRODUCT_MODEL = "model";
	public static final String PICKUP_TIMES = "pickUpTimes";

	private String brand = "";
	private String model = "";
	private int pickUpTimes;

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public int getPickUpTimes() {
		return pickUpTimes;
	}

	public void setPickUpTimes(int pickUpTimes) {
		this.pickUpTimes = pickUpTimes;
	}

	@Override
	public String toString() {
		return brand + ' ' + model;
	}

	private static final String DATA_TAG_NAME = "pickupRates";
	public static List<ReportPickupRate> parseReportPickupRate(JSONObject data) throws JSONException {
		JSONArray dataArray = data.getJSONArray(DATA_TAG_NAME);
		List<ReportPickupRate> reportPickupRateList = new ArrayList<ReportPickupRate>();
		for (int i = 0; i < dataArray.length(); i++) {
			JSONObject dataObject = dataArray.getJSONObject(i);
			ReportPickupRate reportPickupRate = new ReportPickupRate();
			if (dataObject.has(PRODUCT_BRAND) && !dataObject.isNull(PRODUCT_BRAND)) {
				reportPickupRate.setBrand(dataObject.getString(PRODUCT_BRAND));
			}
			if (dataObject.has(PRODUCT_MODEL) && !dataObject.isNull(PRODUCT_MODEL)) {
				reportPickupRate.setModel(dataObject.getString(PRODUCT_MODEL));
			}
			if (dataObject.has(PICKUP_TIMES) && !dataObject.isNull(PICKUP_TIMES)) {
				reportPickupRate.setPickUpTimes(dataObject.getInt(PICKUP_TIMES));
			}
			reportPickupRateList.add(reportPickupRate);
		}
		return reportPickupRateList;
	}
}
