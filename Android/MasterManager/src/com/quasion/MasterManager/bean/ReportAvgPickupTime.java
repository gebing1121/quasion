package com.quasion.MasterManager.bean;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ReportAvgPickupTime extends BaseBean {
	/* 返回参数 */
	private static final String PRODUCT_BRAND = "brand";
	private static final String PRODUCT_MODEL = "model";
	public static final String AVG_PICKUP_TIME = "avgPickUpTime";

	private String brand = "";
	private String model = "";
	private double avgPickUpTime;

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public double getAvgPickUpTime() {
		return avgPickUpTime;
	}

	public void setAvgPickUpTime(double avgPickUpTime) {
		this.avgPickUpTime = avgPickUpTime;
	}

	@Override
	public String toString() {
		return brand + ' ' + model;
	}

	private static final String DATA_TAG_NAME = "pickupRates";
	public static List<ReportAvgPickupTime> parseReportPickupTime(JSONObject data) throws JSONException {
		JSONArray dataArray = data.getJSONArray(DATA_TAG_NAME);
		List<ReportAvgPickupTime> reportPickupTimeList = new ArrayList<ReportAvgPickupTime>();
		for (int i = 0; i < dataArray.length(); i++) {
			JSONObject dataObject = dataArray.getJSONObject(i);
			ReportAvgPickupTime reportPickupTime = new ReportAvgPickupTime();
			if (dataObject.has(PRODUCT_BRAND) && !dataObject.isNull(PRODUCT_BRAND)) {
				reportPickupTime.setBrand(dataObject.getString(PRODUCT_BRAND));
			}
			if (dataObject.has(PRODUCT_MODEL) && !dataObject.isNull(PRODUCT_MODEL)) {
				reportPickupTime.setModel(dataObject.getString(PRODUCT_MODEL));
			}
			if (dataObject.has(AVG_PICKUP_TIME) && !dataObject.isNull(AVG_PICKUP_TIME)) {
				reportPickupTime.setAvgPickUpTime(dataObject.getDouble(AVG_PICKUP_TIME));
			}
			reportPickupTimeList.add(reportPickupTime);
		}
		return reportPickupTimeList;
	}
}
