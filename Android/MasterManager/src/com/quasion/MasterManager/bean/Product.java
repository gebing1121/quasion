package com.quasion.MasterManager.bean;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Product extends BaseBean {
	private static final String PRODUCT_ID = "id";
	private static final String BRAND_NAME = "brand";
	private static final String MODEL_NAME = "model";

	private String id;
	private String brand;
	private List<String> model;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public List<String> getModel() {
		return model;
	}

	public void setModel(List<String> model) {
		this.model = model;
	}

	private static final String DATA_TAG_NAME = "products";
	public static List<Product> parseProductList(JSONObject data) throws JSONException {
		JSONArray dataArray = data.getJSONArray(DATA_TAG_NAME);
		List<Product> productList = new ArrayList<Product>();
		for (int i = 0; i < dataArray.length(); i++) {
			JSONObject dataObject = dataArray.getJSONObject(i);
			Product product = new Product();
			if (dataObject.has(PRODUCT_ID) && !dataObject.isNull(PRODUCT_ID)) {
				product.setId(dataObject.getString(PRODUCT_ID));
			}
			if (dataObject.has(BRAND_NAME) && !dataObject.isNull(BRAND_NAME)) {
				product.setBrand(dataObject.getString(BRAND_NAME));
			}
			List<String> models = new ArrayList<String>();
			if (dataObject.has(MODEL_NAME) && !dataObject.isNull(MODEL_NAME)) {
				JSONArray modelArray = dataObject.getJSONArray(MODEL_NAME);
				for (int j = 0; j < modelArray.length(); j++) {
					models.add(modelArray.getString(j));
				}
			}
			product.setModel(models);
			productList.add(product);
		}
		return productList;
	}
}
