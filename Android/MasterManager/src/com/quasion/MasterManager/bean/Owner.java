package com.quasion.MasterManager.bean;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Owner extends BaseBean {
	private static final String OWNER_ID = "id";
	private static final String CHINESE_NAME = "chineseName";
	private static final String ENGLISH_NAME = "englishName";

	private String id;
	private String chineseName;
	private String englishName;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getChineseName() {
		return chineseName;
	}

	public void setChineseName(String chineseName) {
		this.chineseName = chineseName;
	}

	public String getEnglishName() {
		return englishName;
	}

	public void setEnglishName(String englishName) {
		this.englishName = englishName;
	}

	@Override
	public String toString() {
		// used in ArrayAdapter<Owner>
		return englishName;
	}

	private static final String DATA_TAG_NAME = "owners";
	public static List<Owner> parseOwnerList(JSONObject data) throws JSONException {
		JSONArray dataArray = data.getJSONArray(DATA_TAG_NAME);
		List<Owner> ownerList = new ArrayList<Owner>();
		for (int i = 0; i < dataArray.length(); i++) {
			JSONObject dataObject = dataArray.getJSONObject(i);
			Owner owner = new Owner();
			if (dataObject.has(OWNER_ID) && !dataObject.isNull(OWNER_ID)) {
				owner.setId(dataObject.getString(OWNER_ID).trim());
			}
			if (dataObject.has(CHINESE_NAME) && !dataObject.isNull(CHINESE_NAME)) {
				owner.setChineseName(dataObject.getString(CHINESE_NAME).trim());
			}
			if (dataObject.has(ENGLISH_NAME) && !dataObject.isNull(ENGLISH_NAME)) {
				owner.setEnglishName(dataObject.getString(ENGLISH_NAME).trim());
			}
			ownerList.add(owner);
		}
		return ownerList;
	}
}
