package com.quasion.MasterManager.bean;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class SalesReport extends BaseBean {
	private static final String PRODUCT_BRAND = "brand";
	private static final String PRODUCT_MODEL = "model";
	private static final String SALES_NUM = "sales";

	private String brand = "";
	private String model = "";
	private int sales;

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public int getSales() {
		return sales;
	}

	public void setSales(int sales) {
		this.sales = sales;
	}

	@Override
	public String toString() {
		return brand + ' ' + model;
	}

	private static final String DATA_TAG_NAME = "products";
	public static List<SalesReport> parseReportSalesRate(JSONObject data) throws JSONException {
		JSONArray dataArray = data.getJSONArray(DATA_TAG_NAME);
		List<SalesReport> sales = new ArrayList<SalesReport>();
		for (int i = 0; i < dataArray.length(); i++) {
			JSONObject dataObject = dataArray.getJSONObject(i);
			SalesReport salesReport = new SalesReport();
			if (dataObject.has(PRODUCT_BRAND) && !dataObject.isNull(PRODUCT_BRAND)) {
				salesReport.setBrand(dataObject.getString(PRODUCT_BRAND));
			}
			if (dataObject.has(PRODUCT_MODEL) && !dataObject.isNull(PRODUCT_MODEL)) {
				salesReport.setModel(dataObject.getString(PRODUCT_MODEL));
			}
			if (dataObject.has(SALES_NUM) && !dataObject.isNull(SALES_NUM)) {
				salesReport.setSales(dataObject.getInt(SALES_NUM));
			}
			sales.add(salesReport);
		}
		return sales;
	}
}
