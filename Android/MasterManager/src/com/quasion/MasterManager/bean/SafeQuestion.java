package com.quasion.MasterManager.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class SafeQuestion extends BaseBean implements Serializable {
	/* 返回安全问题 */
	private static final String ID = "id";
	private static final String QUESTION = "question";

	/* 返回安全验证结果 */
	private static final String IS_VERIFY = "isVerified";

	private String id;
	private String question;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	@Override
	public String toString() {
		// used in ArrayAdapter<SafeQuestion>
		return question;
	}

	private static final String DATA_TAG_NAME = "secretQuestions";
	public static List<SafeQuestion> parseSafeQuestions(JSONObject data) throws JSONException {
		JSONArray secretQuestionArray = data.getJSONArray(DATA_TAG_NAME);
		List<SafeQuestion> safeQuestions = new ArrayList<SafeQuestion>();
		for (int i = 0; i < secretQuestionArray.length(); i++) {
			SafeQuestion safeQuestion = new SafeQuestion();
			JSONObject safeQuestionObj = secretQuestionArray.getJSONObject(i);
			if (safeQuestionObj.has(ID)) {
				safeQuestion.setId(safeQuestionObj.getString(ID));
			}
			if (safeQuestionObj.has(QUESTION)) {
				safeQuestion.setQuestion(safeQuestionObj.getString(QUESTION));
			}
			safeQuestions.add(safeQuestion);
		}
		return safeQuestions;
	}

	public static boolean parseVerifyQuestions(JSONObject data) {
		try {
			return data.getBoolean(SafeQuestion.IS_VERIFY);
		} catch (JSONException e) {
			return false;
		}
	}
}
