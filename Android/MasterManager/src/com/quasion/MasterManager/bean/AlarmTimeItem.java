package com.quasion.MasterManager.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by Gebing on 2014/7/23.
 */
public class AlarmTimeItem extends ArrayList<AlarmTime> implements Serializable {
	public AlarmTimeItem() {
		super();
	}

	public AlarmTimeItem(Collection<? extends AlarmTime> collection) {
		super(collection);
	}
}
