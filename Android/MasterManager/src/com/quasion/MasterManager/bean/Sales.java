package com.quasion.MasterManager.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Sales extends BaseBean implements Serializable {
	private static final String SALES_ID = "id";
	private static final String BRAND_NAME = "brand";
	private static final String MODEL_NAME = "model";
	private static final String SALES_NUM = "sales";

	private String id = "";
	private String brand = "";
	private String model = "";
	private int sales;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public int getSales() {
		return sales;
	}

	public void setSales(int sales) {
		this.sales = sales;
	}

	private static final String DATA_TAG_NAME = "products";
	public static List<Sales> parseSalesList(JSONObject data) throws JSONException {
		JSONArray dataArray = data.getJSONArray(DATA_TAG_NAME);
		List<Sales> salesList = new ArrayList<Sales>();
		for (int i = 0; i < dataArray.length(); i++) {
			JSONObject dataObject = dataArray.getJSONObject(i);
			Sales sales = new Sales();
			if (dataObject.has(SALES_ID) && !dataObject.isNull(SALES_ID)) {
				sales.setId(dataObject.getString(SALES_ID));
			}
			if (dataObject.has(BRAND_NAME) && !dataObject.isNull(BRAND_NAME)) {
				sales.setBrand(dataObject.getString(BRAND_NAME));
			}
			if (dataObject.has(MODEL_NAME) && !dataObject.isNull(MODEL_NAME)) {
				sales.setModel(dataObject.getString(MODEL_NAME));
			}
			if (dataObject.has(SALES_NUM) && !dataObject.isNull(SALES_NUM)) {
				sales.setSales(dataObject.getInt(SALES_NUM));
			}
			salesList.add(sales);
		}
		return salesList;
	}
}
