package com.quasion.MasterManager.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by Gebing on 2014/7/23.
 */
public class OffhookTimeItem extends ArrayList<OffhookTime> implements Serializable {
	public OffhookTimeItem() {
		super();
	}

	public OffhookTimeItem(Collection<? extends OffhookTime> collection) {
		super(collection);
	}
}
