package com.quasion.MasterManager.bean;

import com.quasion.MasterManager.util.DateUtil;

import java.io.Serializable;

public class AlarmTime implements Serializable {
	/* 报警时间点 */
	private long alarmTime;
	/* 报警时长 */
	private int times;
	/* 报警解锁KEY ID */
	private String unlockNfccardId;

	public long getAlarmTime() {
		return alarmTime;
	}

	public void setAlarmTime(long alarmTime) {
		this.alarmTime = alarmTime;
	}

	public int getTimes() {
		return times;
	}

	public void setTimes(int times) {
		this.times = times;
	}

	public String getUnlockNfccardId() {
		return unlockNfccardId;
	}

	public void setUnlockNfccardId(String unlockNfccardId) {
		this.unlockNfccardId = unlockNfccardId;
	}

	@Override
	public String toString() {
		return "AlarmContent {" +
				"alarmTime=" + alarmTime + "," + DateUtil.formatTime(alarmTime) +
				", times=" + times +
				", unlockNfccardId='" + unlockNfccardId + '\'' +
				'}';
	}
}
