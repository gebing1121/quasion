package com.quasion.MasterManager.bean;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Gebing on 2014/7/30.
 */
public class Update extends BaseBean {
	private static final String VERSION_CODE = "versionCode";
	private static final String VERSION_NAME = "versionName";
	private static final String RECENT_CHANGE = "recentChange";
	private static final String CLIENT_FILE = "pkgFile";

	private int versionCode;
	private String versionName = "";
	private String recentChange = "";
	private String clientFile = "";

	public int getVersionCode() {
		return versionCode;
	}

	public void setVersionCode(int versionCode) {
		this.versionCode = versionCode;
	}

	public String getVersionName() {
		return versionName;
	}

	public void setVersionName(String versionName) {
		this.versionName = versionName;
	}

	public String getRecentChange() {
		return recentChange;
	}

	public void setRecentChange(String recentChange) {
		this.recentChange = recentChange;
	}

	public String getClientFile() {
		return clientFile;
	}

	public void setClientFile(String clientFile) {
		this.clientFile = clientFile;
	}

	/* 解析多个nfc列表数据 */
	private static final String DATA_TAG_NAME = "client";
	public static Update parseUpdate(JSONObject data) throws JSONException {
		if (!data.has(DATA_TAG_NAME) || data.isNull(DATA_TAG_NAME)) {
			return null;
		}
		JSONObject dataObject = data.getJSONObject(DATA_TAG_NAME);
		Update update = new Update();
		if (dataObject.has(VERSION_CODE) && !dataObject.isNull(VERSION_CODE)) {
			update.setVersionCode(dataObject.getInt(VERSION_CODE));
		}
		if (dataObject.has(VERSION_NAME) && !dataObject.isNull(VERSION_NAME)) {
			update.setVersionName(dataObject.getString(VERSION_NAME).trim());
		}
		if (dataObject.has(RECENT_CHANGE) && !dataObject.isNull(RECENT_CHANGE)) {
			update.setRecentChange(dataObject.getString(RECENT_CHANGE).trim());
		}
		if (dataObject.has(CLIENT_FILE) && !dataObject.isNull(CLIENT_FILE)) {
			update.setClientFile(dataObject.getString(CLIENT_FILE).trim());
		}
		return update;
	}
}
