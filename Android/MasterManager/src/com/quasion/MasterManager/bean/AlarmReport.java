package com.quasion.MasterManager.bean;

import java.util.*;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.quasion.MasterManager.util.DateUtil;

public class AlarmReport {
	private static final String UNLOCK_CARD_ID = "unlockNfccardId";
	private static final String UNLOCK_OWNER_NAME = "unlockOwnerName";
	private static final String ALARM_TIME = "alarmTime";
	private static final String ALARM_DURATION = "times";

	/* 解锁NFC CARD ID */
	private String unlockCardId = "";
	/* 解锁人 */
	private String unlockOwnerName = "";
	/* 报警时间 */
	private String alarmTime = "";
	/* 报警日期 */
	private String alarmDate = "";
	/* 报警时长 */
	private int alarmDuration;

	public String getUnlockCardId() {
		return unlockCardId;
	}

	public void setUnlockCardId(String unlockCardId) {
		this.unlockCardId = unlockCardId;
	}

	public String getUnlockOwnerName() {
		return unlockOwnerName;
	}

	public void setUnlockOwnerName(String unlockOwnerName) {
		this.unlockOwnerName = unlockOwnerName;
	}

	public String getAlarmDate() {
		return alarmDate;
	}

	public void setAlarmDate(String alarmDate) {
		this.alarmDate = alarmDate;
	}

	public String getAlarmTime() {
		return alarmTime;
	}

	public void setAlarmTime(String alarmTime) {
		this.alarmTime = alarmTime;
	}

	public int getAlarmDuration() {
		return alarmDuration;
	}

	public void setAlarmDuration(int alarmDuration) {
		this.alarmDuration = alarmDuration;
	}

	private static final String DATA_TAG_NAME = "behaviorDatas";
	public static List<AlarmReport> parseAlarmDataList(JSONObject data) throws JSONException {
		JSONArray dataArray = data.getJSONArray(DATA_TAG_NAME);
		List<AlarmReport> alarmReportList = new ArrayList<AlarmReport>();
		for (int i = 0; i < dataArray.length(); i++) {
			JSONObject dataObject = dataArray.getJSONObject(i);
			AlarmReport alarmReport = new AlarmReport();
			if (dataObject.has(UNLOCK_CARD_ID) && !dataObject.isNull(UNLOCK_CARD_ID)) {
				alarmReport.setUnlockCardId(dataObject.getString(UNLOCK_CARD_ID).trim());
			}
			if (dataObject.has(ALARM_DURATION) && !dataObject.isNull(ALARM_DURATION)) {
				alarmReport.setAlarmDuration(dataObject.getInt(ALARM_DURATION));
			}
			if (dataObject.has(UNLOCK_OWNER_NAME) && !dataObject.isNull(UNLOCK_OWNER_NAME)) {
				alarmReport.setUnlockOwnerName(dataObject.getString(UNLOCK_OWNER_NAME).trim());
			}
			if (dataObject.has(ALARM_TIME) && !dataObject.isNull(ALARM_TIME)) {
				long time = dataObject.getLong(ALARM_TIME);
				alarmReport.setAlarmDate(DateUtil.formatTime(time, "yyyy/MM/dd"));
				alarmReport.setAlarmTime(DateUtil.formatTime(time, "HH:mm:ss"));
			}
			alarmReportList.add(alarmReport);
		}
		return alarmReportList;
	}

}
