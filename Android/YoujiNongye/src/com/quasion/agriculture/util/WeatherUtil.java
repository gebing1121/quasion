package com.quasion.agriculture.util;


import com.quasion.agriculture.R;

import java.util.Calendar;
import java.util.TimeZone;

/**
 * Author Gebing
 * Date: 2014/09/30
 */
public class WeatherUtil {

	public static boolean isNight() {
		Calendar now = Calendar.getInstance(TimeZone.getDefault());
		int hour = now.get(Calendar.HOUR_OF_DAY);
		return hour <= 6 || hour >= 19;
	}

	/**
	 * 获取天气ICON的资源id
	 * @param info
	 * @return
	 */
	public static int getWeatherImage(String info) {
		// 确定最终天气
		if (info.indexOf("转") >= 0 && info.indexOf("到") >= 0) {
			info = info.split("转")[0].split("到")[1];
		} else if (info.indexOf("转") >= 0) {
			info = info.split("转")[0];
		}
		// 确定当前是白天还是夜晚
		boolean isNight = isNight();
		// 根据天气情况获取对应图标
		if (info.indexOf("晴") >= 0) {
			return isNight ? R.drawable.weather_img_clear_night : R.drawable.weather_img_clear_day;
		} else if (info.indexOf("多云") >= 0) {
			return isNight ? R.drawable.weather_img_cloudy_night : R.drawable.weather_img_cloudy_day;
		} else if (info.indexOf("阴") >= 0) {
			return isNight ? R.drawable.weather_img_overcast_night : R.drawable.weather_img_overcast_day;
		} else if (info.indexOf("小雨") >= 0) {
			return R.drawable.weather_img_small_rain;
		} else if (info.indexOf("中雨") >= 0) {
			return R.drawable.weather_img_middle_rain;
		} else if (info.indexOf("大雨") >= 0) {
			return R.drawable.weather_img_large_rain;
		} else if (info.indexOf("阵雨") >= 0) {
			return R.drawable.weather_img_thunder_rain;
		} else if (info.indexOf("暴雨") >= 0) {
			return R.drawable.weather_img_stormy_rain;
		} else if (info.indexOf("雨夹雪") >= 0) {
			return R.drawable.weather_img_snow_rain;
		} else if (info.indexOf("小雪") >= 0) {
			return R.drawable.weather_img_small_snow;
		} else if (info.indexOf("中雪") >= 0) {
			return R.drawable.weather_img_middle_snow;
		} else if (info.indexOf("大雪") >= 0) {
			return R.drawable.weather_img_large_snow;
		} else if (info.indexOf("暴雪") >= 0) {
			return R.drawable.weather_img_stormy_snow;
		} else if (info.indexOf("冰雹") >= 0) {
			return R.drawable.weather_img_hailstone;
		} else if (info.indexOf("雾") >= 0) {
			return R.drawable.weather_img_fog;
		} else if (info.indexOf("风") >= 0) {
			return R.drawable.weather_img_wind;
		} else {
			return isNight ? R.drawable.weather_img_clear_night : R.drawable.weather_img_clear_day;
		}
	}
}
