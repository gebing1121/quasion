package com.quasion.agriculture.util;

import android.content.res.AssetManager;
import android.util.Log;
import com.androidquery.util.AQUtility;
import com.winsland.framework.protocol.BaseJsonProtocol;

import java.io.*;
import java.util.Arrays;

/**
 * Author Gebing
 * Date: 2014/10/20
 */
public class AssetsUtil {
	public static boolean verifyFile(AssetManager assetManager, String srcFile, File dstFile) {
		InputStream src = null;
		InputStream dst = null;
		try {
			src = assetManager.open(srcFile);
			dst = new FileInputStream(dstFile);
			return Arrays.equals(AQUtility.toBytes(src), AQUtility.toBytes(dst));
		} catch (IOException ex) {
			return false;
		} finally {
			AQUtility.close(src);
			AQUtility.close(dst);
		}

	}

	public static void copyFiles(AssetManager assetManager, String srcPath, File dstPath) {
		InputStream src = null;
		OutputStream dst = null;
		// first try copy as file
		try {
			dstPath.getParentFile().delete();
			dstPath.getParentFile().mkdirs();
			src = assetManager.open(srcPath);
			dst = new FileOutputStream(dstPath);
			AQUtility.copy(src, dst);
			AQUtility.close(src);
			AQUtility.close(dst);
			return;
		} catch (FileNotFoundException fnfe) {
			// src path is directory
		} catch (IOException ex) {
			Log.w("AssetsUtil", "copyFiles(" + srcPath + ") as file failed ", ex);
			return;
		} finally {
			AQUtility.close(src);
			AQUtility.close(dst);
		}
		// if failed, path mat be directory
		try {
			String[] list = assetManager.list(srcPath);
			for (String file : list) {
				copyFiles(assetManager, srcPath + File.separator + file, new File(dstPath, file));
			}
			return;
		} catch (IOException ex) {
			Log.w("AssetsUtil", "copyFiles(" + srcPath + ") as directory failed", ex);
			return;
		} finally {
			AQUtility.close(src);
			AQUtility.close(dst);
		}
	}

	public static void cacheFile(AssetManager assetManager, BaseJsonProtocol request, String file) {
		if (request != null && file != null && !request.isCacheExist()) {
			try {
				boolean rt = request.setCacheResult(assetManager.open(file));
				Log.i("AssetsUtil", "cacheFile(" + file + ") return=" + rt);
			} catch (IOException ex) {
				Log.w("AssetsUtil", "cacheFile(" + file + ") failed=", ex);
			}
		}
	}
}
