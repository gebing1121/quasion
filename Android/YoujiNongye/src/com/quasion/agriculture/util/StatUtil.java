package com.quasion.agriculture.util;

import android.app.Fragment;
import android.content.Context;
import com.androidquery.util.AQUtility;
import com.quasion.agriculture.BuildConfig;
import com.umeng.analytics.MobclickAgent;

import java.util.HashMap;

public class StatUtil {
	static public void init(){
		MobclickAgent.setDebugMode(BuildConfig.DEBUG);
		MobclickAgent.updateOnlineConfig(AQUtility.getContext());
	}

	static public void exit() {
		MobclickAgent.onKillProcess(AQUtility.getContext());
	}

	static public void onResume(Context context){
		MobclickAgent.onResume(context);
	}

	static public void onResume(Fragment context){
		MobclickAgent.onPageStart(context.getClass().getSimpleName());
	}

	static public void onPause(Fragment context){
		MobclickAgent.onPageEnd(context.getClass().getSimpleName());
	}

	static public void onPause(Context context){
		MobclickAgent.onPause(context);
	}

	static public void onEvent(Context context, String event_id, int value) {
		MobclickAgent.onEvent(context, event_id, value);
	}

	static public void onEvent(Context context, String event_id, String... keyValues) {
		if (keyValues.length >= 2) {
			MobclickAgent.onEvent(context, event_id);
		} else {
			HashMap<String, String> map = new HashMap<String, String>();
			for (int i = 0; i < keyValues.length - 1; i += 2) {
				map.put(keyValues[i], keyValues[i+1]);
			}
			MobclickAgent.onEvent(context, event_id, map);
		}
	}

	static public String getConfigParams(String key) {
		return MobclickAgent.getConfigParams(AQUtility.getContext(), key);
	}
}
