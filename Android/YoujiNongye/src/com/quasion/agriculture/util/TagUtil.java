package com.quasion.agriculture.util;

/**
 * Author: Gebing
 * Date: 2013/6/26
 */
public class TagUtil {
	static public String getTag(Class clazz) {
		return "quasion." + clazz.getSimpleName();
	}
}
