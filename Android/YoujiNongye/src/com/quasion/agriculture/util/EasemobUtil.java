package com.quasion.agriculture.util;

import android.app.ActivityManager;
import android.content.Context;
import android.content.pm.PackageManager;
import com.androidquery.util.AQUtility;
import com.easemob.chat.EMChat;
import com.easemob.chat.EMChatManager;
import com.easemob.chat.EMChatOptions;
import com.winsland.framework.util.SystemInfoUtil;

import java.util.Iterator;
import java.util.List;

/**
 * Author Gebing
 * Date: 2014/10/23
 */
public class EasemobUtil {
	public static void init() {
		// 如果使用到百度地图或者类似启动remote service的第三方库，这个if判断不能少
		// 百度定位sdk，定位服务运行在一个单独的进程，每次定位服务启动的时候，都会调用application::onCreate创建新的进程。
		// 但环信的sdk只需要在主进程中初始化一次。 这个特殊处理是，如果从pid 找不到对应的processInfo的processName，
		// 则此application::onCreate 是被service 调用的，直接返回
		if (SystemInfoUtil.getProcessName() == null) return;
		// 初始化EMChat
		EMChat.getInstance().setAutoLogin(true);
		EMChat.getInstance().setDebugMode(true);
		EMChat.getInstance().init(AQUtility.getContext());
		// 设置EMChat选项
		EMChatOptions options = EMChatManager.getInstance().getChatOptions();
		//默认添加好友时，是不需要验证的，改成需要验证
		options.setAcceptInvitationAlways(false);
		//设置收到消息是否有新消息通知，默认为true
		options.setNotificationEnable(false);
		//设置收到消息是否有声音提示，默认为true
		options.setNoticeBySound(false);
		//设置收到消息是否震动 默认为true
		options.setNoticedByVibrate(false);
		//设置语音消息播放是否设置为扬声器播放 默认为true
		options.setUseSpeaker(false);
	}
}
