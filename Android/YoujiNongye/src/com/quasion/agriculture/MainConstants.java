package com.quasion.agriculture;

import android.net.Uri;
import com.quasion.agriculture.bean.*;

import java.io.File;

public class MainConstants {
	// 定义Intent参数
	public static final String INTENT_EXTRA_NEWS_INDEX = "newsIndex";
	public static final String INTENT_EXTRA_COURSE_INDEX = "courseIndex";
	public static final String INTENT_EXTRA_COURSE_DETAIL = "courseDetail";
	public static final String INTENT_EXTRA_QUESTION_INDEX = "questionIndex";
	public static final String INTENT_EXTRA_LOCAL_FILE = "localFile";
	public static final String INTENT_EXTRA_REMOTE_FILE = "remoteFile";

	// 定义使用到的SharedPreferences的文件名和项目名
	public static final String SP_SETTING = "setting";

	public static final String SP_RUNTIME = "runtime";
	public static final String SP_RUNTIME_FIRST_START = "FirstStart";
	public static final String SP_RUNTIME_WEATHER_CODE = "WeatherCode";

	public static final String DEFAULT_WEATHER_CODE = "101290601";		// 文山

	public static News[] NewsList;
	public static Course[] CourseList;
	public static CourseDetail[] CourseDetail;
	public static QuestionNews[] QuestionNews;
	public static Question[] QuestionList;

	// 手机硬件信息
	public static String SystemVersion; 		// 操作系统版本
	public static String SystemDevice; 		// 手机型号
	public static String SystemResolution; 	// 屏幕分辨率
	public static String SystemImei; 			// 手机设备id
	public static String SystemImsi; 			// 手机SIM卡id
	public static String SystemMac; 			// 手机WIFI的MAC

	// 服务器地址信息
	public static String ServerProtocol = "/data/data/com.quasion.agriculture/files/data/";
	public static String ServerImage = "";

	public static final boolean AutoCheckUpdate = false;
	public static final long CacheRefreshTime = 10L * 60L * 1000L;					// 10分钟
	public static final long AreaRefreshTime = 30L * 24L * 60L * 60L * 1000L;		// 30天
}
