package com.quasion.agriculture;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.v13.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.Window;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.Toast;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.handmark.pulltorefresh.library.PullToRefreshScrollView;
import com.quasion.agriculture.util.StatUtil;
import com.quasion.agriculture.util.TagUtil;
import com.quasion.agriculture.view.home.HomeFragment;
import com.quasion.agriculture.view.question.QuestionFragment;
import com.viewpagerindicator.EmbedPageIndicator;

public class MainActivity extends Activity {
	private static final String TAG = TagUtil.getTag(MainActivity.class);

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		initDisplay();
	}

	@Override
	protected void onPause() {
		super.onPause();
		StatUtil.onPause(this);
	}

	@Override
	protected void onResume() {
		super.onResume();
		StatUtil.onResume(this);
	}

	private long backPressTime = 0;
	@Override
	public void onBackPressed() {
		if (System.currentTimeMillis() - backPressTime > 2000) {
			Toast.makeText(this, "再按一次退出程序!", Toast.LENGTH_SHORT).show();
			backPressTime = System.currentTimeMillis();
		} else {
			InitActivity.exit(this);
		}
	}

	private void initDisplay() {
		// set content view
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.main_layout);
		// set view pager adapter
		final Fragment[] fragments = new Fragment[] { new HomeFragment(), new QuestionFragment() };
		ViewPager viewPager = (ViewPager) findViewById(R.id.main_content);
		viewPager.setAdapter(new FragmentPagerAdapter(getFragmentManager()) {
			@Override
			public Fragment getItem(int position) {
				return position >= 0 && position < getCount() ? fragments[position] : null;
			}

			@Override
			public int getCount() {
				return fragments.length;
			}
		});
		// set tab
		EmbedPageIndicator indicator = (EmbedPageIndicator) findViewById(R.id.main_tabHost);
		indicator.setViewPager(viewPager, 0);
		indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
			@Override
			public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
			}

			@Override
			public void onPageSelected(int position) {
				View view = fragments[position].getView();
				if (view == null) return;
				if (position == 0) {
					ScrollView refreshView = ((PullToRefreshScrollView) view.findViewById(R.id.home_content_layout)).getRefreshableView();
					refreshView.scrollTo(0, 0);
				} else if (position == 1) {
					ListView refreshView = ((PullToRefreshListView) view.findViewById(R.id.question_content_layout)).getRefreshableView();
					if (refreshView.getSelectedItemPosition() != 0) {
						refreshView.requestFocusFromTouch();
						refreshView.setSelection(0);
					}
					refreshView.scrollTo(0, 0);
				}
			}

			@Override
			public void onPageScrollStateChanged(int state) {
			}
		});
	}

}
