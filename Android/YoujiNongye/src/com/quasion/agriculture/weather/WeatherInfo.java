package com.quasion.agriculture.weather;

/**
 * Author Gebing
 * Date: 2014/09/29
 */
public class WeatherInfo {
	public final static int IDX_DESC = 1;			// 天气情况
	public final static int IDX_TEMPERATURE = 2;	// 温度
	public final static int IDX_WIND = 3;			// 风向
	public final static int IDX_GRADE = 4;			// 风力
	public final static int IDX_TIME = 5;			// 时间

	public String date;			// 时间
	public WeatherDetail info;		// 天气信息

	public static class WeatherDetail {
		public String[] day;		// 白天
		public String[] dawn;		// 傍晚
		public String[] night;		// 晚上
	}
}
