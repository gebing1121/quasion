package com.quasion.agriculture.weather;

/**
 * Author Gebing
 * Date: 2014/09/29
 */
public class LifeInfo {
	public final static int IDX_INFO = 0;			// 指数
	public final static int IDX_DESC = 1;			// 描述

	public String date;		// 时间
	public LifeDetail info;	// 信息

	public static class LifeDetail {
		public String[] kongtiao;			// 空调指数
		public String[] yundong;			// 运动指数
		public String[] ziwaixian;		// 紫外线指数
		public String[] ganmao;			// 感冒指数
		public String[] xiche; 			// 洗车指数
		public String[] wuran;				// 污染指数
		public String[] chuanyi;			// 穿衣指数
	}
}
