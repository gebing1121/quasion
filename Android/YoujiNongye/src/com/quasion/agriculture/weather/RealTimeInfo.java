package com.quasion.agriculture.weather;

/**
 * Author Gebing
 * Date: 2014/09/29
 */
public class RealTimeInfo {
	public String date;				// 日期
	public String time;				// 时间
	public WeatherDetail weather;		// 天气
	public WindDetail wind;			// 风力信息
	public String pressure;			// 气压
	public long dataUptime;			// 更新时间

	public static class WeatherDetail {
		public int img;				// 图片ID
		public String humidity;		// 湿度
		public String temperature;	// 温度
		public String info;			// 描述信息
	}

	public static class WindDetail {
		public String windspeed;		// 风速
		public String direct;			// 风向
		public String power;			// 风力
	}
}
