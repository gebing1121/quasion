package com.quasion.agriculture.weather;

import com.androidquery.util.Constants;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.winsland.framework.protocol.BaseJsonProtocol;

public class WeatherApi<T> extends BaseJsonProtocol<T> {
	public WeatherApi(Class<T> type, int... option) {
		super(type, option);
		init();
	}

	private void init() {
		// 协议统一使用GET请求
		method(Constants.METHOD_GET);
	}

	@Override
	protected JsonElement getResultJson(byte[] data, String encoding) throws Exception {
		if (data != null) {
			String str = new String(data, encoding);
			str = str.substring(str.indexOf('(') + 1, str.lastIndexOf(')'));
			data = str.getBytes(encoding);
		}
		JsonElement result = super.getResultJson(data, encoding);
		// 去除非法数据
		if (result != null && result.isJsonObject()) {
			JsonArray weather = ((JsonObject) result).getAsJsonArray("weather");
			for (JsonElement element : weather) {
				JsonElement info = ((JsonObject) element).get("info");
				if (!info.isJsonObject()) {
					((JsonObject) element).remove("info");
				}
			}
		}
		return result;
	}

	// **********************************************************************************************************
	// 所有服务器接口和方法定义
	// **********************************************************************************************************
	public static BaseJsonProtocol<String[][]> getProvinceList() {
		BaseJsonProtocol<String[][]> protocol = new WeatherApi<String[][]>(String[][].class).url("http://cdn.weather.hao.360.cn/sed_api_area_query.php?grade=province");
		return protocol;
	}

	public static BaseJsonProtocol<String[][]> getCityList(String provinceCode) {
		BaseJsonProtocol<String[][]> protocol = new WeatherApi<String[][]>(String[][].class).url("http://cdn.weather.hao.360.cn/sed_api_area_query.php?grade=city&&code=" + provinceCode);
		return protocol;
	}

	public static BaseJsonProtocol<String[][]> getTownList(String cityCode) {
		BaseJsonProtocol<String[][]> protocol = new WeatherApi<String[][]>(String[][].class).url("http://cdn.weather.hao.360.cn/sed_api_area_query.php?grade=town&code=" + cityCode);
		return protocol;
	}

	public static BaseJsonProtocol<WeatherResponse> getWeather(String townCode) {
		BaseJsonProtocol<WeatherResponse> protocol = new WeatherApi<WeatherResponse>(WeatherResponse.class).url("http://tq.360.cn/api/weatherquery/query?app=tq360&code=" + townCode);
		return protocol;
	}
}
