package com.quasion.agriculture.weather;

/**
 * Author Gebing
 * Date: 2014/09/29
 */
public class Pm25Info {
	public String pm10;		// pm1.0的值
	public String pm25;		// pm2.5的值
	public int level;			// 污染级别
	public String quality;	// 空气质量
	public String advice;		// 建议
	public String color;		// 显示颜色
}
