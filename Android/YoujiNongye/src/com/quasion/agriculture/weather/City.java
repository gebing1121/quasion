package com.quasion.agriculture.weather;

import java.util.List;

/**
 * Author Gebing
 * Date: 2014/09/29
 */
public class City {
	public String name;
	public String code;
	public List<Town> townList;
}
