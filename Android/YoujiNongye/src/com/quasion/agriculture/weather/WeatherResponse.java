package com.quasion.agriculture.weather;

import java.util.List;

/**
 * Author Gebing
 * Date: 2014/09/29
 */
public class WeatherResponse {
	public String area[][];				// 地区
	public LifeInfo life;					// 生活信息
	public Pm25Info pm;					// PM2.5信息
	public RealTimeInfo realtime;			// 实时信息
	public List<WeatherInfo> weather;		// 7天的天气信息
}
