package com.quasion.agriculture.Event;

/**
 * Author Gebing
 * Date: 2014/10/03
 */
public class WeatherEvent {
	static class Success extends WeatherEvent { }
	static class Failed extends WeatherEvent { }
	static class NoUpdate extends WeatherEvent { }
	public final static WeatherEvent UpdateSuccess = new Success();
	public final static WeatherEvent UpdateFailed = new Failed();
	public final static WeatherEvent NoUpdate = new NoUpdate();
}
