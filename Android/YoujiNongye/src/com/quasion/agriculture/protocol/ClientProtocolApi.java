package com.quasion.agriculture.protocol;

import com.androidquery.util.Constants;
import com.quasion.agriculture.MainConstants;
import com.quasion.agriculture.bean.*;
import com.quasion.agriculture.weather.Province;
import com.winsland.framework.protocol.BaseJsonProtocol;

import java.util.Map;

public class ClientProtocolApi<T> extends BaseJsonProtocol<T> {
	private static final String PARAM_METHOD = "method";
	private static final String PARAM_VERSION = "Version";
	private static final String PARAM_TIMESTAMP = "timestamp";
	private static final String PARAM_SIGN = "sign";
	private static final String PARAM_PLATFORM = "OS";

	private static final String VALUE_VERSION = "1.0";
	private static final String VALUE_PLATFORM = "Android";
	private static final String VALUE_SECRET = "c00f1240-bc1f-a18c-4197-07a75b6a603e";

	public ClientProtocolApi(Class<T> type, int... option) {
		super(type, option);
		init();
	}

	private void init() {
		// 协议统一使用POST请求
		method(Constants.METHOD_POST);
		// 设置协议的日期格式
		getGsonBuilder().setDateFormat("yyyyMMddHHmmss");
	}

	@Override
	protected String getRootUrl() {
		return MainConstants.ServerProtocol;
	}

	/**
	 * 重载返回协议请求的HTTP参数的函数，添加appKey/timestamp等固定参数并计算sign参数。
	 * @return HTTP参数，如果未指定则返回null
	 */
	@Override
	protected Map<String, Object> getRequestParams() {
		return super.getRequestParams();
	}

	/**
	 * 重载生成缓存HTTP应答消息的Key的函数，去掉HTTP请求参数中影响Cache的与时间相关的参数
	 * @param url
	 * @param params
	 * @return 缓存HTTP应答消息的Key。如果为null，则表示使用HTTP实际请求的URL
	 */
	@Override
	protected String getHttpCacheUrl(String url, Map<String, Object> params) {
		return super.getHttpCacheUrl(url, params);
	}

	// **********************************************************************************************************
	// 所有服务器接口和方法定义
	// **********************************************************************************************************

	public static BaseJsonProtocol<News[]> getNewsList() {
		BaseJsonProtocol<News[]> protocol = new ClientProtocolApi<News[]>(News[].class).url("/News");
		return protocol;
	}

	public static BaseJsonProtocol<Course[]> getCourseList() {
		BaseJsonProtocol<Course[]> protocol = new ClientProtocolApi<Course[]>(Course[].class).url("/Course");
		return protocol;
	}

	public static BaseJsonProtocol<CourseDetail[]> getCourseDetail(String courseId) {
		BaseJsonProtocol<CourseDetail[]> protocol = new ClientProtocolApi<CourseDetail[]>(CourseDetail[].class).url("/CourseDetail" + courseId);
		return protocol;
	}

	public static BaseJsonProtocol<QuestionNews[]> getQuestionNews() {
		BaseJsonProtocol<QuestionNews[]> protocol = new ClientProtocolApi<QuestionNews[]>(QuestionNews[].class).url("/QuestionNews");
		return protocol;
	}

	public static BaseJsonProtocol<Question[]> getQuestionList() {
		BaseJsonProtocol<Question[]> protocol = new ClientProtocolApi<Question[]>(Question[].class).url("/Question");
		return protocol;
	}

	public static BaseJsonProtocol<Province[]> getAreaInfo() {
		BaseJsonProtocol<Province[]> protocol = new ClientProtocolApi<Province[]>(Province[].class).url("/AreaInfo");
		return protocol;
	}
}
