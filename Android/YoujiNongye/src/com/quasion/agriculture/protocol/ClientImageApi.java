package com.quasion.agriculture.protocol;

import com.quasion.agriculture.MainConstants;
import com.winsland.framework.util.CommonUtil;

public class ClientImageApi {
	public static String getHxImageUrl(String name) {
		if (!CommonUtil.isEmpty(name)) {
			name = name.trim();
			if (!name.startsWith("http://") && !name.startsWith("https://") && !name.startsWith("file://")) {
				name = MainConstants.ServerImage + name;
			}
		}
		return name;
	}
}
