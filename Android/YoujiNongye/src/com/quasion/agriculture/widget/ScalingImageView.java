package com.quasion.agriculture.widget;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.ImageView;

/**
 * Author Gebing
 * Date: 2014/10/13
 */
public class ScalingImageView extends ImageView {

	public ScalingImageView(Context context) {
		super(context);
	}

	public ScalingImageView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public ScalingImageView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		Drawable drawable = getDrawable();
		if (drawable == null || getScaleType() != ScaleType.FIT_CENTER || drawable.getIntrinsicWidth() == -1 || drawable.getIntrinsicHeight() == -1) {
			drawable = getBackground();
		}
		if (drawable != null && drawable.getIntrinsicWidth() != -1 && drawable.getIntrinsicHeight() != -1) {
			int mDrawableWidth = drawable.getIntrinsicWidth();
			int mDrawableHeight = drawable.getIntrinsicHeight();
			float actualAspect = (float) mDrawableWidth / (float) mDrawableHeight;
			// Assuming the width is ok, so we calculate the height.
			int actualWidth = MeasureSpec.getSize(widthMeasureSpec);
			int height = (int) (actualWidth / actualAspect);
			heightMeasureSpec = MeasureSpec.makeMeasureSpec(height, MeasureSpec.EXACTLY);
		}
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
	}
}