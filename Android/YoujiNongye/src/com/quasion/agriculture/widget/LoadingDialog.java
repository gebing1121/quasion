package com.quasion.agriculture.widget;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.quasion.agriculture.R;

public class LoadingDialog extends Dialog {
	public LoadingDialog(Context context, int theme) {
		super(context, theme);
		setCancelable(false);
	}

	public LoadingDialog(Context context) {
		super(context, R.style.LoadingDialog);
		setCancelable(false);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.common_loading_dialog);
		ImageView mImageView = (ImageView) findViewById(R.id.common_loading_dialog_image);
		AnimationDrawable ad = (AnimationDrawable) mImageView.getDrawable();
		ad.start();
	}

	public void show(String message) {
		show();
		setMessage(message);
	}

	public void setProgress(int progress) {
		if (progress < 0) progress = 0;
		else if (progress > 100) progress = 100;
		setProgress(String.valueOf(progress) + "%");
	}

	public void setProgress(String progress) {
		TextView progressText = (TextView) findViewById(R.id.common_loading_dialog_progress);
		progressText.setVisibility(View.VISIBLE);
		progressText.setText(progress);
	}

	public void setMessage(String message) {
		TextView messageText = (TextView) findViewById(R.id.common_loading_dialog_message);
		messageText.setVisibility(View.VISIBLE);
		messageText.setText(message);
	}
}
