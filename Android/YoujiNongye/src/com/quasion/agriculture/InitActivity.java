package com.quasion.agriculture;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import com.androidquery.util.AQUtility;
import com.quasion.agriculture.util.StatUtil;
import com.quasion.agriculture.util.TagUtil;
import com.quasion.agriculture.view.splash.SplashActivity;
import com.quasion.agriculture.view.weather.WeatherActivity;
import com.winsland.framework.util.PreferencesUtil;

public class InitActivity extends Activity {
	static private final String TAG = TagUtil.getTag(InitActivity.class);

	private static boolean isExit = false;

	@Override
	protected void onStart() {
		super.onStart();
		if (!isExit) {
			// Initialize stat functions
			StatUtil.init();
			// Start startup activity or main activity
			boolean isFirstStart = PreferencesUtil.getBoolean(MainConstants.SP_RUNTIME, MainConstants.SP_RUNTIME_FIRST_START, true);
			isFirstStart = false;
			Intent intent = new Intent(this, isFirstStart ? SplashActivity.class : MainActivity.class);
			startActivity(intent);
			// Start to update weather
			AQUtility.postDelayed(new Runnable() {
				@Override
				public void run() {
					WeatherActivity.updateAreaInfo();
					WeatherActivity.updateWeather(true);
				}
			}, 100);
		} else {
			isExit = false;
			// Close init activity to exit application
			finish();
			// Delayed kill process so that all onDestroy() will be called!
			AQUtility.postDelayed(new Runnable() {
				@Override
				public void run() {
					// Finalize stat functions
					StatUtil.exit();
					// Kill process
					System.exit(0);
					android.os.Process.killProcess(android.os.Process.myPid());
				}
			}, 500);
		}
	}

	// 退出程序
	public static void exit(Context context) {
		if (MainApplication.stopApplication()) {
			if (AQUtility.isDebug()) Log.i(TAG, "Exiting application.");
			isExit = true;
			Intent intent = new Intent(context, InitActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			context.startActivity(intent);
		} else {
			if (AQUtility.isDebug()) Log.i(TAG, "Hide application.");
			Intent intent = new Intent(Intent.ACTION_MAIN);
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			intent.addCategory(Intent.CATEGORY_HOME);
			context.startActivity(intent);
		}
	}
}
