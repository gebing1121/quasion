package com.quasion.agriculture;

import android.content.res.AssetManager;
import android.os.Environment;
import android.util.Log;
import com.androidquery.util.AQUtility;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.quasion.agriculture.util.AssetsUtil;
import com.quasion.agriculture.util.EasemobUtil;
import com.quasion.agriculture.util.TagUtil;
import com.umeng.update.UmengUpdateAgent;
import com.winsland.framework.BaseApplication;
import com.winsland.framework.util.SystemInfoUtil;

import java.io.File;

public class MainApplication extends BaseApplication {
	static final String TAG = TagUtil.getTag(MainApplication.class);

	@Override
	public void onCreate() {
		// 必须在BaseApplication.onCreate()之前执行
		ENABLE_DEBUG_MSG = true;
		ENABLE_TRACE_LIFECYCLE = false;
		NETWORK_THREAD_NUM = 4;
		BITMAP_THREAD_NUM = 4;
		FILE_CACHE_DIR = "{EXTERNAL_CACHE}";
		FILE_CACHE_MAX = 16L * 1024 * 1024;
		FILE_CACHE_KEEP = 10L * 1024 * 1024;
		// 调用BaseApplication的onCreate()
		super.onCreate();
		// 初始化本地常量
		init();
	}

	private void init() {
		// 初始化全局常量
		MainConstants.SystemVersion = SystemInfoUtil.getVersion();
		MainConstants.SystemDevice = SystemInfoUtil.getManufacturer() + "/" + SystemInfoUtil.getBrand() + "/" + SystemInfoUtil.getModel();
		MainConstants.SystemResolution = SystemInfoUtil.getResolution();
		MainConstants.SystemImei = SystemInfoUtil.getPhoneImei();
		MainConstants.SystemImsi = SystemInfoUtil.getSimImsi();
		MainConstants.SystemMac = SystemInfoUtil.getWifiMac();
		// 初始化IM
		EasemobUtil.init();
		// 检查更新
		if (MainConstants.AutoCheckUpdate) {
			UmengUpdateAgent.update(this);
		}
		// 拷贝assets目录下文件
		AQUtility.post(new Runnable() {
			@Override
			public void run() {
				processAssets();
			}
		});
	}

	private final static String ASSETS_CHECKSUM_FILE = "checksum";
	private final static String ASSETS_DATA_PATH = "data";
	private final static String[] ASSETS_RESOURCE_PATHS = { ASSETS_CHECKSUM_FILE, ASSETS_DATA_PATH, "html" };
	private void processAssets() {
		AssetManager assetManager = getAssets();
		// 验证checksum文件，确认是否需要拷贝assets目录下文件
		Log.i(TAG, "processAssets: verifying checksum");
		if (AssetsUtil.verifyFile(assetManager, ASSETS_CHECKSUM_FILE, new File(getFilesDir(), ASSETS_CHECKSUM_FILE))) {
			return;
		}
		// 将assets目录下指定的文件和目录拷贝到手机内存
		Log.i(TAG, "processAssets: copying files");
		for (String path : ASSETS_RESOURCE_PATHS) {
			AssetsUtil.copyFiles(assetManager, path, new File(getFilesDir(), path));
		}
		// 将assets目录下的指定数据保存到缓存
		Log.i(TAG, "processAssets: caching files");
//		AssetsUtil.cacheFile(assetManager, ClientProtocolApi.getNewsList(), ASSETS_DATA_PATH + File.separator + News.class.getSimpleName());
	}

	// 判断当前应用可以停止，停止是否会中断后台任务？返回：true - 没有后台任务，可以停止；false - 存在后台任务，不能停止
	public static boolean stopApplication() {
		// 释放数据库
		OpenHelperManager.releaseHelper();
		// 返回可以停止
		return true;
	}
}
