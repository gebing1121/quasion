package com.quasion.agriculture.bean;

/**
 * Author Gebing
 * Date: 2014/10/10
 */
public class Question {
	public String id;
	public String ask;
	public String[] askImage;
	public String answer;
	public String[] answerImage;
}
