package com.quasion.agriculture.bean;

/**
 * Author Gebing
 * Date: 2014/10/10
 */
public class CourseDetail {
	public String id;
	public String title;
	public String[] image;
	public String info;
}
