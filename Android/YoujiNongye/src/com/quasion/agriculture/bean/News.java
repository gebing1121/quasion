package com.quasion.agriculture.bean;

/**
 * Author Gebing
 * Date: 2014/09/27
 */
public class News {
	public String id;
	public String title;
	public String image;
	public String url;
}
