package com.quasion.agriculture.view.splash;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Window;
import com.androidquery.util.AQUtility;
import com.quasion.agriculture.R;
import com.quasion.agriculture.util.TagUtil;
import com.quasion.agriculture.view.home.HomeFragment;

public class SplashActivity extends Activity {
	private static final String TAG = TagUtil.getTag(SplashActivity.class);
	private static final long SPLASH_DELAY = 3000;

	public void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splash_layout);

		AQUtility.postDelayed(new Runnable() {
			@Override
			public void run() {
				startActivity(new Intent(SplashActivity.this, HomeFragment.class));
				overridePendingTransition(R.anim.activity_in_fade, R.anim.activity_out_fade);
			}
		}, SPLASH_DELAY);

	}
}
