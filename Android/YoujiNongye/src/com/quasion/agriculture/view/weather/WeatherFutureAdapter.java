package com.quasion.agriculture.view.weather;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import com.androidquery.AQuery;
import com.quasion.agriculture.R;
import com.quasion.agriculture.util.WeatherUtil;
import com.quasion.agriculture.weather.WeatherInfo;
import com.winsland.framework.util.ConvertUtil;

import java.util.Date;

/**
 * Author Gebing
 * Date: 2014/09/30
 */
public class WeatherFutureAdapter extends BaseAdapter {
	private AQuery aq;

	public WeatherFutureAdapter(Activity activity) {
		super();
		aq = new AQuery(activity);
	}

	@Override
	public int getCount() {
		if (WeatherActivity.weatherInfo == null || WeatherActivity.weatherInfo.weather == null) return 0;
		return WeatherActivity.weatherInfo.weather.size();
	}

	@Override
	public WeatherInfo getItem(int position) {
		if (position < 0 || position >= getCount()) return null;
		return WeatherActivity.weatherInfo.weather.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// get item and inflate view
		WeatherInfo weather = getItem(position);
		if (weather == null) return null;
		convertView = aq.inflate(convertView, R.layout.weather_future_item, parent);
		// reset aq to item view
		aq.recycle(convertView);
		int left = convertView.getPaddingLeft();
		int top = convertView.getPaddingTop();
		int right = convertView.getPaddingRight();
		int bottom = convertView.getPaddingBottom();
		convertView.setBackgroundResource(position == 0 ? R.drawable.weather_item_bg_current : R.drawable.weather_item_bg);
		convertView.setPadding(left, top, right, bottom);
		// show item
		aq.id(R.id.weather_future_item_date).text(getDateString(weather.date));
		aq.id(R.id.weather_future_item_temperature).text(weather.info.night[WeatherInfo.IDX_TEMPERATURE] + " - "
														 + weather.info.day[WeatherInfo.IDX_TEMPERATURE] + "℃");
		if (position == 0 && WeatherUtil.isNight()) {
			aq.id(R.id.weather_future_item_image).image(WeatherUtil.getWeatherImage(weather.info.night[WeatherInfo.IDX_DESC]));
			aq.id(R.id.weather_future_item_info).text(weather.info.night[WeatherInfo.IDX_DESC]);
		} else {
			aq.id(R.id.weather_future_item_image).image(WeatherUtil.getWeatherImage(weather.info.day[WeatherInfo.IDX_DESC]));
			aq.id(R.id.weather_future_item_info).text(weather.info.day[WeatherInfo.IDX_DESC]);
		}
		return convertView;
	}

	private static final String[] weekDay = { "日", "一", "二", "三", "四", "五", "六" };
	private String getDateString(String dateString) {
		Date date = ConvertUtil.parseDateTime(dateString, "yyyy-MM-dd");
		if (date == null) return "";
		else return String.valueOf(date.getMonth() + 1) + "月" + date.getDate() + "日 周" + weekDay[date.getDay()];
	}
}
