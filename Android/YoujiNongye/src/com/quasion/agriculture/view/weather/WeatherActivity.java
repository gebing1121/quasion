package com.quasion.agriculture.view.weather;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Toast;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.androidquery.util.AQUtility;
import com.google.gson.Gson;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshLayoutView;
import com.quasion.agriculture.Event.WeatherEvent;
import com.quasion.agriculture.MainConstants;
import com.quasion.agriculture.R;
import com.quasion.agriculture.protocol.ClientProtocolApi;
import com.quasion.agriculture.util.StatUtil;
import com.quasion.agriculture.util.TagUtil;
import com.quasion.agriculture.util.WeatherUtil;
import com.quasion.agriculture.weather.Province;
import com.quasion.agriculture.weather.WeatherApi;
import com.quasion.agriculture.weather.WeatherInfo;
import com.quasion.agriculture.weather.WeatherResponse;
import com.quasion.agriculture.widget.LoadingDialog;
import com.winsland.framework.protocol.BaseJsonProtocol;
import com.winsland.framework.util.CommonUtil;
import com.winsland.framework.util.PreferencesUtil;
import de.greenrobot.event.EventBus;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Author Gebing
 * Date: 2014/09/30
 */
public class WeatherActivity extends Activity {
	private static final String TAG = TagUtil.getTag(WeatherActivity.class);

	public static WeatherResponse weatherInfo = null;
	public static List<Province> areaInfo = Collections.synchronizedList(new ArrayList<Province>());

	public static void updateAreaInfo() {
		// 更新地区信息
		BaseJsonProtocol<Province[]> areaInfoRequest = ClientProtocolApi.getAreaInfo();
		areaInfoRequest.callback(new AjaxCallback<Province[]>() {
			@Override
			public void callback(String url, Province[] data, AjaxStatus status) {
				Log.i(TAG, "getAreaInfo() return from " + status.getSource() + ".\n" + new Gson().toJson(data));
				// 如果请求被取消，则直接退出
				if (getAbort()) return;
				// 如果获取数据失败，则不需要更新数据
				if (data == null) return;
				// 如果数据是从缓存获取，则不需要更新数据
				if (status.getSource() != AjaxStatus.NETWORK && areaInfo.size() > 0) return;
				// 更新地区信息
				areaInfo.clear();
				areaInfo.addAll(Arrays.asList(data));
			}
		});
		areaInfoRequest.getCallback().failCache(true);
		areaInfoRequest.execute(new AQuery(AQUtility.getContext()), MainConstants.AreaRefreshTime);
	}

	synchronized public static void updateWeather(boolean useCache) {
		// 更新气象信息
		String weatherCode = PreferencesUtil.getString(MainConstants.SP_RUNTIME, MainConstants.SP_RUNTIME_WEATHER_CODE, MainConstants.DEFAULT_WEATHER_CODE);
		BaseJsonProtocol<WeatherResponse> weatherRequest = WeatherApi.getWeather(weatherCode);
		weatherRequest.callback(new AjaxCallback<WeatherResponse>() {
			@Override
			public void callback(String url, WeatherResponse data, AjaxStatus status) {
				Log.i(TAG, "getWeather() return from " + status.getSource() + ".\n" + new Gson().toJson(data));
				// 如果请求被取消，则直接退出
				if (getAbort()) return;
				// 如果获取数据失败，则不需要更新显示
				if (data == null) {
					EventBus.getDefault().post(WeatherEvent.UpdateFailed);
					return;
				}
				// 如果数据是从缓存获取，则不需要更新显示
				if (status.getSource() != AjaxStatus.NETWORK && weatherInfo != null) {
					EventBus.getDefault().post(WeatherEvent.NoUpdate);
					return;
				}
				// 去除无效数据
				for (int i = 0; i < data.weather.size(); ) {
					WeatherInfo info = data.weather.get(i);
					if (info == null || info.date == null || info.info == null) {
						data.weather.remove(i);
					} else {
						i++;
					}
				}
				// 更新气象信息
				weatherInfo = data;
				EventBus.getDefault().post(WeatherEvent.UpdateSuccess);
			}
		});
		weatherRequest.getCallback().failCache(useCache);
		weatherRequest.execute(new AQuery(AQUtility.getContext()), useCache ? MainConstants.CacheRefreshTime : -1);
	}

	private AQuery aq;
	private LoadingDialog loadingDialog;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		aq = new AQuery(this);
		initDisplay();
		initData();

		// 注册EventBus，监听天气数据更新事件
		EventBus.getDefault().register(this);
	}

	@Override
	protected void onDestroy() {
		// 取消EventBus监听
		EventBus.getDefault().unregister(this);
		super.onDestroy();
	}

	@Override
	protected void onPause() {
		super.onPause();
		StatUtil.onPause(this);
	}
	@Override
	protected void onResume() {
		super.onResume();
		StatUtil.onResume(this);
	}

	private void initDisplay() {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.weather_layout);
		// init loading dialog
		loadingDialog = new LoadingDialog(this);
		// init title
		aq.id(R.id.common_title_text_title).text("天气情况");
		aq.id(R.id.common_title_btn_back).visible().clicked(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
		// init pull to refresh view
		PullToRefreshLayoutView pullToRefresh = (PullToRefreshLayoutView) aq.id(R.id.weather_content_layout).getView();
		pullToRefresh.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener<ViewGroup>() {
			@Override
			public void onRefresh(final PullToRefreshBase<ViewGroup> refreshView) {
				loadingDialog.show();
				updateWeather(false);
				AQUtility.post(new Runnable() {
					@Override
					public void run() {
						refreshView.onRefreshComplete();
					}
				});
			}
		});
		// init future list view and adapter
		aq.id(R.id.weather_list_future).adapter(new WeatherFutureAdapter(this));
	}

	private void initData() {
		loadingDialog.show();
		showWeather();
		updateWeather(true);
	}

	private void showWeather() {
		if (weatherInfo == null) return;
		aq.id(R.id.weather_head_area).text(weatherInfo.area[2][0]);
		aq.id(R.id.weather_head_desc).text(weatherInfo.realtime.weather.info);
		aq.id(R.id.weather_head_temperature).text(weatherInfo.realtime.weather.temperature + "℃");
		aq.id(R.id.weather_head_wind).text(weatherInfo.realtime.wind.direct + (weatherInfo.realtime.wind.power.indexOf("0级") < 0 ? weatherInfo.realtime.wind.power : ""));
		aq.id(R.id.weather_head_time).text(weatherInfo.realtime.time.substring(0, weatherInfo.realtime.time.lastIndexOf(":")) + " 发布");
		aq.id(R.id.weather_head_date).text(weatherInfo.realtime.date.replaceAll("-", "/"));
		aq.id(R.id.weather_head_image).image(WeatherUtil.getWeatherImage(weatherInfo.realtime.weather.info));
		aq.id(R.id.weather_list_future).dataChanged();
	}

	public void onEventMainThread(WeatherEvent event) {
		if (AQUtility.isDebug()) Log.i(TAG, "onEventMainThread(" + event + ")");
		loadingDialog.dismiss();
		if (event == WeatherEvent.UpdateSuccess) {
			showWeather();
		} else if (event == WeatherEvent.UpdateFailed) {
			CommonUtil.toast(Toast.LENGTH_LONG, "网络故障，无法获取天气信息！");
			if (weatherInfo == null) finish();
		}
	}
}
