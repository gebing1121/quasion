package com.quasion.agriculture.view.course;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.util.Log;
import android.view.*;
import android.view.ViewGroup.LayoutParams;
import android.widget.FrameLayout;
import android.widget.ImageView;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.androidquery.callback.ImageOptions;
import com.androidquery.util.AQUtility;
import com.google.gson.Gson;
import com.quasion.agriculture.MainConstants;
import com.quasion.agriculture.R;
import com.quasion.agriculture.bean.CourseDetail;
import com.quasion.agriculture.protocol.ClientProtocolApi;
import com.quasion.agriculture.util.StatUtil;
import com.quasion.agriculture.util.TagUtil;
import com.quasion.agriculture.widget.LoadingDialog;
import com.quasion.agriculture.widget.ScalingImageView;
import com.winsland.framework.protocol.BaseJsonProtocol;
import com.winsland.framework.util.BitmapUtil;

import java.util.ArrayList;
import java.util.List;

public class CourseDetailActivity extends Activity implements ViewPager.OnPageChangeListener, View.OnTouchListener {
	private static final String TAG = TagUtil.getTag(CourseDetailActivity.class);

	private AQuery aq;
	private ImageOptions imgOptions;
	private LoadingDialog loadingDialog;
	private int courseIndex;
	private ViewPager imagePager;
	private List<String[]> detailList = new ArrayList<String[]>();

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// init activity's parameter
		Bundle bundle = getIntent().getExtras();
		int currentCourse = bundle.getInt(MainConstants.INTENT_EXTRA_COURSE_INDEX);
		if (MainConstants.CourseList == null || MainConstants.CourseList.length == 0) {
			finish();
			return;
		}
		currentCourse = Math.min(Math.max(0, currentCourse), MainConstants.CourseList.length);

		int currentDetail = bundle.getInt(MainConstants.INTENT_EXTRA_COURSE_DETAIL);
		if (MainConstants.CourseDetail == null || MainConstants.CourseDetail.length == 0) {
			finish();
			return;
		}
		currentDetail = Math.min(Math.max(0, currentDetail), MainConstants.CourseDetail.length);

		aq = new AQuery(this);
		imgOptions = BitmapUtil.getLoadImageOptions(R.drawable.course_detail_item_img_default);
		// init display and data
		initDisplay();
		initData(currentCourse, currentDetail, false);
	}

	@Override
	protected void onPause() {
		super.onPause();
		StatUtil.onPause(this);
	}

	@Override
	protected void onResume() {
		super.onResume();
		StatUtil.onResume(this);
	}

	private void initDisplay() {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.course_detail_layout);
		// init loading dialog
		loadingDialog = new LoadingDialog(this);
		// init title
		aq.id(R.id.common_title_layout).backgroundColor(android.R.color.black);
		aq.id(R.id.common_title_btn_back).visible().clicked(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
		// init course item list view
		imagePager = (ViewPager) aq.id(R.id.course_detail_images).getView();
		imagePager.setAdapter(new ImageAdapter());
		imagePager.setOnPageChangeListener(this);
		imagePager.setOnTouchListener(this);
		imagePager.setPageMargin(getResources().getDimensionPixelSize(R.dimen.course_detail_imageMargin));
		// init preview
		aq.id(R.id.course_detail_preview).clicked(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				aq.id(R.id.course_detail_preview).gone();
			}
		});
	}

	private void initData(final int currentCourse, final int currentDetail, boolean refresh) {
		if (!refresh && MainConstants.CourseDetail != null) {
			showData(currentCourse, currentDetail);
			return;
		}
		// setup protocol request object
		final String courseId = MainConstants.CourseList[currentCourse].id;
		BaseJsonProtocol<CourseDetail[]> courseRequest = ClientProtocolApi.getCourseDetail(courseId);
		// setup request callback
		courseRequest.callback(new AjaxCallback<CourseDetail[]>() {
			@Override
			public void callback(String url, CourseDetail[] data, AjaxStatus status) {
				if (AQUtility.isDebug())
					Log.i(TAG, "getCourseDetail(" + courseId + ") return from " + status.getSource() + ".\n" + new Gson().toJson(data));
				loadingDialog.dismiss();
				// 如果请求被取消，则直接退出
				if (getAbort()) return;
				// 如果获取数据失败，则不需要更新显示
				if (data == null) return;
				// 更新显示
				MainConstants.CourseDetail = data;
				showData(currentCourse, currentDetail);
			}
		});
		// execute request to network if cache expired
		loadingDialog.show();
		courseRequest.getCallback().failCache(true);
		courseRequest.execute(aq, MainConstants.CacheRefreshTime);
	}

	private void showData(int currentCourse, int currentDetail) {
		if (MainConstants.CourseDetail == null) return;
		// save current course index
		courseIndex = currentCourse;
		// init view pager's image and info list, and current item
		int index = 0;
		detailList.clear();
		for (int i = 0; i < MainConstants.CourseDetail.length; i++) {
			CourseDetail detail = MainConstants.CourseDetail[i];
			String title = detail.title;
			String info = detail.info;
			if (i == currentDetail) index = detailList.size();
			for (String url : detail.image) {
				detailList.add(new String[] { url, title, info });
			}
		}
		imagePager.getAdapter().notifyDataSetChanged();
		if (index >= detailList.size()) index = 0;
		imagePager.setCurrentItem(index, false);
		// setup title
		aq.id(R.id.common_title_text_title).text(MainConstants.CourseList[currentCourse].title);
		aq.id(R.id.course_detail_total).text(String.valueOf(detailList.size()));
		onPageSelected(index);
	}

	private class ImageAdapter extends PagerAdapter {
		@Override
		public int getCount() {
			return detailList.size();
		}

		@Override
		public Object instantiateItem(ViewGroup container, int position) {
			if (position < 0 || position >= getCount()) return null;
			// display image
			FrameLayout layout = new FrameLayout(container.getContext());
			final ImageView view = new ScalingImageView(container.getContext());
			BitmapUtil.loadImage(aq.id(view), detailList.get(position)[0], imgOptions);
			layout.addView(view, new FrameLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT, Gravity.CENTER_VERTICAL));
			container.addView(layout, new ViewGroup.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
			// set image click listener
			view.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					aq.id(R.id.course_detail_preview).visible();
					aq.id(R.id.course_detail_previewImage).image(view.getDrawable());
				}
			});
			return layout;
		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			container.removeView((View) object);
		}

		@Override
		public boolean isViewFromObject(View view, Object object) {
			return view == object;
		}

		public int getItemPosition(Object object) {
			return POSITION_NONE;		// 使Adapter的notifyDataChanged()能够更新ViewPager的显示
		}
	}

	@Override
	public void onPageSelected(int position) {
		if (position < 0 || position >= detailList.size()) return;
		aq.id(R.id.course_detail_title).text(detailList.get(position)[1]);
		aq.id(R.id.course_detail_info).text(Html.fromHtml(detailList.get(position)[2]));
		aq.id(R.id.course_detail_current).text(String.valueOf(position + 1));
	}

	@Override
	public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
	}

	@Override
	public void onPageScrollStateChanged(int state) {
	}

	float startTouchX = -1, lastTouchX = -1;
	@Override
	public boolean onTouch(View v, MotionEvent event) {
		switch (event.getAction() & MotionEvent.ACTION_MASK) {
		case MotionEvent.ACTION_MOVE:
			if (startTouchX < 0) {
				startTouchX = event.getX();
			} else {
				lastTouchX = event.getX();
			}
			break;
		case MotionEvent.ACTION_UP:
			int currentDetail = imagePager.getCurrentItem();
			float triggerWith = imagePager.getWidth() * 0.3f;
			if (courseIndex > 0 && currentDetail == 0 && lastTouchX - startTouchX >= triggerWith) {
				initData(courseIndex - 1, 0, true);
			} else if (courseIndex < MainConstants.CourseList.length - 1 && currentDetail == detailList.size() - 1 && startTouchX - lastTouchX > triggerWith) {
				initData(courseIndex + 1, 0, true);
			}
		case MotionEvent.ACTION_DOWN:
		case MotionEvent.ACTION_CANCEL:
			startTouchX = lastTouchX = -1;
			break;
		}
		return false;
	}
}
