package com.quasion.agriculture.view.course;

import android.app.Activity;
import android.content.Intent;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import com.androidquery.AQuery;
import com.androidquery.callback.ImageOptions;
import com.quasion.agriculture.MainConstants;
import com.quasion.agriculture.R;
import com.quasion.agriculture.bean.CourseDetail;
import com.winsland.framework.util.BitmapUtil;
import com.winsland.framework.util.CommonUtil;


public class CourseListAdapter extends BaseAdapter{
	private AQuery aq;
	private int courseIndex;
	private ImageOptions imgOptions;

	public CourseListAdapter(Activity activity) {
		super();
		aq = new AQuery(activity);
		imgOptions = BitmapUtil.getLoadImageOptions(R.drawable.course_list_item_img_default);
	}

	public void setData(int courseIndex, CourseDetail[] data) {
		this.courseIndex = courseIndex;
		MainConstants.CourseDetail = data;
		notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		return MainConstants.CourseDetail == null ? 0 : MainConstants.CourseDetail.length;
	}

	@Override
	public CourseDetail getItem(int position) {
		if (position < 0 || position >= getCount()) return null;
		return MainConstants.CourseDetail[position];
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		CourseDetail item = getItem(position);
		if (item == null) return null;
		// inflate view
		convertView = aq.inflate(convertView, R.layout.course_list_item, parent);
		// reset aq to item view
		aq.recycle(convertView);
		// show item
		aq.id(R.id.course_list_item_title).text(item.title);
		aq.id(R.id.course_list_item_info).text(Html.fromHtml(item.info)).visibility(CommonUtil.isEmpty(item.info) ? View.GONE : View.VISIBLE);
		aq.id(R.id.course_list_item_multiImage).visibility(item.image.length > 1 ? View.VISIBLE : View.INVISIBLE);
		BitmapUtil.loadImage(aq.id(R.id.course_list_item_image), item.image[0], imgOptions);
		// setup click listener
		convertView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (MainConstants.CourseDetail == null || MainConstants.CourseDetail.length == 0) return;
				Intent intent = new Intent(aq.getContext(), CourseDetailActivity.class);
				intent.putExtra(MainConstants.INTENT_EXTRA_COURSE_INDEX, courseIndex);
				intent.putExtra(MainConstants.INTENT_EXTRA_COURSE_DETAIL, position);
				aq.getContext().startActivity(intent);
			}
		});
		return convertView;
	}
}
