package com.quasion.agriculture.view.course;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ListView;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.androidquery.util.AQUtility;
import com.google.gson.Gson;
import com.handmark.pulltorefresh.library.ILoadingLayout;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.quasion.agriculture.MainConstants;
import com.quasion.agriculture.R;
import com.quasion.agriculture.bean.CourseDetail;
import com.quasion.agriculture.protocol.ClientProtocolApi;
import com.quasion.agriculture.util.StatUtil;
import com.quasion.agriculture.util.TagUtil;
import com.winsland.framework.protocol.BaseJsonProtocol;

public class CourseListActivity extends Activity {
	private static final String TAG = TagUtil.getTag(CourseListActivity.class);

	private AQuery aq;
	private PullToRefreshListView pullToRefresh;
	private CourseListAdapter courseListAdapter;
	private int courseIndex;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Bundle bundle = getIntent().getExtras();
		courseIndex = bundle.getInt(MainConstants.INTENT_EXTRA_COURSE_INDEX);
		if (MainConstants.CourseList == null || MainConstants.CourseList.length == 0) {
			finish();
			return;
		}
		courseIndex = Math.min(Math.max(0, courseIndex), MainConstants.CourseList.length);

		aq = new AQuery(this);
		initDisplay();
		initData();
	}

	@Override
	protected void onPause() {
		super.onPause();
		StatUtil.onPause(this);
	}

	@Override
	protected void onResume() {
		super.onResume();
		StatUtil.onResume(this);
	}

	private void initDisplay() {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.course_list_layout);
		// init title
		aq.id(R.id.common_title_btn_back).visible().clicked(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
		// init pull to refresh view
		pullToRefresh = (PullToRefreshListView) aq.id(R.id.course_list_layout).getView();
		pullToRefresh.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener<ListView>() {
			@Override
			public void onRefresh(final PullToRefreshBase<ListView> refreshView) {
				if (refreshView.getState() == PullToRefreshBase.State.MANUAL_REFRESHING) {
					return;
				} else if (refreshView.getCurrentMode() == PullToRefreshBase.Mode.PULL_FROM_START && courseIndex > 0) {
					courseIndex--;
					initData();
				} else if (refreshView.getCurrentMode() == PullToRefreshBase.Mode.PULL_FROM_END && courseIndex < MainConstants.CourseList.length - 1) {
					courseIndex++;
					initData();
				} else {
					AQUtility.post(new Runnable() {
						@Override
						public void run() {
							refreshView.onRefreshComplete();
						}
					});
				}
			}
		});
		// init course item list view
		courseListAdapter = new CourseListAdapter(this);
		pullToRefresh.getRefreshableView().setAdapter(courseListAdapter);
	}

	private void initData() {
		// setup title
		aq.id(R.id.common_title_text_title).text(MainConstants.CourseList[courseIndex].title);
		// setup protocol request object
		final String courseId = MainConstants.CourseList[courseIndex].id;
		BaseJsonProtocol<CourseDetail[]> courseRequest = ClientProtocolApi.getCourseDetail(courseId);
		// setup request callback
		courseRequest.callback(new AjaxCallback<CourseDetail[]>() {
			@Override
			public void callback(String url, CourseDetail[] data, AjaxStatus status) {
				if (AQUtility.isDebug())
					Log.i(TAG, "getCourseDetail(" + courseId + ") return from " + status.getSource() + ".\n" + new Gson().toJson(data));
				pullToRefresh.onRefreshComplete();
				// 如果请求被取消，则直接退出
				if (getAbort()) return;
				// 如果获取数据失败，则不需要更新显示
				if (data == null) return;
				// 更新显示
				courseListAdapter.setData(courseIndex, data);
				ListView listView = pullToRefresh.getRefreshableView();
				if (listView.getSelectedItemPosition() != 0) {
					listView.requestFocusFromTouch();
					listView.setSelection(0);
					listView.smoothScrollToPosition(0);
				}
				// set pullToRefresh label
				if (courseIndex == 0 && courseIndex == MainConstants.CourseList.length - 1) {
					pullToRefresh.setMode(PullToRefreshBase.Mode.DISABLED);
				} else if (courseIndex == 0) {
					pullToRefresh.setMode(PullToRefreshBase.Mode.PULL_FROM_END);
					ILoadingLayout footerLayout = pullToRefresh.getLoadingLayoutProxy(false, true);
					footerLayout.setPullLabel("下一章 " + MainConstants.CourseList[courseIndex + 1].title);
					footerLayout.setReleaseLabel("下一章 " + MainConstants.CourseList[courseIndex + 1].title);
				} else if (courseIndex == MainConstants.CourseList.length - 1) {
					pullToRefresh.setMode(PullToRefreshBase.Mode.PULL_FROM_START);
					ILoadingLayout headerLayout = pullToRefresh.getLoadingLayoutProxy(true, false);
					headerLayout.setPullLabel("上一章 " + MainConstants.CourseList[courseIndex - 1].title);
					headerLayout.setReleaseLabel("上一章 " + MainConstants.CourseList[courseIndex - 1].title);
				} else {
					pullToRefresh.setMode(PullToRefreshBase.Mode.BOTH);
					ILoadingLayout headerLayout = pullToRefresh.getLoadingLayoutProxy(true, false);
					headerLayout.setPullLabel("上一章 " + MainConstants.CourseList[courseIndex - 1].title);
					headerLayout.setReleaseLabel("上一章 " + MainConstants.CourseList[courseIndex - 1].title);
					ILoadingLayout footerLayout = pullToRefresh.getLoadingLayoutProxy(false, true);
					footerLayout.setPullLabel("下一章 " + MainConstants.CourseList[courseIndex + 1].title);
					footerLayout.setReleaseLabel("下一章 " + MainConstants.CourseList[courseIndex + 1].title);
				}
			}
		});
		// execute request to network if cache expired
		pullToRefresh.setRefreshing();
		courseRequest.getCallback().failCache(true);
		courseRequest.execute(aq, MainConstants.CacheRefreshTime);
	}
}
