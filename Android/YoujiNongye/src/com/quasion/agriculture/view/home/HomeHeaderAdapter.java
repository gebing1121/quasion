package com.quasion.agriculture.view.home;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import com.androidquery.AQuery;
import com.androidquery.callback.ImageOptions;
import com.quasion.agriculture.MainConstants;
import com.quasion.agriculture.R;
import com.quasion.agriculture.bean.News;
import com.quasion.agriculture.view.news.NewsActivity;
import com.winsland.framework.util.BitmapUtil;

public class HomeHeaderAdapter extends PagerAdapter {
	private AQuery aq;
	private ImageOptions imgOptions;

	public HomeHeaderAdapter(Activity activity) {
		super();
		aq = new AQuery(activity);
		imgOptions = BitmapUtil.getLoadImageOptions(R.drawable.home_header_item_img_default);
	}

	@Override
	public int getCount() {
		return MainConstants.NewsList == null ? 0 : MainConstants.NewsList.length;
	}

	@Override
	public boolean isViewFromObject(View view, Object object) {
		return view == object;
	}

	@Override
	public Object instantiateItem(ViewGroup container, final int position) {
		if (position < 0 || position >= getCount()) return null;
		// inflate item view if necessary
		View view = aq.inflate(null, R.layout.home_header_item, container);
		container.addView(view);
		// reset aq to item view
		aq.recycle(view);
		// show item
		News item = MainConstants.NewsList[position];
		aq.id(R.id.home_header_item_title).text(item.title);
		BitmapUtil.loadImage(aq.id(R.id.home_header_item_image), item.image, imgOptions);
		view.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (MainConstants.NewsList == null || MainConstants.NewsList.length == 0) return;
				Intent intent = new Intent(aq.getContext(), NewsActivity.class);
				intent.putExtra(MainConstants.INTENT_EXTRA_NEWS_INDEX, position);
				aq.getContext().startActivity(intent);
			}
		});
		return view;
	}

	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		container.removeView((View) object);
	}
}
