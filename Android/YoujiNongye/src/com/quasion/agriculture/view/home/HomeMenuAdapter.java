package com.quasion.agriculture.view.home;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import com.androidquery.AQuery;
import com.androidquery.callback.ImageOptions;
import com.quasion.agriculture.MainConstants;
import com.quasion.agriculture.R;
import com.quasion.agriculture.bean.Course;
import com.quasion.agriculture.util.WeatherUtil;
import com.quasion.agriculture.view.course.CourseListActivity;
import com.quasion.agriculture.view.weather.WeatherActivity;
import com.quasion.agriculture.weather.WeatherInfo;
import com.quasion.agriculture.weather.WeatherResponse;
import com.winsland.framework.util.BitmapUtil;


public class HomeMenuAdapter extends BaseAdapter{
	private AQuery aq;
	private ImageOptions imgOptions;

	public HomeMenuAdapter(Activity activity) {
		super();
		aq = new AQuery(activity);
		imgOptions = BitmapUtil.getLoadImageOptions(R.drawable.home_menu_item_img_default);
	}

	@Override
	public int getCount() {
		int count = MainConstants.CourseList == null ? 0 : MainConstants.CourseList.length;
		return count + 1;
	}

	@Override
	public Course getItem(int index) {
		if (index <= 0 || index >= getCount()) return null;
		return MainConstants.CourseList[index - 1];
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		Course item = getItem(position);
		if (position == 0) {
			// weather forecast
			convertView = aq.inflate(convertView, R.layout.home_weather_item, parent);
			// show weather
			showWeather(convertView);
			// setup click listener
			convertView.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					aq.getContext().startActivity(new Intent(aq.getContext(), WeatherActivity.class));
				}
			});
		} else if (item != null) {
			// course menu
			convertView = aq.inflate(convertView, R.layout.home_menu_item, parent);
			// reset aq to item view
			aq.recycle(convertView);
			// show item
			aq.id(R.id.home_menu_item_title).text(item.title);
			BitmapUtil.loadImage(aq.id(R.id.home_menu_item_image), item.image, imgOptions);
			// setup click listener
			final String id = item.id;
			convertView.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if (MainConstants.CourseList == null || MainConstants.CourseList.length == 0) return;
					Intent intent = new Intent(aq.getContext(), CourseListActivity.class);
					intent.putExtra(MainConstants.INTENT_EXTRA_COURSE_INDEX, position - 1);
					aq.getContext().startActivity(intent);
				}
			});
		}
		return convertView;
	}

	public void showWeather(View view) {
		aq.recycle(view);
		WeatherResponse weatherInfo = WeatherActivity.weatherInfo;
		if (weatherInfo != null) {
			aq.id(R.id.home_weather_item_area).text(weatherInfo.area[2][0]);
			aq.id(R.id.home_weather_item_image).image(WeatherUtil.getWeatherImage(weatherInfo.realtime.weather.info)).visible();
			aq.id(R.id.home_weather_item_info).text(weatherInfo.realtime.weather.info);
			WeatherInfo weather = weatherInfo.weather.get(0);
			aq.id(R.id.home_weather_item_temperature).text(weather.info.night[WeatherInfo.IDX_TEMPERATURE] + "/"
														   + weather.info.day[WeatherInfo.IDX_TEMPERATURE] + "℃");
		} else {
			aq.id(R.id.home_weather_item_area).text("");
			aq.id(R.id.home_weather_item_temperature).text("");
			aq.id(R.id.home_weather_item_image).invisible();
			aq.id(R.id.home_weather_item_info).text("无法获取天气信息");
		}
	}
}
