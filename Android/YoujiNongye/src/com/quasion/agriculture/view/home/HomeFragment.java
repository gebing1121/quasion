package com.quasion.agriculture.view.home;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ScrollView;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.androidquery.util.AQUtility;
import com.google.gson.Gson;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshScrollView;
import com.quasion.agriculture.Event.WeatherEvent;
import com.quasion.agriculture.MainConstants;
import com.quasion.agriculture.R;
import com.quasion.agriculture.bean.Course;
import com.quasion.agriculture.bean.News;
import com.quasion.agriculture.protocol.ClientProtocolApi;
import com.quasion.agriculture.util.StatUtil;
import com.quasion.agriculture.util.TagUtil;
import com.quasion.agriculture.view.setting.SettingActivity;
import com.quasion.agriculture.view.weather.WeatherActivity;
import com.quasion.agriculture.widget.LoadingDialog;
import com.viewpagerindicator.PageIndicator;
import com.winsland.framework.protocol.BaseJsonProtocol;
import de.greenrobot.event.EventBus;

public class HomeFragment extends Fragment {
	private static final String TAG = TagUtil.getTag(HomeFragment.class);

	private AQuery aq;
	private LoadingDialog loadingDialog;
	private ViewPager headerPager;
	private HomeHeaderAdapter headerAdapter;
	private GridView menuGrid;
	private HomeMenuAdapter menuAdapter;
	private PullToRefreshScrollView pullToRefresh;
	private HeaderAutoScroll headerAutoScroll = new HeaderAutoScroll();

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.home_layout, container, false);

		aq = new AQuery(getActivity(), view);
		initDisplay();
		initData(true);

		// 注册EventBus，监听天气数据更新事件
		EventBus.getDefault().register(this);
		return view;
	}

	@Override
	public void onDestroyView() {
		// 取消EventBus监听
		EventBus.getDefault().unregister(this);
		super.onDestroyView();
	}

	@Override
	public void onResume() {
		super.onResume();
		StatUtil.onResume(this);
		headerAutoScroll.start();
	}

	@Override
	public void onPause() {
		super.onPause();
		StatUtil.onPause(this);
		headerAutoScroll.stop();
	}

	private void initDisplay() {
		// init loading dialog
		loadingDialog = new LoadingDialog(getActivity());
		// init title
		aq.id(R.id.common_title_text_title).text("首页");
		aq.id(R.id.common_title_btn_back).gone();
		aq.id(R.id.common_title_btn_setting).visible().clicked(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivity(new Intent(getActivity(), SettingActivity.class));
			}
		});
		// init push to refresh
		pullToRefresh = (PullToRefreshScrollView) aq.id(R.id.home_content_layout).getView();
		pullToRefresh.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener<ScrollView>() {
			@Override
			public void onRefresh(final PullToRefreshBase<ScrollView> refreshView) {
				initData(false);
				WeatherActivity.updateWeather(false);
				AQUtility.post(new Runnable() {
					@Override
					public void run() {
						refreshView.onRefreshComplete();
					}
				});
			}
		});
		// init header view pager
		headerAdapter = new HomeHeaderAdapter(getActivity());
		headerPager = (ViewPager) aq.id(R.id.home_headerPager).getView();
		headerPager.setAdapter(headerAdapter);
		PageIndicator pageIndicator = (PageIndicator) aq.id(R.id.home_headerIndicator).getView();
		pageIndicator.setViewPager(headerPager);
		// init menu grid view
		menuAdapter = new HomeMenuAdapter(getActivity());
		menuGrid = aq.id(R.id.home_menuGrid).getGridView();
		menuGrid.setAdapter(menuAdapter);
	}

	private BaseJsonProtocol<News[]> newsRequest;
	private BaseJsonProtocol<Course[]> menuRequest;
	private void initData(boolean useCache) {
		// setup protocol request object
		if (newsRequest == null) {
			newsRequest = ClientProtocolApi.getNewsList();
			newsRequest.callback(new AjaxCallback<News[]>() {
				@Override
				public void callback(String url, News[] data, AjaxStatus status) {
					if (AQUtility.isDebug())
						Log.i(TAG, "getNewsList() return from " + status.getSource() + ".\n" + new Gson().toJson(data));
					loadingDialog.dismiss();
					// 如果请求被取消，则直接退出
					if (getAbort()) return;
					// 如果获取数据失败，则不需要更新显示
					if (data == null) return;
					// 如果数据是从缓存获取，则不需要更新显示
					if (status.getSource() != AjaxStatus.NETWORK && MainConstants.NewsList != null) return;
					// 保存数据并更新显示
					MainConstants.NewsList = data;
					headerAdapter.notifyDataSetChanged();
				}
			});
		}
		if (menuRequest == null) {
			menuRequest = ClientProtocolApi.getCourseList();
			menuRequest.callback(new AjaxCallback<Course[]>() {
				@Override
				public void callback(String url, Course[] data, AjaxStatus status) {
					if (AQUtility.isDebug())
						Log.i(TAG, "getCourseList() return from " + status.getSource() + ".\n" + new Gson().toJson(data));
					loadingDialog.dismiss();
					// 如果请求被取消，则直接退出
					if (getAbort()) return;
					// 如果获取数据失败，则不需要更新显示
					if (data == null) return;
					// 如果数据是从缓存获取，则不需要更新显示
					if (status.getSource() != AjaxStatus.NETWORK && MainConstants.CourseList != null) return;
					// 保存数据并更新显示
					MainConstants.CourseList = data;
					menuAdapter.notifyDataSetChanged();
					// 设置Grid的高度以显示全部内容
					menuGrid.measure(0, View.MeasureSpec.makeMeasureSpec(Integer.MAX_VALUE >> 2, View.MeasureSpec.AT_MOST));
					ViewGroup.LayoutParams layoutParams = menuGrid.getLayoutParams();
					layoutParams.height = menuGrid.getMeasuredHeight();
					menuGrid.setLayoutParams(layoutParams);
					// 滚动到顶部
					AQUtility.post(new Runnable() {
						@Override
						public void run() {
							pullToRefresh.getRefreshableView().smoothScrollTo(0, 0);
						}
					});
				}
			});
		}
		// show data from cache and execute request to network if cache expired
		loadingDialog.show();
		newsRequest.getCallback().failCache(useCache);
		menuRequest.getCallback().failCache(useCache);
		newsRequest.execute(aq, useCache ? MainConstants.CacheRefreshTime : -1);
		menuRequest.execute(aq, useCache ? MainConstants.CacheRefreshTime : -1);
	}

	public void onEventMainThread(WeatherEvent event) {
		if (AQUtility.isDebug()) Log.i(TAG, "onEventMainThread(" + event + ")");
		if (event == WeatherEvent.UpdateSuccess) {
			if (menuGrid.getFirstVisiblePosition() <= 0) {
				menuAdapter.showWeather(menuGrid.getChildAt(0));
			}
		}
	}

	class HeaderAutoScroll {
		private static final long SCROLL_TIME = 5 * 1000;
		private void stop() {
			AQUtility.removePost(runnable);
		}

		private void start() {
			AQUtility.postDelayed(runnable, SCROLL_TIME);
		}

		final private Runnable runnable = new Runnable() {
			@Override
			public void run() {
				if (isRemoving() || isHidden() || isDetached()) return;
				if (isVisible() && headerPager != null && headerAdapter != null) {
					if (headerAdapter.getCount() > 1) {
						int next = (headerPager.getCurrentItem() + 1) % headerAdapter.getCount();
						headerPager.setCurrentItem(next, true);
					}
				}
				AQUtility.postDelayed(runnable, SCROLL_TIME);
			}
		};
	}
}
