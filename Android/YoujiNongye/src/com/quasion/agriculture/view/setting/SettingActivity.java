package com.quasion.agriculture.view.setting;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Toast;
import com.androidquery.AQuery;
import com.quasion.agriculture.R;
import com.quasion.agriculture.util.StatUtil;
import com.quasion.agriculture.util.TagUtil;
import com.quasion.agriculture.widget.LoadingDialog;
import com.umeng.update.UmengUpdateAgent;
import com.umeng.update.UmengUpdateListener;
import com.umeng.update.UpdateResponse;
import com.umeng.update.UpdateStatus;
import com.winsland.framework.util.CommonUtil;

/**
 * Author Gebing
 * Date: 2014/09/30
 */
public class SettingActivity extends Activity {
	private static final String TAG = TagUtil.getTag(SettingActivity.class);

	private AQuery aq;
	private UpdateResponse updateInfo;
	private LoadingDialog loadingDialog;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		aq = new AQuery(this);
		initDisplay();
		initData();
	}

	@Override
	protected void onPause() {
		super.onPause();
		StatUtil.onPause(this);
	}
	@Override
	protected void onResume() {
		super.onResume();
		StatUtil.onResume(this);
	}

	private void initDisplay() {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.setting_layout);
		// init loading dialog
		loadingDialog = new LoadingDialog(this);
		// init title
		aq.id(R.id.common_title_text_title).text("设置");
		aq.id(R.id.common_title_btn_back).visible().clicked(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
		// init setting buttons
		aq.id(R.id.setting_btn_update).clicked(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				UmengUpdateAgent.showUpdateDialog(SettingActivity.this, updateInfo);
			}
		});
	}

	private void initData() {
		// check for update
		UmengUpdateAgent.setUpdateAutoPopup(false);
		UmengUpdateAgent.setUpdateListener(new UmengUpdateListener() {
			@Override
			public void onUpdateReturned(int updateStatus, UpdateResponse updateResponse) {
				loadingDialog.dismiss();
				switch (updateStatus) {
				case UpdateStatus.Yes: // has update
					updateInfo = updateResponse;
					aq.id(R.id.setting_btn_update_new).visible();
					UmengUpdateAgent.showUpdateNotification(SettingActivity.this, updateResponse);
					break;
				case UpdateStatus.No: // has no update
					aq.id(R.id.setting_btn_update_new).invisible();
					break;
				case UpdateStatus.NoneWifi: // none wifi
				case UpdateStatus.Timeout: // time out
					CommonUtil.toast(Toast.LENGTH_SHORT, "检测客户端更新失败，请确认网络是否正常！");
					break;
				}
			}
		});
		loadingDialog.show("检测更新");
		UmengUpdateAgent.forceUpdate(this);
	}
}
