package com.quasion.agriculture.view.news;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.androidquery.AQuery;
import com.androidquery.util.AQUtility;
import com.handmark.pulltorefresh.library.ILoadingLayout;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshWebView;
import com.quasion.agriculture.MainConstants;
import com.quasion.agriculture.R;
import com.quasion.agriculture.util.StatUtil;
import com.quasion.agriculture.util.TagUtil;
import com.quasion.agriculture.widget.LoadingDialog;

/**
 * Author Gebing
 * Date: 2014/09/30
 */
public class NewsActivity extends Activity {
	private static final String TAG = TagUtil.getTag(NewsActivity.class);

	private AQuery aq;
	private LoadingDialog loadingDialog;
	private PullToRefreshWebView pullToRefresh;
	private int newsIndex;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Bundle bundle = getIntent().getExtras();
		newsIndex = bundle.getInt(MainConstants.INTENT_EXTRA_NEWS_INDEX);
		if (MainConstants.NewsList == null || MainConstants.NewsList.length == 0) {
			finish();
			return;
		}

		aq = new AQuery(this);
		initDisplay();
		showNews();
	}

	@Override
	protected void onPause() {
		super.onPause();
		StatUtil.onPause(this);
	}

	@Override
	protected void onResume() {
		super.onResume();
		StatUtil.onResume(this);
	}

	private void initDisplay() {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.news_layout);
		// init loading dialog
		loadingDialog = new LoadingDialog(this);
		// init title
		aq.id(R.id.common_title_text_title).text("农业动态");
		aq.id(R.id.common_title_btn_back).visible().clicked(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
		// init pull to refresh view
		pullToRefresh = (PullToRefreshWebView) aq.id(R.id.news_content_layout).getView();
		pullToRefresh.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener<WebView>() {
			@Override
			public void onRefresh(final PullToRefreshBase<WebView> refreshView) {
				if (refreshView.getState() == PullToRefreshBase.State.MANUAL_REFRESHING) {
					return;
				} else if (refreshView.getCurrentMode() == PullToRefreshBase.Mode.PULL_FROM_START && newsIndex > 0) {
					newsIndex--;
					showNews();
				} else if (refreshView.getCurrentMode() == PullToRefreshBase.Mode.PULL_FROM_END && newsIndex < MainConstants.NewsList.length - 1) {
					newsIndex++;
					showNews();
				} else {
					AQUtility.post(new Runnable() {
						@Override
						public void run() {
							refreshView.onRefreshComplete();
						}
					});
				}
			}
		});
		// init news web view
		WebView webView = pullToRefresh.getRefreshableView();
		WebSettings websettings = webView.getSettings();
		websettings.setJavaScriptEnabled(true);
		websettings.setPluginState(WebSettings.PluginState.ON);
		websettings.setRenderPriority(WebSettings.RenderPriority.HIGH);
		websettings.setCacheMode(WebSettings.LOAD_NO_CACHE);
		websettings.setAppCacheEnabled(true);
		websettings.setDomStorageEnabled(true);
		websettings.setAllowFileAccess(true);
		websettings.setSavePassword(false);
		websettings.setUseWideViewPort(true);
		websettings.setBuiltInZoomControls(false);
		websettings.setSupportZoom(false);
		websettings.setNeedInitialFocus(false);
		websettings.setLoadWithOverviewMode(true);
		websettings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NORMAL);
		websettings.setLoadsImagesAutomatically(true);
		webView.setVerticalScrollBarEnabled(false);
		webView.setHorizontalScrollBarEnabled(false);
		webView.setWebChromeClient(new WebChromeClient());
		webView.setWebViewClient(new WebViewClient() {
			@Override
			public void onPageFinished(WebView view, String url) {
				super.onPageFinished(view, url);
				loadingDialog.dismiss();
				pullToRefresh.onRefreshComplete();
			}
		});
	}

	private void showNews() {
		// loading web view content
		if (newsIndex < 0 || newsIndex >= MainConstants.NewsList.length) return;
		loadingDialog.show();
		pullToRefresh.getRefreshableView().loadUrl(MainConstants.NewsList[newsIndex].url);
		// set pullToRefresh label
		if (newsIndex == 0 && newsIndex == MainConstants.NewsList.length - 1) {
			pullToRefresh.setMode(PullToRefreshBase.Mode.DISABLED);
		} else if (newsIndex == 0) {
			pullToRefresh.setMode(PullToRefreshBase.Mode.PULL_FROM_END);
			ILoadingLayout footerLayout = pullToRefresh.getLoadingLayoutProxy(false, true);
			footerLayout.setPullLabel(MainConstants.NewsList[newsIndex + 1].title);
			footerLayout.setReleaseLabel(MainConstants.NewsList[newsIndex + 1].title);
		} else if (newsIndex == MainConstants.NewsList.length - 1) {
			ILoadingLayout headerLayout = pullToRefresh.getLoadingLayoutProxy(true, false);
			pullToRefresh.setMode(PullToRefreshBase.Mode.PULL_FROM_START);
			headerLayout.setPullLabel(MainConstants.NewsList[newsIndex - 1].title);
			headerLayout.setReleaseLabel(MainConstants.NewsList[newsIndex - 1].title);
		} else {
			pullToRefresh.setMode(PullToRefreshBase.Mode.BOTH);
			ILoadingLayout headerLayout = pullToRefresh.getLoadingLayoutProxy(true, false);
			headerLayout.setPullLabel(MainConstants.NewsList[newsIndex - 1].title);
			headerLayout.setReleaseLabel(MainConstants.NewsList[newsIndex - 1].title);
			ILoadingLayout footerLayout = pullToRefresh.getLoadingLayoutProxy(false, true);
			footerLayout.setPullLabel(MainConstants.NewsList[newsIndex + 1].title);
			footerLayout.setReleaseLabel(MainConstants.NewsList[newsIndex + 1].title);
		}
	}
}
