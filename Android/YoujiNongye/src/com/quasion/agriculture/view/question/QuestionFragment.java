package com.quasion.agriculture.view.question;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.FrameLayout;
import android.widget.ListView;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.androidquery.util.AQUtility;
import com.google.gson.Gson;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.quasion.agriculture.MainConstants;
import com.quasion.agriculture.R;
import com.quasion.agriculture.bean.Question;
import com.quasion.agriculture.bean.QuestionNews;
import com.quasion.agriculture.protocol.ClientProtocolApi;
import com.quasion.agriculture.util.StatUtil;
import com.quasion.agriculture.util.TagUtil;
import com.quasion.agriculture.view.splash.SplashActivity;
import com.quasion.agriculture.widget.LoadingDialog;
import com.viewpagerindicator.PageIndicator;
import com.winsland.framework.protocol.BaseJsonProtocol;

/**
 * Author Gebing
 * Date: 2014/10/12
 */
public class QuestionFragment extends Fragment {
	private static final String TAG = TagUtil.getTag(QuestionFragment.class);

	private AQuery aq;
	private LoadingDialog loadingDialog;
	private QuestionListAdapter listAdapter;
	private ViewPager headerPager;
	private QuestionHeaderAdapter headerAdapter;
	private PullToRefreshListView pullToRefresh;
	private HeaderAutoScroll headerAutoScroll = new HeaderAutoScroll();

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.question_layout, container, false);

		aq = new AQuery(getActivity(), view);
		initDisplay();
		initData(true);
		return view;
	}

	@Override
	public void onResume() {
		super.onResume();
		StatUtil.onResume(this);
		headerAutoScroll.start();
	}

	@Override
	public void onPause() {
		super.onPause();
		StatUtil.onPause(this);
		headerAutoScroll.stop();
	}

	private void initDisplay() {
		// init loading dialog
		loadingDialog = new LoadingDialog(getActivity());
		// init title
		aq.id(R.id.common_title_text_title).text("全部");
		aq.id(R.id.common_title_btn_back).gone();
		// init push to refresh
		pullToRefresh = (PullToRefreshListView) aq.id(R.id.question_content_layout).getView();
		pullToRefresh.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener<ListView>() {
			@Override
			public void onRefresh(final PullToRefreshBase<ListView> refreshView) {
				initData(false);
				AQUtility.post(new Runnable() {
					@Override
					public void run() {
						refreshView.onRefreshComplete();
					}
				});
			}
		});
		// init list view and adapter
		listAdapter = new QuestionListAdapter(getActivity());
		pullToRefresh.getRefreshableView().setAdapter(listAdapter);
		// init header view
		View headerView = aq.inflate(null, R.layout.question_header_layout, null);
		pullToRefresh.getRefreshableView().addHeaderView(headerView);
		headerAdapter = new QuestionHeaderAdapter(getActivity());
		headerPager = (ViewPager) aq.id(R.id.question_header_images).getView();
		headerPager.setAdapter(headerAdapter);
		PageIndicator pageIndicator = (PageIndicator) aq.id(R.id.question_header_indicator).getView();
		pageIndicator.setViewPager(headerPager);
		aq.id(R.id.question_header_btn_ask).clicked(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getActivity(), MyQuestionActivity.class);
				startActivity(intent);
			}
		});
		// init footer view
		View footerView = new FrameLayout(aq.getContext());
		int height = aq.getContext().getResources().getDimensionPixelSize(R.dimen.main_tab_height);
		footerView.setLayoutParams(new AbsListView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, height));
		pullToRefresh.getRefreshableView().addFooterView(footerView);
	}

	private BaseJsonProtocol<QuestionNews[]> newsRequest;
	private BaseJsonProtocol<Question[]> listRequest;
	private void initData(boolean useCache) {
		// setup protocol request object
		if (newsRequest == null) {
			newsRequest = ClientProtocolApi.getQuestionNews();
			newsRequest.callback(new AjaxCallback<QuestionNews[]>() {
				@Override
				public void callback(String url, QuestionNews[] data, AjaxStatus status) {
					if (AQUtility.isDebug())
						Log.i(TAG, "getQuestionNews() return from " + status.getSource() + ".\n" + new Gson().toJson(data));
					loadingDialog.dismiss();
					// 如果请求被取消，则直接退出
					if (getAbort()) return;
					// 如果获取数据失败，则不需要更新显示
					if (data == null) return;
					// 如果数据是从缓存获取，则不需要更新显示
					if (status.getSource() != AjaxStatus.NETWORK && MainConstants.QuestionNews != null) return;
					// 保存数据并更新显示
					MainConstants.QuestionNews = data;
					if (listRequest.getFinished()) {
						refreshDisplay();
					}
				}
			});
		}
		if (listRequest == null) {
			listRequest = ClientProtocolApi.getQuestionList();
			listRequest.callback(new AjaxCallback<Question[]>() {
				@Override
				public void callback(String url, Question[] data, AjaxStatus status) {
					if (AQUtility.isDebug())
						Log.i(TAG, "getCourseList() return from " + status.getSource() + ".\n" + new Gson().toJson(data));
					loadingDialog.dismiss();
					// 如果请求被取消，则直接退出
					if (getAbort()) return;
					// 如果获取数据失败，则不需要更新显示
					if (data == null) return;
					// 如果数据是从缓存获取，则不需要更新显示
					if (status.getSource() != AjaxStatus.NETWORK && MainConstants.QuestionList != null) return;
					// 保存数据并更新显示
					MainConstants.QuestionList = data;
					if (newsRequest.getFinished()) {
						refreshDisplay();
					}
				}
			});
		}
		// show data from cache and execute request to network if cache expired
		loadingDialog.show();
		newsRequest.getCallback().failCache(useCache);
		listRequest.getCallback().failCache(useCache);
		newsRequest.execute(aq, useCache ? MainConstants.CacheRefreshTime : -1);
		listRequest.execute(aq, useCache ? MainConstants.CacheRefreshTime : -1);
	}

	private void refreshDisplay() {
		headerAdapter.notifyDataSetChanged();
		listAdapter.notifyDataSetChanged();
		ListView listView = pullToRefresh.getRefreshableView();
		if (listView.getSelectedItemPosition() != 0) {
			listView.requestFocusFromTouch();
			listView.setSelection(0);
		}
		listView.scrollTo(0, 0);
	}

	class HeaderAutoScroll {
		private static final long SCROLL_TIME = 5 * 1000;
		private void stop() {
			AQUtility.removePost(runnable);
		}

		private void start() {
			AQUtility.postDelayed(runnable, SCROLL_TIME);
		}

		final private Runnable runnable = new Runnable() {
			@Override
			public void run() {
				if (isRemoving() || isHidden() || isDetached()) return;
				if (isVisible() && headerPager != null && headerAdapter != null) {
					if (headerAdapter.getCount() > 1) {
						int next = (headerPager.getCurrentItem() + 1) % headerAdapter.getCount();
						headerPager.setCurrentItem(next, true);
					}
				}
				AQUtility.postDelayed(runnable, SCROLL_TIME);
			}
		};
	}
}
