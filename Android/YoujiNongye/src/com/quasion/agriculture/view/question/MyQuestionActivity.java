package com.quasion.agriculture.view.question;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.TextView;
import com.androidquery.AQuery;
import com.androidquery.util.AQUtility;
import com.easemob.EMCallBack;
import com.easemob.chat.*;
import com.easemob.util.NetUtils;
import com.easemob.util.PathUtil;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.quasion.agriculture.R;
import com.quasion.agriculture.util.StatUtil;
import com.quasion.agriculture.util.TagUtil;
import com.quasion.agriculture.widget.LoadingDialog;
import com.winsland.framework.util.CommonUtil;
import com.winsland.framework.util.InputMethodUtil;

import java.io.File;

public class MyQuestionActivity extends Activity {
	private static final String TAG = TagUtil.getTag(MyQuestionActivity.class);

	private static final int REQUEST_CODE_ALBUM = 0;
	private static final int REQUEST_CODE_CAMERA = 1;

	private AQuery aq;
	private LoadingDialog loadingDialog;
	private File localImageFile;
	private ListView listView;
	private MyQuestionAdapter listAdapter;
	private BroadcastReceiver messageReceiver;
	private EMConversation conversation;
	private String chat_login_username;
	private String chat_login_password;
	private String chat_to_username;

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == RESULT_OK) {
			switch (requestCode) {
			case REQUEST_CODE_CAMERA:
				aq.id(R.id.my_question_photo_layout).gone();
				sendImageMessage(localImageFile);
				break;
			case REQUEST_CODE_ALBUM:
				aq.id(R.id.my_question_photo_layout).gone();
				if (data != null && data.getData() != null) {
					Uri uri = data.getData();
					Cursor cursor = getContentResolver().query(uri, new String[]{MediaStore.Images.Media.DATA}, null, null, null);
					cursor.moveToFirst();
					int index = cursor.getColumnIndex(MediaStore.Images.Media.DATA);
					File imagePath = new File(cursor.getString(index));
					cursor.close();
					sendImageMessage(imagePath);
				}
				break;
			}
		}
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		aq = new AQuery(this);
		chat_login_username = getResources().getString(R.string.chat_login_user);
		chat_login_password = getResources().getString(R.string.chat_login_pass);
		chat_to_username = getResources().getString(R.string.chat_to_user);
		initDisplay();
		initData();
	}

	@Override
	protected void onDestroy() {
		if (messageReceiver != null) unregisterReceiver(messageReceiver);
		messageReceiver = null;
		super.onDestroy();
	}

	@Override
	protected void onPause() {
		super.onPause();
		StatUtil.onPause(this);
	}

	@Override
	protected void onResume() {
		super.onResume();
		StatUtil.onResume(this);
	}

	private void initDisplay() {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.my_question_layout);
		// init loading dialog
		loadingDialog = new LoadingDialog(this);
		// init title
		aq.id(R.id.common_title_text_title).text("我的问题");
		aq.id(R.id.common_title_btn_back).visible().clicked(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
		// init pull to updateDisplay view
		PullToRefreshListView pullToRefresh = (PullToRefreshListView) aq.id(R.id.my_question_list).getView();
		pullToRefresh.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener<ListView>() {
			@Override
			public void onRefresh(final PullToRefreshBase<ListView> refreshView) {
				if (refreshView.getState() == PullToRefreshBase.State.MANUAL_REFRESHING) {
					return;
				} else {
					// todo:
				}
				AQUtility.post(new Runnable() {
					@Override
					public void run() {
						refreshView.onRefreshComplete();
					}
				});
			}
		});
		// init course item list view
		listView = pullToRefresh.getRefreshableView();
		listAdapter = new MyQuestionAdapter(listView);
		listView.setAdapter(listAdapter);
		// init navigator view
		aq.id(R.id.my_question_btn_photo).clicked(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				InputMethodUtil.hideSoftInput(v);
				aq.id(R.id.my_question_photo_layout).visible();
			}
		});
		aq.id(R.id.my_question_text_message).getEditText().setOnEditorActionListener(new TextView.OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				aq.id(R.id.my_question_btn_send).click();
				return true;
			}
		});
		aq.id(R.id.my_question_text_message).textChanged(new Object() {
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				aq.id(R.id.my_question_btn_send).enabled(!CommonUtil.isEmpty(s.toString()));
			}
		}, "onTextChanged");
		aq.id(R.id.my_question_btn_send).clicked(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				String s = aq.id(R.id.my_question_text_message).getText().toString();
				if (CommonUtil.isEmpty(s)) {
					CommonUtil.toast("不能发送空消息！");
					return;
				}
				sendTextMessage(s);
				// 清楚输入框原有文字
				aq.id(R.id.my_question_text_message).text("");
				// 隐藏输入法和清除输入焦点
				InputMethodUtil.hideSoftInput(v);
			}
		});
		// init photo layout
		aq.id(R.id.my_question_btn_camera_layout).clicked(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
				localImageFile = new File(PathUtil.getInstance().getImagePath(), "TX_" + String.valueOf(System.currentTimeMillis()) + ".jpg");
				intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(localImageFile));
				startActivityForResult(intent, REQUEST_CODE_CAMERA);
			}
		});
		aq.id(R.id.my_question_btn_album_layout).clicked(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent;
				if (Build.VERSION.SDK_INT < 19) {
					intent = new Intent(Intent.ACTION_GET_CONTENT);
					intent.setType("image/*");
					intent.putExtra("return-data", true);
				} else {
					intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
				}
				startActivityForResult(intent, REQUEST_CODE_ALBUM);
			}
		});
		aq.id(R.id.my_question_btn_cancel).clicked(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				aq.id(R.id.my_question_photo_layout).gone();
			}
		});
		aq.id(R.id.my_question_photo_layout).clicked(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				aq.id(R.id.my_question_photo_layout).gone();
			}
		});
	}

	private void initData() {
		// login to im server
		if (!EMChatManager.getInstance().isConnected() || !EMChatManager.getInstance().getCurrentUser().equals(chat_login_username)) {
			loadingDialog.show();
			loadingDialog.setProgress("正在登录");
			if (EMChatManager.getInstance().isConnected()) EMChatManager.getInstance().logout();
			EMChatManager.getInstance().login(chat_login_username, chat_login_password, new EMCallBack() {
				@Override
				public void onSuccess() {
					Log.i(TAG, "Login success");
					AQUtility.post(new Runnable() {
						@Override
						public void run() {
							loadingDialog.dismiss();
							EMChat.getInstance().setAppInited();
							initData();
						}
					});
				}
				@Override
				public void onProgress(int progress, String status) {
				}
				@Override
				public void onError(int code, String message) {
					Log.w(TAG, "Login failed, code=" + code + ", msg=" + message);
					AQUtility.post(new Runnable() {
						@Override
						public void run() {
							loadingDialog.dismiss();
							CommonUtil.toast("无法连接IM服务器，请稍候再试！");
							finish();
						}
					});
				}
			});
			return;
		}
		//
		conversation = EMChatManager.getInstance().getConversation(chat_to_username);
		conversation.resetUnsetMsgCount();
		listAdapter.setData(conversation);
		updateDisplay();
		// 注册接收消息广播
		messageReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				// 如果消息不是发给当前会话，则直接退出
				String from = intent.getStringExtra("from");
				if (!chat_to_username.equals(from)) return;
				// 收到这个广播的时候，message已经在db和内存里了，可以通过id获取message对象
				String msgid = intent.getStringExtra("msgid");
				EMMessage message = EMChatManager.getInstance().getMessage(msgid);
				Log.i(TAG, "Receive message, msg=" + dumpMessage(message));
				// 通知adapter有新消息，更新ui
				updateDisplay();
				listAdapter.receiveMessage(message);
				// 记得把广播给终结掉
				abortBroadcast();
			}
		};
		IntentFilter intentFilter = new IntentFilter(EMChatManager.getInstance().getNewMessageBroadcastAction());
		registerReceiver(messageReceiver, intentFilter);
		// 注册一个监听连接状态的listener
		EMChatManager.getInstance().addConnectionListener(new ConnectionListener(){
			@Override
			public void onConnected() {
			}
			@Override
			public void onDisConnected(String errorMessage) {
				Log.w(TAG, "Connection disconnected, msg=" + errorMessage);
				if (errorMessage != null && errorMessage.contains("conflict")) {
					// 显示帐号在其他设备登陆dialog
					CommonUtil.toast("同一帐号已在其他设备上登录！");
				} else if (NetUtils.hasNetwork(MyQuestionActivity.this)) {
					CommonUtil.toast("与服务器的连接中断！");
				} else {
					CommonUtil.toast("当前网络不可用，请检查网络设置");
				}
				finish();
			}
			@Override
			public void onReConnected() {
			}
			@Override
			public void onReConnecting() {
			}
			@Override
			public void onConnecting(String s) {
			}
		});
	}

	private void updateDisplay() {
		listView.setStackFromBottom(true);
		listView.setTranscriptMode(AbsListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
		listAdapter.notifyDataSetChanged();
		if (listView.getCount() > 0) listView.setSelection(listView.getCount() - 1);
	}

	private void sendTextMessage(String textMessage) {
		EMMessage message = EMMessage.createSendMessage(EMMessage.Type.TXT);
		TextMessageBody txtBody = new TextMessageBody(textMessage);
		message.addBody(txtBody);
		message.setReceipt(chat_to_username);
		conversation.addMessage(message);
		listAdapter.sendMessage(message);
		updateDisplay();
	}

	private void sendImageMessage(File imagePath) {
		EMMessage message = EMMessage.createSendMessage(EMMessage.Type.IMAGE);
		ImageMessageBody body = new ImageMessageBody(imagePath);
//		body.setSendOriginalImage(true);		// 默认超过100k的图片会压缩后发给对方，可以设置成发送原图
		message.addBody(body);
		message.setReceipt(chat_to_username);
		conversation.addMessage(message);
		listAdapter.sendMessage(message);
		updateDisplay();
	}

	public static String dumpMessage(EMMessage message) {
		String extraMsg = "";
		if (message.getType() == EMMessage.Type.IMAGE) extraMsg += ",filename:" + ((ImageMessageBody) message.getBody()).getFileName()
																   + ",downloaded=" + ((ImageMessageBody) message.getBody()).downloaded;
		return message.getMsgId() + ",status=" + message.status + "," + message.toString() + extraMsg;
	}

}
