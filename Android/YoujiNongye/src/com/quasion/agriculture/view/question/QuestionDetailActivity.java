package com.quasion.agriculture.view.question;

import android.app.Activity;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.androidquery.AQuery;
import com.androidquery.callback.ImageOptions;
import com.quasion.agriculture.MainConstants;
import com.quasion.agriculture.R;
import com.quasion.agriculture.bean.Question;
import com.quasion.agriculture.util.StatUtil;
import com.quasion.agriculture.util.TagUtil;
import com.quasion.agriculture.widget.ScalingImageView;
import com.winsland.framework.util.BitmapUtil;

public class QuestionDetailActivity extends Activity {
	private static final String TAG = TagUtil.getTag(QuestionDetailActivity.class);

	private AQuery aq;
	private ImageOptions imgOptions;
	private Question question;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// init activity's parameter
		Bundle bundle = getIntent().getExtras();
		int questionIndex = bundle.getInt(MainConstants.INTENT_EXTRA_QUESTION_INDEX);
		if (MainConstants.QuestionList == null || questionIndex < 0 || questionIndex >= MainConstants.QuestionList.length) {
			finish();
			return;
		}
		question = MainConstants.QuestionList[questionIndex];

		aq = new AQuery(this);
		imgOptions = BitmapUtil.getLoadImageOptions(R.drawable.question_detail_img_default);
		// init display and data
		initDisplay();
	}

	@Override
	protected void onPause() {
		super.onPause();
		StatUtil.onPause(this);
	}

	@Override
	protected void onResume() {
		super.onResume();
		StatUtil.onResume(this);
	}

	private void initDisplay() {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.question_detail_layout);
		// init title
		aq.id(R.id.common_title_text_title).text("问题详情");
		aq.id(R.id.common_title_btn_back).visible().clicked(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
		// init course item list view
		aq.id(R.id.question_detail_ask).text(Html.fromHtml("&nbsp;&nbsp;&nbsp;&nbsp;" + question.ask));
		aq.id(R.id.question_detail_answer).text(Html.fromHtml("&nbsp;&nbsp;&nbsp;&nbsp;" + question.answer));
		addImages((LinearLayout) aq.id(R.id.question_detail_ask_images).getView(), question.askImage);
		addImages((LinearLayout) aq.id(R.id.question_detail_answer_images).getView(), question.answerImage);
	}

	private void addImages(LinearLayout layout, String[] imageUrls) {
		if (layout == null || imageUrls == null) return;
		for (String image : imageUrls) {
			ImageView view = new ScalingImageView(this);
			BitmapUtil.loadImage(aq.id(view), image, imgOptions);
			LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
			layoutParams.topMargin = getResources().getDimensionPixelOffset(R.dimen.question_detail_textLineSpace);
			layout.addView(view, layoutParams);
		}
	}
}
