package com.quasion.agriculture.view.question;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.text.Html;
import android.util.Log;
import android.view.*;
import android.view.ViewGroup.LayoutParams;
import android.widget.FrameLayout;
import android.widget.ImageView;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.androidquery.callback.BitmapAjaxCallback;
import com.androidquery.callback.ImageOptions;
import com.androidquery.util.AQUtility;
import com.google.gson.Gson;
import com.quasion.agriculture.MainConstants;
import com.quasion.agriculture.R;
import com.quasion.agriculture.bean.CourseDetail;
import com.quasion.agriculture.protocol.ClientProtocolApi;
import com.quasion.agriculture.util.StatUtil;
import com.quasion.agriculture.util.TagUtil;
import com.quasion.agriculture.widget.LoadingDialog;
import com.quasion.agriculture.widget.ScalingImageView;
import com.winsland.framework.protocol.BaseJsonProtocol;
import com.winsland.framework.util.BitmapUtil;
import com.winsland.framework.util.CommonUtil;
import com.winsland.framework.util.SystemInfoUtil;

import java.io.File;

public class MyQuestionImageActivity extends Activity {
	private static final String TAG = TagUtil.getTag(MyQuestionImageActivity.class);

	private AQuery aq;
	private LoadingDialog loadingDialog;
	private File localFile;
	private String remoteUrl;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// init activity's parameter
		Bundle bundle = getIntent().getExtras();

		String localFileName = bundle.getString(MainConstants.INTENT_EXTRA_LOCAL_FILE);
		if (!CommonUtil.isEmpty(localFileName) && !localFileName.equals("null")) {
			localFile = new File(localFileName);
			if (!localFile.exists()) localFile = null;
		}
		remoteUrl = bundle.getString(MainConstants.INTENT_EXTRA_REMOTE_FILE);
		if (CommonUtil.isEmpty(remoteUrl) || remoteUrl.equals("null")) {
			remoteUrl = null;
		}
		if (localFile == null && remoteUrl == null) {
			CommonUtil.toast("原始文件已经被删除，无法显示图片！");
			finish();
			return;
		}

		aq = new AQuery(this);
		// init display and data
		initDisplay();
	}

	@Override
	protected void onPause() {
		super.onPause();
		StatUtil.onPause(this);
	}

	@Override
	protected void onResume() {
		super.onResume();
		StatUtil.onResume(this);
	}

	private void initDisplay() {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.my_question_image_layout);
		// init loading dialog
		loadingDialog = new LoadingDialog(this);
		loadingDialog.show();
		// init preview
		BitmapAjaxCallback callback = new BitmapAjaxCallback() {
			private long lastOffset = 0, fileLength = 0;
			@Override
			protected void callback(String url, ImageView iv, Bitmap bm, AjaxStatus status) {
				super.callback(url, iv, bm, status);
				aq.id(R.id.my_question_image_layout).background(android.R.color.black);
				loadingDialog.dismiss();
			}
			@Override
			public void progress(String url, long current, long max) {
				// 为了避免发送过多的Progress消息，引起程序频繁更新界面。
				if (current != max && current - lastOffset < max / 100) return;
				lastOffset = current;
				fileLength = max;
				// 通知其他界面更新下载进度
				AQUtility.post(new Runnable() {
					@Override
					public void run() {
						if (loadingDialog.isShowing()) loadingDialog.setProgress(String.valueOf("正在下载\n" + lastOffset * 100 / fileLength + "%"));
					}
				});
			}
		};
		if (localFile != null && localFile.exists()) {
			aq.id(R.id.my_question_full_image).image(localFile, false, SystemInfoUtil.getWidth(), callback);
		} else {
			aq.id(R.id.my_question_full_image).image(remoteUrl, false, true, SystemInfoUtil.getWidth(), R.drawable.my_question_img_default, callback);
		}
		aq.id(R.id.my_question_image_layout).clicked(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
	}
}
