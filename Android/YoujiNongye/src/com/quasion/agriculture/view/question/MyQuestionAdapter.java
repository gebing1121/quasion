package com.quasion.agriculture.view.question;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.androidquery.AQuery;
import com.androidquery.callback.BitmapAjaxCallback;
import com.androidquery.util.AQUtility;
import com.easemob.EMCallBack;
import com.easemob.chat.*;
import com.easemob.util.*;
import com.quasion.agriculture.MainConstants;
import com.quasion.agriculture.R;
import com.quasion.agriculture.util.TagUtil;
import com.winsland.framework.util.CommonUtil;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;


public class MyQuestionAdapter extends BaseAdapter{
	private final static String TAG = TagUtil.getTag(MyQuestionAdapter.class);

	private static final int MESSAGE_TYPE_TX_TXT = 0;
	private static final int MESSAGE_TYPE_RX_TXT = 1;
	private static final int MESSAGE_TYPE_TX_IMAGE = 2;
	private static final int MESSAGE_TYPE_RX_IMAGE = 3;
	private static final int MESSAGE_TYPE_TX_LOCATION = 4;
	private static final int MESSAGE_TYPE_RX_LOCATION = 5;
	private static final int MESSAGE_TYPE_TX_VOICE = 6;
	private static final int MESSAGE_TYPE_RX_VOICE = 7;
	private static final int MESSAGE_TYPE_TX_VIDEO = 8;
	private static final int MESSAGE_TYPE_RX_VIDEO = 9;
	private static final int MESSAGE_TYPE_TX_FILE = 10;
	private static final int MESSAGE_TYPE_RX_FILE = 11;
	private static final int MESSAGE_TYPE_TX_CALL = 12;
	private static final int MESSAGE_TYPE_RX_CALL = 13;
	private static final int MESSAGE_TYPE_COUNT = 14;
	private static final String MESSAGE_ATTR_IS_VOICE_CALL = "is_voice_call";

	private AQuery aq;
	private ListView listView;
	private int imageWidth;
	private EMConversation conversation;

	public MyQuestionAdapter(ListView listView) {
		super();
		this.listView = listView;
		aq = new AQuery(listView);
		imageWidth = listView.getResources().getDimensionPixelSize(R.dimen.my_question_item_image_width);
	}

	public void setData(EMConversation conversation) {
		this.conversation = conversation;
		notifyDataSetChanged();
	}

	public void receiveMessage(final EMMessage message) {
		if (message.status == EMMessage.Status.INPROGRESS && message.getBody() instanceof FileMessageBody) {
			((FileMessageBody) message.getBody()).setDownloadCallback(new EMCallBack() {
				@Override
				public void onSuccess() {
					Log.w(TAG, "Download success, msg=" + MyQuestionActivity.dumpMessage(message));
					AQUtility.post(new Runnable() {
						@Override
						public void run() {
							updateView(message);
						}
					});
				}
				@Override
				public void onError(int errorCode, String errorMessage) {
					Log.w(TAG, "Download Failed, code=" + errorCode + ", error=" + errorMessage + ", msg=" + MyQuestionActivity.dumpMessage(message));
					AQUtility.post(new Runnable() {
						@Override
						public void run() {
							updateView(message);
						}
					});
				}
				@Override
				public void onProgress(int progress, String status) {
//					Log.w(TAG, "Download Progress, progress=" + progress + ", status=" + status + ", msg=" + MyQuestionActivity.dumpMessage(message));
				}
			});
		}
	}

	public void sendMessage(final EMMessage message) {
		EMChatManager.getInstance().sendMessage(message, new EMCallBack() {
			@Override
			public void onSuccess() {
				Log.w(TAG, "Send success, msg=" + MyQuestionActivity.dumpMessage(message));
				AQUtility.post(new Runnable() {
					@Override
					public void run() {
						updateView(message);
					}
				});
			}
			@Override
			public void onError(int errorCode, String errorMessage) {
				Log.w(TAG, "Send Failed, code=" + errorCode + ", error=" + errorMessage + ", msg=" + MyQuestionActivity.dumpMessage(message));
				AQUtility.post(new Runnable() {
					@Override
					public void run() {
						updateView(message);
					}
				});
			}
			@Override
			public void onProgress(int progress, String status) {
//				Log.w(TAG, "Send Progress, progress=" + progress + ", status=" + status + ", msg=" + MyQuestionActivity.dumpMessage(message));
			}
		});
	}

	private void updateView(EMMessage message) {
		for (int i = 0; i < listView.getChildCount(); i++) {
			View view = listView.getChildAt(i);
			if (message.getMsgId().equals(view.getTag())) {
				aq.recycle(view);
				showMessageStatus(message);
				if (message.getType() == EMMessage.Type.IMAGE && message.status == EMMessage.Status.SUCCESS) {
					showImageContent(message);
				}
				break;
			}
		}
	}

	@Override
	public int getCount() {
		return conversation != null ? conversation.getMsgCount() : 0;
	}

	@Override
	public EMMessage getItem(int position) {
		if (position < 0 || position >= getCount()) return null;
		return conversation.getMessage(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public int getItemViewType(int position) {
		EMMessage message = getItem(position);
		if (message != null) {
			switch (message.getType()) {
			case TXT:
				if (message.getBooleanAttribute(MESSAGE_ATTR_IS_VOICE_CALL, false)) {
					return message.direct == EMMessage.Direct.SEND ? MESSAGE_TYPE_TX_CALL : MESSAGE_TYPE_RX_CALL;
				}
				return message.direct == EMMessage.Direct.SEND ? MESSAGE_TYPE_TX_TXT : MESSAGE_TYPE_RX_TXT;
			case IMAGE:
				return message.direct == EMMessage.Direct.SEND ? MESSAGE_TYPE_TX_IMAGE : MESSAGE_TYPE_RX_IMAGE;
			case LOCATION:
				return message.direct == EMMessage.Direct.SEND ? MESSAGE_TYPE_TX_LOCATION : MESSAGE_TYPE_RX_LOCATION;
			case VOICE:
				return message.direct == EMMessage.Direct.SEND ? MESSAGE_TYPE_TX_VOICE : MESSAGE_TYPE_RX_VOICE;
			case VIDEO:
				return message.direct == EMMessage.Direct.SEND ? MESSAGE_TYPE_TX_VIDEO : MESSAGE_TYPE_RX_VIDEO;
			case FILE:
				return message.direct == EMMessage.Direct.SEND ? MESSAGE_TYPE_TX_FILE : MESSAGE_TYPE_RX_FILE;
			}
		}
		return -1;
	}

	public int getViewTypeCount() {
		return MESSAGE_TYPE_COUNT;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final EMMessage message = getItem(position);
		if (message == null) return null;
		switch (message.getType()) {
		case TXT:
			// inflate view and reset aq
			convertView = aq.inflate(convertView, message.direct == EMMessage.Direct.SEND ? R.layout.my_question_item_tx_text : R.layout.my_question_item_rx_text, parent);
			aq.recycle(convertView);
			// show message content
			showTextContent(message);
			break;
		case IMAGE:
			// inflate view and reset aq
			convertView = aq.inflate(convertView, message.direct == EMMessage.Direct.SEND ? R.layout.my_question_item_tx_image : R.layout.my_question_item_rx_image, parent);
			aq.recycle(convertView);
			// show message content
			showImageContent(message);
			break;
		default:
			return null;
		}
		// show message status
		showMessageStatus(message);
		// setup long click listener
		convertView.setOnLongClickListener(new View.OnLongClickListener() {
			@Override
			public boolean onLongClick(View v) {
				PopupMenu pop = new PopupMenu(aq.getContext(), v);
				pop.inflate(R.menu.my_question_item_menu);
				pop.show();
				pop.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
					@Override
					public boolean onMenuItemClick(MenuItem item) {
						switch (item.getItemId()) {
						case R.id.delete:
							conversation.removeMessage(message.getMsgId());
							notifyDataSetChanged();
							break;
						case R.id.deleteAll:
							EMChatManager.getInstance().clearConversation(conversation.getUserName());
							notifyDataSetChanged();
						}
						return true;
					}
				});
				return true;
			}
		});
		// setup retry listener
		aq.id(R.id.my_question_item_retry).clicked(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (message.direct == EMMessage.Direct.SEND) {
					sendMessage(message);
					updateView(message);
				} else {
					AQUtility.postAsync(new Runnable() {
						@Override
						public void run() {
							EMChatManager.getInstance().asyncFetchMessage(message);
							receiveMessage(message);
							AQUtility.post(new Runnable() {
								@Override
								public void run() {
									updateView(message);
								}
							});
						}
					});
				}
			}
		});
		// 便于更新显示时找到相应的View
		convertView.setTag(message.getMsgId());
		// 如果第一条记录或者两条消息时间距离较长，则显示时间
		if (position == 0 || DateUtils.isCloseEnough(message.getMsgTime(), getItem(position - 1).getMsgTime())) {
			aq.id(R.id.my_question_item_time).text(DateUtils.getTimestampString(new Date(message.getMsgTime()))).visible();
		} else {
			aq.id(R.id.my_question_item_time).gone();
		}
		return convertView;
	}

	private void showMessageStatus(EMMessage message) {
		switch (message.status) {
		case SUCCESS:
			aq.id(R.id.my_question_item_progress).gone();
			aq.id(R.id.my_question_item_retry).gone();
			break;
		case FAIL:
			aq.id(R.id.my_question_item_progress).gone();
			aq.id(R.id.my_question_item_retry).visible();
			break;
		case INPROGRESS:
		default:
			aq.id(R.id.my_question_item_progress).visible();
			aq.id(R.id.my_question_item_retry).gone();
			break;
		}
	}

	private void showTextContent(EMMessage message) {
		TextMessageBody txtBody = (TextMessageBody) message.getBody();
		// process message text
		aq.id(R.id.my_question_item_textMessage).text(txtBody.getMessage());
	}

	private void showImageContent(final EMMessage message) {
		ImageMessageBody imgBody = (ImageMessageBody) message.getBody();
		// process message image
		View imageView = aq.id(R.id.my_question_item_imageMessage).getView();
		String imageUrl = imgBody.getLocalUrl();
		setDefaultDrawable(imageView, imageUrl, imgBody.getWidth(), imgBody.getHeight());
		if (imageUrl != null && (message.direct != EMMessage.Direct.RECEIVE || message.status == EMMessage.Status.SUCCESS)) {
			setImageDrawable(imageView, imageUrl);
		}
		imageView.setTag(message.getMsgId());
		if (message.status == EMMessage.Status.SUCCESS) {
			imageView.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					Intent intent = new Intent(aq.getContext(), MyQuestionImageActivity.class);
					intent.putExtra(MainConstants.INTENT_EXTRA_LOCAL_FILE, ((ImageMessageBody) message.getBody()).getLocalUrl());
					intent.putExtra(MainConstants.INTENT_EXTRA_REMOTE_FILE, ((ImageMessageBody) message.getBody()).getRemoteUrl());
					aq.getContext().startActivity(intent);
				}
			});
		} else {
			imageView.setOnClickListener(null);
		}
	}

	private void setDefaultDrawable(View imageView, String imageUrl, int width, int height) {
		// set ImageView to default image
		imageView.setBackgroundResource(R.drawable.my_question_item_img_default);
		// get image actual width and height
		if (width == 0 || height == 0) {
			if (CommonUtil.isEmpty(imageUrl)) return;
			File imageFile = new File(imageUrl);
			if (!imageFile.exists()) {
				imageFile = new File(PathUtil.getInstance().getImagePath(), "th" + imageFile.getName());
				if (!imageFile.exists()) return;
			}
			BitmapFactory.Options options = ImageUtils.getBitmapOptions(imageFile.getAbsolutePath());
			if (options == null) return;
			width = options.outWidth;
			height = options.outHeight;
		}
		// set ImageView's scaled width and height
		ViewGroup.LayoutParams layoutParams = imageView.getLayoutParams();
		layoutParams.width = imageWidth;
		layoutParams.height = imageWidth * height / width;
		imageView.setLayoutParams(layoutParams);
	}

	private void setImageDrawable(final View imageView, final String imageUrl) {
		AQUtility.postAsync(new Runnable() {
			@Override
			public void run() {
				// 如果源文件不存在，则使用缩略图
				File imageFile = new File(imageUrl);
				File thumbnailFile = new File(PathUtil.getInstance().getImagePath(), "th" + imageFile.getName());
				if (!imageFile.exists()) {
					imageFile = thumbnailFile;
					if (!imageFile.exists()) return;
				}
				// 生成Bitmap
				final Bitmap bitmap = BitmapAjaxCallback.getResizedImage(imageFile.getAbsolutePath(), null, imageWidth, true, 0);
				if (bitmap == null) return;
				// 如果缩略图不存在，则保存缩略图
				if (!thumbnailFile.exists()) {
					FileOutputStream output = null;
					try {
						output = new FileOutputStream(thumbnailFile);
						bitmap.compress(Bitmap.CompressFormat.JPEG, 100, output);
						output.flush();
					} catch (IOException ex) {
					} finally {
						CommonUtil.close(output);
					}
				}
				// 在UI线程设置ImageView的图片
				AQUtility.post(new Runnable() {
					@Override
					public void run() {
						imageView.setBackgroundDrawable(new BitmapDrawable(aq.getContext().getResources(), bitmap));
					}
				});
			}
		});
	}
}
