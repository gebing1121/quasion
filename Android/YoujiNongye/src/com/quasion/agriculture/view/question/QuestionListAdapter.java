package com.quasion.agriculture.view.question;

import android.app.Activity;
import android.content.Intent;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import com.androidquery.AQuery;
import com.quasion.agriculture.MainConstants;
import com.quasion.agriculture.R;
import com.quasion.agriculture.bean.Question;


public class QuestionListAdapter extends BaseAdapter{
	private AQuery aq;

	public QuestionListAdapter(Activity activity) {
		super();
		aq = new AQuery(activity);
	}

	@Override
	public int getCount() {
		return MainConstants.QuestionList == null ? 0 : MainConstants.QuestionList.length;
	}

	@Override
	public Question getItem(int position) {
		if (position < 0 || position >= getCount()) return null;
		return MainConstants.QuestionList[position];
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		Question item = getItem(position);
		if (item == null) return null;
		// inflate question view
		convertView = aq.inflate(convertView, R.layout.question_list_item, parent);
		// reset aq to item view
		aq.recycle(convertView);
		// show item
		aq.id(R.id.question_list_item_ask).text(Html.fromHtml("&nbsp;&nbsp;&nbsp;&nbsp;" + item.ask));
		aq.id(R.id.question_list_item_answer).text(Html.fromHtml("&nbsp;&nbsp;&nbsp;&nbsp;" + item.answer));
		// setup click listener
		final String id = item.id;
		convertView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (MainConstants.QuestionList == null || MainConstants.QuestionList.length == 0) return;
				Intent intent = new Intent(aq.getContext(), QuestionDetailActivity.class);
				intent.putExtra(MainConstants.INTENT_EXTRA_QUESTION_INDEX, position);
				aq.getContext().startActivity(intent);
			}
		});
		return convertView;
	}
}
