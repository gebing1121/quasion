package com.quasion.agriculture.weather;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.androidquery.util.AQUtility;
import com.google.gson.Gson;
import com.winsland.framework.BaseTestCase;
import com.winsland.framework.protocol.BaseJsonProtocol;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

@RunWith(RobolectricTestRunner.class)
@Config(manifest=Config.NONE)
public class WeatherApiTest extends BaseTestCase {

	@Test
	public void testGetAreaInfo() throws Exception {
		enterTestCase();
		final List<Province> areaInfo = new ArrayList<Province>();
		// get province list
		AQuery aq = new AQuery(Robolectric.application.getApplicationContext());
		BaseJsonProtocol<String[][]> provinceRequest = WeatherApi.getProvinceList();
		provinceRequest.callback(new AjaxCallback<String[][]>() {
			@Override
			public void callback(String url, String[][] data, AjaxStatus status) {
				System.out.println("Request=" + url);
				System.out.println("Response=" + new Gson().toJson(data));
				System.out.println("Status=" + status.getCode() + "," + status.getMessage() + ", From=" + status.getSource());
				for (String[] item : data) {
					Province province = new Province();
					province.name = item[0];
					province.code = item[1];
					province.cityList = new ArrayList<City>();
					areaInfo.add(province);
				}
				notifyTestComplete();
			}
		});
		enableTestNetwork(true);
		provinceRequest.execute(aq, -1);
		waitTestComplete();
		waitTestComplete(1000);		// sleep so that JUnit to flash file content

		Assert.assertTrue(provinceRequest.getResult() != null);
		Assert.assertTrue(provinceRequest.getStatus().getSource() == AjaxStatus.NETWORK);
		Assert.assertTrue(provinceRequest.getStatus().getCode() == 200);
		Assert.assertTrue(areaInfo.size() >  0);

		// get city list for every province
		for (final Province province : areaInfo) {
			BaseJsonProtocol<String[][]> cityRequest = WeatherApi.getCityList(province.code);
			cityRequest.callback(new AjaxCallback<String[][]>() {
				@Override
				public void callback(String url, String[][] data, AjaxStatus status) {
					System.out.println("Request=" + url);
					System.out.println("Response=" + new Gson().toJson(data));
					System.out.println("Status=" + status.getCode() + "," + status.getMessage() + ", From=" + status.getSource());
					for (String[] item : data) {
						City city = new City();
						city.name = item[0];
						city.code = item[1];
						city.townList = new ArrayList<Town>();
						province.cityList.add(city);
					}
					notifyTestComplete();
				}
			});
			enableTestNetwork(true);
			cityRequest.execute(aq, -1);
			waitTestComplete();
			waitTestComplete(1000);		// sleep so that JUnit to flash file content

			Assert.assertTrue(cityRequest.getResult() != null);
			Assert.assertTrue(cityRequest.getStatus().getSource() == AjaxStatus.NETWORK);
			Assert.assertTrue(cityRequest.getStatus().getCode() == 200);
			Assert.assertTrue(province.cityList.size() >  0);

			// get town list for every city
			for (final City city: province.cityList) {
				BaseJsonProtocol<String[][]> townRequest = WeatherApi.getTownList(city.code);
				townRequest.callback(new AjaxCallback<String[][]>() {
					@Override
					public void callback(String url, String[][] data, AjaxStatus status) {
						System.out.println("Request=" + url);
						System.out.println("Response=" + new Gson().toJson(data));
						System.out.println("Status=" + status.getCode() + "," + status.getMessage() + ", From=" + status.getSource());
						for (String[] item : data) {
							Town town = new Town();
							town.name = item[0];
							town.code = item[1];
							city.townList.add(town);
						}
						notifyTestComplete();
					}
				});
				enableTestNetwork(true);
				townRequest.execute(aq, -1);
				waitTestComplete();
				waitTestComplete(1000);		// sleep so that JUnit to flash file content

				Assert.assertTrue(townRequest.getResult() != null);
				Assert.assertTrue(townRequest.getStatus().getSource() == AjaxStatus.NETWORK);
				Assert.assertTrue(townRequest.getStatus().getCode() == 200);
				Assert.assertTrue(city.townList.size() >  0);
			}
		}

		// save area info to file
		String json = new Gson().toJson(areaInfo);
		File output = new File("assets/data/AreaInfo");
		System.out.println("output=" + output.getCanonicalPath());
		AQUtility.write(output, json.getBytes("UTF-8"));

		exitTestCase();
	}

	@Test
	public void testGetWeather() throws Exception {
		enterTestCase();

		AQuery aq = new AQuery(Robolectric.application.getApplicationContext());
		BaseJsonProtocol<WeatherResponse> request = WeatherApi.getWeather("101010100");
		request.callback(new AjaxCallback<WeatherResponse>() {
			@Override
			public void callback(String url, WeatherResponse data, AjaxStatus status) {
				System.out.println("Request=" + url);
				System.out.println("Response=" + new Gson().toJson(data));
				System.out.println("Status=" + status.getCode() + "," + status.getMessage() + ", From=" + status.getSource());
				notifyTestComplete();
			}
		});
		enableTestNetwork(true);
		request.execute(aq, -1);
		waitTestComplete();
		waitTestComplete(1000);		// sleep so that JUnit to flash file content

		Assert.assertTrue(request.getResult() != null);
		Assert.assertTrue(request.getStatus().getSource() == AjaxStatus.NETWORK);
		Assert.assertTrue(request.getStatus().getCode() == 200);

		exitTestCase();
	}
}