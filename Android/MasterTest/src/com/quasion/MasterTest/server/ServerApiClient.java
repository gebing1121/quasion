package com.quasion.MasterTest.server;

import android.os.Handler;
import android.util.Log;
import com.quasion.MasterTest.app.AppContext;
import com.quasion.MasterTest.app.AppException;
import com.quasion.MasterTest.bean.BaseBean;
import com.quasion.MasterTest.bean.NfcCard;
import com.quasion.MasterTest.nfc.NfcBaseMessage;
import org.apache.commons.httpclient.*;
import org.apache.commons.httpclient.cookie.CookiePolicy;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.multipart.MultipartRequestEntity;
import org.apache.commons.httpclient.methods.multipart.Part;
import org.apache.commons.httpclient.methods.multipart.StringPart;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class ServerApiClient {
	public static final String TAG = ServerApiClient.class.getSimpleName();

	private static final String UTF_8 = "UTF-8";
	private final static int TIMEOUT_CONNECTION = 20000;
	private final static int TIMEOUT_SOCKET = 20000;
	private final static int RETRY_TIME = 3;

	public static String appCookie;
	public static String appUserAgent;
	public static String appSessionToken;

	/* 对应哪一页 */
	public static final String PAGE_NO = "pageNo";
	/* 每页最多显示数量 */
	public static final String PAGE_SIZE = "pageSize";
	/* 缺省每页显示数量 */
	public static final int PAGE_DEFAULT_SIZE = 100;

	private static BaseBean.Parser loginParser = new BaseBean.Parser() {
		@Override
		public Object parse(JSONObject data) throws JSONException {
			NfcBaseMessage.MASTER_KEY_UID = null;
			if (data != null) {
				if (data.has("sessionToken")) appSessionToken = data.getString("sessionToken");
				if (data.has("masterKey")) NfcBaseMessage.MASTER_KEY_UID = data.getString("masterKey").trim();
			}
			if (NfcBaseMessage.MASTER_KEY_UID == null || NfcBaseMessage.MASTER_KEY_UID.length() != NfcBaseMessage.MASTER_KEY_LENGTH) {
				throw new JSONException("Invalid Master Key!");
			}
			return this;
		}

	};
	private static BaseBean.Parser nfcCardParser = new BaseBean.Parser() {
		@Override
		public Object parse(JSONObject data) throws JSONException {
			return NfcCard.parseNfcCards(data);
		}
	};

	/**
	 * 登录， 自动处理cookie
	 *
	 * @param mHandler
	 * @param msgWhat
	 * @param loginAccount
	 * @param password
	 */
	public static void login(Handler mHandler, int msgWhat, String loginAccount, String password) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(ServerApiURLs.LOGIN_ACCOUNT, loginAccount);
		params.put(ServerApiURLs.LOGIN_PASSWORD, password);
		try {
			String responseData = doPost(ServerApiURLs.LOGIN_URL, params);
			BaseBean.handleResponse(mHandler, msgWhat, responseData, loginParser);
		} catch (Exception ex) {
			BaseBean.handleException(mHandler, ex);
		}
	}

	/**
	 * 退出
	 *
	 * @param mHandler
	 * @param msgWhat
	 */
	public static void logout(Handler mHandler, int msgWhat) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(ServerApiURLs.SESSION_TOKEN, appSessionToken);

		try {
			String responseData = doPost(ServerApiURLs.LOGOUT_URL, params);
			BaseBean.handleResponse(mHandler, msgWhat, responseData, null);
		} catch (Exception ex) {
			BaseBean.handleException(mHandler, ex);
		}
	}

	/**
	 *
	 * 获取nfc card的列表
	 *
	 * @param mHandler
	 * @param msgWhat
	 * @param pageNo
	 * @param pageSize
	 */
	public static void getNfcCardList(Handler mHandler, int msgWhat, int pageNo, int pageSize) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(ServerApiURLs.SESSION_TOKEN, appSessionToken);
		params.put(PAGE_NO, "" + pageNo);
		params.put(PAGE_SIZE, "" + pageSize);

		try {
			String responseData = doPost(ServerApiURLs.NFC_CARD_LIST_URL, params);
			BaseBean.handleResponse(mHandler, msgWhat, responseData, nfcCardParser);
		} catch (Exception ex) {
			BaseBean.handleException(mHandler, ex);
		}
	}

	private static String getUserAgent(AppContext appContext) {
		if (appUserAgent == null || appUserAgent == "") {
			StringBuilder ua = new StringBuilder("OSChina.NET");
			ua.append('/' + appContext.getPackageInfo().versionName + '_' + appContext.getPackageInfo().versionCode);// App版本
			ua.append("/Android");// 手机系统平台
			ua.append("/" + android.os.Build.VERSION.RELEASE);// 手机系统版本
			ua.append("/" + android.os.Build.MODEL); // 手机型号
			ua.append("/" + UUID.randomUUID().toString());// 客户端唯一标识
			appUserAgent = ua.toString();
		}
		return appUserAgent;
	}

	private static HttpClient getHttpClient() {
		HttpClient httpClient = new HttpClient();
		// 设置 HttpClient 接收 Cookie,用与浏览器一样的策略
		httpClient.getParams().setCookiePolicy(CookiePolicy.BROWSER_COMPATIBILITY);
		// 设置 默认的超时重试处理策略
		httpClient.getParams().setParameter(HttpMethodParams.RETRY_HANDLER, new DefaultHttpMethodRetryHandler());
		// 设置 连接超时时间
		httpClient.getHttpConnectionManager().getParams().setConnectionTimeout(TIMEOUT_CONNECTION);
		// 设置 读数据超时时间
		httpClient.getHttpConnectionManager().getParams().setSoTimeout(TIMEOUT_SOCKET);
		// 设置 字符集
		httpClient.getParams().setContentCharset(UTF_8);
		return httpClient;
	}

	private static GetMethod getHttpGet(String url, String cookie, String userAgent) {
		GetMethod httpGet = new GetMethod(url);
		// 设置 请求超时时间
		httpGet.getParams().setSoTimeout(TIMEOUT_SOCKET);
		httpGet.setRequestHeader("Host", ServerApiURLs.HOST);
		httpGet.setRequestHeader("Connection", "Keep-Alive");
		httpGet.setRequestHeader("Cookie", cookie);
		httpGet.setRequestHeader("User-Agent", userAgent);
		return httpGet;
	}

	private static PostMethod getHttpPost(String url, String cookie, String userAgent) {
		PostMethod httpPost = new PostMethod(url);
		// 设置 请求超时时间
		httpPost.getParams().setSoTimeout(TIMEOUT_SOCKET);
		httpPost.setRequestHeader("Host", ServerApiURLs.HOST);
		httpPost.setRequestHeader("Connection", "Keep-Alive");
		httpPost.setRequestHeader("Cookie", cookie);
		httpPost.setRequestHeader("User-Agent", userAgent);
		return httpPost;
	}

	public static String doPost(String url, Map<String, Object> params) throws AppException {
		AppContext appContext = AppContext.getAppContext();
		String userAgent = getUserAgent(appContext);

		HttpClient httpClient = null;
		PostMethod httpPost = null;
		Part[] parts = new Part[0];
		String responseBody = "";

		// post表单参数处理
		Log.d(TAG, "doPost: url=" + url);
		if (params != null) {
			parts = new Part[params.size()];
			int i = 0;
			for (String name : params.keySet()) {
				parts[i++] = new StringPart(name, String.valueOf(params.get(name)), UTF_8);
				Log.d(TAG, "doPost: post_key=" + name + ",value=" + String.valueOf(params.get(name)));
			}
		}

		for (int time = 0; time < RETRY_TIME;) {
			try {
				httpClient = getHttpClient();
				httpPost = getHttpPost(url, appCookie, userAgent);
				httpPost.setRequestEntity(new MultipartRequestEntity(parts, httpPost.getParams()));
				int statusCode = httpClient.executeMethod(httpPost);
				Log.d(TAG, "doPost: statusCode:" + statusCode);
				if (statusCode != HttpStatus.SC_OK) {
					throw AppException.http(statusCode);
				} else if (statusCode == HttpStatus.SC_OK) {
					Cookie[] cookies = httpClient.getState().getCookies();
					String tmpcookies = "";
					for (Cookie ck : cookies) {
						tmpcookies += ck.toString() + ";";
					}
					// 保存cookie
					if (appContext != null && tmpcookies != "") {
						appCookie = tmpcookies;
					}
				}
				responseBody = httpPost.getResponseBodyAsString();
				break;
			} catch (HttpException e) {
				if (++time < RETRY_TIME) {
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e1) {
					}
					continue;
				}
				// 发生致命的异常，可能是协议不对或者返回的内容有问题
				e.printStackTrace();
				throw AppException.http(e);
			} catch (IOException e) {
				if (++time < RETRY_TIME) {
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e1) {
					}
					continue;
				}
				// 发生网络异常
				e.printStackTrace();
				throw AppException.network(e);
			} finally {
				// 释放连接
				httpPost.releaseConnection();
				httpClient = null;
			}
		}

		responseBody = responseBody.replaceAll("\\p{Cntrl}", "");
		Log.d(TAG, "doPost: responseBody:" + responseBody);
		return responseBody;
	}

	public static String doGet(String url) throws AppException {
		AppContext appContext = AppContext.getAppContext();
		String userAgent = getUserAgent(appContext);
		HttpClient httpClient = null;
		GetMethod httpGet = null;
		String responseBody = "";
		for (int time = 0; time < RETRY_TIME;) {
			try {
				httpClient = getHttpClient();
				httpGet = getHttpGet(url, appCookie, userAgent);
				int statusCode = httpClient.executeMethod(httpGet);
				if (statusCode != HttpStatus.SC_OK) {
					throw AppException.http(statusCode);
				}
				responseBody = httpGet.getResponseBodyAsString();
				break;
			} catch (HttpException e) {
				time++;
				if (time < RETRY_TIME) {
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e1) {
					}
					continue;
				}
				// 发生致命的异常，可能是协议不对或者返回的内容有问题
				e.printStackTrace();
				throw AppException.http(e);
			} catch (IOException e) {
				time++;
				if (time < RETRY_TIME) {
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e1) {
					}
					continue;
				}
				// 发生网络异常
				e.printStackTrace();
				throw AppException.network(e);
			} finally {
				// 释放连接
				httpGet.releaseConnection();
				httpClient = null;
			}
		}

		responseBody = responseBody.replaceAll("\\p{Cntrl}", "");
		Log.d(TAG, "doGet: responseBody:" + responseBody);
		return responseBody;
	}

}
