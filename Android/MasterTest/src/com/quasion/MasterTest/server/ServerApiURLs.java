package com.quasion.MasterTest.server;

public class ServerApiURLs {
	public static String HOST = "api.quasionwireless.com:9000";
//	public final static String HOST = "182.92.110.141:9000";
	public final static String HTTP = "http://";
	public final static String HTTPS = "https://";

	private final static String URL_SPLITTER = "/";
	private final static String URL_UNDERLINE = "_";
	private final static String URL_API_HOST = HTTP + HOST + URL_SPLITTER;

	/* login url */
	public static final String LOGIN_URL = URL_API_HOST + "quasion/oauth/login";
	/* logout url */
	public static final String LOGOUT_URL = URL_API_HOST + "quasion/oauth/logout";
	/* get all safe question by token */
	public static final String GET_ALL_SAFE_QUESTION_BY_TOKEN = URL_API_HOST + "quasion/oauth/querySecretQuestions";
	/* set safe questions url by token */
	public static final String SET_SAFE_QUESTION_BY_TOKEN_URL = URL_API_HOST + "quasion/oauth/setSecretQuestion";
	/* get safe questions by token url */
	public static final String GET_SAFE_QUESTION_BY_TOKEN_URL = URL_API_HOST + "quasion/oauth/queryAccountSecretQuestions";
	/* get safe question by login account url */
	public static final String GET_SAFE_QUESTION_BY_USERNAME_URL = URL_API_HOST + "quasion/oauth/querySecretQuestionsByLoginAccount";
	/* verify safe questions by token url */
	public static final String VERIFY_SAFE_QUESTION_BY_TOKEN_URL = URL_API_HOST + "quasion/oauth/verifySecretQuestions";
	/* verify safe questions by login account url */
	public static final String VERIFY_SAFE_QUESTION_BY_USERNAME_URL = URL_API_HOST + "quasion/oauth/verifySecretQuestionsByLoginAccount";
	/* modify password url */
	public static final String MODIFY_PASSWORD_URL = URL_API_HOST + "quasion/oauth/modifyPassword";
	/* reset password url */
	public static final String RESET_PASSWORD_URL = URL_API_HOST + "quasion/oauth/resetPassword";
	/* user feedback url */
	public static final String USER_FEEDBACK_URL = URL_API_HOST + "quasion/feedback/addUserFeedback";

	/* get sales data url */
	public static final String GET_SALES_DATA_URL = URL_API_HOST + "quasion/store/getSalesData";
	/* get product list url */
	public static final String GET_PRODUCT_LIST_URL = URL_API_HOST + "quasion/store/getProducts";
	/* get product list url */
	public static final String SET_SALES_DATA_URL = URL_API_HOST + "quasion/store/batchModifyProductSales";

	/* get nfc card list url */
	public static final String NFC_CARD_LIST_URL = URL_API_HOST + "quasion/nfc/getNFCCardList";
	/* add nfc card url */
	public static final String NFC_CARD_ADD_URL = URL_API_HOST + "quasion/nfc/addNFCCard";
	/* delete nfc card url */
	public static final String NFC_CARD_DELETE_URL = URL_API_HOST + "quasion/nfc/deleteNFCCard";
	/* get owner list url */
	public static final String OWNER_LIST_URL = URL_API_HOST + "quasion/nfc/getOwnerList";
	/* add owner url */
	public static final String ADD_OWNER_URL = URL_API_HOST + "quasion/nfc/addOwner";
	/* assign owner to nfc card url */
	public static final String ADD_OWNER_TO_NFCCARD_URL = URL_API_HOST + "quasion/nfc/addOwnerToNFCCard";

	/* add off hook data url */
	public static final String ADD_OFFHOOK_TIMES_URL = URL_API_HOST + "quasion/behavior/addOffhookTimes";
	/* add alarm data url */
	public static final String ADD_ALARM_DATA_URL = URL_API_HOST + "quasion/behavior/addAlarmTimes";
	/* get alarm data list url */
	public static final String GET_ALARM_DATA_LIST_URL = URL_API_HOST + "quasion/behavior/queryAlarmTimes";

	/* get average pickup time report url */
	public static final String REPORT_GET_AVERAGE_PICKUP_TIME_URL = URL_API_HOST + "quasion/report/statisticMainDeviceAvgPickupTime";
	/* get pickup rate report url */
	public static final String REPORT_GET_PICKUP_RATE_URL = URL_API_HOST + "quasion/report/statisticMainDevicePickupRates";
	/* get sales rate report url */
	public static final String REPORT_GET_SALES_RATE_URL = URL_API_HOST + "quasion/report/statisticMainDeviceSales";

	/* check update url */
	public static final String CHECK_UPDATE_URL = URL_API_HOST + "quasion/client/upgrade";

	public static final String SESSION_TOKEN = "sessionToken";
	public static final String LOGIN_ACCOUNT = "loginAccount";
	public static final String LOGIN_PASSWORD = "loginPassword";

	public static final String NEW_PASSWORD = "newPassword";
	public static final String OLD_PASSWORD = "oldPassword";
	public static final String FEEDBACK = "feedback";
	public static final String CONTACT = "contact";
	public static final String SECRET_QUESTION_ID1 = "questionId1";
	public static final String SECRET_QUESTION_ID2 = "questionId2";
	public static final String SECRET_QUESTION_ID3 = "questionId3";
	public static final String SECRET_ANSWER1 = "answer1";
	public static final String SECRET_ANSWER2 = "answer2";
	public static final String SECRET_ANSWER3 = "answer3";
	public static final String NFC_CARD_KEY_ID = "keyId";
	public static final String OWNER_ID = "ownerId";
	public static final String ENGLISH_NAME = "englishName";
	public static final String TIME_TYPE = "timeType";
	public static final String BASE_ID = "baseId";
	public static final String UUID = "uuid";
	public static final String ALARM_TIMES = "alarmTimes";
	public static final String DEVICE_BRAND = "deviceBrand";
	public static final String DEVICE_MODEL = "deviceModel";
	public static final String DEVICE_IMEI = "deviceImei";
	public static final String OFFHOOKE_TIMES = "offhookTimes";
	public static final String SALES_DATE = "statisticDate";
	public static final String SEARCH_CONTENT = "searchContent";
	public static final String PRODUCT_SALES = "productSales";
	public static final String VERSION_CODE = "versionCode";
}
