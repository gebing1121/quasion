package com.quasion.MasterTest.app;

import android.app.Application;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;

public class AppContext extends Application {
	private static final String TAG = AppContext.class.getSimpleName();
	private static AppContext appContext;

	@Override
	public void onCreate() {
		super.onCreate();
		appContext = this;
	}

	/**
	 * 返回ApplicationContext
	 * @return
	 */
	public static AppContext getAppContext() {
		return appContext;
	}

	/**
	 * 获取App安装包信息
	 *
	 * @return
	 */
	public PackageInfo getPackageInfo() {
		PackageInfo info = null;
		try {
			info = getPackageManager().getPackageInfo(getPackageName(), 0);
		} catch (NameNotFoundException e) {
			e.printStackTrace(System.err);
		}
		if (info == null)
			info = new PackageInfo();
		return info;
	}
}
