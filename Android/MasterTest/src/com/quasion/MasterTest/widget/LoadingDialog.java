package com.quasion.MasterTest.widget;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import com.quasion.MasterTest.R;

public class LoadingDialog extends Dialog {
	public LoadingDialog(Context context, int theme) {
		super(context, theme);
	}

	public LoadingDialog(Context context) {
		super(context);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.common_loading_dialog);
		ImageView mImageView = (ImageView) findViewById(R.id.loading_dialog_image);
		mImageView.setBackgroundResource(R.drawable.common_loading_dialog_progressbar);
		AnimationDrawable ad = (AnimationDrawable) mImageView.getBackground();
		ad.start();
	}

	public void setDialogProgress(int progress) {
		if (progress < 0) progress = 0;
		else if (progress > 100) progress = 100;
		setDialogProgress(String.valueOf(progress) + "%");
	}

	public void setDialogProgress(String progress) {
		TextView progressText = (TextView) findViewById(R.id.loading_dialog_progress);
		progressText.setText(progress);
	}

	public void setDialogMessage(String message) {
		TextView messageText = (TextView) findViewById(R.id.loading_dialog_message);
		messageText.setText(message);
	}
}
