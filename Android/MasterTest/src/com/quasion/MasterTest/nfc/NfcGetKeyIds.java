package com.quasion.MasterTest.nfc;

/**
 * Created by Gebing on 2014/7/12.
 */
public class NfcGetKeyIds extends NfcBaseMessage {
	static public final int KEY_ID_LENGTH = 4*2;
	static public final int KEY_ID_START = Integer.valueOf(CMD_TO_BASE_GET_KEY_ID_MIN, 16);
	static public final int KEY_ID_MAXIMUM = Integer.valueOf(CMD_TO_BASE_GET_KEY_ID_MAX, 16) - KEY_ID_START;

	public int nfcKeyCnt;
	public String nfcKeyIds;

	@Override
	public boolean isValid() {
		return nfcKeyCnt >= 0 && nfcKeyCnt <= KEY_ID_MAXIMUM
				&& nfcKeyIds != null && nfcKeyIds.length() == nfcKeyCnt * KEY_ID_LENGTH;
	}

	@Override
	protected int fromNdefData(String ndefData) {
		// 处理NFC Key Id数据
		if (ndefData != null || ndefData.length() < CMD_FIELD_LENGTH) {
			// 获取NFC Key Id的数量
			nfcKeyCnt = Integer.valueOf(ndefData.substring(0, CMD_FIELD_LENGTH), 16) - KEY_ID_START;
			int endPos = CMD_FIELD_LENGTH + KEY_ID_LENGTH * nfcKeyCnt;
			if (nfcKeyCnt >= 0 && nfcKeyCnt <= KEY_ID_MAXIMUM && ndefData.length() >= endPos) {
				// 获取NFC Key Id数据
				nfcKeyIds = ndefData.substring(CMD_FIELD_LENGTH, endPos);
				return endPos;
			}
		}
		// 无效数据
		nfcKeyCnt = -1;
		nfcKeyIds = null;
		return -1;
	}

	@Override
	protected String toNdefData() {
		return CMD_TO_BASE_GET_KEY_ID;
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + '{' +
				(isValid() ? "nfcKeyCnt=" + nfcKeyCnt + ", nfcKeyIds='" + nfcKeyIds + '\'' : "") +
				'}';
	}
}
