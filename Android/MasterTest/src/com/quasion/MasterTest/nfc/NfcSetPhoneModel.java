package com.quasion.MasterTest.nfc;

import com.quasion.MasterTest.util.StringUtil;

import java.io.Serializable;

/**
 * Created by Gebing on 2014/7/12.
 */
public class NfcSetPhoneModel extends NfcGetPhoneModel implements Serializable {
	public NfcSetPhoneModel(String phoneBrand, String phoneModel) {
		this.phoneBrand = phoneBrand;
		this.phoneModel = phoneModel;
		if (!isValid()) {
			this.phoneBrand = null;
			this.phoneModel = null;
		}
	}

	@Override
	protected int fromNdefData(String ndefData) {
		// NFC Base不会返回该类型的数据
		return -1;
	}

	@Override
	protected String toNdefData() {
		if (!isValid()) return "";
		String data = phoneBrand + ' ' + phoneModel + StringUtil.fill(' ', PHONE_MODEL_LENGTH - 1 - phoneBrand.length() - phoneModel.length());
		return CMD_TO_BASE_SET_MODEL + StringUtil.toHex(data.substring(0, PHONE_MODEL_LENGTH).getBytes());
	}
}
