package com.quasion.MasterTest.nfc;

/**
 * Created by Gebing on 2014/7/13.
 */
public class NfcBaseMirror extends NfcBaseMessage {
	@Override
	protected String toNdefData() {
		return "";
	}

	@Override
	public boolean isValid() {
		return true;
	}

	@Override
	protected int fromNdefData(String ndefData) {
		return -1;
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + "{}";
	}
}
