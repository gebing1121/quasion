package com.quasion.MasterTest.nfc;

import com.quasion.MasterTest.bean.OffhookTime;
import com.quasion.MasterTest.util.StringUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Gebing on 2014/7/12.
 */
public class NfcGetHookData extends NfcBaseMessage {
	static protected final int HOOK_TIME_LENGTH = 4*2;
	static protected final int HOOK_DURATION_LENGTH = 2*2;
	static protected final int HOOK_OTHER_LENGTH = 2*2;
	static protected final int HOOK_DATA_LENGTH = HOOK_TIME_LENGTH + HOOK_DURATION_LENGTH + HOOK_OTHER_LENGTH;
	static protected final String HOOK_DATA_END = StringUtil.fill('F', HOOK_DATA_LENGTH);

	public List<OffhookTime> hookData;

	@Override
	public boolean isValid() {
		return hookData != null;
	}

	@Override
	protected int fromNdefData(String ndefData) {
		// 处理摘机数据
		if (ndefData != null) {
			hookData = new ArrayList<OffhookTime>();
			for (int pos = CMD_FIELD_LENGTH; pos + HOOK_DATA_LENGTH <= ndefData.length(); pos += HOOK_DATA_LENGTH) {
				String data = ndefData.substring(pos, pos + HOOK_DATA_LENGTH);
				// 必须以8个FF结束
				if (HOOK_DATA_END.equalsIgnoreCase(data)) {
					return pos + HOOK_DATA_LENGTH;
				}
				// 获取摘机数据
				OffhookTime hookTime = new OffhookTime();
				hookTime.setOffhookTime(Long.valueOf(data.substring(0, HOOK_TIME_LENGTH), 16) * 1000);		// 转换秒到毫秒
				hookTime.setTimes(Integer.valueOf(data.substring(HOOK_TIME_LENGTH, HOOK_TIME_LENGTH + HOOK_DURATION_LENGTH), 16));
				hookData.add(hookTime);
				// 判断是否结束？
				if (pos + HOOK_DATA_LENGTH == ndefData.length()) {
					return pos + HOOK_DATA_LENGTH;
				}
			}
		}
		// 无效数据
		hookData = null;
		return -1;
	}

	@Override
	protected String toNdefData() {
		return CMD_TO_BASE_GET_HOOK_DATA;
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + '{' +
				(isValid() ? "hookData=" + hookData : "") +
				'}';
	}
}
