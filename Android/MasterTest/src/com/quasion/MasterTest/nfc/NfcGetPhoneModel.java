package com.quasion.MasterTest.nfc;

import com.quasion.MasterTest.util.StringUtil;

/**
 * Created by Gebing on 2014/7/12.
 */
public class NfcGetPhoneModel extends NfcBaseMessage {
	static public final int PHONE_MODEL_LENGTH = 64;
	public String phoneBrand;
	public String phoneModel;


	@Override
	public boolean isValid() {
		return !StringUtil.isEmpty(phoneBrand) && !StringUtil.isEmpty(phoneModel)
				&& phoneBrand.length() < PHONE_MODEL_LENGTH - 1;
	}
	@Override
	protected int fromNdefData(String ndefData) {
		// 处理手机型号数据
		if (ndefData != null && CMD_FIELD_LENGTH + PHONE_MODEL_LENGTH * 2 <= ndefData.length()) {
			String data = ndefData.substring(CMD_FIELD_LENGTH, CMD_FIELD_LENGTH + PHONE_MODEL_LENGTH * 2);
			data = new String(StringUtil.fromHex(data)).trim();
			// 拆分手机品牌和型号
			int pos = data.indexOf(' ');
			if (pos < 0) {
				phoneBrand = "";
				phoneModel = "";
			} else {
				phoneBrand = data.substring(0, pos);
				phoneModel = data.substring(pos + 1);
			}
			return CMD_FIELD_LENGTH + PHONE_MODEL_LENGTH * 2;
		}
		// 无效数据
		phoneBrand = null;
		phoneModel = null;
		return -1;
	}

	@Override
	protected String toNdefData() {
		return CMD_TO_BASE_GET_MODEL;
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + '{' +
				(isValid() ? "phoneBrand='" + phoneBrand + '\'' + ", phoneModel='" + phoneModel + '\'' : "") +
				'}';
	}
}
