package com.quasion.MasterTest.nfc;

import com.quasion.MasterTest.util.StringUtil;

/**
 * Created by Gebing on 2014/7/12.
 */
public class NfcSetBaseTime extends NfcBaseMessage {
	public long nfcBaseTime = -1;

	public NfcSetBaseTime(long baseTime) {
		nfcBaseTime = baseTime;
	}

	@Override
	public boolean isValid() {
		return nfcBaseTime > 0;
	}

	@Override
	protected int fromNdefData(String ndefData) {
		// NFC Base不会返回该类型的数据
		return -1;
	}

	@Override
	protected String toNdefData() {
		if (!isValid()) return "";
		return NfcBaseMessage.CMD_TO_BASE_SET_BASE_TIME + StringUtil.toHex(nfcBaseTime / 1000, 8);
	}

	static protected String toNdefData(long baseTime) {
		return NfcBaseMessage.CMD_TO_BASE_SET_BASE_TIME + StringUtil.toHex(baseTime / 1000, 8);
	}
}
