package com.quasion.MasterTest.nfc;

import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.util.Log;
import com.quasion.MasterTest.util.DateUtil;
import com.quasion.MasterTest.util.StringUtil;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

/**
 * Created by Gebing on 2014/7/12.
 */
public abstract class NfcBaseMessage {
	private static final String TAG = NfcBaseMessage.class.getSimpleName();

	// 协议MasterKey的身份UID
	public static String MASTER_KEY_UID;
	// 协议MasterKey的长度
	public static final int MASTER_KEY_LENGTH = 4*2;
	// 协议命令字段的长度
	public static final int CMD_FIELD_LENGTH = 1*2;
	// NFC Base启动后缺省消息标志
	private static final String BASE_QUASION_UID = "Quasion:";

	// 常用的命令实例
	public static final NfcBaseMessage MSG_BASE_REBOOT = new NfcBaseReboot();
	public static final NfcBaseMessage MSG_BASE_MIRROR = new NfcBaseMirror();
	public static final NfcBaseMessage MSG_GET_KEY_ID = new NfcGetKeyIds();
	public static final NfcBaseMessage MSG_GET_IMEI = new NfcGetPhoneImei();
	public static final NfcBaseMessage MSG_GET_MODEL = new NfcGetPhoneModel();
	public static final NfcBaseMessage MSG_GET_BASE_ID = new NfcGetBaseId();
	public static final NfcBaseMessage MSG_GET_BASE_TIME = new NfcGetBaseTime();
	public static final NfcBaseMessage MSG_GET_ALARM_DATA = new NfcGetAlarmData();
	public static final NfcBaseMessage MSG_GET_HOOK_DATA = new NfcGetHookData();
	// 手机发送给NFC Base的指令
	static protected final String CMD_TO_BASE_SET_KEY_ID_MIN = "00";
	static protected final String CMD_TO_BASE_SET_KEY_ID_MAX = "1F";
	static protected final String CMD_TO_BASE_SET_IMEI = "21";
	static protected final String CMD_TO_BASE_SET_MODEL = "22";
	static protected final String CMD_TO_BASE_SET_BASE_ID = "23";
	static protected final String CMD_TO_BASE_SET_BASE_TIME = "24";
	static protected final String CMD_TO_BASE_SET_ALARM_DATA = "2A";
	static protected final String CMD_TO_BASE_SET_HOOK_DATA = "2B";
	static protected final String CMD_TO_BASE_GET_KEY_ID = "61";
	static protected final String CMD_TO_BASE_GET_KEY_ID_MIN = "60";
	static protected final String CMD_TO_BASE_GET_KEY_ID_MAX = "7F";
	static protected final String CMD_TO_BASE_GET_IMEI = "81";
	static protected final String CMD_TO_BASE_GET_MODEL = "82";
	static protected final String CMD_TO_BASE_GET_BASE_ID = "83";
	static protected final String CMD_TO_BASE_GET_BASE_TIME = "84";
	static protected final String CMD_TO_BASE_GET_ALARM_DATA = "8A";
	static protected final String CMD_TO_BASE_GET_HOOK_DATA = "8B";

	static public String txData;
	static public String rxData;
	/**
	 * 返回数据是否有效
	 * @return true - 有效；false - 无效
	 */
	public abstract boolean isValid();

	/**
	 * 生成发送给NFC Base的数据。如果不支持该类型命令，则返回空字符串。
	 *
	 * @return 发送给NFC Base的数据，包括CmdType但不包括Key。不能返回null，只能返回空字符串
	 */
	protected abstract String toNdefData();

	/**
	 * 解析NFC Base返回的数据到类成员。
	 *
	 * @param ndefData NFC Base返回的数据，包括CmdType但不包括Key。
	 * @return > 0 成功解析，返回解析数据长度；<= 0 - 数据解析失败或不支持该类型的数据
	 */
	protected abstract int fromNdefData(String ndefData);

	/**
	 * 生成发送给NFC Base的NdefMessage。支持将多个命令合并到一个NdefMessage
	 *
	 * @param firstBaseMessage 发送给NFC Base的命令集合
	 * @param otherBaseMessages 其他发送给NFC Base的命令集合
	 * @return 不能返回null
	 */
	public static NdefMessage toNdefMessages(NfcBaseMessage firstBaseMessage, NfcBaseMessage... otherBaseMessages) {
		boolean hasSetTime = false;
		// 生成消息字符串
		StringBuffer sb = new StringBuffer(MASTER_KEY_UID);
		Log.i(TAG, "toNdefMessages: key=" + sb);
		if (firstBaseMessage != null) {
			Log.i(TAG, "toNdefMessages: message=" + firstBaseMessage);
			sb.append(firstBaseMessage.toNdefData());
			Log.i(TAG, "toNdefMessages: data=" + sb);
			hasSetTime = firstBaseMessage instanceof NfcSetBaseTime;
		}
		for (NfcBaseMessage baseMessage : otherBaseMessages) {
			Log.i(TAG, "toNdefMessages: message=" + baseMessage);
			sb.append(baseMessage.toNdefData());
			Log.i(TAG, "toNdefMessages: data=" + sb);
			if (!hasSetTime) hasSetTime = baseMessage instanceof NfcSetBaseTime;
		}
		if (!hasSetTime) {
			long baseTime = System.currentTimeMillis();
			Log.i(TAG, "toNdefMessages: time=" + baseTime + "," + DateUtil.formatTime(baseTime));
			sb.append(NfcSetBaseTime.toNdefData(baseTime));
		}
		Log.i(TAG, "toNdefMessages: data=" + sb);
		txData = sb.toString();
		// 生成Text格式的NdefRecord
		NdefRecord record = toNdefTextRecord(null, sb.toString(), Locale.ENGLISH, true);
		// 生成NdefMessage
		return new NdefMessage(new NdefRecord[] { record });
	}

	/**
	 * 生成发送给NFC Base的NdefMessage。支持将多个命令合并到一个NdefMessage
	 *
	 * @param baseMessages 发送给NFC Base的命令集合
	 * @return 不能返回null
	 */
	public static NdefMessage toNdefMessages(NfcBaseMessage[] baseMessages) {
		return toNdefMessages(null, baseMessages);
	}

	/**
	 * 生成发送给NFC Base的NdefMessage。支持将多个命令合并到一个NdefMessage
	 *
	 * @param fisrtNdefMessage 发送给NFC Base的命令集合
	 * @return 不能返回null
	 */
	public static List<NfcBaseMessage> fromNdefMessages(NdefMessage fisrtNdefMessage, NdefMessage... otherNdefMessages) {
		List<NfcBaseMessage> baseMessages = fromNdefMessages(otherNdefMessages);
		if (baseMessages.size() == 0) {
			baseMessages.addAll(fromNdefMessages(new NdefMessage[]{fisrtNdefMessage}));
		}
		return baseMessages;
	}

	/**
	 * 解析NFC Base返回的数据，返回数据集合。
	 *
	 * @param ndefMessages NFC Base返回的NdefMessage
	 * @return 不能返回null。如果NFC Base返回的数据无效，则返回空集合。
	 */
	public static List<NfcBaseMessage> fromNdefMessages(NdefMessage[] ndefMessages) {
		List<NfcBaseMessage> baseMessages = new ArrayList<NfcBaseMessage>();
		// 循环解析所有NdefMessage/NdefRecord，如果遇到不支持的数据则直接退出并返回空集合
		allLoop:
		if (ndefMessages != null) {
			Log.d(TAG, "fromNdefMessages: ndefMessages num=" + ndefMessages.length);
			for (NdefMessage ndefMessage : ndefMessages) {
				Log.d(TAG, "fromNdefMessages: NdefRecord num=" + ndefMessage.getRecords().length);
				for (NdefRecord ndefRecord : ndefMessage.getRecords()) {
					String message = null;
					// NFC Base只支持TNF_WELL_KNOWN/RTD_TEXT格式的NdefMessage
					short tnf = ndefRecord.getTnf();
					byte[] type = ndefRecord.getType();
					Log.d(TAG, "fromNdefMessages: tnf=" + tnf + ",type=" + new String(type));
					if (tnf == NdefRecord.TNF_WELL_KNOWN && Arrays.equals(NdefRecord.RTD_TEXT, type)) {
						// 解析Text格式的NdefRecord
						message = fromNdefTextRecord(ndefRecord);
					}
					if (message == null || message.length() <= MASTER_KEY_LENGTH + CMD_FIELD_LENGTH) {
						break allLoop;
					}
					rxData = message;
					// 如果是NFC Base启动后缺省消息，则返回NfcBaseBoot
					if (message.startsWith(BASE_QUASION_UID)) {
						Log.d(TAG, "fromNdefMessages: message=" + MSG_BASE_REBOOT);
						baseMessages.add(MSG_BASE_REBOOT);
						break allLoop;
					}
					// 解析消息字符串，消息格式必须是BaseId+CmdType+CmdData
					int pos = MASTER_KEY_LENGTH;
					while (pos + CMD_FIELD_LENGTH <= message.length()) {
						// 解析命令类型和命令数据
						String cmdType = message.substring(pos, pos + CMD_FIELD_LENGTH).toUpperCase();
						String cmdData = message.substring(pos);
						Log.d(TAG, "fromNdefMessages: data=" + cmdData);
						// 根据命令类型生成对应数据对象
						NfcBaseMessage baseMessage = null;
						if (CMD_TO_BASE_GET_KEY_ID_MIN.compareTo(cmdType) <= 0 && CMD_TO_BASE_GET_KEY_ID_MAX.compareTo(cmdType) >= 0) {
							baseMessage = new NfcGetKeyIds();
						} else if (CMD_TO_BASE_GET_IMEI.equals(cmdType)) {
							baseMessage = new NfcGetPhoneImei();
						} else if (CMD_TO_BASE_GET_MODEL.equals(cmdType)) {
							baseMessage = new NfcGetPhoneModel();
						} else if (CMD_TO_BASE_GET_BASE_ID.equals(cmdType)) {
							baseMessage = new NfcGetBaseId();
						} else if (CMD_TO_BASE_GET_BASE_TIME.equals(cmdType)) {
							baseMessage = new NfcGetBaseTime();
						} else if (CMD_TO_BASE_SET_ALARM_DATA.equals(cmdType)) {
							baseMessage = new NfcGetAlarmData();
						} else if (CMD_TO_BASE_SET_HOOK_DATA.equals(cmdType)) {
							baseMessage = new NfcGetHookData();
						}
						// 解析数据填充数据对象的内容
						if (baseMessage == null) {
							Log.w(TAG, "fromNdefMessages: invalid cmd type=" + cmdType);
							break;
						}
						int skipLength = baseMessage.fromNdefData(cmdData);
						if (skipLength <= 0) {
							Log.w(TAG, "fromNdefMessages: invalid cmd data=" + cmdData);
							break;
						}
						Log.d(TAG, "fromNdefMessages: message=" + baseMessage);
						// 将数据对象加入返回结果
						pos += skipLength;
						baseMessages.add(baseMessage);
					}
					// 数据结束位置异常，返回空集合
					if (pos != message.length()) {
						baseMessages.clear();
					}
					// NFC Base只支持1个NdefRecord数据
					break allLoop;
				}
			}
		}
		Log.d(TAG, "fromNdefMessages: message cnt=" + baseMessages.size());
		return baseMessages;
	}

	private static final byte LANGUAGE_CODE_MASK = 0x1F;
	private static final short TEXT_ENCODING_MASK = 0x80;

	private static NdefRecord toNdefTextRecord(byte[] id, String text, Locale locale, boolean encodeInUtf8) {
		if (text == null) {
			throw new IllegalArgumentException("Expected text");
		}
		if (locale == null) {
			throw new IllegalArgumentException("Expected locale");
		}
		// get language prefix
		StringBuffer language = new StringBuffer(locale.getLanguage());
		if (!StringUtil.isEmpty(locale.getCountry())) language.append('-').append(locale.getCountry());
		if (language.length() > LANGUAGE_CODE_MASK) {
			throw new IllegalArgumentException("Expected language code length < 32 bytes, not " + language.length() + " bytes");
		}
		byte[] languageData = language.toString().getBytes();
		// get text bytes
		Charset encoding = Charset.forName(encodeInUtf8 ? "UTF-8" : "UTF-16");
		byte[] textData = text.getBytes(encoding);
		// get status
		byte statusData = (byte) (languageData.length | (encodeInUtf8 ? 0x00 : TEXT_ENCODING_MASK));
		// construct whole payload for ndefRecord
		byte[] payload = new byte[1 + languageData.length + textData.length];
		payload[0] = statusData;
		System.arraycopy(languageData, 0, payload, 1, languageData.length);
		System.arraycopy(textData, 0, payload, 1 + languageData.length, textData.length);
		NdefRecord ndefRecord = new NdefRecord(NdefRecord.TNF_WELL_KNOWN, NdefRecord.RTD_TEXT, id, payload);
		Log.d(TAG, "toNdefTextRecord(" + text + ")");
		Log.d(TAG, "Result=" + ndefRecord);
		return ndefRecord;
	}

	private static String fromNdefTextRecord(NdefRecord ndefRecord) {
		// get whole payload from ndefRecord
		byte[] payload = ndefRecord.getPayload();
		// get status
		int statusData = payload[0] & 0xff;
		// get language prefix
		int languageLength = (statusData & LANGUAGE_CODE_MASK);
		String languageData = new String(payload, 1, languageLength);
		// get text string
		Charset encoding = Charset.forName((statusData & TEXT_ENCODING_MASK) != 0 ? "UTF-16" : "UTF-8");
		String textData = new String(payload, 1 + languageLength, payload.length - languageLength - 1, encoding);
		Log.d(TAG, "fromNdefTextRecord(" + ndefRecord + ")");
		Log.d(TAG, "Result=" + textData);
		return textData;
	}
}
