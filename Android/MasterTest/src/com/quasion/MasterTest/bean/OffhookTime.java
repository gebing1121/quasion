package com.quasion.MasterTest.bean;

import com.quasion.MasterTest.util.DateUtil;

import java.io.Serializable;

public class OffhookTime implements Serializable {
	/* 摘机时间点 */
	private long offhookTime;
	/* 摘机时长 */
	private int times;

	public long getOffhookTime() {
		return offhookTime;
	}

	public void setOffhookTime(long offhookTime) {
		this.offhookTime = offhookTime;
	}

	public int getTimes() {
		return times;
	}

	public void setTimes(int times) {
		this.times = times;
	}

	@Override
	public String toString() {
		return "OffhookTime {" +
				"offhookTime=" + offhookTime + "," + DateUtil.formatTime(offhookTime) +
				", times=" + times +
				'}';
	}
}
