package com.quasion.MasterTest.bean;

import android.os.Handler;
import android.os.Message;
import android.util.Log;
import com.quasion.MasterTest.app.AppContext;
import com.quasion.MasterTest.app.AppException;
import com.quasion.MasterTest.ui.BaseActivity;
import org.json.JSONException;
import org.json.JSONObject;

public class BaseBean {
	private static final String TAG = BaseBean.class.getSimpleName();
	public static final String EXPIRE_CODE = "999999";		// 会话过期 的code

	// 处理服务器成功返回的数据接口
	public static interface Parser {
		public Object parse(JSONObject data) throws JSONException;
	}

	public static class StaticParse implements Parser {
		private Object data;
		public StaticParse(Object obj) {
			data = obj;
		}
		@Override
		public Object parse(JSONObject data) throws JSONException {
			return data;
		}
	}

	public static void handleResponse(Handler mHandler, int msgWhat, String responseData, Parser parser) throws JSONException {
		ResponseInfo jsonResponseInfo = ResponseInfo.convertJsonStrToJsonObj(responseData);
		ResponseStatus responseStatus = jsonResponseInfo.status;
		JSONObject data = jsonResponseInfo.data;
		Log.d(TAG, "status: " + responseStatus);
		Message msg = new Message();
		if (responseStatus.getState()) {
			msg.what = msgWhat;
			if (parser != null) msg.obj = parser.parse(data);
			else msg.obj = data;
		} else if (EXPIRE_CODE.equals(responseStatus.getCode())) {
			msg.what = BaseActivity.SESSION_EXPIRE;
			msg.obj = "Session expire, please login again!";
		} else {
			msg.what = BaseActivity.INVOKE_FAIL;
			msg.obj = responseStatus.getMessage();
		}
		mHandler.sendMessage(msg);
	}

	public static void handleException(Handler mHandler, Exception exception) {
		Log.d(TAG, "exception:" + exception);
		if (exception instanceof JSONException) {
			exception = AppException.xml(exception);
		} else if (!(exception instanceof AppException)) {
			exception = AppException.network(exception);
		}
		Message msg = new Message();
		msg.what = BaseActivity.INVOKE_FAIL;
		msg.obj = ((AppException) exception).getMessage(AppContext.getAppContext());
		mHandler.sendMessage(msg);
	}
}
