package com.quasion.MasterTest.bean;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * 前台和后台做交互时，后台响应给前台的JSON格式的数据模型。
 *
 * 建立此模型主要是为了统一约定响应信息的格式。
 *
 */
public class ResponseInfo {
	private static final String STATUS = "status";
	private static final String STATUS_STATE = "state";
	private static final String STATUS_CODE = "code";
	private static final String STATUS_MESSAGE= "message";
	private static final String STATUS_TYPE= "type";
	private static final String DATA = "data";

	//返回json的状态码
	public ResponseStatus status;
	/* 用户在UI界面发起交互操作后，需要后台返回给前台的一些其他数据。 */
	public JSONObject data;

	public static ResponseInfo convertJsonStrToJsonObj(String jsonStr){
		try {
			if (jsonStr == null) return null;
			ResponseInfo responseInfo = new ResponseInfo();
			JSONObject jsonObject = new JSONObject(jsonStr);
			if (!jsonObject.isNull(STATUS)){
				JSONObject jsonObjectStatus = jsonObject.getJSONObject(STATUS);
				ResponseStatus status = new ResponseStatus();
				status.setCode(jsonObjectStatus.getString(STATUS_CODE));
				status.setState(jsonObjectStatus.getBoolean(STATUS_STATE));
				status.setMessage(jsonObjectStatus.getString(STATUS_MESSAGE));
				status.setType(jsonObjectStatus.getInt(STATUS_TYPE));
				responseInfo.status = status;
			}
			if (!jsonObject.isNull(DATA)){
				JSONObject data = jsonObject.getJSONObject(DATA);
				responseInfo.data = data;
			}
			return responseInfo;
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
	}

}
