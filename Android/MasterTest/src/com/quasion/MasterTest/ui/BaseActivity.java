package com.quasion.MasterTest.ui;

import android.app.Activity;
import android.os.Bundle;
import com.quasion.MasterTest.R;
import com.quasion.MasterTest.util.StringUtil;
import com.quasion.MasterTest.util.UiUtil;
import com.quasion.MasterTest.widget.LoadingDialog;

public class BaseActivity extends Activity {
	/* 会话过期 */
	public static final int SESSION_EXPIRE = 999;
	/* 调用URL返回成功 */
	public static final int INVOKE_SUCCESS = 200;
	/* 调用URL返回失败 */
	public static final int INVOKE_FAIL = 500;

	private LoadingDialog mLoadingDialog;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mLoadingDialog = new LoadingDialog(this, R.style.LoadingDialog);
		mLoadingDialog.setCancelable(false);
	}

	public void showLoadingPage(String ...message) {
		mLoadingDialog.show();
		if (message != null && message.length > 0) mLoadingDialog.setDialogMessage(message[0]);
	}

	public void hideLoadingPage() {
		mLoadingDialog.dismiss();
	}

	public boolean isLoadingDialogShowing() {
		return mLoadingDialog.isShowing();
	}

	public void setLoadingProgress(int progress) {
		if (mLoadingDialog != null) mLoadingDialog.setDialogProgress(progress);
	}

	public void setLoadingProgress(String progress) {
		if (mLoadingDialog != null) mLoadingDialog.setDialogProgress(progress);
	}

	public void setLoadingMessage(String message) {
		if (mLoadingDialog != null) mLoadingDialog.setDialogMessage(message);
	}

	public void onInvokeFailed(String errorMsg) {
		UiUtil.showToastMessage(getApplicationContext(), errorMsg);
	}

	public void onSessionExpired(String expireMsg) {
		if (!StringUtil.isEmpty(expireMsg)) {
			UiUtil.showToastMessage(getApplicationContext(), expireMsg);
		}
	}
}
