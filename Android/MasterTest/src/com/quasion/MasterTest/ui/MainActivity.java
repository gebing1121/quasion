package com.quasion.MasterTest.ui;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.FormatException;
import android.nfc.NdefMessage;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.nfc.tech.NfcA;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;
import com.quasion.MasterTest.R;
import com.quasion.MasterTest.bean.AlarmTime;
import com.quasion.MasterTest.bean.NfcCard;
import com.quasion.MasterTest.bean.OffhookTime;
import com.quasion.MasterTest.nfc.*;
import com.quasion.MasterTest.server.ServerApiClient;
import com.quasion.MasterTest.server.ServerApiURLs;
import com.quasion.MasterTest.util.DateUtil;
import com.quasion.MasterTest.util.StringUtil;
import com.quasion.MasterTest.util.UiUtil;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends BaseActivity {
	private static final String TAG = MainActivity.class.getSimpleName();

	private static final int LOGIN_SUCCESS = 1;
	private static final int GET_CARD_LIST = 2;

	private View mBtnSend, mBtnLogin, mBtnGetKeys;
	private TextView mTextTxData, mTextRxData, mTextBaseId, mTextBaseTime, mTextNfcKeys, mTextPhoneModel, mTextPhoneImei, mTextAlarmData, mTextHookData;
	private TextView mEditMasterKey, mEditBaseId, mEditBaseTime, mEditNfcKeys, mEditPhoneModel, mEditPhoneImei;
	private CompoundButton mCheckGetBaseId, mCheckSetBaseId, mCheckGetBaseTime, mCheckSetBaseTime, mCheckSetBaseCurrentTime, mCheckGetNfcKeys, mCheckSetNfcKeys;
	private CompoundButton mCheckGetPhoneModel, mCheckSetPhoneModel, mCheckGetPhoneImei, mCheckSetPhoneImei, mCheckGetAlarmData, mCheckGetHookData;

	private Tag mNfcBaseTag;
	private NfcAdapter mAdapter;
	private String[][] mTechList;
	private IntentFilter[] mIntentFilter;
	private PendingIntent mPendingIntent;

	private Handler mHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			hideLoadingPage();
			switch (msg.what) {
			case LOGIN_SUCCESS:
				if (!StringUtil.isEmpty(NfcBaseMessage.MASTER_KEY_UID)) {
					mEditMasterKey.setText(NfcBaseMessage.MASTER_KEY_UID);
				}
				break;
			case GET_CARD_LIST:
				List<NfcCard> nfcCards = (List<NfcCard>) msg.obj;
				StringBuffer sbBuffer = new StringBuffer();
				for (NfcCard nfcCard : nfcCards) {
					sbBuffer.append(nfcCard.getKeyId());
				}
				mEditNfcKeys.setText(sbBuffer.toString());
				break;
			case BaseActivity.INVOKE_FAIL:
				onInvokeFailed((String) msg.obj);
				break;
			case BaseActivity.SESSION_EXPIRE:
				onSessionExpired((String) msg.obj);
				break;
			}
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_layout);
		initView();
		prepareNfc();
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (mAdapter != null) mAdapter.enableForegroundDispatch(this, mPendingIntent, mIntentFilter, mTechList);
	}

	@Override
	protected void onPause() {
		super.onPause();
		if (mAdapter != null) mAdapter.disableForegroundDispatch(this);
	}

	private long backPressTime = 0;
	@Override
	public void onBackPressed() {
		if (System.currentTimeMillis() - backPressTime > 2000) {
			Toast.makeText(this, "Press back again to exit!", Toast.LENGTH_SHORT).show();
			backPressTime = System.currentTimeMillis();
		} else {
			finish();
		}
	}

	@Override
	protected void onNewIntent(Intent intent) {
		// 当检测到NFC Based时，调用onNewIntent()
		Tag nfcTag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
		Parcelable[] nfcMessages = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
		if (nfcTag == null) return;
		Ndef ndef = Ndef.get(nfcTag);
		Log.d(TAG, "Found NFC device, Action=" + intent.getAction() + ", Tag=" + StringUtil.toHex(nfcTag.getId()) + "," + nfcTag.toString());
		Log.d(TAG, "Ndef=" + ndef.getType() + ", MaxSize=" + ndef.getMaxSize() + ", isWritable=" + ndef.isWritable());
		if (!ndef.isWritable() || nfcMessages == null) return;
		receiveNfcMessage(nfcTag, nfcMessages);
	}

	private void initView() {
		getActionBar().setTitle("Master Test");
		mBtnSend = findViewById(R.id.btn_sendCommand);
		mBtnSend.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				executeSendCommand();
			}
		});
		mBtnLogin = findViewById(R.id.btn_login);
		mBtnLogin.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				prepareLogin();
			}
		});
		mBtnGetKeys = findViewById(R.id.btn_getKeys);
		mBtnGetKeys.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				executeGetNfcKeys();
			}
		});

		mTextTxData = (TextView) findViewById(R.id.text_txData);
		mTextTxData.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				executeEditDialog((TextView) v, "Show Data Sent To Base", false, 8);
			}
		});
		mTextRxData = (TextView) findViewById(R.id.text_rxData);
		mTextRxData.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				executeEditDialog((TextView) v, "Show Data Receive From Base", false, 10);
			}
		});
		mTextBaseId = (TextView) findViewById(R.id.text_baseId);
		mTextBaseTime = (TextView) findViewById(R.id.text_baseTime);
		mTextBaseTime.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				executeEditDialog((TextView) v, "Show Base Time", false, 1);
			}
		});
		mTextNfcKeys = (TextView) findViewById(R.id.text_nfcKeys);
		mTextNfcKeys.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				executeEditDialog((TextView) v, "Show NFC Keys Id", false, 5);
			}
		});
		mTextPhoneModel = (TextView) findViewById(R.id.text_phoneModel);
		mTextPhoneModel.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				executeEditDialog((TextView) v, "Show Phone Brand/Model", false, 2);
			}
		});
		mTextPhoneImei = (TextView) findViewById(R.id.text_phoneImei);
		mTextPhoneImei.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				executeEditDialog((TextView) v, "Show Phone IMEI", false, 1);
			}
		});
		mTextAlarmData = (TextView) findViewById(R.id.text_alarmData);
		mTextAlarmData.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				executeEditDialog((TextView) v, "Show Alarm Data", false, 10);
			}
		});
		mTextHookData = (TextView) findViewById(R.id.text_hookData);
		mTextHookData.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				executeEditDialog((TextView) v, "Show Off Hook Data", false, 10);
			}
		});

		mEditMasterKey = (TextView) findViewById(R.id.edit_masterKey);
		mEditMasterKey.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				executeEditDialog((TextView) v, "Edit Master Key", true, 1);
			}
		});
		mEditBaseId = (TextView) findViewById(R.id.edit_baseId);
		mEditBaseId.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				executeEditDialog((TextView) v, "Edit Base Id", true, 1);
			}
		});
		mEditBaseTime = (TextView) findViewById(R.id.edit_baseTime);
		mEditBaseTime.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				executeEditDialog((TextView) v, "Edit Base Time", true, 1);
			}
		});
		mEditNfcKeys = (TextView) findViewById(R.id.edit_nfcKeys);
		mEditNfcKeys.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				executeEditDialog((TextView) v, "Edit NFC Keys Id", true, 5);
			}
		});
		mEditPhoneModel = (TextView) findViewById(R.id.edit_phoneModel);
		mEditPhoneModel.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				executeEditDialog((TextView) v, "Edit Phone Brand/Model", true, 2);
			}
		});
		mEditPhoneImei = (TextView) findViewById(R.id.edit_phoneImei);
		mEditPhoneImei.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				executeEditDialog((TextView) v, "Edit Phone IMEI", true, 1);
			}
		});

		mCheckGetBaseId = (CompoundButton) findViewById(R.id.cb_getBaseId);
		mCheckSetBaseId = (CompoundButton) findViewById(R.id.cb_setBaseId);
		mCheckGetBaseTime = (CompoundButton) findViewById(R.id.cb_getBaseTime);
		mCheckSetBaseTime = (CompoundButton) findViewById(R.id.cb_setBaseTime);
		mCheckSetBaseCurrentTime = (CompoundButton) findViewById(R.id.cb_setBaseCurrentTime);
		mCheckGetNfcKeys = (CompoundButton) findViewById(R.id.cb_getNfcKeys);
		mCheckSetNfcKeys = (CompoundButton) findViewById(R.id.cb_setNfcKeys);
		mCheckGetPhoneModel = (CompoundButton) findViewById(R.id.cb_getPhoneModel);
		mCheckSetPhoneModel = (CompoundButton) findViewById(R.id.cb_setPhoneModel);
		mCheckGetPhoneImei = (CompoundButton) findViewById(R.id.cb_getPhoneImei);
		mCheckSetPhoneImei = (CompoundButton) findViewById(R.id.cb_setPhoneImei);
		mCheckGetAlarmData = (CompoundButton) findViewById(R.id.cb_getAlarmData);
		mCheckGetHookData = (CompoundButton) findViewById(R.id.cb_getHookData);
		mCheckSetBaseTime.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				mCheckSetBaseCurrentTime.setChecked(!mCheckSetBaseTime.isChecked());
			}
		});
		mCheckSetBaseCurrentTime.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				mCheckSetBaseTime.setChecked(!mCheckSetBaseCurrentTime.isChecked());
			}
		});

		if (!StringUtil.isEmpty(NfcBaseMessage.MASTER_KEY_UID)) {
			mEditMasterKey.setText(NfcBaseMessage.MASTER_KEY_UID);
		}
	}

	private void executeEditDialog(final TextView textView, final String title, final boolean editable, final int lines) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		LayoutInflater inflater = LayoutInflater.from(this);
		View view = inflater.inflate(R.layout.edit_dialog, null);
		final Dialog dialog = builder.setView(view).setTitle(title).create();
		final Button btnOk = (Button) view.findViewById(R.id.edit_btn_ok);
		final Button btnCancel = (Button) view.findViewById(R.id.edit_btn_cancel);
		final EditText editText = (EditText) view.findViewById(R.id.edit_text);
		editText.setText(textView.getText());
		editText.setLines(lines);
		if (!editable) btnCancel.setVisibility(View.GONE);
		// setup click listener
		btnOk.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (editable) textView.setText(editText.getText().toString().trim());
				dialog.dismiss();
			}
		});
		btnCancel.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});
		// show dialog
		dialog.setOnShowListener(new DialogInterface.OnShowListener() {
			@Override
			public void onShow(DialogInterface dialog) {
				if (editable) {
					editText.requestFocus();
					((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE)).showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
				}
			}
		});
		dialog.show();
	}

	private static final String SP_NAME = "MasterTest";
	private static final String SP_USER = "Username";
	private static final String SP_PASS = "Password";

	private void prepareLogin() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		LayoutInflater inflater = LayoutInflater.from(this);
		View view = inflater.inflate(R.layout.login_dialog, null);
		final Dialog dialog = builder.setView(view).setTitle("登录服务器").create();
		final Button btnLogin = (Button) view.findViewById(R.id.login_btn_login);
		final Button btnCancel = (Button) view.findViewById(R.id.login_btn_cancel);
		final EditText editServerUrl = (EditText) view.findViewById(R.id.login_text_serverUrl);
		final EditText editUsername = (EditText) view.findViewById(R.id.login_text_username);
		final EditText editPassword = (EditText) view.findViewById(R.id.login_text_password);
		// setup edit value
		editServerUrl.setText(ServerApiURLs.HOST);
		editUsername.setText(getSharedPreferences(SP_NAME, Context.MODE_PRIVATE).getString(SP_USER, ""));
		editPassword.setText(getSharedPreferences(SP_NAME, Context.MODE_PRIVATE).getString(SP_PASS, ""));
		// setup click listener
		btnLogin.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				String username = editUsername.getText().toString().trim();
				String password = editPassword.getText().toString().trim();
				ServerApiURLs.HOST = editServerUrl.getText().toString().trim();
				getSharedPreferences(SP_NAME, Context.MODE_PRIVATE).edit().putString(SP_USER, username).putString(SP_PASS, password).commit();
				if (executeLogin(username, password)) {
					dialog.dismiss();
				}
			}
		});
		btnCancel.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});
		// show dialog
		dialog.show();
	}

	private boolean executeLogin(final String username, final String password) {
		if (StringUtil.isEmpty(username)) {
			UiUtil.showToastMessage(getApplicationContext(), "Account username can not be empty");
			return false;
		} else if (StringUtil.isEmpty(password)) {
			UiUtil.showToastMessage(getApplicationContext(), "Password can not be empty");
			return false;
		} else {
			showLoadingPage();
			new Thread() {
				public void run() {
					ServerApiClient.login(mHandler, LOGIN_SUCCESS, username, password);
				}
			}.start();
			return true;
		}
	}

	private boolean executeGetNfcKeys() {
		if (StringUtil.isEmpty(ServerApiClient.appSessionToken)) {
			UiUtil.showToastMessage(getApplicationContext(), "Must login to get NFC keys from server!");
			return false;
		} else {
			showLoadingPage();
			new Thread() {
				public void run() {
					ServerApiClient.getNfcCardList(mHandler, GET_CARD_LIST, 0, ServerApiClient.PAGE_DEFAULT_SIZE);
				}
			}.start();
			return true;
		}
	}

	private boolean executeSendCommand() {
		if (mNfcBaseTag == null) {
			UiUtil.showToastMessage(getApplicationContext(), "Base has not connected, please connect first!");
			return false;
		}
		// set master key
		String masterKey = mEditMasterKey.getText().toString().trim();
		if (masterKey.length() != NfcBaseMessage.MASTER_KEY_LENGTH) {
			UiUtil.showToastMessage(getApplicationContext(), "Master key' length must be " + NfcBaseMessage.MASTER_KEY_LENGTH + "!");
			mEditMasterKey.requestFocus();
			return false;
		}
		NfcBaseMessage.MASTER_KEY_UID = masterKey;
		// get all nfc messages
		List<NfcBaseMessage> nfcMessages = new ArrayList<NfcBaseMessage>();
		if (mCheckGetBaseId.isChecked()) {
			nfcMessages.add(NfcBaseMessage.MSG_GET_BASE_ID);
		}
		if (mCheckSetBaseId.isChecked()) {
			String nfcBaseId = mEditBaseId.getText().toString().trim();
			if (nfcBaseId.length() != NfcGetBaseId.BASE_ID_LENGTH) {
				UiUtil.showToastMessage(getApplicationContext(), "Base Id' length must be " + NfcGetBaseId.BASE_ID_LENGTH + "!");
				mEditBaseId.requestFocus();
				return false;
			}
			nfcMessages.add(new NfcSetBaseId(nfcBaseId));
		}
		if (mCheckGetBaseTime.isChecked()) {
			nfcMessages.add(NfcBaseMessage.MSG_GET_BASE_TIME);
		}
		if (mCheckSetBaseTime.isChecked()) {
			long nfcBaseTime = DateUtil.parseTimeToLong(mEditBaseTime.getText().toString().trim(), "yyyy/MM/dd HH:mm:ss");
			if (nfcBaseTime < 0) {
				UiUtil.showToastMessage(getApplicationContext(), "Date/time format must be 'yyyy/MM/dd HH:mm:ss'!");
				mEditBaseTime.requestFocus();
				return false;
			}
			nfcMessages.add(new NfcSetBaseTime(nfcBaseTime));
		}
		if (mCheckGetNfcKeys.isChecked()) {
			nfcMessages.add(NfcBaseMessage.MSG_GET_KEY_ID);
		}
		if (mCheckSetNfcKeys.isChecked()) {
			String nfcKeyIds = mEditNfcKeys.getText().toString().trim();
			if (nfcKeyIds.length() % NfcGetKeyIds.KEY_ID_LENGTH != 0) {
				UiUtil.showToastMessage(getApplicationContext(), "NFC key's length must be multiplied of " + NfcGetKeyIds.KEY_ID_LENGTH + "!");
				mEditNfcKeys.requestFocus();
				return false;
			}
			nfcMessages.add(new NfcSetKeyIds(nfcKeyIds));
		}
		if (mCheckGetPhoneModel.isChecked()) {
			nfcMessages.add(NfcBaseMessage.MSG_GET_MODEL);
		}
		if (mCheckSetPhoneModel.isChecked()) {
			String phoneModel = mEditPhoneModel.getText().toString().trim();
			if (StringUtil.isEmpty(phoneModel)) {
				UiUtil.showToastMessage(getApplicationContext(), "Phone brand and model must not be empty!");
				mEditPhoneModel.requestFocus();
				return false;
			}
			int pos = phoneModel.indexOf(' ');
			if (pos <= 0) {
				UiUtil.showToastMessage(getApplicationContext(), "Phone brand and model must be split by space!");
				mEditPhoneModel.requestFocus();
				return false;
			}
			if (phoneModel.length() > NfcGetPhoneModel.PHONE_MODEL_LENGTH) {
				UiUtil.showToastMessage(getApplicationContext(), "Phone brand and model's length must not exceed " + NfcGetPhoneModel.PHONE_MODEL_LENGTH + "!");
				mEditPhoneModel.requestFocus();
				return false;
			}
			nfcMessages.add(new NfcSetPhoneModel(phoneModel.substring(0, pos), phoneModel.substring(pos + 1)));
		}
		if (mCheckGetPhoneImei.isChecked()) {
			nfcMessages.add(NfcBaseMessage.MSG_GET_IMEI);
		}
		if (mCheckSetPhoneImei.isChecked()) {
			String phoneImei = mEditPhoneImei.getText().toString().trim();
			if (phoneImei.length() != NfcGetPhoneImei.PHONE_IMEI_LENGTH) {
				UiUtil.showToastMessage(getApplicationContext(), "Phone imei's length must be " + NfcGetPhoneImei.PHONE_IMEI_LENGTH + "!");
				mEditPhoneImei.requestFocus();
				return false;
			}
			nfcMessages.add(new NfcSetPhoneImei(phoneImei));
		}
		if (mCheckGetAlarmData.isChecked()) {
			nfcMessages.add(NfcBaseMessage.MSG_GET_ALARM_DATA);
		}
		if (mCheckGetHookData.isChecked()) {
			nfcMessages.add(NfcBaseMessage.MSG_GET_HOOK_DATA);
		}
		// send nfc messages
		NdefMessage ndefMessage = NfcBaseMessage.toNdefMessages(nfcMessages.toArray(new NfcBaseMessage[] {}));
		mTextTxData.setText(NfcBaseMessage.txData);
		boolean rt = sendNfcMessage(mNfcBaseTag, ndefMessage);
		UiUtil.showToastMessage(getApplicationContext(), "Send nfc message to base " + (rt ? "success" : "failed") + "!");
		return rt;
	}

	private void prepareNfc() {
		// 获得NfcAdapter，以检测是否支持NFC
		mAdapter = NfcAdapter.getDefaultAdapter(this);
		if (mAdapter == null) {
			UiUtil.showToastMessage(getApplicationContext(), "NFC is not available");
			finish();
			return;
		}
		// 检查NFC是否被打开
		if (!mAdapter.isEnabled()) {
			UiUtil.showToastMessage(getApplicationContext(), "Please enable NFC in system setting");
			finish();
			return;
		}
		// 创建techList和intentFilters
		mTechList = new String[][] { new String[] { NfcA.class.getName(), Ndef.class.getName() } };
		mIntentFilter = new IntentFilter[] { new IntentFilter(NfcAdapter.ACTION_TECH_DISCOVERED) };
		// 创建PendingIntent对象, 这样Android系统就能在一个tag被检测到时定位到这个对象
		mPendingIntent = PendingIntent.getActivity(this, 0, new Intent(this, this.getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
		Log.d(TAG, "prepareNfc() success");
	}

	private boolean sendNfcMessage(Tag nfcTag, NdefMessage nfcMessage) {
		// 获取NDEF对象
		Ndef ndef = Ndef.get(nfcTag);
		if (ndef == null) return false;
		// 连接NDEF设备
		try {
			ndef.close();
		} catch (IOException e) {
		}
		try {
			ndef.connect();
		} catch (IOException e) {
			Log.w(TAG, "sendNfcMessage() failed for connect!");
			e.printStackTrace();
			return false;
		}
		// 发送NDEF数据
		try {
			ndef.writeNdefMessage(nfcMessage);
			Log.d(TAG, "sendNfcMessage() success.");
			return true;
		} catch (IOException e) {
			Log.w(TAG, "sendNfcMessage() with IOException!");
			// 发送数据到NFC Base时，有时成功发送也会产生IOException
			return true;
		} catch (FormatException e) {
			Log.w(TAG, "sendNfcMessage() failed for FormatException!");
			e.printStackTrace();
			return false;
		} finally {
			try {
				if (ndef != null) ndef.close();
			} catch (IOException e) {
			}
		}
	}

	private boolean receiveNfcMessage(Tag nfcTag, Parcelable[] nfcMessages) {
		// 解析接收到的NFC数据
		if (nfcMessages == null || nfcMessages.length == 0) return false;
		NdefMessage[] ndefMessages = new NdefMessage[nfcMessages.length];
		for (int i = 0; i < nfcMessages.length; i++) ndefMessages[i] = (NdefMessage) nfcMessages[i];
		List<NfcBaseMessage> baseMessages = NfcBaseMessage.fromNdefMessages(ndefMessages);
		// 如果解析成功，则表示连接的是NFC Base设备
		Log.d(TAG, "receiveNfcMessage()=" + baseMessages);
		if (baseMessages == null || baseMessages.size() == 0) return false;
		mNfcBaseTag = nfcTag;
		mTextRxData.setText(NfcBaseMessage.rxData);
		// 处理接收到的NFC Base数据
		mTextBaseId.setText("");
		mTextBaseTime.setText("");
		mTextPhoneModel.setText("");
		mTextPhoneImei.setText("");
		mTextNfcKeys.setText("");
		mTextAlarmData.setText("");
		mTextHookData.setText("");
		for (NfcBaseMessage baseMessage : baseMessages) {
			if (baseMessage instanceof NfcGetBaseId) {
				mTextBaseId.setText(((NfcGetBaseId) baseMessage).nfcBaseId);
			} else if (baseMessage instanceof NfcGetBaseTime) {
				mTextBaseTime.setText(DateUtil.formatTime(((NfcGetBaseTime) baseMessage).nfcBaseTime, "yyyy/MM/dd HH:mm:ss"));
			} else if (baseMessage instanceof NfcGetPhoneModel) {
				mTextPhoneModel.setText(((NfcGetPhoneModel) baseMessage).phoneBrand + " " + ((NfcGetPhoneModel) baseMessage).phoneModel);
			} else if (baseMessage instanceof NfcGetPhoneImei) {
				mTextPhoneImei.setText(((NfcGetPhoneImei) baseMessage).phoneImei);
			} else if (baseMessage instanceof NfcGetKeyIds) {
				mTextNfcKeys.setText(((NfcGetKeyIds) baseMessage).nfcKeyIds);
			} else if (baseMessage instanceof NfcGetAlarmData) {
				List<AlarmTime> alarmData = ((NfcGetAlarmData) baseMessage).alarmData;
				String msg = "";
				for (AlarmTime data : alarmData) {
					msg += "[" + DateUtil.formatTime(data.getAlarmTime()) + ", " + data.getTimes() + ", " + data.getUnlockNfccardId() + "] ";
				}
				mTextAlarmData.setText(msg);
			} else if (baseMessage instanceof NfcGetHookData) {
				List<OffhookTime> hookData = ((NfcGetHookData) baseMessage).hookData;
				String msg = "";
				for (OffhookTime data : hookData) {
					msg += "[" + DateUtil.formatTime(data.getOffhookTime()) + ", " + data.getTimes() + "] ";
				}
				mTextHookData.setText(msg);
			}
		}
		return true;
	}
}
