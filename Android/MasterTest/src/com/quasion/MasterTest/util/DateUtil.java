package com.quasion.MasterTest.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {
	/**
	 * 格式化输出日期/时间为字符串.
	 *
	 * @param time Date类型时间数据。如果为null，则使用系统当前时间
	 * @param format (可选) 输出的格式。如果未提供或者为null，则使用缺省格式："yyyy-MM-dd HH:mm:ss".
	 */
	public static String formatTime(Date time, String... format) {
		String dateFormat = (format.length == 0 || format[0] == null) ? "yyyy-MM-dd HH:mm:ss" : format[0];
		if (time == null) time = new Date(System.currentTimeMillis());
		return new SimpleDateFormat(dateFormat).format(time);
	}

	/**
	 * 格式化输出日期/时间为字符串.
	 *
	 * @param time long型时间数据。.
	 * @param format (可选) 输出的格式。如果未提供或者为null，则使用缺省格式："yyyy-MM-dd HH:mm:ss".
	 */
	public static String formatTime(long time, String... format) {
		return formatTime(new Date(time), format);
	}

	/**
	 * 格式化输出当前日期/时间为字符串.
	 *
	 * @param format (可选) 输出的格式。如果未提供或者为null，则使用缺省格式："yyyy-MM-dd HH:mm:ss".
	 */
	public static String formatCurrentTime(String... format) {
		return formatTime(null, format);
	}

	/**
	 * 将字符串按格式化转换为日期/时间
	 *
	 * @param text 日期/时间字符串
	 * @param format (可选) 输出的格式。如果未提供或者为null，则使用缺省格式："yyyy-MM-dd HH:mm:ss".
	 * @return Date类型时间数据。如果text为null或无效，则返回null
	 */
	public static Date parseTimeToDate(String text, String... format) {
		String dateFormat = (format.length == 0 || format[0] == null) ? "yyyy-MM-dd HH:mm:ss" : format[0];
		try {
			if (text == null) return null;
			return new SimpleDateFormat(dateFormat).parse(text);
		} catch (ParseException e) {
			return null;
		}
	}

	/**
	 * 将字符串按格式化转换为日期/时间
	 *
	 * @param text 日期/时间字符串
	 * @param format (可选) 输出的格式。如果未提供或者为null，则使用缺省格式："yyyy-MM-dd HH:mm:ss".
	 * @return long类型时间数据。如果text为null或无效，则返回-1
	 */
	public static long parseTimeToLong(String text, String... format) {
		String dateFormat = (format.length == 0 || format[0] == null) ? "yyyy-MM-dd HH:mm:ss" : format[0];
		try {
			if (text == null) return -1;
			Date date = new SimpleDateFormat(dateFormat).parse(text);
			return date.getTime();
		} catch (ParseException e) {
			return -1;
		}
	}
}
